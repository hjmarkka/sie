module sparse_matrix
use common
use integrals
use integration_points
use omp_lib
use octtree

implicit none

contains


 

subroutine build_matrix_efie_edge_mlfma(matrices, mesh, otree)

type (mesh_struct) :: mesh
type (data) :: matrices
type (level_struct), dimension(:) :: otree

integer :: test_T, basis_T, T_nodes(3), B_nodes(3), my_id, N_procs, N1, N2, N_loc
real(dp) :: T_coord(3,3), B_coord(3,3), T_coord2(3,3), B_coord2(3,3)
real(dp), allocatable :: P0_tri(:,:), P02_tri(:,:)
real(dp), allocatable :: w0_tri(:), P0_line(:), w0_line(:)
real(dp), allocatable :: w02_tri(:), P02_line(:), w02_line(:)
real(dp) :: T_dN(3,3), T_nxdN(3,3), T_nvec(3), T_dN2(3,3), T_nxdN2(3,3), T_nvec2(3)
real(dp) :: B_dN(3,3), B_nxdN(3,3), B_nvec(3), B_dN2(3,3), B_nxdN2(3,3), B_nvec2(3)
integer :: te, be, test, basis, near, block_size, ierr, max_level, level_ind
integer :: sph1_loc, box, box2_loc, box2, sph2_loc
real(dp) :: signt, signb, sign, eta0
integer :: test_tri(2), basis_tri(2), tt, bt, be2
complex(dp) :: k1, k2, eta1, eta2
complex(dp) :: alok(3,3), alok2(3,3), alok3(3,3), alok4(3,3,3), elem(5)
complex(dp) :: alok_2(3,3), alok2_2(3,3), alok3_2(3,3), alok4_2(3,3,3), elem_2(5)
integer(ip) :: las, max_las
eta0 = sqrt(mu/epsilon)


alok(:,:) = 0.0
alok2(:,:) = 0.0
alok3(:,:) = 0.0
alok4(:,:,:) = 0.0

k1 = dcmplx(mesh%k, 0.0)
k2 = mesh%k * sqrt(mesh%eps_r)

eta1 = sqrt(mu/epsilon)
eta2 = sqrt(mu/(epsilon*mesh%eps_r))

call inttri(P0_tri,w0_tri,5)
call gaussint(P0_line,w0_line,5)

call inttri(P02_tri,w02_tri,5)
call gaussint(P02_line,w02_line,5)

max_level = size(otree) - 1
level_ind = max_level + 1


!************************************
las = 0_ip
do box = 1, size(otree(level_ind)%tree)
   do sph1_loc = 1, otree(level_ind)%tree(box)%N_source
      test_T = otree(level_ind)%tree(box)%sources(sph1_loc)
      do box2_loc = 1,27
         box2 = otree(level_ind)%tree(box)%near_neighbours(box2_loc)
         if(box2 == 0) exit
         do sph2_loc = 1, otree(level_ind)%tree(box2)%N_source
             basis_T =  otree(level_ind)%tree(box2)%sources(sph2_loc)
             las = las + 1_ip
          end do
       end do
    end do
 end do
allocate(matrices%sparse_M(las))
allocate(matrices%ind_M(las,2_ip))
max_las = las
matrices%sparse_M(:) = dcmplx(0.0,0.0)
!***********************************


print*, las
las = 0_ip
do box = 1, size(otree(level_ind)%tree)
 
   do sph1_loc = 1, otree(level_ind)%tree(box)%N_source
      
      test = otree(level_ind)%tree(box)%sources(sph1_loc)

      test_tri = mesh%edgesT(:,test) 

      
      do box2_loc = 1,27
   
          box2 = otree(level_ind)%tree(box)%near_neighbours(box2_loc)
          if(box2 == 0) exit

          do sph2_loc = 1, otree(level_ind)%tree(box2)%N_source
             
             basis =  otree(level_ind)%tree(box2)%sources(sph2_loc)
          
             basis_tri = mesh%edgesT(:,basis) 


             

       !___________________________________________________!
             elem(:) = dcmplx(0.0,0.0)
             do tt = 1,2

                
                T_coord = mesh%coord(:,mesh%etopol(:,test_tri(tt)))
                call gradshape_tri(T_dN,T_nxdN, T_nvec, T_coord)

                do te = 1,3
                   if(test == mesh%etopol_edges(te,test_tri(tt))) exit
                end do
                
                do bt = 1,2
                   B_coord = mesh%coord(:,mesh%etopol(:,basis_tri(bt)))
                   call gradshape_tri(B_dN,B_nxdN, B_nvec, B_coord)
                  
                   do be = 1, 3                                  
                      if(basis == mesh%etopol_edges(be,basis_tri(bt))) exit
                   end do

                  alok2(:,:) = dcmplx(0.0,0.0)
                   call integrate_S_S_GNN(alok2, P0_tri, w0_tri, T_coord, B_coord, k1)

                   !print*, test,  test_tri(tt), te
                  
                   signt = RWG_sign(mesh, test, test_tri(tt),te)
                   signb = RWG_sign(mesh, basis, basis_tri(bt),be)

                   sign = signt*signb
           
                   elem = elem + sign * EFIE_elements(alok, alok2, alok3, alok4,T_dN, B_dN, T_nxdN, B_nxdN, &
                        te, be, mesh%Eori, T_coord, B_coord, B_nvec)
                  ! print*, test,basis
                end do
             end do
           
             las = las + 1_ip 
             matrices%ind_M(las,:) = [test, basis] 
             matrices%sparse_M(las) =  eta1/(dcmplx(0.0,1.0)*k1) * (k1**2*elem(1)-elem(2))
             
          

          end do

       end do
    end do
 end do

 print*, las

!print*, matrices%ind_M
end subroutine build_matrix_efie_edge_mlfma





subroutine build_matrix_pmchwt_edge_mlfma(matrices, mesh, otree)

type (mesh_struct) :: mesh
type (data) :: matrices
type (level_struct), dimension(:) :: otree

integer :: test_T, basis_T, T_nodes(3), B_nodes(3), my_id, N_procs, N1, N2, N_loc, threads, chunk
real(dp) :: T_coord(3,3), B_coord(3,3), T_coord2(3,3), B_coord2(3,3)
real(dp), allocatable :: P0_tri(:,:), P02_tri(:,:)
real(dp), allocatable :: w0_tri(:), P0_line(:), w0_line(:)
real(dp), allocatable :: w02_tri(:), P02_line(:), w02_line(:)
real(dp) :: T_dN(3,3), T_nxdN(3,3), T_nvec(3), T_dN2(3,3), T_nxdN2(3,3), T_nvec2(3)
real(dp) :: B_dN(3,3), B_nxdN(3,3), B_nvec(3), B_dN2(3,3), B_nxdN2(3,3), B_nvec2(3)
integer :: te, be, test, basis, near, block_size, ierr, max_level, level_ind
integer :: sph1_loc, box, box2_loc, box2, sph2_loc
real(dp) :: signt, signb, sign, eta0
integer :: test_tri(2), basis_tri(2), tt, bt, be2
complex(dp) :: k1, k2, eta1, eta2
complex(dp) :: alok(3,3), alok2(3,3), alok3(3,3), alok4(3,3,3), elem(5)
complex(dp) :: alok_2(3,3), alok2_2(3,3), alok3_2(3,3), alok4_2(3,3,3), elem_2(5)
integer(ip) :: las, max_las
eta0 = sqrt(mu/epsilon)


alok(:,:) = 0.0
alok2(:,:) = 0.0
alok3(:,:) = 0.0
alok4(:,:,:) = 0.0

k1 = dcmplx(mesh%k, 0.0)
k2 = mesh%k * sqrt(mesh%eps_r)

eta1 = sqrt(mu/epsilon)
eta2 = sqrt(mu/(epsilon*mesh%eps_r))

call inttri(P0_tri,w0_tri,5)
call gaussint(P0_line,w0_line,5)

call inttri(P02_tri,w02_tri,5)
call gaussint(P02_line,w02_line,5)

max_level = size(otree) - 1
level_ind = max_level + 1


!************************************
las = 0_ip
do box = 1, size(otree(level_ind)%tree)
   do sph1_loc = 1, otree(level_ind)%tree(box)%N_source
      test_T = otree(level_ind)%tree(box)%sources(sph1_loc)
      do box2_loc = 1,27
         box2 = otree(level_ind)%tree(box)%near_neighbours(box2_loc)
         if(box2 == 0) exit
         do sph2_loc = 1, otree(level_ind)%tree(box2)%N_source
             basis_T =  otree(level_ind)%tree(box2)%sources(sph2_loc)
             las = las + 1_ip
          end do
       end do
    end do
 end do
allocate(matrices%sparse_M(4_ip*las))
allocate(matrices%ind_M(4_ip*las,2_ip))
max_las = las
matrices%sparse_M(:) = dcmplx(0.0,0.0)
print*, las
print*, 'Size of the near zone matrix =', floor(4.0_dp*dble(las)/1024.0_dp/8.0_dp * 64.0_dp &
     *  2.0_dp / 1024.0_dp  +  4.0_dp*dble(las)*2.0_dp/1024.0_Dp/8.0_dp * 64.0_dp / 1024.0_dp),'Mb' 

las = 0_ip

!***********************************
!$omp parallel default(private) &
!$omp firstprivate(P0_tri, P0_line, w0_tri, w0_line, alok, chunk) &
!$omp firstprivate(P02_tri, P02_line, w02_tri, w02_line) &
!$omp firstprivate(k1, k2, eta1, eta2, eta0, max_las, level_ind) &
!$omp shared(matrices, mesh, otree, las)
!$omp do schedule(guided)

do box = 1, size(otree(level_ind)%tree)
 
   do sph1_loc = 1, otree(level_ind)%tree(box)%N_source
      
      test = otree(level_ind)%tree(box)%sources(sph1_loc)

      test_tri = mesh%edgesT(:,test) 

      
      do box2_loc = 1,27
   
          box2 = otree(level_ind)%tree(box)%near_neighbours(box2_loc)
          if(box2 == 0) exit

          do sph2_loc = 1, otree(level_ind)%tree(box2)%N_source
             
             basis =  otree(level_ind)%tree(box2)%sources(sph2_loc)
          
             basis_tri = mesh%edgesT(:,basis) 


             

       !___________________________________________________!
             elem(:) = dcmplx(0.0,0.0)
             elem_2(:) = dcmplx(0.0,0.0)
             do tt = 1,2

                
                T_coord = mesh%coord(:,mesh%etopol(:,test_tri(tt)))
                call gradshape_tri(T_dN,T_nxdN, T_nvec, T_coord)

                do te = 1,3
                   if(test == mesh%etopol_edges(te,test_tri(tt))) exit
                end do
                
                do bt = 1,2
                   B_coord = mesh%coord(:,mesh%etopol(:,basis_tri(bt)))
                   call gradshape_tri(B_dN,B_nxdN, B_nvec, B_coord)
                  
                   do be = 1, 3                                  
                      if(basis == mesh%etopol_edges(be,basis_tri(bt))) exit
                   end do

                   alok(:,:) = dcmplx(0.0,0.0)
                   if(test_tri(tt) == basis_tri(bt)) then
                      call integrate_S_NN(alok, P0_tri, w0_tri, T_coord)
                   end if
                      
                   call integrate_S_S_GNN(alok2, P0_tri, w0_tri, T_coord, B_coord, k1)
                   call integrate_S_S_ngradGNN(alok3, P0_tri, w0_tri, T_coord, B_coord, k1)
                   call integrate_dS_S_GNN(alok4, P0_tri, w0_tri, P0_line, w0_line, T_coord, B_coord, k1, mesh%Eori)
                   
                   call integrate_S_S_GNN(alok2_2, P0_tri, w0_tri, T_coord, B_coord, k2)
                   call integrate_S_S_ngradGNN(alok3_2, P0_tri, w0_tri, T_coord, B_coord, k2)
                   call integrate_dS_S_GNN(alok4_2, P0_tri, w0_tri, P0_line, w0_line, T_coord, B_coord, k2, mesh%Eori)
                   !print*, test,  test_tri(tt), te
                  
                   signt = RWG_sign(mesh, test, test_tri(tt),te)
                   signb = RWG_sign(mesh, basis, basis_tri(bt),be)

                   sign = signt*signb
           
                   elem = elem + sign * EFIE_elements(alok, alok2, alok3, alok4,T_dN, B_dN, T_nxdN, B_nxdN, &
                        te, be, mesh%Eori, T_coord, B_coord, B_nvec)

                   elem_2 = elem_2 + sign * EFIE_elements(alok, alok2_2, alok3_2, alok4_2,T_dN, B_dN, &
                              T_nxdN, B_nxdN, te, be, mesh%Eori, T_coord, B_coord, B_nvec)

                  ! print*, test,basis
                end do
             end do

             !$OMP CRITICAL
             las = las + 1_ip 
             !matrices%ind_M(las,:) = [test, basis] 
             !matrices%sparse_M(las) =  eta1/(dcmplx(0.0,1.0)*k1) * (k1**2*elem(1)-elem(2))
             
             ! PMCHWT
             if(mesh%form == 1) then
                matrices%ind_M(las,:) = [test, basis] 
                matrices%sparse_M(las) =  1.0_dp/eta0*eta1/(dcmplx(0.0,1.0)*k1) * (k1**2*elem(1)-elem(2)) + &
                     1.0_dp/eta0*eta2/(dcmplx(0.0,1.0)*k2) * (k2**2*elem_2(1)-elem_2(2))

                matrices%ind_M(max_las+las,:) = [mesh%N_edge+test, mesh%N_edge+basis] 
                matrices%sparse_M(max_las+las) = eta0*1.0_dp/(eta1*dcmplx(0.0,1.0)*k1) * (k1**2*elem(1)-elem(2)) + &
                     eta0*1.0_dp/(eta2*dcmplx(0.0,1.0)*k2) * (k2**2*elem_2(1)-elem_2(2))

                matrices%ind_M(2_ip*max_las+las,:) = [test, mesh%N_edge+basis] 
                matrices%sparse_M(2_ip*max_las+las) = 1.0_dp/eta0 * (-elem(3)+elem(4)) + 1.0_dp/eta0*(-elem_2(3)+elem_2(4))

                matrices%ind_M(3_ip*max_las+las,:) = [mesh%N_edge+test, basis] 
                matrices%sparse_M(3_ip*max_las+las) = -eta0* (-elem(3)+elem(4)) - eta0*(-elem_2(3)+elem_2(4))

             else   
             ! ********* CTF *********************************************************
                matrices%ind_M(las,:) = [test, basis] 
                matrices%sparse_M(las) =  1.0_dp/(dcmplx(0.0,1.0)*k1) * (k1**2*elem(1)-elem(2)) + &
                     1.0_dp/(dcmplx(0.0,1.0)*k2) * (k2**2*elem_2(1)-elem_2(2))

                matrices%ind_M(max_las+las,:) = [mesh%N_edge+test, mesh%N_edge+basis] 
                matrices%sparse_M(max_las+las) = 1.0_dp/(dcmplx(0.0,1.0)*k1) * (k1**2*elem(1)-elem(2)) + &
                     1.0_dp/(dcmplx(0.0,1.0)*k2) * (k2**2*elem_2(1)-elem_2(2))

                matrices%ind_M(2_ip*max_las+las,:) = [test, mesh%N_edge+basis] 
                matrices%sparse_M(2_ip*max_las+las) = 1.0_dp/eta1 * (-elem(3)+elem(4)) + &
                     1.0_dp/eta2 * (-elem_2(3)+elem_2(4)) + 0.5_dp*(1.0_dp/eta1 - 1.0_dp/eta2)*elem(5)

                matrices%ind_M(3_ip*max_las+las,:) = [mesh%N_edge+test, basis] 
                matrices%sparse_M(3_ip*max_las+las) = -eta1*(-elem(3)+elem(4)) - &
                     eta2*(-elem_2(3)+elem_2(4)) - 0.5_dp*(eta1 - eta2)*elem(5)

             end if
             !$OMP END CRITICAL 
          end do

       end do
    end do
 end do
!$omp end do
!$omp end parallel
 
 allocate(matrices%iao(2*mesh%N_edge+1))
 allocate(matrices%ao(4_ip*max_las))
 allocate(matrices%jao(4_ip*max_las))
 
 call  cmplx_coocsr(2*mesh%N_edge, 4_ip*max_las, matrices%sparse_M, matrices%ind_M(:,1) &
      , matrices%ind_M(:,2), matrices%ao, matrices%jao, matrices%iao)

 deallocate(matrices%sparse_M, matrices%ind_M)
!print*, matrices%ind_M
end subroutine build_matrix_pmchwt_edge_mlfma


subroutine ilu(matrices, mesh, otree)
type (mesh_struct) :: mesh
type (data) :: matrices
type (level_struct), dimension(:) :: otree

integer :: i, n, p, j
integer(ip) :: k
complex(dp), dimension(size(matrices%ao)) :: a

a = matrices%ao
n = size(a)

! do i = 2, n ! row index
   
!    do p = ia(i), ia(i+1_ip)-1_ip ! ja(k) column index, k a index
!       if(ja(p) .gt. i-1) exit 

!       a(p) = a(p)
       
!       do j = ia(i), ia(i+1_ip)-1_ip

!          if(ja(j) .gt. ja(p)) then

!          end if
          
!       end do
!          t = t + a(k) * x(ja(k))
!    end do
!    y(i) = t
! end do
  
end subroutine ilu


end module sparse_matrix
