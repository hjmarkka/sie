function tr = interpol(L, theta1, phi1)

p = 3;
N = 2*(L+1);
M = L + 1;

tr = zeros(1,N*M);


%ph = 0:2*pi/(N):(2*pi-2*pi/(N));
ph = pi/(N):2*pi/(N):(2*pi-pi/(N));

[th,W]=gaussint(M,-1,1);

th = acos(th);

fu = cos(th);

fuu = [fu,fu,fu];

fum = sin(ph);
fuum = [fum,fum,fum];

theta = [th+pi; th; th-pi];
phi=[ph-2*pi, ph, ph+2*pi];

ith = [1:size(W),1:size(W),1:size(W)];

for s = 1: N
   if(phi1 < ph(s))
     ss = s;
     break
    end
end
     
s = s -1;

for t = 1: M
   if(theta1 >  th(t))
     tt = t;
    break
    end
end
    
t = t-1 ;



f = 0.0;
las = 1;
for i1 = t-p+1: t+p
     vi_th = 1.0d0;   
     for i2 = t-p+1: t+p
      if(i1 ~= i2) 
	vi_th = vi_th*(theta1 - theta(i2+M)) / (theta(i1+M) - theta(i2+M));
	
      end 
   end 
      f = f + vi_th*fu(ith(i1+M));
vi(las) = vi_th;
ind_theta(las) = i1;
las = las + 1;
end 



  
  f = 0.0;
las = 1;
for i1 = s-p+1: s+p
     wj_phi = 1.0d0;   
     for i2 = s-p+1: s+p
      if(i1 ~= i2) 
	wj_phi = wj_phi*(phi1 - phi(i2+N)) / (phi(i1+N) - phi(i2+N));        
      end 
   end 
f = f + wj_phi*fuum(i1+N);
wj(las) = wj_phi;
ind_phi(las) = i1;
las = las + 1;
end 

  f2 = 0;
  f = 0;
for i1 = 1:2*p
    iphi = ind_phi(i1);
    if(iphi > N)
    iphi = iphi-N;
    end
    if(iphi < 1)
    iphi = N+iphi;
    end
    f = 0;
    for i2 = 1:2*p
       ith = ind_theta(i2);
      
       tr_ind = (iphi-1)*M + ith;
       if(ith > M)
	 ith = M+(M-ith)+1;
         iphi2 = N/2 + iphi;
         if(iphi2 > N)
	   iphi2 = iphi2 - N;         
         end
         tr_ind = (iphi2-1)*M + ith;
       end
       
       if(ith < 1)
          ith = 1-ith; 
          iphi2 = N/2 + iphi;
          if(iphi2 > N)
	    iphi2 = iphi2 - N;         
          end
          tr_ind = (iphi2-1)*M + ith;
       end
       
       f = f + fu(ith)*vi(i2);
      tr(tr_ind) = tr(tr_ind) +vi(i2)*wj(i1);

end 
    f2 = f2 + fum(iphi)*wj(i1);    
end			
			
figure(3), plot(th,fu,theta1,f,'*')
figure(4), plot(ph,fum,phi1,f2,'*')

 
