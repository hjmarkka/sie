module solver_direct
use common
use field_pmchwt
use io
use integration_points
use rhs_pmchwt
use omp_lib

implicit none

contains
function compute_mueller_ave(matrices, mesh, phi, theta, np, mp, crs) result(mueller)
type (data) :: matrices
type (mesh_struct) :: mesh
double precision :: phi, theta, phi_sca, crs(3)
integer :: np, mp, my_id, i1
double precision :: inc_vec(3,3), theta_sca, R , abcd2(2,2), Csca
double precision :: obs_point(3), sca_vec(3,3), ksca(3), abcd(2,2), mat(2,2), Cabs1, Cabs2, Cabs , Cext1, Cext2, Cext
integer :: T1, T2, rate, n, m, las, ind1, ind2, cont, ierr, N_procs, m_start, m_stop
double precision, dimension(:,:), allocatable :: mueller
double complex :: E(3), S(mp*np,5), A, F(mp*np,5)



!print*, my_id, N_procs, m_start, m_stop

allocate(mueller(mp*np,17))
mueller(:,:) = 0.0

R  = 100000.0 * 2*pi/mesh%k 

inc_vec = sph_unit_vectors(theta,phi)

abcd(1,1) = 1.0!inc_vec(:,2)
abcd(1,2) = 0.0!inc_vec(:,2)
abcd(2,1) = 0.0!inc_vec(:,3)
abcd(2,2) = 1.0!inc_vec(:,3)


matrices%khat = inc_vec(:,1)
matrices%E0 = inc_vec(:,2)



call rhs(matrices, mesh)

print*,'Solving...'
call system_clock(T1,rate)
!call gmres_full_mpi(matrices,mesh)
matrices%x = matmul(matrices%A, matrices%rhs)

Cabs1 = absorption_cross_section(matrices, mesh)
!print*, 'Cabs1 =', Cabs1



call system_clock(T2)
print*,'Matmul done in', real(T2-T1)/real(rate), 'seconds'

call system_clock(T1,rate)



F(:,:) = dcmplx(0.0,0.0)

!if(my_id == 0) then

!$omp parallel default(private) &
!$omp firstprivate(mp,np,abcd2,R, phi,theta) &
!$omp shared(matrices, mesh, F)
!$omp do 
do m = 0,mp-1

   phi_sca = 2*pi*m/(mp)
   mat(1,1) = cos(phi_sca)
   mat(1,2) = sin(phi_sca)
   mat(2,1) = sin(phi_sca)
   mat(2,2) = -cos(phi_sca)
   abcd2 = matmul(abcd,mat)

  

   do n = 0,np-1
         
      theta_sca = pi*n/(np-1)
      obs_point = sph2cart(R,theta_sca,phi_sca)
      obs_point = rotation(obs_point,2,theta)
      obs_point = rotation(obs_point,3,phi)
    
    
     ! E = farfield_efie(matrices, mesh, obs_point)
       E = fields(matrices, mesh, obs_point)
    
      sca_vec = sph_unit_vectors(theta_sca,phi_sca)
      sca_vec(:,2) =  rotation(sca_vec(:,2),2,theta)
      sca_vec(:,2) =  rotation(sca_vec(:,2),3,phi)

      sca_vec(:,3) =  rotation(sca_vec(:,3),2,theta)
      sca_vec(:,3) =  rotation(sca_vec(:,3),3,phi)

      A = cdexp(dcmplx(0.0, mesh%k*(R))) / (mesh%k*R) 

      las = m*(np) + n+1
      F(las,1) = 1/A  * sum(E * sca_vec(:,2))
      F(las,3) = 1/A  * sum(E * sca_vec(:,3))
     

   end do

end do
!$omp end do
!$omp end parallel

 E = fields(matrices, mesh, matrices%khat*R)
 A = cdexp(dcmplx(0.0, mesh%k*(R))) / R
 
 Cext1 = vlen(imag(E/A) * 4*pi / mesh%k)
!end if

!if(my_id .ne. 0) then 
!   deallocate(matrices%x)
!end if



call system_clock(T2)
print*,'Fields done in', real(T2-T1)/real(rate), 'seconds'
!end if


matrices%E0 = inc_vec(:,3)

call rhs(matrices, mesh)


!print*,'Solving...'
call system_clock(T1,rate)
!call gmres_full_mpi(matrices,mesh)
matrices%x = matmul(matrices%A, matrices%rhs)
Cabs2 = absorption_cross_section(matrices, mesh)
!print*, 'Cabs2 =', Cabs2
call system_clock(T2)
!print*,'Done in', real(T2-T1)/real(rate), 'seconds'


!print*,'Compute fields...'
call system_clock(T1,rate)




!$omp parallel default(private) &
!$omp firstprivate(mp,np,abcd2,R, phi,theta) &
!$omp shared(matrices, mesh, F)
!$omp do 
do m = 0,mp-1

   phi_sca = 2*pi*m/(mp)
   mat(1,1) = cos(phi_sca)
   mat(1,2) = sin(phi_sca)
   mat(2,1) = sin(phi_sca)
   mat(2,2) = -cos(phi_sca)
   abcd2 = matmul(abcd,mat)

   do n = 0,np-1
      
     
      theta_sca = pi*n/(np-1)
     
      obs_point = sph2cart(R,theta_sca,phi_sca)
      obs_point = rotation(obs_point,2,theta)
      obs_point = rotation(obs_point,3,phi)
    

     
!      E = farfield_efie(matrices, mesh, obs_point)
     
       E = fields(matrices, mesh, obs_point)

      sca_vec = sph_unit_vectors(theta_sca,phi_sca)
      sca_vec(:,2) =  rotation(sca_vec(:,2),2,theta)
      sca_vec(:,2) =  rotation(sca_vec(:,2),3,phi)

      sca_vec(:,3) =  rotation(sca_vec(:,3),2,theta)
      sca_vec(:,3) =  rotation(sca_vec(:,3),3,phi)


      ksca = obs_point/R * mesh%k

      A = cdexp(dcmplx(0.0, mesh%k*(R) )) / (mesh%k*R) 

      las = m*(np) + n+1
      F(las,2) = 1/A  * sum(E * sca_vec(:,2))
      F(las,4) = 1/A  * sum(E * sca_vec(:,3))
      F(las,5) = theta_sca

   end do
end do
!$omp end do
!$omp end parallel


 E = fields(matrices, mesh, matrices%khat*R)
 A = cdexp(dcmplx(0.0, mesh%k*(R))) / R
 
 Cext2 = vlen(imag(E/A) * 4*pi / mesh%k)

!if(my_id .ne. 0) then 
!   deallocate(matrices%x)
!end if


call system_clock(T2)
!print*,'Done in', real(T2-T1)/real(rate), 'seconds'
print*,'Fields done in', real(T2-T1)/real(rate), 'seconds'




do m = 0,mp-1
   phi_sca = 2*pi*m/(mp)
   mat(1,1) = cos(phi_sca)
   mat(1,2) = sin(phi_sca)
   mat(2,1) = sin(phi_sca)
   mat(2,2) = -cos(phi_sca)
   abcd2 = matmul(abcd,mat)

   ind1 = m*np + 1
   ind2 = (m+1)*np

   S(ind1:ind2,2) =  -dcmplx(0.0, 1.0) * (F(ind1:ind2,1) * abcd2(1,1) + F(ind1:ind2,2) * abcd2(2,1))
   S(ind1:ind2,3) =  dcmplx(0.0, 1.0) * (F(ind1:ind2,1) * abcd2(1,2) + F(ind1:ind2,2) * abcd2(2,2))
   S(ind1:ind2,4) =  dcmplx(0.0, 1.0) * (F(ind1:ind2,3) * abcd2(1,1) + F(ind1:ind2,4) * abcd2(2,1))
   S(ind1:ind2,1) =  -dcmplx(0.0, 1.0) * (F(ind1:ind2,3) * abcd2(1,2) + F(ind1:ind2,4) * abcd2(2,2)) 

   S(ind1:ind2,5) = F(ind1:ind2,5)

end do



mueller(:,1) = real(S(:,5))*180/pi

mueller(:,2) = 0.5 * (abs(S(:,1))**2 + abs(S(:,2))**2 + abs(S(:,3))**2 + abs(S(:,4))**2)
mueller(:,3) = 0.5 * (abs(S(:,2))**2 - abs(S(:,1))**2 + abs(S(:,4))**2 - abs(S(:,3))**2)
mueller(:,4) = -(real(S(:,2)*conjg(S(:,3)) + S(:,1)*conjg(S(:,4))))
mueller(:,5) = imag(S(:,2)*conjg(S(:,3)) - S(:,1)*conjg(S(:,4)))

mueller(:,6) = 0.5 * (abs(S(:,2))**2 - abs(S(:,1))**2 + abs(S(:,3))**2 - abs(S(:,4))**2)
mueller(:,7) = 0.5 * (abs(S(:,1))**2 + abs(S(:,2))**2 - abs(S(:,3))**2 - abs(S(:,3))**2)
mueller(:,8) = real(S(:,2)*conjg(S(:,3)) - S(:,1)*conjg(S(:,4)))
mueller(:,9) = imag(S(:,2)*conjg(S(:,3)) + S(:,1)*conjg(S(:,4)))

mueller(:,10) = real(S(:,2)*conjg(S(:,4)) + S(:,1)*conjg(S(:,3)))
mueller(:,11) = -(real(S(:,2)*conjg(S(:,4)) - S(:,1)*conjg(S(:,3))))
mueller(:,12) = -(real(S(:,1)*conjg(S(:,2)) + S(:,3)*conjg(S(:,4))))
mueller(:,13) = -(imag(S(:,2)*conjg(S(:,1)) + S(:,4)*conjg(S(:,3))))

mueller(:,14) = -(imag(S(:,4)*conjg(S(:,2)) + S(:,1)*conjg(S(:,3))))
mueller(:,15) = -(imag(S(:,4)*conjg(S(:,2)) - S(:,1)*conjg(S(:,3))))
mueller(:,16) = -(imag(S(:,1)*conjg(S(:,2)) - S(:,3)*conjg(S(:,4))))
mueller(:,17) = -(real(S(:,1)*conjg(S(:,2)) - S(:,3)*conjg(S(:,4))))


Cabs = (Cabs1 + Cabs2)/2.0d0
Cext = (Cext1 + Cext2)/2.0d0

print*, 'Cabs =', Cabs
print*, 'Cext =', Cext

crs(1) = Cabs
crs(2) = Cext

Csca = 0.0d0
do m = 1, mp*np

   Csca = Csca + mueller(m,2)*sin(mueller(m,1)/180d0*pi)/mp/(np-1) * pi * 2.0* pi/mesh%k**2.0d0
   !Csca = Csca + sin(mueller(m,1)/180d0*pi)/mp/(np-1) *pi
end do
crs(3) = Csca
print*, 'Csca =', Csca
end function compute_mueller_ave

!____________________________________________________________________________
!
!
!
!____________________________________________________________________________-

function compute_mueller(matrices, mesh, phi, theta, np, phi_sca) result(mueller)
type (data) :: matrices
type (mesh_struct) :: mesh
double precision :: phi, theta, phi_sca
integer :: np, my_id
double precision :: inc_vec(3,3), theta_sca, R, abcd(2,2), mat(2,2) 
double precision :: obs_point(3), sca_vec(3,3), abcd2(2,2)
integer :: T1, T2, rate, n, ierr, cont
double precision, dimension(:,:), allocatable :: mueller
double complex :: E(3), S(np,5), A, F(np,5)



allocate(mueller(np,17))
mueller(:,:) = 0.0


R  = 100000.0 * 2*pi/mesh%k 

inc_vec = sph_unit_vectors(theta,phi)

abcd(1,1) = 1.0!inc_vec(:,2)
abcd(1,2) = 0.0!inc_vec(:,2)
abcd(2,1) = 0.0!inc_vec(:,3)
abcd(2,2) = 1.0!inc_vec(:,3)


mat(1,1) = cos(phi_sca)
mat(1,2) = sin(phi_sca)
mat(2,1) = sin(phi_sca)
mat(2,2) = -cos(phi_sca)
abcd2 = matmul(abcd,mat)



matrices%khat = inc_vec(:,1)
matrices%E0 = inc_vec(:,2)

call rhs(matrices, mesh)

print*,'Solving...'
call system_clock(T1,rate)
matrices%x = matmul(matrices%A, matrices%rhs)
call system_clock(T2)
print*,'Done in', real(T2-T1)/real(rate), 'seconds'


do n = 0,np-1

   theta_sca = pi*n/(np-1)
 
   obs_point = sph2cart(R,theta_sca,phi_sca)
   obs_point = rotation(obs_point,2,theta)
   obs_point = rotation(obs_point,3,phi)

 
   E = fields(matrices, mesh, obs_point)
 
     
   sca_vec = sph_unit_vectors(theta_sca,phi_sca)
   sca_vec(:,2) =  rotation(sca_vec(:,2),2,theta)
   sca_vec(:,2) =  rotation(sca_vec(:,2),3,phi)

   sca_vec(:,3) =  rotation(sca_vec(:,3),2,theta)
   sca_vec(:,3) =  rotation(sca_vec(:,3),3,phi)

   A = cdexp(dcmplx(0.0, mesh%k*(R) )) / (mesh%k*R) 

   F(n+1,1) = 1/A  * sum(E * sca_vec(:,2)) !f11
   F(n+1,3) = 1/A  * sum(E * sca_vec(:,3)) !ff21 
end do



matrices%E0 = inc_vec(:,3)
call rhs(matrices, mesh)


print*,'Solving...'
call system_clock(T1,rate)
matrices%x = matmul(matrices%A, matrices%rhs)
call system_clock(T2)
print*,'Done in', real(T2-T1)/real(rate), 'seconds'



do n = 0,np-1

   theta_sca = pi*n/(np-1)
  
   obs_point = sph2cart(R,theta_sca,phi_sca)
   obs_point = rotation(obs_point,2,theta)
   obs_point = rotation(obs_point,3,phi)

   E = farfield_efie(matrices, mesh, obs_point)

  
  
   sca_vec = sph_unit_vectors(theta_sca,phi_sca)
   sca_vec(:,2) =  rotation(sca_vec(:,2),2,theta)
   sca_vec(:,2) =  rotation(sca_vec(:,2),3,phi)

   sca_vec(:,3) =  rotation(sca_vec(:,3),2,theta)
   sca_vec(:,3) =  rotation(sca_vec(:,3),3,phi)

   A = cdexp(dcmplx(0.0, mesh%k*(R) )) / (mesh%k*R) 

   F(n+1,2) = 1/A  * sum(E * sca_vec(:,2)) !f12
   F(n+1,4) = 1/A  * sum(E * sca_vec(:,3)) !f22
   S(n+1,5) = theta_sca
end do



S(:,2) =  -dcmplx(0.0, 1.0) * (F(:,1) * abcd2(1,1) + F(:,2) * abcd2(2,1))
S(:,3) =  dcmplx(0.0, 1.0) * (F(:,1) * abcd2(1,2) + F(:,2) * abcd2(2,2))
S(:,4) =  dcmplx(0.0, 1.0) * (F(:,3) * abcd2(1,1) + F(:,4) * abcd2(2,1))
S(:,1) =  -dcmplx(0.0, 1.0) * (F(:,3) * abcd2(1,2) + F(:,4) * abcd2(2,2))

mueller(:,1) = real(S(:,5))*180/pi

mueller(:,2) = 0.5 * (abs(S(:,1))**2 + abs(S(:,2))**2 + abs(S(:,3))**2 + abs(S(:,4))**2)
mueller(:,3) = 0.5 * (abs(S(:,2))**2 - abs(S(:,1))**2 + abs(S(:,4))**2 - abs(S(:,3))**2)
mueller(:,4) = -(real(S(:,2)*conjg(S(:,3)) + S(:,1)*conjg(S(:,4))))
mueller(:,5) = imag(S(:,2)*conjg(S(:,3)) - S(:,1)*conjg(S(:,4)))

mueller(:,6) = 0.5 * (abs(S(:,2))**2 - abs(S(:,1))**2 + abs(S(:,3))**2 - abs(S(:,4))**2)
mueller(:,7) = 0.5 * (abs(S(:,1))**2 + abs(S(:,2))**2 - abs(S(:,3))**2 - abs(S(:,3))**2)
mueller(:,8) = real(S(:,2)*conjg(S(:,3)) - S(:,1)*conjg(S(:,4)))
mueller(:,9) = imag(S(:,2)*conjg(S(:,3)) + S(:,1)*conjg(S(:,4)))

mueller(:,10) = real(S(:,2)*conjg(S(:,4)) + S(:,1)*conjg(S(:,3)))
mueller(:,11) = -(real(S(:,2)*conjg(S(:,4)) - S(:,1)*conjg(S(:,3))))
mueller(:,12) = -(real(S(:,1)*conjg(S(:,2)) + S(:,3)*conjg(S(:,4))))
mueller(:,13) = -(imag(S(:,2)*conjg(S(:,1)) + S(:,4)*conjg(S(:,3))))

mueller(:,14) = -(imag(S(:,4)*conjg(S(:,2)) + S(:,1)*conjg(S(:,3))))
mueller(:,15) = -(imag(S(:,4)*conjg(S(:,2)) - S(:,1)*conjg(S(:,3))))
mueller(:,16) = -(imag(S(:,1)*conjg(S(:,2)) - S(:,3)*conjg(S(:,4))))
mueller(:,17) = -(real(S(:,1)*conjg(S(:,2)) - S(:,3)*conjg(S(:,4))))

!call write2file(S)
!print*, my_id


end function compute_mueller

!______ Orientation averaging routine_____________1

subroutine orientation_ave(mesh,matrices, N_theta,M,halton_init, mueller_ave, crs)
type (data) :: matrices
type (mesh_struct) :: mesh

double precision, dimension(:,:), allocatable :: mueller, mueller_ave
integer :: M1,M2,M,N, i, N_theta, i1, halton_init, my_id, ierr
double precision :: vec(3), Cabs_ave, Cabs, crs(3), Cext_ave, Csca_ave, crs2(3)
 
!N = M
!N = int(floor(sqrt(dble(M))*2))
N = 64

!if(my_id == 0) then
allocate(mueller(N_theta*N,17))
allocate(mueller_ave(N_theta,17))
mueller_ave(:,:) = 0.0 

Cabs_ave = 0.0d0
Cext_ave = 0.0d0
Csca_ave = 0.0d0
crs(:) = 0.0_dp

 
do i = 1,M

   vec(2) = acos(2*halton_seq(halton_init+i, 2)-1)
   vec(3) = halton_seq(halton_init+i, 3)*2*pi

   !print*,'theta=',180/pi*vec(2), 'phi=',180/pi*vec(3)  
  
   mueller = compute_mueller_ave(matrices, mesh, vec(3), vec(2), N_theta, N, crs2) 
!print*,'kkkkkkkk'
   !if(my_id == 0) then
   do i1 = 1,N
      mueller_ave = mueller_ave + mueller(N_theta*(i1-1)+1:N_theta*i1,:)/dble(N*M)
     
   end do
   Cabs_ave = Cabs_ave + crs2(1)/dble(M)
   Cext_ave = Cext_ave + crs2(2)/dble(M)
   Csca_ave = Csca_ave + crs2(3)/dble(M)
   crs = crs + crs2/dble(M)
   print*,'Orientation averaging', i,'/',M
   !end if 
end do


Print*, 'Cabs =', Cabs_ave
Print*, 'Cext =', Cext_ave
Print*, 'Csca =', Csca_ave


end subroutine orientation_ave

end module solver_direct
