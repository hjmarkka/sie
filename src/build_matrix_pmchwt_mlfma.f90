module sparse_matrix
use common
use integrals
use integration_points
use omp_lib
use octtree

implicit none

subroutine build_matrix_pmchwt_mlfma(matrices, mesh, otree)

type (mesh_struct) :: mesh
type (data) :: matrices
type (level_struct), dimension(:) :: otree

integer :: test_T, basis_T, T_nodes(3), B_nodes(3), my_id, N_procs, N1, N2, N_loc
double precision :: T_coord(3,3), B_coord(3,3)
double precision, allocatable :: P0_tri(:,:), P02_tri(:,:)
double precision, allocatable :: w0_tri(:), P0_line(:), w0_line(:)
double precision, allocatable :: w02_tri(:), P02_line(:), w02_line(:)
double precision :: T_dN(3,3), T_nxdN(3,3), T_nvec(3)
double precision :: B_dN(3,3), B_nxdN(3,3), B_nvec(3)
integer :: te, be, test, basis, near, block_size, ierr, max_level, level_ind
integer :: sph1_loc, box, box2_loc, box2, sph2_loc
double precision :: signt, signb, sign, eta0
double complex :: k1, k2, eta1, eta2
double complex :: alok(3,3), alok2(3,3), alok3(3,3), alok4(3,3,3), elem(5)
double complex :: alok_2(3,3), alok2_2(3,3), alok3_2(3,3), alok4_2(3,3,3), elem_2(5)

eta0 = sqrt(mu/epsilon)

allocate(matrices%A(2*mesh%N_edge,2*mesh%N_edge))
matrices%A(:,:) = dcmplx(0.0,0.0)


alok3(:,:) = 0.0
alok4(:,:,:) = 0.0

k1 = dcmplx(mesh%k, 0.0)
k2 = mesh%k * sqrt(mesh%eps_r)

eta1 = sqrt(mu/epsilon)
eta2 = sqrt(mu/(epsilon*mesh%eps_r))

call inttri(P0_tri,w0_tri,5)
call gaussint(P0_line,w0_line,5)

call inttri(P02_tri,w02_tri,5)
call gaussint(P02_line,w02_line,5)

max_level = size(otree) - 1
level_ind = max_level + 1

!$omp parallel default(private) &
!$omp firstprivate(P0_tri, P0_line, w0_tri, w0_line) &
!$omp firstprivate(P02_tri, P02_line, w02_tri, w02_line) &
!$omp firstprivate(alok3,alok4, k1, k2, eta1, eta2, eta0) &
!$omp shared(matrices, mesh)
!$omp do

print*, size(otree)

do box = 1, size(otree(level_ind)%tree)
   !print*, box, size(otree(level_ind)%tree)
   !print*, otree(level_ind)%tree(box)%N_source
   do sph1_loc = 1, otree(level_ind)%tree(box)%N_source
      print*, sph1_loc
      test_T = otree(level_ind)%tree(box)%sources(sph1_loc)
   
      print*, test_T
      !T_nodes = mesh%etopol_edges(:,test_T)
      T_coord = mesh%coord(:,mesh%etopol(:,test_T))

      call gradshape_tri(T_dN,T_nxdN, T_nvec, T_coord)

      do box2_loc = 1,27
   
          box2 = otree(level_ind)%tree(box)%near_neighbours(box2_loc)
          if(box2 == 0) exit

          do sph2_loc = 1, otree(level_ind)%tree(box2)%N_source
             
             basis_T =  otree(level_ind)%tree(box2)%sources(sph2_loc)
          
             !B_nodes = mesh%etopol_edges(:,basis_T)
             B_coord = mesh%coord(:,mesh%etopol(:,basis_T))     

             call gradshape_tri(B_dN,B_nxdN, B_nvec, B_coord)
             
             near = near_zone(mesh%etopol(:,test_T), mesh%etopol(:,basis_T))
      
             if(near == 1) then
          
                call integrate_S_S_GNN(alok2, P02_tri, w02_tri, T_coord, B_coord, k1)
                call integrate_S_S_ngradGNN(alok3, P02_tri, w02_tri, T_coord, B_coord, k1)
                call integrate_dS_S_GNN(alok4, P02_tri, w02_tri, P02_line, w02_line, T_coord, B_coord, k1, mesh%Eori)
       
                call integrate_S_S_GNN(alok2_2, P02_tri, w02_tri, T_coord, B_coord, k2)
                call integrate_S_S_ngradGNN(alok3_2, P02_tri, w02_tri, T_coord, B_coord, k2)
                call integrate_dS_S_GNN(alok4_2, P02_tri, w02_tri, P02_line, w02_line, T_coord, B_coord, k2, mesh%Eori)
       
             else

                call integrate_S_S_GNN(alok2, P0_tri, w0_tri, T_coord, B_coord, k1)
                call integrate_S_S_ngradGNN(alok3, P0_tri, w0_tri, T_coord, B_coord, k1)
                call integrate_dS_S_GNN(alok4, P0_tri, w0_tri, P0_line, w0_line, T_coord, B_coord, k1, mesh%Eori)
         
                call integrate_S_S_GNN(alok2_2, P0_tri, w0_tri, T_coord, B_coord, k2)
                call integrate_S_S_ngradGNN(alok3_2, P0_tri, w0_tri, T_coord, B_coord, k2)
                call integrate_dS_S_GNN(alok4_2, P0_tri, w0_tri, P0_line, w0_line, T_coord, B_coord, k2, mesh%Eori)

             end if


             alok(:,:) = dcmplx(0.0,0.0)
             
             if(test_T == basis_T) then
                call integrate_S_NN(alok, P0_tri, w0_tri, T_coord)
             end if


       !___________________________________________________!
      

             do te = 1,3
                test = mesh%etopol_edges(te,test_T)
                if(mesh%boundary(test) == 0) then
                   signt = RWG_sign(mesh, test, test_T,te)

                   do be = 1,3
                      basis = mesh%etopol_edges(be,basis_T)
                      if(mesh%boundary(basis) == 0) then
                         signb = RWG_sign(mesh, basis, basis_T,be)

                         !print*, test, basis
                         
                         sign = signt*signb
           
                         elem = EFIE_elements(alok, alok2, alok3, alok4,T_dN, B_dN, T_nxdN, B_nxdN, &
                              te, be, mesh%Eori, T_coord, B_coord, B_nvec)

                         elem_2 = EFIE_elements(alok, alok2_2, alok3_2, alok4_2,T_dN, B_dN, &
                              T_nxdN, B_nxdN, te, be, mesh%Eori, T_coord, B_coord, B_nvec)


                         !print*, elem
             
                         matrices%A(test,basis) = matrices%A(test,basis) + &
                              1/eta0*eta1/(dcmplx(0.0,1.0)*k1) * sign*(k1**2*elem(1)-elem(2)) + &
                              1/eta0*eta2/(dcmplx(0.0,1.0)*k2) * sign*(k2**2*elem_2(1)-elem_2(2)) 
             
                         matrices%A(mesh%N_edge+test, mesh%N_edge+basis) = &
                              matrices%A(mesh%N_edge+test, mesh%N_edge+basis) + &
                              eta0*1.0/(eta1*dcmplx(0.0,1.0)*k1) * sign*(k1**2*elem(1)-elem(2)) + &
                              eta0*1.0/(eta2*dcmplx(0.0,1.0)*k2) * sign*(k2**2*elem_2(1)-elem_2(2)) 
             
                         matrices%A(test, mesh%N_edge+basis) = &
                              matrices%A(test, mesh%N_edge+basis) + &
                              1/eta0*sign*(-elem(3)+elem(4)) + 1/eta0*sign*(-elem_2(3)+elem_2(4))
            
                         matrices%A(mesh%N_edge+test, basis) = &
                              matrices%A(mesh%N_edge+test, basis) - &
                              eta0*sign*(-elem(3)+elem(4)) - eta0*sign*(-elem_2(3)+elem_2(4))
                      end if

                   end do
                end if
             end do

          end do

       end do
    end do
 end do
!$omp end do
!$omp end parallel

end subroutine build_matrix_pmchwt_mlfma

end module sparse_matrix
