program main
use common
use geometry
use io
use field_efie
use possu
!use solver_direct
implicit none


type (mesh_struct) :: mesh
type (data) :: matrices

integer :: ierr, rc, my_id, N_procs, Eori(2,3), t1,t2, rate

integer :: restart, maxit, mueller, ave, halton_init, i1
double precision :: k, tol, khat(3), E0(3), cell_size, epsr, epsi, r(3), theta
double complex :: eps_r, E(3)
double precision :: p1(3), p2(3), maxlength, minlength, avelength
CHARACTER(LEN=28) :: meshname, fname, mueller_out, J_out, arg_name, arg, rcs_out
integer :: num_args, i_arg
double precision, dimension(:,:), allocatable :: mueller_mat, mueller_mat_ave


integer :: n, m, N_th, N_phi, NN, M_thphi
real(dp) :: d_phi, a, s, rs, th, phi
   print *,'*************************************************************'
   print *,'**                                                         **'
   print *,'**               SIE-PMCHWT v. 0.1                     **'
   print *,'**                                                         **'
   print *,'*************************************************************'


! Default arguments
   meshname = 'mesh.h5'
   mueller_out = 'mueller.h5'
   J_out = 'J.h5'
   k = 1
   khat = [0,0,1] 
   E0 = [1,0,0]
   mueller = 0 
   phi = 0.0
   theta = 0.0
   ave = 0
   halton_init = 0
   tol = 1e-5 
   maxit = 50  
   restart = 4
   epsr = 2.0
   epsi = 0.0

   num_args = command_argument_count()
   do i_arg = 1,num_args,2
      call get_command_argument(i_arg,arg_name)
  
      select case(arg_name)
      
      case('-mesh')
         call get_command_argument(i_arg+1,arg)
         meshname = arg
         print*, 'mesh file is:', meshname
      case('-S_out')
         call get_command_argument(i_arg+1,arg)
         mueller_out = arg
         print*, 'Mueller matrix is written in the file:', mueller_out
      case('-J_out')
         call get_command_argument(i_arg+1,arg)
         J_out = arg
         print*, 'Solution is written in the file:', J_out
      case('-k')
         call get_command_argument(i_arg+1,arg)
         read(arg,*) k 
      case('-eps_r')
         call get_command_argument(i_arg+1,arg)
         read(arg,*) epsr 
      case('-eps_i')
         call get_command_argument(i_arg+1,arg)
         read(arg,*) epsi 
      case('-mueller')
         call get_command_argument(i_arg+1,arg)
         read(arg,*) mueller
      case('-phi')
         call get_command_argument(i_arg+1,arg)
         read(arg,*) phi
      case('-theta')
         call get_command_argument(i_arg+1,arg)
         read(arg,*) theta
      case('-ave')
         call get_command_argument(i_arg+1,arg)
         read(arg,*) ave
      case('-halton_int')
         call get_command_argument(i_arg+1,arg)
         read(arg,*) halton_init
      case('-tol')
         call get_command_argument(i_arg+1,arg)
         read(arg,*) tol
      case('-maxit')
         call get_command_argument(i_arg+1,arg)
         read(arg,*) maxit
      case('-restart')
         call get_command_argument(i_arg+1,arg)
         read(arg,*) restart
      
      case('-help')
         print*, 'Command line parameters' 
         print*, '-mesh mesh.h5      "Read mesh from file"' 
         print*, '-S_out mueller.h5  "Output file: Mueller matrix"'
         print*, '-J_out J.h5        "Output file: Solution coefficients"'
         print*, '-k 1.0             "Wavenumber"'
         print*, '-mueller 1         "Compute mueller matrix (1) yes (0) no"'
         print*, '-phi 0.0           "Incident angel phi"'
         print*, '-theta 0.0         "Incident angel theta"'
         print*, '-ave 0             "Orientation averaging (number of orientations)"'
         print*, '-halton_init 0     "Orientation averaging (beginning of Halton sequence)"'
         print*, '-tol 1e-5          "GMRES tolerance"'
         print*, '-restart 4         "GMRES restart"'
         print*, '-maxit 50          "Maximum number of GMRES iterations"'
         stop
      case default 
         print '(a,a,/)', 'Unrecognized command-line option: ', arg_name
         stop
      end select
   end do

   mesh%tol = tol
   mesh%restart = restart
   mesh%maxit = maxit
   matrices%khat = khat
   matrices%E0 = dcmplx(E0, 0.0)
   mesh%k = k
   mesh%eps_r = dcmplx(epsr, epsi)

   call read_mesh(mesh, meshname)
  ! call read_field_points(matrices)
  

Eori(:,1) = [3,2]
Eori(:,2) = [1,3]
Eori(:,3) = [2,1]

mesh%Eori = Eori


call find_edges(mesh)

!print*, mesh%edges
   print*, '   Number of edges      =', mesh%N_edge
   print*, '   k  = ', real(k)

   maxlength = 0.0
   avelength = 0.0

   p1 = mesh%coord(:,mesh%edges(1,1))
   p2 = mesh%coord(:,mesh%edges(2,1))
   minlength = norm(p1,p2)
    do i1=1,mesh%N_edge
       p1 = mesh%coord(:,mesh%edges(1,i1))
       p2 = mesh%coord(:,mesh%edges(2,i1))
       if (norm(p1,p2) > maxlength) then
          maxlength = norm(p1,p2)
       end if
       if (norm(p1,p2) < minlength) then
          minlength = norm(p1,p2)
       end if
       avelength = avelength + norm(p1,p2)

    end do

    avelength = avelength/dble(mesh%N_edge)
    print*, '   Ave elem. size', real((2*pi/k)/avelength)
    print*, '   Max elem. size', real((2*pi/k)/maxlength)
    print*, '   Min elem. size', real((2*pi/k)/minlength)




allocate(matrices%rhs(mesh%N_edge))
allocate(matrices%x(mesh%N_edge))
allocate(matrices%Ax(mesh%N_edge))

call rhs_efie(matrices, mesh)
!call rhs_mfie(matrices,mesh)
!call rhs(matrices,mesh)

print*, 'Build matrix...'
!print*, (matrices%rhs)
call system_clock(t1,rate)
!call build_matrix_pmchwt_mpi(matrices,mesh,my_id,N_procs)
!call build_matrix_pmchwt(matrices, mesh)
call system_clock(t2,rate)

call build_matrix_efie(matrices,mesh)
!call build_matrix_mfie(matrices,mesh)

print*,'Done in', real(T2-T1)/real(rate), 'seconds'

fname = "A.h5"
call write2file(matrices%A,fname) 



print*, 'Solving...'
matrices%A = Cinv(matrices%A)
matrices%x = matmul(matrices%A,matrices%rhs)
!call gmres_full(matrices, mesh)
print*,'Done'



if(mueller == 0) then

  
!call calc_fields(matrices, mesh)
NN=10000
a=4.0_dp*pi / NN   
N_th = nint(pi/sqrt(a))
d_phi = a*dble(N_th)/pi

s = -3.0_dp
rs = (mesh%k /(2.0_dp*pi) + 10.0_dp ** (s)) *2*pi/mesh%k
fname = 'c_4600_-3_EFIE.csv'
OPEN(UNIT=12, FILE=fname, ACTION="write", STATUS="replace")
do n = 0, N_th-1
   th = (dble(n) + 0.5_dp)*pi / dble(N_th)
   M_thphi = nint(2.0_dp*pi*sin(th)/d_phi)   
   do m = 0, M_thphi-1
      phi = 2.0_dp*pi*m/dble(M_thphi)
      r = sph2cart(rs,th,phi)    
      E = fields(matrices, mesh,r)
       WRITE(12,*), real(dot_product(E,E)),',',th,',',phi     
   end do
end do
CLOSE(12)



s = -2.0_dp
rs = (mesh%k /(2.0_dp*pi) + 10.0_dp ** (s)) *2*pi/mesh%k
fname = 'c_4600_-2_EFIE.csv'
OPEN(UNIT=12, FILE=fname, ACTION="write", STATUS="replace")
do n = 0, N_th-1
   th = (dble(n) + 0.5_dp)*pi / dble(N_th)
   M_thphi = nint(2.0_dp*pi*sin(th)/d_phi)   
   do m = 0, M_thphi-1
      phi = 2.0_dp*pi*m/dble(M_thphi)
      r = sph2cart(rs,th,phi)    
      E = fields(matrices, mesh,r)
       WRITE(12,*), real(dot_product(E,E)),',',th,',',phi     
   end do
end do
CLOSE(12)


s = -1.0_dp
rs = (mesh%k /(2.0_dp*pi) + 10.0_dp ** (s)) *2*pi/mesh%k
fname = 'c_4600_-1_EFIE.csv'
OPEN(UNIT=12, FILE=fname, ACTION="write", STATUS="replace")
do n = 0, N_th-1
   th = (dble(n) + 0.5_dp)*pi / dble(N_th)
   M_thphi = nint(2.0_dp*pi*sin(th)/d_phi)   
   do m = 0, M_thphi-1
      phi = 2.0_dp*pi*m/dble(M_thphi)
      r = sph2cart(rs,th,phi)    
      E = fields(matrices, mesh,r)
       WRITE(12,*), real(dot_product(E,E)),',',th,',',phi     
   end do
end do
CLOSE(12)


s = 0.0_dp
rs = (mesh%k /(2.0_dp*pi) + 10.0_dp ** (s)) *2*pi/mesh%k
fname = 'c_4600_0_EFIE.csv'
OPEN(UNIT=12, FILE=fname, ACTION="write", STATUS="replace")
do n = 0, N_th-1
   th = (dble(n) + 0.5_dp)*pi / dble(N_th)
   M_thphi = nint(2.0_dp*pi*sin(th)/d_phi)   
   do m = 0, M_thphi-1
      phi = 2.0_dp*pi*m/dble(M_thphi)
      r = sph2cart(rs,th,phi)    
      E = fields(matrices, mesh,r)
       WRITE(12,*), real(dot_product(E,E)),',',th,',',phi     
   end do
end do
CLOSE(12)


s = 1.0_dp
rs = (mesh%k /(2.0_dp*pi) + 10.0_dp ** (s)) *2*pi/mesh%k
fname = 'c_4600_1_EFIE.csv'
OPEN(UNIT=12, FILE=fname, ACTION="write", STATUS="replace")
do n = 0, N_th-1
   th = (dble(n) + 0.5_dp)*pi / dble(N_th)
   M_thphi = nint(2.0_dp*pi*sin(th)/d_phi)   
   do m = 0, M_thphi-1
      phi = 2.0_dp*pi*m/dble(M_thphi)
      r = sph2cart(rs,th,phi)    
      E = fields(matrices, mesh,r)
       WRITE(12,*), real(dot_product(E,E)),',',th,',',phi     
   end do
end do
CLOSE(12)


s = 2.0_dp
rs = (mesh%k /(2.0_dp*pi) + 10.0_dp ** (s)) *2*pi/mesh%k
fname = 'c_4600_2_EFIE.csv'
OPEN(UNIT=12, FILE=fname, ACTION="write", STATUS="replace")
do n = 0, N_th-1
   th = (dble(n) + 0.5_dp)*pi / dble(N_th)
   M_thphi = nint(2.0_dp*pi*sin(th)/d_phi)   
   do m = 0, M_thphi-1
      phi = 2.0_dp*pi*m/dble(M_thphi)
      r = sph2cart(rs,th,phi)    
      E = fields(matrices, mesh,r)
       WRITE(12,*), real(dot_product(E,E)),',',th,',',phi     
   end do
end do
CLOSE(12)




call compute_rcs(matrices,mesh)
rcs_out = "rcs.h5"
call real_write2file(matrices%rcs,rcs_out)









!fname = "x.h5"
!call cmplx_vec_write2file(matrices%x,fname)

!fname = "rhs.h5"
!call cmplx_vec_write2file(matrices%rhs,fname)


end if


!if (mueller==1) then
!   if(ave == 0) then
!      mueller_mat = compute_mueller(matrices, mesh, pi/180.0*phi, pi/180.0*theta, 360, pi/180.0 * 0.0)
!   else
!      mueller_mat = orientation_ave(mesh,matrices,181,ave,halton_init)
!   end if
  
!   call real_write2file(mueller_mat,mueller_out)

!end if




end program main

