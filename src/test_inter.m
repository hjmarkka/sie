clear
path(path, '~/GITrepos/emiem')


L = 7;
M = L+1;
N = 2*(L+1);

L1 = 19;
M1 = L1+1;
N1 = 2*(L1+1);


I = zeros(M1*N1,M*N);

[P, w] = sample_points(M1,N1);
[P0, w0] = sample_points(M,N);



[th,W]=gaussint(M1,-1,1);
las = 1
for i1 = 1:size(w,2)
%for i1 = 1:N1

%phi = (i1-1)*2*pi/(N1);  
%for i2 = 1:size(th)
%      theta = acos(th(i2));		      
     
     pp = P(:,i1);

     r = norm(pp);
     theta = acos(pp(3)/r);
     phi = atan2(pp(2),pp(1));
     if(phi < 0 )
       phi = 2*pi+phi;
     end

     tr = interpol(L, theta, phi);  

%      I(las,:) = tr;
      I(i1,:) = tr;

x = sin(theta)*cos(phi);
y = sin(theta)*sin(phi);
z = cos(theta);

pl(:,las) = [x;y;z]; 

las = las +1; 
%end

end
f = I*P0';
figure(6), plot_points(f')
f2 = I'*f;
figure(7), plot_points(f2')

[th,W]=gaussint(M,-1,1);
las = 1;
for i1 = 1:N
for i2 = 1:M
 phi = (i1-1)*2*pi/(N);  
 theta = acos(th(i2));
x = sin(theta)*cos(phi);
y = sin(theta)*sin(phi);
z = cos(theta);

im(i1,i2) = y*x*z;

ff(las) = y*x*z;
las = las +1;
end
end


  
g = I*ff';
g2 = I'*g;

las = 1;
for i1 = 1:N1
for i2 = 1:M1
im2(i1,i2) = g(las);
las = las + 1;
end
end

las = 1;
for i1 = 1:N
for i2 = 1:M
im4(i1,i2) = g2(las);
las = las + 1;
end
end
figure(8), imagesc(im)
figure(9), imagesc(im2)
figure(10), imagesc(im4)
figure(11), imagesc(im4-im)



A = h5read('~/GITrepos/sie/mat.h5','/mueller');

gg = A*ff';
las = 1;
for i1 = 1:N1
for i2 = 1:M1
im3(i1,i2) = gg(las);
las = las + 1;
end
end
figure(110), imagesc(im3)
figure(111), imagesc(im3-im2)
