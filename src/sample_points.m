function [P,w] = sample_points(M,N)

phi = 0:2*pi/(N):(2*pi-pi/(N));
[theta,W]=gaussint(M,-1,1);

theta = acos(theta)

j1=1;
for i1=1:length(phi)

    for i2=1:length(theta);
    		P(1,j1) = cos(phi(i1))*sin(theta(i2));
    		P(2,j1) = sin(phi(i1))*sin(theta(i2));
    		P(3,j1) = cos(theta(i2));
    		w(j1) = W(i2)/(N)*2*pi;
         j1=j1+1;
    end
end
