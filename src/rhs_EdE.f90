subroutine rhs_EdE(matrices, mesh)
use common
use integration_points
implicit none

type (mesh_struct) :: mesh
type (data) :: matrices

integer :: test_T, i1, te, test, ti, tj
double precision :: T_coord(3,3), T_dN(3,3), T_nxdN(3,3), T_nvec(3), r(3)
double precision, allocatable :: P0_tri(:,:), Pt(:,:)
double precision, allocatable :: w0_tri(:),wt(:)
double complex :: alok(3)
double precision :: signt, el, eta0, T_area


eta0 = sqrt(mu/epsilon)
matrices%rhs(:) = dcmplx(0.0,0.0)

call inttri(P0_tri,w0_tri,5)

allocate(Pt(3,size(w0_tri)))
allocate(wt(size(w0_tri)))



do test_T = 1, mesh%N_tri

   T_coord = mesh%coord(:,mesh%etopol(:,test_T))
   T_area = tri_area(T_coord)

   call gradshape_tri(T_dN,T_nxdN, T_nvec, T_coord)

   call linmap_tri(Pt, wt, T_coord, P0_tri, W0_tri)
   
   alok(:) = dcmplx(0.0,0.0)
  
   do i1 = 1,size(w0_tri)
      r=Pt(:,i1)
      
      alok = alok + matrices%E0 * & 
           exp(dcmplx(0.0,mesh%k*dot_product(matrices%khat,r))) * wt(i1)

   end do

   matrices%rhs(3*(test_T-1)+1) = alok(1)/sqrt(T_area)
   matrices%rhs(3*(test_T-1)+2) = alok(2)/sqrt(T_area)
   matrices%rhs(3*(test_T-1)+3) = alok(3)/sqrt(T_area)

end do

end subroutine rhs_EdE
