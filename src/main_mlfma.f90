program main
use common
use geometry
use io
use possu
use solver_mlfma
use rhs_pmchwt
use field_pmchwt
use octtree
use sparse_matrix
use gmres
use interpolation
use mlfma_pmchwt

implicit none


type (mesh_struct) :: mesh
type (data) :: matrices
type (level_struct), dimension(:), allocatable :: otree

integer :: ierr, rc, my_id, N_procs, Eori(2,3), t1,t2, rate, acc, p

integer :: restart, maxit, mueller, ave, halton_init, i1, max_level, N_theta
real(dp) :: k, tol, khat(3), E0(3), cell_size, phi, theta, epsr, epsi, lambda, near_dist
complex(dp) :: eps_r
real(dp) :: p1(3), p2(3), maxlength, minlength, avelength, scale, crs(3)
CHARACTER(LEN=98) :: meshname, fname, mueller_out, J_out, arg_name, arg, rcs_out
integer :: num_args, i_arg, num_threads
real(dp), dimension(:,:), allocatable :: mueller_mat, mueller_mat_ave, r
complex(dp), dimension(:,:), allocatable :: EH 
integer :: form

   print *,'*************************************************************'
   print *,'**                                                         **'
   print *,'**               SIE-PMCHWT v. 0.1                         **'
   print *,'**                                                         **'
   print *,'*************************************************************'
   !$omp parallel default(shared) 
   !$ num_threads = omp_get_num_threads()
   !$omp end parallel
   
   print*, 'Number of threads =', num_threads

   
   ! Default arguments
   meshname = 'mesh.h5'
   mueller_out = 'mueller.h5'
   J_out = 'J.h5'
   k = 2*pi
   khat = [0.0,0.0,1.0] 
   E0 = [1,0,0]
   mueller = 1 
   phi = 0.0
   theta = 0.0
   ave = 0
   halton_init = 0
   tol = 1e-5 
   maxit = 50  
   restart = 4
   epsr = 1.6
   epsi = 0.1
   scale = 1.0d0
   p = 3
   acc = 2
   form = 1
   near_dist = 0.4
   N_theta = 361
   num_args = command_argument_count()
   do i_arg = 1,num_args,2
      call get_command_argument(i_arg,arg_name)
  
      select case(arg_name)
      
      case('-mesh')
         call get_command_argument(i_arg+1,arg)
         meshname = arg
         print*, 'mesh file is:', meshname
      case('-S_out')
         call get_command_argument(i_arg+1,arg)
         mueller_out = arg
         print*, 'Mueller matrix is written in the file:', mueller_out
      case('-J_out')
         call get_command_argument(i_arg+1,arg)
         J_out = arg
         print*, 'Solution is written in the file:', J_out
      case('-lambda')
         call get_command_argument(i_arg+1,arg)
         read(arg,*) k 
      case('-m_r')
         call get_command_argument(i_arg+1,arg)
         read(arg,*) epsr 
      case('-m_i')
         call get_command_argument(i_arg+1,arg)
         read(arg,*) epsi 
      case('-mueller')
         call get_command_argument(i_arg+1,arg)
         read(arg,*) mueller
      case('-phi')
         call get_command_argument(i_arg+1,arg)
         read(arg,*) phi
      case('-theta')
         call get_command_argument(i_arg+1,arg)
         read(arg,*) theta
      case('-ave')
         call get_command_argument(i_arg+1,arg)
         read(arg,*) ave        
      case('-halton_init')
         call get_command_argument(i_arg+1,arg)
         read(arg,*) halton_init
      case('-tol')
         call get_command_argument(i_arg+1,arg)
         read(arg,*) tol
        case('-scale_size')
         call get_command_argument(i_arg+1,arg)
         read(arg,*) scale 
      case('-maxit')
         call get_command_argument(i_arg+1,arg)
         read(arg,*) maxit
      case('-restart')
         call get_command_argument(i_arg+1,arg)
         read(arg,*) restart
      case('-formulation')
         call get_command_argument(i_arg+1,arg)
         read(arg,*) form
      case('-acc')
         call get_command_argument(i_arg+1,arg)
         read(arg,*) acc 
      case('-P')
         call get_command_argument(i_arg+1,arg)
         read(arg,*) p 
      case('-near_dist')
         call get_command_argument(i_arg+1,arg)
         read(arg,*) near_dist
      case('-N_theta')
         call get_command_argument(i_arg+1,arg)
         read(arg,*) N_theta 
         
      case('-help')
         print*, 'Command line parameters' 
         print*, '-mesh mesh.h5      "Read mesh from file"' 
         print*, '-S_out mueller.h5  "Output file: Mueller matrix"'
         print*, '-J_out J.h5        "Output file: Solution coefficients"'
         print*, '-k 1.0             "Wavenumber"'
         print*, '-mueller 1         "Compute mueller matrix (1) yes (0) no"'
         print*, '-phi 0.0           "Incident angel phi"'
         print*, '-theta 0.0         "Incident angel theta"'
         print*, '-ave 0             "Orientation averaging (number of orientations)"'
         print*, '-halton_init 0     "Orientation averaging (beginning of Halton sequence)"'
         print*, '-tol 1e-5          "GMRES tolerance"'
         print*, '-restart 4         "GMRES restart"'
         print*, '-maxit 50          "Maximum number of GMRES iterations"'
         print*, '-formulation 1     "used formulation 1 = PMCHWT, 0 = CFT"'
         print*, '-acc 2             "Number of significant digits in MLFMA"'
         print*, '-P 3               "Lagrange interpolation order in MLFMA"'
         print*, '-near_dist 0.4     "near zone distance [lambda]"'
        
         stop
      case default 
         print '(a,a,/)', 'Unrecognized command-line option: ', arg_name
         stop
      end select
   end do

   mesh%tol = tol
   mesh%restart = restart
   mesh%maxit = maxit
   matrices%khat = khat
   matrices%E0 = dcmplx(E0, 0.0)
   mesh%k = 2*pi/k
   mesh%eps_r = dcmplx(epsr, epsi)**2.0
   mesh%p = p
   mesh%acc = acc
   mesh%form = form
   mesh%near_dist = near_dist
   call read_mesh(mesh, meshname)
  ! call read_field_points(matrices)

   mesh%coord = mesh%coord * scale

Eori(:,1) = [3,2]
Eori(:,2) = [1,3]
Eori(:,3) = [2,1]

mesh%Eori = Eori

call system_clock(t1,rate)
call find_edges3(mesh)
call system_clock(t2,rate)
print*,'Edges done in', real(T2-T1)/real(rate), 'seconds'

!print*, mesh%edges
   print*, '   Number of edges      =', mesh%N_edge
   print*, '   lambda  = ', real(k)

   maxlength = 0.0
   avelength = 0.0

   p1 = mesh%coord(:,mesh%edges(1,1))
   p2 = mesh%coord(:,mesh%edges(2,1))
   minlength = norm(p1,p2)
    do i1=1,mesh%N_edge
       p1 = mesh%coord(:,mesh%edges(1,i1))
       p2 = mesh%coord(:,mesh%edges(2,i1))
       if (norm(p1,p2) > maxlength) then
          maxlength = norm(p1,p2)
       end if
       if (norm(p1,p2) < minlength) then
          minlength = norm(p1,p2)
       end if
       avelength = avelength + norm(p1,p2)

    end do

    avelength = avelength/dble(mesh%N_edge)
    print*, '   Ave elem. size', real((k)/avelength)
    print*, '   Max elem. size', real((k)/maxlength)
    print*, '   Min elem. size', real((k)/minlength)

print*, 'Create octtree...'
call create_octtree(mesh, otree, k, max_level)

do i1 = 1,size(otree)
   print*, 'level',i1-1 ,size(otree(i1)%tree), ', dl=',real(otree(i1)%tree(1)%dl), 'Nmax', otree(i1)%tree(1)%Nmax
end do

allocate(matrices%rhs(2*mesh%N_edge))
allocate(matrices%x(2*mesh%N_edge))
allocate(matrices%Ax(2*mesh%N_edge))

print*, 'Size of x', floor(2.0*mesh%N_edge*64.0*2.0 /8.0/1024.0/1024.0), 'Mb'

matrices%rhs(:) = dcmplx(0.0,0.0)
matrices%x(:) = dcmplx(0.0,0.0)
matrices%Ax(:) = dcmplx(0.0,0.0)


call rhs(matrices,mesh)
!call build_interpolation_matrix_mlfma(otree, mesh)
print*, 'Build interpolation matrices...'
call build_interpolation_matrix_mlfma2(otree, mesh)
print*, 'allocate memory...' 
call allocate_memory_mlfma(otree, mesh, matrices)


print*, 'Build the near zone matrix...'
!print*, size(otree)
call system_clock(t1,rate)
!call build_matrix_pmchwt_mpi(matrices,mesh,my_id,N_procs)
!call build_matrix_pmchwt_mlfma(matrices, mesh, otree)
call build_matrix_pmchwt_edge_mlfma(matrices, mesh, otree)
!call build_matrix_pmchwt(matrices, mesh)


!call rhs(matrices,mesh)
!call build_interpolation_matrix_mlfma2(otree, mesh)
!call allocate_memory_mlfma(otree, mesh, matrices)




call system_clock(t2,rate)
print*,'Near zone matrix done in', real(T2-T1)/real(rate), 'seconds'

print*, 'Compute translators'
call system_clock(t1,rate)
!call compute_translators(otree, mesh, matrices)
!call compute_translators_mlfma(otree, mesh, matrices)
call compute_translators_mlfma2(otree, mesh, matrices)
call system_clock(t2,rate)
print*, 'Translators done in',  real(T2-T1)/real(rate), 'seconds'

print*, 'Compute radiation patterns'
call system_clock(t1,rate)
!call radiation_pattern_edge(otree, mesh, matrices)
call radiation_pattern_mlfma(otree, mesh, matrices)
call system_clock(t2,rate)
print*, 'Radiation patterns done in',  real(T2-T1)/real(rate), 'seconds'

!matrices%x = matrices%rhs
!call matvec_mlfma_efie(otree, mesh, matrices)

!print*, matrices%Ax(1:10)

!matrices%x = matmul(matrices%A,matrices%rhs)
!print*, '*********'
!print*, matrices%x(1:10)
!call build_interpolation_matrix(otree, mesh)

!stop

!print*, 'matvec'
!call matvec_mlfma(otree, mesh, matrices)

!matrices%x(:) = matrices%rhs
!matrices%x(:) = dcmplx(0.0,0.0)
!matrices%x(1)=1.0
!call matvec_mlfma_pmchwt(otree, mesh, matrices)
!call matvec_fmm_edge(otree, mesh, matrices)
!print*, 'matvec done'

!matrices%x = matmul(matrices%A,matrices%rhs)

!print*, matrices%Ax(550:560)
!print*, '*******************'
!print*, abs(matrices%Ax - matrices%x)/abs(matrices%x)
!stop
!print*,'**********'
!print*, matrices%A(550:560,1)

!print*, matrices%A(2,200)
!stop
!print*, (real((matrices%A(:,280) - matrices%Ax(:))/matrices%A(:,280)))
!print*, (imag((matrices%A(:,280) - matrices%Ax(:))/matrices%A(:,280)))
!stop
!call build_matrix_efie(matrices,mesh)
!call build_matrix_mfie(matrices,mesh)

print*,'Done in', real(T2-T1)/real(rate), 'seconds'

!allocate(matrices%A(2*mesh%N_edge,2*mesh%N_edge))
!matrices%A(:,:) = 0.0_dp
!do i1 = 1, 2*mesh%N_edge
!   matrices%A(i1,i1) = 1.0_dp
!end do

print*, 'Solving...'
!call system_clock(t1,rate)
!matrices%A = Cinv(matrices%A)
!call system_clock(t2,rate)
!matrices%x = matmul(matrices%A,matrices%rhs)

!call gmres_mlfma(matrices, mesh, otree)
!call gmres_full(matrices, mesh)

if(mueller == 0) then

  
!call calc_fields(matrices, mesh)
!fname = "A.h5"
!call write2file(matrices%E_field,fname)       
call gmres_mlfma(matrices, mesh, otree)

call compute_rcs(matrices,mesh)
rcs_out = "rcs.h5"
call real_write2file(matrices%rcs,rcs_out)

fname = "x.h5"
call cmplx_vec_write2file(matrices%x,fname)

fname = "rhs.h5"
call cmplx_vec_write2file(matrices%rhs,fname)

!matrices%x = matrices%rhs
!call surface_fields(matrices, mesh, EH, r)

!fname = "EH.h5"
!call write2file(EH,fname)
!fname ="r.h5"
!call real_write2file(r,fname)

end if


if (mueller==1) then
   if(ave == 0) then
      mueller_mat = compute_mueller(matrices, mesh, otree, pi/180.0*phi, pi/180.0*theta, 360, pi/180.0 * 0.0)
   else
      call orientation_ave(mesh,matrices,otree, N_theta ,ave,halton_init, mueller_mat, crs)
   end if
  
   !call real_write2file(mueller_mat,mueller_out)
   call write2file_mueller(mueller_mat, crs, mueller_out)

end if




end program main

