module octtree
use common

!USE IFPORT ! intel compiler

implicit none


type octtree_struct
integer :: near_neighbours(27) 
integer, dimension(:), allocatable :: sources
real(dp) :: cp(3), dl
integer :: N_source, Nmax
integer :: level, parent, children(8)
complex(dp), dimension(:,:), allocatable :: rad_pattern_theta, rad_pattern_phi, rad_pattern2_theta, rad_pattern2_phi
complex(dp), dimension(:,:), allocatable :: rec_pattern_theta, rec_pattern_phi, rec_pattern2_theta, rec_pattern2_phi
complex(dp), dimension(:,:), allocatable :: rad_pattern_pole_x, rad_pattern_pole_y
complex(dp), dimension(:), allocatable :: in_pattern_theta, in_pattern_phi, in_Mpattern_theta, in_Mpattern_phi
complex(dp), dimension(:), allocatable :: in_pattern2_theta, in_pattern2_phi, in_Mpattern2_theta, in_Mpattern2_phi
complex(dp), dimension(:), allocatable :: out_pattern_theta, out_pattern_phi, out_Mpattern_theta, out_Mpattern_phi
complex(dp), dimension(:), allocatable :: out_pattern2_theta, out_pattern2_phi, out_Mpattern2_theta, out_Mpattern2_phi
complex(dp), dimension(:), allocatable :: out_pattern_pole_x, out_pattern_pole_y
complex(dp), dimension(:,:), allocatable :: translator, translator2
integer, dimension(:), allocatable :: translator_rec_ind
end type octtree_struct

type level_struct
   type (octtree_struct), dimension(:), allocatable :: tree
   real(dp), dimension(:,:), allocatable :: P
   real(dp), dimension(:), allocatable :: w
   integer :: L
   real(dp), dimension(:,:), allocatable :: Intermat
   integer, dimension(:,:), allocatable :: intermat_ind, intermat_th_ind, intermat_phi_ind
   real(dp), dimension(:), allocatable :: Intermat_sparse, Intermat_th_sparse, Intermat_phi_sparse
   complex(dp), dimension(:,:), allocatable :: translators, translators2
   real(dp), dimension(:), allocatable :: ao, ao_th
   integer, dimension(:), allocatable :: jao, jao_th
   integer, dimension(:), allocatable :: iao, iao_th
end type level_struct


contains

!*************************************************************


subroutine sources_in_cube(cubes, parent, mesh, cube_ind)
type (octtree_struct), dimension(:) :: cubes, parent
type (mesh_struct) :: mesh
integer :: cube_ind
integer :: i1, edge, parent_ind, las
real(dp) :: cp(3)
real(dp) :: xmax, xmin, ymax, ymin, zmax, zmin

parent_ind = cubes(cube_ind)%parent
las = 0

do i1 = 1, size(parent(parent_ind)%sources)

   edge = parent(parent_ind)%sources(i1)
  

   cp = (mesh%coord(:,mesh%edges(1,edge)) + mesh%coord(:,mesh%edges(2,edge)))/2.0_dp
   
   xmax = cubes(cube_ind)%cp(1) + cubes(cube_ind)%dl / 2.0_dp
   xmin = cubes(cube_ind)%cp(1) - cubes(cube_ind)%dl / 2.0_dp

   ymax = cubes(cube_ind)%cp(2) + cubes(cube_ind)%dl / 2.0_dp
   ymin = cubes(cube_ind)%cp(2) - cubes(cube_ind)%dl / 2.0_dp

   zmax = cubes(cube_ind)%cp(3) + cubes(cube_ind)%dl / 2.0_dp
   zmin = cubes(cube_ind)%cp(3) - cubes(cube_ind)%dl / 2.0_dp

   if(cp(1) <= xmax .and. cp(1) > xmin .and. &
        cp(2) <= ymax .and. cp(2) > ymin .and. &
        cp(3) <= zmax .and. cp(3) > zmin) then
      las = las + 1
   end if
end do

cubes(cube_ind)%N_source = las

if(las>0) then
   allocate(cubes(cube_ind)%sources(las))
else 
   allocate(cubes(cube_ind)%sources(1))   
   cubes(cube_ind)%sources(1) = 0
end if

las = 0

do i1 = 1, size(parent(parent_ind)%sources)
   edge = parent(parent_ind)%sources(i1)
   
   
   cp = (mesh%coord(:,mesh%edges(1,edge)) + mesh%coord(:,mesh%edges(2,edge)))/2.0_dp

   xmax = cubes(cube_ind)%cp(1) + cubes(cube_ind)%dl / 2.0_dp
   xmin = cubes(cube_ind)%cp(1) - cubes(cube_ind)%dl / 2.0_dp

   ymax = cubes(cube_ind)%cp(2) + cubes(cube_ind)%dl / 2.0_dp
   ymin = cubes(cube_ind)%cp(2) - cubes(cube_ind)%dl / 2.0_dp

   zmax = cubes(cube_ind)%cp(3) + cubes(cube_ind)%dl / 2.0_dp
   zmin = cubes(cube_ind)%cp(3) - cubes(cube_ind)%dl / 2.0_dp

   if(cp(1) <= xmax .and. cp(1) > xmin .and. &
        cp(2) <= ymax .and. cp(2) > ymin .and. &
        cp(3) <= zmax .and. cp(3) > zmin) then
      las = las + 1
      cubes(cube_ind)%sources(las) = edge
   end if
end do


end subroutine sources_in_cube



!*************************************************************
subroutine divide_cube(cubes, parent, mesh, cube_ind, parent_ind, k)
type (octtree_struct), dimension(:) :: cubes, parent
type (mesh_struct) :: mesh
real(dp) :: cp(3), dl
real(dp) :: cp2(3), dl2, ka, k
integer :: cube_ind, parent_ind, Nmax



cp = parent(parent_ind)%cp
dl = parent(parent_ind)%dl

dl2 = dl/2.0_dp

ka = dl2 * sqrt(3.0_dp) / 2.0_dp * k


Nmax = 1!truncation_order(ka)

cp2 = cp + [-dl2/2.0_dp, -dl2/2.0_dp, -dl2/2.0_dp]
cubes(cube_ind+1)%cp = cp2
cubes(cube_ind+1)%dl = dl2
cubes(cube_ind+1)%parent = parent_ind
cubes(cube_ind+1)%Nmax = Nmax
call sources_in_cube(cubes, parent, mesh, cube_ind+1)

cp2 = cp + [dl2/2.0_dp, -dl2/2.0_dp, -dl2/2.0_dp]
cubes(cube_ind+2)%cp = cp2
cubes(cube_ind+2)%dl = dl2
cubes(cube_ind+2)%parent = parent_ind
cubes(cube_ind+2)%Nmax = Nmax 
call sources_in_cube(cubes, parent, mesh, cube_ind+2)

cp2 = cp + [-dl2/2.0_dp, dl2/2.0_dp, -dl2/2.0_dp]
cubes(cube_ind+3)%cp = cp2
cubes(cube_ind+3)%dl = dl2
cubes(cube_ind+3)%parent = parent_ind
cubes(cube_ind+3)%Nmax = Nmax 
call sources_in_cube(cubes, parent, mesh, cube_ind+3)

cp2 = cp + [dl2/2.0_dp, dl2/2.0_dp, -dl2/2.0_dp]
cubes(cube_ind+4)%cp = cp2
cubes(cube_ind+4)%dl = dl2
cubes(cube_ind+4)%parent = parent_ind
cubes(cube_ind+4)%Nmax = Nmax 
call sources_in_cube(cubes, parent, mesh, cube_ind+4)

cp2 = cp + [-dl2/2.0_dp, -dl2/2.0_dp, dl2/2.0_dp]
cubes(cube_ind+5)%cp = cp2
cubes(cube_ind+5)%dl = dl2
cubes(cube_ind+5)%parent = parent_ind
cubes(cube_ind+5)%Nmax = Nmax 
call sources_in_cube(cubes, parent, mesh, cube_ind+5)

cp2 = cp + [dl2/2.0_dp, -dl2/2.0_dp, dl2/2.0_dp]
cubes(cube_ind+6)%cp = cp2
cubes(cube_ind+6)%dl = dl2
cubes(cube_ind+6)%parent = parent_ind
cubes(cube_ind+6)%Nmax = Nmax 
call sources_in_cube(cubes, parent, mesh, cube_ind+6)

cp2 = cp + [-dl2/2.0_dp, dl2/2.0_dp, dl2/2.0_dp]
cubes(cube_ind+7)%cp = cp2
cubes(cube_ind+7)%dl = dl2
cubes(cube_ind+7)%parent = parent_ind
cubes(cube_ind+7)%Nmax = Nmax 
call sources_in_cube(cubes, parent, mesh, cube_ind+7)

cp2 = cp + [dl2/2.0_dp, dl2/2.0_dp, dl2/2.0_dp]
cubes(cube_ind+8)%cp = cp2
cubes(cube_ind+8)%dl = dl2
cubes(cube_ind+8)%parent = parent_ind
cubes(cube_ind+8)%Nmax = Nmax 
call sources_in_cube(cubes, parent, mesh, cube_ind+8)

end subroutine divide_cube

!****************************************************************

subroutine create_octtree(mesh, otree, k, max_level)
type (mesh_struct) :: mesh
type (level_struct), dimension(:), allocatable :: otree
type (octtree_struct), dimension(:), allocatable :: tree
real(dp) :: k

integer, allocatable, dimension(:) :: las_vec
integer ::  sph, max_level, level, i1, i2
integer :: parent_ind, cube_ind, las, Nmax
real(dp) :: rmax, cp(3), r, dl, cp1(3), cp2(3), a_max,a, ka
real(dp) :: xyz_max(3), xyz_min(3), min_dl

rmax = 0.0_dp
a_max = 0.0_dp

xyz_max(:) = 0.0_dp
xyz_min(:) = 0.0_dp

do sph = 1, mesh%N_edge

   cp = (mesh%coord(:,mesh%edges(1,sph)) + mesh%coord(:,mesh%edges(2,sph)))/2.0_dp
   
   a = vlen(mesh%coord(:,mesh%edges(1,sph)) - mesh%coord(:,mesh%edges(2,sph)))
   
   if(a > a_max) then
      a_max = a
   end if

end do

xyz_max(1) = maxval(mesh%coord(1,:))
xyz_max(2) = maxval(mesh%coord(2,:))
xyz_max(3) = maxval(mesh%coord(3,:))

xyz_min(1) = minval(mesh%coord(1,:))
xyz_min(2) = minval(mesh%coord(2,:))
xyz_min(3) = minval(mesh%coord(3,:))


dl = maxval(xyz_max - xyz_min)*1.01_dp
cp = (xyz_min + (xyz_max - xyz_min)/ 2.0_dp)
min_dl = dl
max_level = 0
do while (min_dl > mesh%near_dist * 2_dp*pi/mesh%k) ! defines min level
   min_dl = min_dl/2.0_dp  
   max_level = max_level + 1
   
end do
print*, 'origin:', real(cp)

!********************************

if(max_level < 2) then
   max_level = 2
end if



allocate(otree(max_level+1))


!********** level 0 **********************
allocate(otree(1)%tree(1))
otree(1)%tree(1)%cp = cp
otree(1)%tree(1)%dl = dl
otree(1)%tree(1)%N_source = mesh%N_edge

ka = dl * sqrt(3.0_dp) / 2.0_dp * k

Nmax = 1!truncation_order(ka)   

otree(1)%tree(1)%Nmax =  Nmax

allocate(otree(1)%tree(1)%sources(mesh%N_edge))

do sph = 1, mesh%N_edge
   otree(1)%tree(1)%sources(sph) = sph
end do

!*****************************************

do level = 1, max_level
   
   !allocate(tree(8**level))
   allocate(tree(8*size(otree(level)%tree)))
   
   cube_ind = 0

   do i1 = 1, size(tree)
      tree(i1)%N_source = 0 
   end do

   do parent_ind = 1, size(otree(level)%tree)
      otree(level)%tree(parent_ind)%children(:) = 0
      call divide_cube(tree, otree(level)%tree, mesh, cube_ind, parent_ind,k)
      cube_ind = cube_ind + 8
   end do

  
   ! remove empty boxes
   las = 0
   do i1 = 1, size(tree)
      if(tree(i1)%N_source > 0) then
          las = las + 1
      end if
   end do

   allocate(otree(level+1)%tree(las))

   las = 0
   do i1 = 1, size(tree)
      if(tree(i1)%N_source > 0) then
          las = las + 1
          otree(level+1)%tree(las) = tree(i1)
        
      end if
   end do

   deallocate(tree)

   !***** find near neighbours ********************
   do i1 = 1, size(otree(level+1)%tree)
      cp1 = otree(level+1)%tree(i1)%cp
      otree(level+1)%tree(i1)%near_neighbours(:) = 0
      las = 0

      do i2 = 1, size(otree(level+1)%tree)
         cp2 = otree(level+1)%tree(i2)%cp

          if(sqrt(dot_product(cp1-cp2,cp1-cp2)) < 1.9_dp*otree(level+1)%tree(i1)%dl) then
             las = las + 1      
             otree(level+1)%tree(i1)%near_neighbours(las) = i2
          end if

      end do

   end do
   !*******************************************
  
end do

 !***** Children cubes **********************!
do level = 1,max_level 

   allocate(las_vec(size(otree(level)%tree)))
   las_vec(:) = 0
   do i1 = 1, size(otree(level+1)%tree)

      parent_ind = otree(level+1)%tree(i1)%parent ! level
   
      las_vec(parent_ind) = las_vec(parent_ind) + 1 
   
      otree(level)%tree(parent_ind)%children(las_vec(parent_ind)) = i1

   end do
   deallocate(las_vec)
end do

!**********************************

end subroutine create_octtree

!**************************************************************


end module octtree
