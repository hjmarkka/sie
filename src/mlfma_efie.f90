module mlfma_efie

  use octtree
  use interpolation
  use translation
  implicit none

contains


  
!*****************************************************************************
subroutine radiation_pattern_mlfma(otree, mesh, matrices)
type (level_struct), dimension(:) :: otree
type (mesh_struct) :: mesh
type (data) :: matrices
integer :: level

integer :: cube, tri, L, acc, M, N, ele, i1, box, loc, i2, be, bi, bj, basis, bt
double precision :: rpc(3), rp(3), B_coord(3,3), khat(3), el, signb, khat_pole(3)
double precision, dimension(:,:), allocatable :: P, P0_tri
double precision, dimension(:), allocatable :: w, w0_tri
double complex, dimension(:), allocatable :: f
double precision :: B_dN(3,3), B_nxdN(3,3), B_nvec(3), alok(3), dyad(3,3), eye(3,3), vec(3), vec2(3,3)
double complex, dimension(3) :: intN, intN_pole0, intN_pole180 
double precision, dimension(:,:), allocatable :: Pt, shape_tri
double precision, dimension(:), allocatable :: wt
double complex :: a(3), a0(3), a180(3)
integer :: basis_tri(2)

khat_pole = [0.0_dp,0.0_dp,1.0_dp]

level = size(otree)

call inttri(P0_tri,w0_tri,5)
P = otree(level)%P
w = otree(level)%w

allocate(shape_tri(3,size(w0_tri)))
allocate(Pt(3,size(w0_tri)))
allocate(wt(size(w0_tri)))

shape_tri(1,:) = 1.0d0 - P0_tri(1,:) - P0_tri(2,:)
shape_tri(2,:) = P0_tri(1,:)
shape_tri(3,:) = P0_tri(2,:)

eye(:,:) = 0.0d0
eye(1,1) = 1.0d0
eye(2,2) = 1.0d0
eye(3,3) = 1.0d0

do box = 1, size(otree(level)%tree)

   allocate(otree(level)%tree(box)%rad_pattern_theta(otree(level)%tree(box)%N_source,size(w)))
   allocate(otree(level)%tree(box)%rad_pattern_phi(otree(level)%tree(box)%N_source,size(w)))

   allocate(otree(level)%tree(box)%rad_pattern_pole_x(otree(level)%tree(box)%N_source,2))
   allocate(otree(level)%tree(box)%rad_pattern_pole_y(otree(level)%tree(box)%N_source,2))
   
   otree(level)%tree(box)%rad_pattern_theta(:,:) = dcmplx(0.0,0.0)
   otree(level)%tree(box)%rad_pattern_phi(:,:) = dcmplx(0.0,0.0)
   
   otree(level)%tree(box)%rad_pattern_pole_x(:,:) = dcmplx(0.0,0.0)
   otree(level)%tree(box)%rad_pattern_pole_y(:,:) = dcmplx(0.0,0.0)
   
   rpc = otree(level)%tree(box)%cp
   do loc = 1, otree(level)%tree(box)%N_source
      
      basis = otree(level)%tree(box)%sources(loc)
      basis_tri = mesh%edgesT(:,basis)
      

      do bt = 1, 2

         B_coord = mesh%coord(:,mesh%etopol(:,basis_tri(bt)))
         
         call gradshape_tri(B_dN,B_nxdN, B_nvec, B_coord)        
         call linmap_tri(Pt, wt, B_coord, P0_tri, w0_tri)

         do be = 1, 3                                  
            if(basis == mesh%etopol_edges(be,basis_tri(bt))) exit
         end do

         el = norm(mesh%coord(:,mesh%edges(1,basis)), mesh%coord(:,mesh%edges(2,basis)))

         bi = mesh%Eori(1,be)
         bj = mesh%Eori(2,be)
          
         if(mesh%boundary(basis) == 0) then
            signb = RWG_sign(mesh, basis, basis_tri(bt),be)
           
            do i1 = 1, size(w)
               khat = P(:,i1)
               
               intN(:) = dcmplx(0.0,0.0)
               intN_pole0(:) = dcmplx(0.0,0.0)
               intN_pole180(:) = dcmplx(0.0,0.0)
               
               do i2 = 1, size(w0_tri)
           
                  rp = Pt(:,i2)         
                  intN = intN + shape_tri(:,i2) * exp(dcmplx(0.0,-1.0) * mesh%k &
                       * dot_product(khat, rp-rpc)) * wt(i2)
                  
                  intN_pole0 = intN_pole0 + shape_tri(:,i2) * exp(dcmplx(0.0,-1.0) * mesh%k &
                       * dot_product(khat_pole, rp-rpc)) * wt(i2)
                  intN_pole180 = intN_pole180 + shape_tri(:,i2) * exp(dcmplx(0.0,-1.0) * mesh%k &
                       * dot_product(-khat_pole, rp-rpc)) * wt(i2)
                  
               end do
              
               dyad = eye - kkdyad(khat)

           
               a =  matmul(dyad, signb * el * (B_nxdN(:,bj)*intN(bi) - B_nxdN(:,bi)*intN(bj)))

   
               vec =  cart2sph(khat)
               vec2 = sph_unit_vectors(vec(2),vec(3))
               
               otree(level)%tree(box)%rad_pattern_theta(loc,i1) = otree(level)%tree(box)%rad_pattern_theta(loc,i1) + &
                    dot_product(vec2(:,2), a)
               otree(level)%tree(box)%rad_pattern_phi(loc,i1) = otree(level)%tree(box)%rad_pattern_phi(loc,i1) + &
                    dot_product(vec2(:,3), a)
        
            end do

            ! ******** poles ************
            a0 =  matmul(dyad, signb * el * (B_nxdN(:,bj)*intN_pole0(bi) - B_nxdN(:,bi)*intN_pole0(bj)))
            a180 =  matmul(dyad, signb * el * (B_nxdN(:,bj)*intN_pole180(bi) - B_nxdN(:,bi)*intN_pole180(bj)))
            
            otree(level)%tree(box)%rad_pattern_pole_x(loc,:) = otree(level)%tree(box)%rad_pattern_pole_x(loc,:) &
                 + [a0(1), a180(1)]
            otree(level)%tree(box)%rad_pattern_pole_y(loc,:) = otree(level)%tree(box)%rad_pattern_pole_y(loc,:) &
                 + [a0(2), a180(2)]
            !****************************
         end if
      end do
   end do
end do
!print*, otree(level)%tree(49)%rad_pattern_theta

print*, 'radiation pattern done'
end subroutine radiation_pattern_mlfma
  
subroutine aggregate(otree, mesh, matrices)
type (level_struct), dimension(:) :: otree
type (mesh_struct) :: mesh
type (data) :: matrices

integer :: i1, tind, bind, level, acc, L, M, N, ele, box, loc, be, basis, Ns, parent, test
integer :: i2, box2, near_box, i3, neighbour, las, N0
double precision, dimension(:,:), allocatable :: P
double precision, dimension(:), allocatable :: w
complex(8), dimension(:), allocatable :: rad_theta, rad_phi
double precision :: cp_box(3), cp_box2(3), box_dl, eta1, signb, r(3), phi, khat_pole(3)
complex(8), dimension(:), allocatable :: Fth, Fphi, Gth_poles, Gphi_poles
complex(8) :: no, rad_x(2), rad_y(2)
CHARACTER(LEN=28) :: fname, fname2

khat_pole = [0.0_dp,0.0_dp,1.0_dp]

level = size(otree)
box_dl =  otree(level)%tree(1)%dl

P = otree(level)%P 
w = otree(level)%w 
L = otree(level)%L

allocate(rad_theta(size(w)))
allocate(rad_phi(size(w)))


do level = size(otree), 3, -1
   do box2 = 1, size(otree(level)%tree)
      otree(level)%tree(box2)%out_pattern_theta(:) = dcmplx(0.0,0.0)
      otree(level)%tree(box2)%out_pattern_phi(:) = dcmplx(0.0,0.0)
      
      otree(level)%tree(box2)%in_pattern_theta(:) = dcmplx(0.0,0.0)
      otree(level)%tree(box2)%in_pattern_phi(:) = dcmplx(0.0,0.0)

      otree(level)%tree(box2)%out_pattern_pole_x(:) = dcmplx(0.0,0.0)
      otree(level)%tree(box2)%out_pattern_pole_y(:) = dcmplx(0.0,0.0)

!      otree(level)%tree(box2)%in_pattern_pole_x(:) = dcmplx(0.0,0.0)
!      otree(level)%tree(box2)%in_pattern_pole_y(:) = dcmplx(0.0,0.0)

      
   end do
end do


! Finest level
level = size(otree)
do box = 1, size(otree(level)%tree)
  
   Ns = otree(level)%tree(box)%N_source
   cp_box =  otree(level)%tree(box)%cp ! from
   box_dl =  otree(level)%tree(box)%dl
   
   rad_theta(:) = dcmplx(0.0,0.0)
   rad_phi(:) = dcmplx(0.0,0.0)

   rad_x(:) = dcmplx(0.0,0.0)
   rad_y(:) = dcmplx(0.0,0.0)
 
   !print*, size(otree(level)%tree(box)%rad_pattern_theta)
   do loc = 1, Ns
      
      basis = otree(level)%tree(box)%sources(loc)
   
      rad_theta = rad_theta + otree(level)%tree(box)%rad_pattern_theta(loc,:) * matrices%x(basis)
      rad_phi = rad_phi + otree(level)%tree(box)%rad_pattern_phi(loc,:) * matrices%x(basis)

      ! Poles
      rad_x = rad_x + otree(level)%tree(box)%rad_pattern_pole_x(loc,:) * matrices%x(basis)
      rad_y = rad_y + otree(level)%tree(box)%rad_pattern_pole_y(loc,:) * matrices%x(basis)
      
   end do

    otree(level)%tree(box)%out_pattern_theta = rad_theta
    otree(level)%tree(box)%out_pattern_phi = rad_phi

    otree(level)%tree(box)%out_pattern_pole_x = rad_x
    otree(level)%tree(box)%out_pattern_pole_y = rad_y
end do


do level = size(otree), 4, -1
   !print*, level

   allocate(Fth(size(otree(level-1)%w)))
   allocate(Fphi(size(otree(level-1)%w)))

   N0 = 2*(otree(level)%L + 1)
   allocate(Gth_poles(size(otree(level)%Intermat,2)))
   allocate(Gphi_poles(size(otree(level)%Intermat,2)))
   
   do box = 1, size(otree(level)%tree)
      cp_box =  otree(level)%tree(box)%cp ! from box

      box2 = otree(level)%tree(box)%parent       
      cp_box2 =  otree(level-1)%tree(box2)%cp ! parent box



      
      !Interpolation to level-1
!      Fth = matmul(otree(level)%Intermat, otree(level)%tree(box)%out_pattern_theta) 
!      Fphi = matmul(otree(level)%Intermat, otree(level)%tree(box)%out_pattern_phi)

!      fname = 'rad1.h5'
!      fname2 = 'rad2.h5'
!      call cmplx_vec_write2file(otree(level)%tree(box)%out_pattern_theta,fname)
!      call cmplx_vec_write2file(Fth,fname2)
!      stop
      !********************************************************************************
      ! Interpolation wiht poles
      do i1 = 1, N0
         phi = dble(i1-1) * 2*pi/dble(N0) + pi/dble(N0)
         !North pole
         Gth_poles(i1) = cos(phi)*otree(level)%tree(box)%out_pattern_pole_x(1) &
              + sin(phi) * otree(level)%tree(box)%out_pattern_pole_y(1)
         Gphi_poles(i1) = -sin(phi)*otree(level)%tree(box)%out_pattern_pole_x(1) &
              + cos(phi) * otree(level)%tree(box)%out_pattern_pole_y(1)
         !South pole
         Gth_poles(N0 + size(otree(level)%w) + i1) = -cos(phi)*otree(level)%tree(box)%out_pattern_pole_x(2) &
              - sin(phi) * otree(level)%tree(box)%out_pattern_pole_y(2)
         Gphi_poles(N0 + size(otree(level)%w) + i1) = -sin(phi)*otree(level)%tree(box)%out_pattern_pole_x(2) &
              + cos(phi) * otree(level)%tree(box)%out_pattern_pole_y(2)
         
      end do

      Gth_poles(N0+1: N0 + size(otree(level)%w)) = otree(level)%tree(box)%out_pattern_theta
      Gphi_poles(N0+1: N0 + size(otree(level)%w)) = otree(level)%tree(box)%out_pattern_phi
      
      !Interpolation to level-1
      Fth = matmul(otree(level)%Intermat, Gth_poles) 
      Fphi = matmul(otree(level)%Intermat, Gphi_poles)

     ! fname = 'rad1.h5'
     ! fname2 = 'rad2.h5'
     ! call cmplx_vec_write2file(Gth_poles,fname)
     ! call cmplx_vec_write2file(Fth,fname2)
     ! stop
      
      !**************************************************************************

      !Fth = otree(level)%tree(box)%out_pattern_theta 
      !Fphi = otree(level)%tree(box)%out_pattern_phi


      
      r = cp_box2-cp_box

      !Shift to the center of the parent box
      !print*, size(otree(level-1)%w), size(otree(level-1)%tree(box2)%out_pattern_theta), size(Fth)
      do i1 = 1, size(otree(level-1)%w)
         otree(level-1)%tree(box2)%out_pattern_theta(i1) =  otree(level-1)%tree(box2)%out_pattern_theta(i1) + &
              exp(dcmplx(0.0,1.0)*mesh%k*dot_product(otree(level-1)%P(:,i1),r)) * Fth(i1)
         otree(level-1)%tree(box2)%out_pattern_phi(i1) =  otree(level-1)%tree(box2)%out_pattern_phi(i1) + &
              exp(dcmplx(0.0,1.0)*mesh%k*dot_product(otree(level-1)%P(:,i1),r)) * Fphi(i1) 
         
      end do

      ! Shift poles
      !NOrth pole
      otree(level-1)%tree(box2)%out_pattern_pole_x(1) =  otree(level-1)%tree(box2)%out_pattern_pole_x(1) + &
           exp(dcmplx(0.0,1.0)*mesh%k*dot_product(khat_pole,r)) * otree(level)%tree(box)%out_pattern_pole_x(1)
      otree(level-1)%tree(box2)%out_pattern_pole_y(1) =  otree(level-1)%tree(box2)%out_pattern_pole_y(1) + &
           exp(dcmplx(0.0,1.0)*mesh%k*dot_product(khat_pole,r)) * otree(level)%tree(box)%out_pattern_pole_y(1)
     ! !South pole!
      otree(level-1)%tree(box2)%out_pattern_pole_x(2) =  otree(level-1)%tree(box2)%out_pattern_pole_x(2) + &
           exp(dcmplx(0.0,1.0)*mesh%k*dot_product(-khat_pole,r)) * otree(level)%tree(box)%out_pattern_pole_x(2)
      otree(level-1)%tree(box2)%out_pattern_pole_y(2) =  otree(level-1)%tree(box2)%out_pattern_pole_y(2) + &
           exp(dcmplx(0.0,1.0)*mesh%k*dot_product(-khat_pole,r)) * otree(level)%tree(box)%out_pattern_pole_y(2)
      
   end do
   deallocate(Fth, Fphi)
   deallocate(Gth_poles, Gphi_poles)
end do


end subroutine aggregate


!********************************************************************************

subroutine compute_translators_mlfma(otree, mesh, matrices)
type (level_struct), dimension(:) :: otree
type (mesh_struct) :: mesh
type (data) :: matrices

integer :: i1, level, acc, L, M, N, box, las, i2, i3
integer ::  box2, near_box, parent, neighbour

double precision, dimension(:,:), allocatable :: P
double precision, dimension(:), allocatable :: w
complex(8), dimension(:), allocatable :: T, T_pole
double precision :: cp_box(3), cp_box2(3), box_dl


do level = size(otree), 3, -1
   box_dl =  otree(level)%tree(1)%dl

   P = otree(level)%P 
   w = otree(level)%w 
   L = otree(level)%L

   allocate(T(size(w)))
   !allocate(T_pole(2))

   do box = 1, size(otree(level)%tree)

      allocate(otree(level)%tree(box)%translator(size(w),189))
      allocate(otree(level)%tree(box)%translator_rec_ind(189))

  !    allocate(otree(level)%tree(box)%translator_pole(2,189))
      
      allocate(otree(level)%tree(box)%out_pattern_theta(size(w)))
      allocate(otree(level)%tree(box)%out_pattern_phi(size(w)))

      allocate(otree(level)%tree(box)%out_pattern_pole_x(2))
      allocate(otree(level)%tree(box)%out_pattern_pole_y(2))
      
      allocate(otree(level)%tree(box)%in_pattern_theta(size(w)))
      allocate(otree(level)%tree(box)%in_pattern_phi(size(w)))

!      allocate(otree(level)%tree(box)%in_pattern_pole_x(2))
!      allocate(otree(level)%tree(box)%in_pattern_pole_y(2))
   
      cp_box =  otree(level)%tree(box)%cp ! from
      box_dl =  otree(level)%tree(box)%dl
      parent = otree(level)%tree(box)%parent

      las = 1
      do i1 = 1, 27 ! near neighbours of parent cube
         neighbour = otree(level-1)%tree(parent)%near_neighbours(i1)
     
         if(neighbour == 0) exit
      
         do i2 = 1, 8 ! children of near neighbours
            box2 = otree(level-1)%tree(neighbour)%children(i2) ! level
            if(box2 == 0) exit

            ! ***check if near box ****
            near_box = 0
            do i3 = 1,27
               if(box2 == otree(level)%tree(box)%near_neighbours(i3)) then
                  near_box = 1
               end if
            end do

            if(near_box == 0) then
   
               cp_box2 =  otree(level)%tree(box2)%cp !to
               T = translator(cp_box2, cp_box, mesh%k, box_dl, P, w, L)
!               T_pole = translator_pole(cp_box2, cp_box, mesh%k, box_dl, P, w, L)
                         
               otree(level)%tree(box)%translator(:,las) = T !* otree(level)%w
               otree(level)%tree(box)%translator_rec_ind(las) = box2

!               otree(level)%tree(box)%translator_pole(:,las) = T_pole !* otree(level)%w
               
               las = las + 1
             
            end if
         end do
      end do
   end do

   
   deallocate(T, w, P)
   !****************************************************
end do
   
end subroutine compute_translators_mlfma


  !******************** disaggregation *************************************

subroutine disaggregation(otree, mesh, matrices)
type (level_struct), dimension(:) :: otree
type (mesh_struct) :: mesh
type (data) :: matrices

complex(8) :: no
real(8) :: eta1, cp_box(3), cp_box2(3), box_dl, box2_dl, r(3)
complex(8), dimension(mesh%N_edge) :: Ax
integer :: i1, tind, bind, box, box2, Ns, loc, test, i2, level, N0
complex(8), dimension(:), allocatable :: Fth, Fphi, Gth, Gphi, an_th, an_phi, Fth2, Fphi2

eta1 = sqrt(mu/epsilon)
!************ sparse matvec multiplication *****************************

Ax(:) = dcmplx(0.0,0.0)
do i1 = 1, size(matrices%sparse_M)
   tind = matrices%ind_M(i1,1)
   bind = matrices%ind_M(i1,2)

   Ax(tind) =  Ax(tind) + matrices%sparse_M(i1) * matrices%x(bind)
end do


!*********************** Downward pass **************************

do level = 3, size(otree)-1
   !print*, 'dis', level, size(otree(level)%w)

   !allocate(Fth2(size(otree(level+1)%w)), Fphi2(size(otree(level+1)%w)))
   allocate(Fth(size(otree(level)%w)), Fphi(size(otree(level)%w)))
   !allocate(an_th(size(otree(level+1)%w)), an_phi(size(otree(level+1)%w)))
   N0 = 2*(otree(level+1)%L + 1)
  ! allocate(Gth(size(otree(level+1)%Intermat,2)), Gphi(size(otree(level+1)%Intermat,2)))

   
   
   do box = 1, size(otree(level)%tree)
      cp_box = otree(level)%tree(box)%cp ! from

      do i1 = 1, 8
         box2 = otree(level)%tree(box)%children(i1) ! level
         if(box2 == 0) exit
         cp_box2 = otree(level+1)%tree(box2)%cp ! to

         r = cp_box2-cp_box


     !    an_th=matmul((transpose(otree(level+1)%Intermat)),otree(level)%tree(box)%in_pattern_theta)  
     !    an_phi=matmul((transpose(otree(level+1)%Intermat)),otree(level)%tree(box)%in_pattern_phi)

         
         !Shift to the center of a child box
      !   do i2 = 1, size(otree(level+1)%w)
      !      Fth2(i2) = exp(dcmplx(0.0,1.0)*mesh%k*dot_product(otree(level+1)%P(:,i2),r)) * an_th(i2) 
      !      Fphi2(i2) = exp(dcmplx(0.0,1.0)*mesh%k*dot_product(otree(level+1)%P(:,i2),r)) * an_phi(i2)
      !          
      !   end do

      !   otree(level+1)%tree(box2)%in_pattern_theta = otree(level+1)%tree(box2)%in_pattern_theta + Fphi2
      !   otree(level+1)%tree(box2)%in_pattern_phi = otree(level+1)%tree(box2)%in_pattern_phi + Fphi2

         
         
         !Shift to the center of a child box
         do i2 = 1, size(otree(level)%w)
            Fth(i2) = exp(dcmplx(0.0,1.0)*mesh%k*dot_product(otree(level)%P(:,i2),r)) * &
                 otree(level)%tree(box)%in_pattern_theta(i2) 
            Fphi(i2) = exp(dcmplx(0.0,1.0)*mesh%k*dot_product(otree(level)%P(:,i2),r)) * &
                 otree(level)%tree(box)%in_pattern_phi(i2) 
         end do

         !anterpolate
     !    otree(level+1)%tree(box2)%in_pattern_theta = otree(level+1)%tree(box2)%in_pattern_theta + &
     !         matmul(transpose(otree(level+1)%Intermat),Fth) 
     !    otree(level+1)%tree(box2)%in_pattern_phi = otree(level+1)%tree(box2)%in_pattern_phi + &
     !          matmul(transpose(otree(level+1)%Intermat),Fphi) 

         
         !anterpolate
         otree(level+1)%tree(box2)%in_pattern_theta = otree(level+1)%tree(box2)%in_pattern_theta + &
              matmul((transpose(otree(level+1)%Intermat(:,N0+1:N0 + size(otree(level+1)%w)))),Fth) 
         otree(level+1)%tree(box2)%in_pattern_phi = otree(level+1)%tree(box2)%in_pattern_phi + &
               matmul((transpose(otree(level+1)%Intermat(:,N0+1:N0 + size(otree(level+1)%w)))),Fphi) 

         !print*, size(otree(level)%w)
         !*******************************


         
         ! poles

 !        Gth = matmul((transpose(otree(level+1)%Intermat)),Fth)
 !        Gphi = matmul((transpose(otree(level+1)%Intermat)),Fphi)
       
 !        otree(level+1)%tree(box2)%in_pattern_theta =  otree(level+1)%tree(box2)%in_pattern_theta &
 !             + Gth(N0+1:N0+size(otree(level+1)%w))
 !        otree(level+1)%tree(box2)%in_pattern_phi =  otree(level+1)%tree(box2)%in_pattern_phi &
 !             + Gphi(N0+1:N0+size(otree(level+1)%w))
          
         !********************
      end do
   end do
   deallocate(Fth, Fphi)
  ! deallocate(an_th,an_phi)
  ! deallocate(Fth2,Fphi2)
 !  deallocate(Gth, Gphi)
end do


!**************** Finest level *********************************
level = size(otree)
no = mesh%k**2 /(dcmplx(0.0,1.0) * mesh%k) * eta1! (4.0*pi))**2 !/ eta1

do box = 1, size(otree(level)%tree)
   Ns = otree(level)%tree(box)%N_source
   cp_box =  otree(level)%tree(box)%cp ! from
   box_dl =  otree(level)%tree(box)%dl
    
   do loc = 1, Ns
      
      test = otree(level)%tree(box)%sources(loc)
          
      do i1 = 1, size(otree(level)%w)
         
         Ax(test) =  Ax(test) + conjg(otree(level)%tree(box)%rad_pattern_theta(loc,i1)) &
              * otree(level)%tree(box)%in_pattern_theta(i1) * no !* otree(level)%w(i1)
         
         Ax(test) =  Ax(test) + conjg(otree(level)%tree(box)%rad_pattern_phi(loc,i1)) &
              * otree(level)%tree(box)%in_pattern_phi(i1) * no !* otree(level)%w(i1)
         
      end do
      
   end do
end do

matrices%Ax = Ax

end subroutine disaggregation



subroutine matvec_mlfma_efie(otree, mesh, matrices)
type (level_struct), dimension(:) :: otree
type (mesh_struct) :: mesh
type (data) :: matrices

integer :: i1, tind, bind, level, acc, L, M, N, ele, box, loc, be, basis, Ns, parent, test
integer :: i2, box2, near_box, i3, neighbour, las
complex(8), dimension(mesh%N_edge) :: Ax
double precision, dimension(:,:), allocatable :: P
double precision, dimension(:), allocatable :: w
complex(8), dimension(:), allocatable :: rad_theta, rad_phi
double precision :: cp_box(3), cp_box2(3), box_dl, eta1, signb
complex(8), dimension(:), allocatable :: T, F_th, F_phi
complex(8), dimension(2) :: F_x, F_y
complex(8) :: no

call aggregate(otree, mesh, matrices)
print*, 'Aggregation done'
!*********************** Translate *************************************
!print*, 'translate'
do level = 3, size(otree)
   print*, 'level',level, 'size w', size(otree(level)%w)

   allocate(F_th(size(otree(level)%w)))
   allocate(F_phi(size(otree(level)%w)))

   do box = 1, size(otree(level)%tree)
      parent = otree(level)%tree(box)%parent
      las = 1
      do i1 = 1, 27 ! near neighbours of parent cube
         neighbour = otree(level-1)%tree(parent)%near_neighbours(i1)
     
         if(neighbour == 0) exit
      
         do i2 = 1, 8 ! children of near neighbours
            box2 = otree(level-1)%tree(neighbour)%children(i2) ! level
            if(box2 == 0) exit

            ! ***check if near box ****
            near_box = 0
            do i3 = 1,27
               if(box2 == otree(level)%tree(box)%near_neighbours(i3)) then
                  near_box = 1
               end if
            end do

            if(near_box == 0) then
               !print*, box, box2,  otree(level)%tree(box)%translator_rec_ind(las)
               
               !T = otree(level)%tree(box)%translator(:,las)
       
               F_th = otree(level)%tree(box)%out_pattern_theta * otree(level)%tree(box)%translator(:,las) &
                    * otree(level)%w
               F_phi = otree(level)%tree(box)%out_pattern_phi * otree(level)%tree(box)%translator(:,las) &
                    * otree(level)%w

               !print*,otree(level)%w
               !*********************
             
              ! print*, F_th
               
               otree(level)%tree(box2)%in_pattern_theta = otree(level)%tree(box2)%in_pattern_theta + F_th
               otree(level)%tree(box2)%in_pattern_phi = otree(level)%tree(box2)%in_pattern_phi + F_phi
       
               las = las +1  
              
            end if
         end do
      end do
   end do
      
  deallocate(F_th, F_phi)
   !****************************************************
end do
   


!******************** disaggregation + near zone interactions*************************************

call disaggregation(otree, mesh, matrices)

!! output: matrices%Ax 

end subroutine matvec_mlfma_efie



end module mlfma_efie
