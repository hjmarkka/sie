module rhs_pmchwt
use common
use integration_points
implicit none

contains  

subroutine rhs(matrices, mesh)

type (mesh_struct) :: mesh
type (data) :: matrices

integer :: test_T, i1, te, test, ti, tj
real(dp) :: T_coord(3,3), T_dN(3,3), T_nxdN(3,3), T_nvec(3), r(3)
real(dp), allocatable :: P0_tri(:,:), Pt(:,:), shape_tri(:,:)
real(dp), allocatable :: w0_tri(:),wt(:)
complex(dp) :: alok(3,3), alokH(3,3), H0(3)
real(dp) :: signt, el, eta0


eta0 = sqrt(mu/epsilon)
matrices%rhs(:) = dcmplx(0.0,0.0)

call inttri(P0_tri,w0_tri,5)

allocate(shape_tri(3,size(w0_tri)))
shape_tri(1,:) = 1.0_dp - P0_tri(1,:) - P0_tri(2,:)
shape_tri(2,:) = P0_tri(1,:)
shape_tri(3,:) = P0_tri(2,:)

allocate(Pt(3,size(w0_tri)))
allocate(wt(size(w0_tri)))

H0 = crossCC(dcmplx(matrices%khat, 0.0), matrices%E0) * sqrt(epsilon/mu)

do test_T = 1, mesh%N_tri

   T_coord = mesh%coord(:,mesh%etopol(:,test_T))
   call gradshape_tri(T_dN,T_nxdN, T_nvec, T_coord)

   call linmap_tri(Pt, wt, T_coord, P0_tri, W0_tri)
   
   alok(:,:) = dcmplx(0.0,0.0)
   alokH(:,:) = dcmplx(0.0,0.0)

   do i1 = 1,size(w0_tri)
      r=Pt(:,i1)
      
      alok(1,:) = alok(1,:) + matrices%E0(1) * & 
           exp(dcmplx(0.0,mesh%k*dot_product(matrices%khat,r))) &
           * shape_tri(:,i1) * wt(i1)

      alok(2,:) = alok(2,:) + matrices%E0(2) * & 
           exp(dcmplx(0.0,mesh%k*dot_product(matrices%khat,r))) &
           * shape_tri(:,i1) * wt(i1)

      alok(3,:) = alok(3,:) + matrices%E0(3) * & 
           exp(dcmplx(0.0,mesh%k*dot_product(matrices%khat,r))) &
           * shape_tri(:,i1) * wt(i1)

      alokH(1,:) = alokH(1,:) + H0(1) * & 
           exp(dcmplx(0.0,mesh%k*dot_product(matrices%khat,r))) &
           * shape_tri(:,i1) * wt(i1)

      alokH(2,:) = alokH(2,:) + H0(2) * & 
           exp(dcmplx(0.0,mesh%k*dot_product(matrices%khat,r))) &
           * shape_tri(:,i1) * wt(i1)

      alokH(3,:) = alokH(3,:) + H0(3) * & 
           exp(dcmplx(0.0,mesh%k*dot_product(matrices%khat,r))) &
           * shape_tri(:,i1) * wt(i1)



   end do
  

   do te = 1, 3
      test = mesh%etopol_edges(te,test_T)

      if(mesh%boundary(test) == 0) then
         signt = RWG_sign(mesh, test, test_T,te)
      
         el = norm(mesh%coord(:,mesh%edges(1,test)), mesh%coord(:,mesh%edges(2,test)))
         
         ti = mesh%Eori(1,te)
         tj = mesh%Eori(2,te)
         
         matrices%rhs(test) = matrices%rhs(test) - 1.0_dp/eta0 * signt * el * & 
              (sum(T_nxdN(:,tj)*alok(:,ti)) - sum(T_nxdN(:,ti)*alok(:,tj)))
         
         matrices%rhs(mesh%N_edge+test)=matrices%rhs(mesh%N_edge+test) - eta0*signt*el * & 
              (sum(T_nxdN(:,tj)*alokH(:,ti)) - sum(T_nxdN(:,ti)*alokH(:,tj)))
         
      end if
   end do

end do
!print*,matrices%rhs
end subroutine rhs

subroutine rhs_tapered(matrices, mesh)

type (mesh_struct) :: mesh
type (data) :: matrices

integer :: test_T, i1, te, test, ti, tj
real(dp) :: T_coord(3,3), T_dN(3,3), T_nxdN(3,3), T_nvec(3), r(3)
real(dp), allocatable :: P0_tri(:,:), Pt(:,:), shape_tri(:,:)
real(dp), allocatable :: w0_tri(:),wt(:)
complex(dp) :: alok(3,3), alokH(3,3), H0(3), E0(3)
real(dp) :: signt, el, eta0, k(3), tap, g


g=1.0_dp
eta0 = sqrt(mu/epsilon)
matrices%rhs(:) = dcmplx(0.0,0.0)
k=matrices%khat * mesh%k

call inttri(P0_tri,w0_tri,5)

allocate(shape_tri(3,size(w0_tri)))
shape_tri(1,:) = 1.0 - P0_tri(1,:) - P0_tri(2,:)
shape_tri(2,:) = P0_tri(1,:)
shape_tri(3,:) = P0_tri(2,:)

allocate(Pt(3,size(w0_tri)))
allocate(wt(size(w0_tri)))


do test_T = 1, mesh%N_tri

   T_coord = mesh%coord(:,mesh%etopol(:,test_T))
   call gradshape_tri(T_dN,T_nxdN, T_nvec, T_coord)

   call linmap_tri(Pt, wt, T_coord, P0_tri, W0_tri)
   
   alok(:,:) = dcmplx(0.0,0.0)
   alokH(:,:) = dcmplx(0.0,0.0)

   do i1 = 1,size(w0_tri)
      r=Pt(:,i1)

      tap = -(r(1)**2.0_dp + r(2)**2.0_dp)/g**2.0_dp
      E0=matrices%E0 * exp(dcmplx(0.0, dot_product(k,r))) * exp(tap)

      H0 = crossCC(dcmplx(matrices%khat, 0.0), E0) * sqrt(epsilon/mu)

      alok(1,:) = alok(1,:) + E0(1)* shape_tri(:,i1) * wt(i1)

      alok(2,:) = alok(2,:) + E0(2)* shape_tri(:,i1) * wt(i1)

      alok(3,:) = alok(3,:) + E0(3) * shape_tri(:,i1) * wt(i1)

      alokH(1,:) = alokH(1,:) + H0(1) * shape_tri(:,i1) * wt(i1)

      alokH(2,:) = alokH(2,:) + H0(2)* shape_tri(:,i1) * wt(i1)

      alokH(3,:) = alokH(3,:) + H0(3)* shape_tri(:,i1) * wt(i1)



   end do
  

   do te = 1, 3
      test = mesh%etopol_edges(te,test_T)

      if(mesh%boundary(test) == 0) then
         signt = RWG_sign(mesh, test, test_T,te)
      
         el = norm(mesh%coord(:,mesh%edges(1,test)), mesh%coord(:,mesh%edges(2,test)))
         
         ti = mesh%Eori(1,te)
         tj = mesh%Eori(2,te)
         
         matrices%rhs(test) = matrices%rhs(test) - 1.0_dp/eta0 * signt * el * & 
              (sum(T_nxdN(:,tj)*alok(:,ti)) - sum(T_nxdN(:,ti)*alok(:,tj)))

         matrices%rhs(mesh%N_edge+test)=matrices%rhs(mesh%N_edge+test)-eta0*signt*el * & 
              (sum(T_nxdN(:,tj)*alokH(:,ti)) - sum(T_nxdN(:,ti)*alokH(:,tj)))
         
      end if
   end do

end do
!print*,matrices%rhs
end subroutine rhs_tapered

subroutine rhs_tapered_HV(matrices, mesh, theta_inc, rhsHV)

type (mesh_struct) :: mesh
type (data) :: matrices

integer :: test_T, i1, te, test, ti, tj
real(dp) :: T_coord(3,3), T_dN(3,3), T_nxdN(3,3), T_nvec(3), r(3)
real(dp), allocatable :: P0_tri(:,:), Pt(:,:), shape_tri(:,:)
real(dp), allocatable :: w0_tri(:),wt(:)
complex(dp) :: alok(3,3), alokH(3,3), H0(3), E0(3), E0V(3), H0V(3), xhat(3)
complex(dp) :: alokV(3,3), alokHV(3,3)
real(dp) :: signt, el, eta0, k(3), tap, g, theta_inc
complex(dp) :: rhsHV(:,:)

g=5.0_dp

!horizontal pol
xhat(1) = dcmplx(1.0,0.0)
xhat(2) = dcmplx(0.0,0.0)
xhat(3) = dcmplx(0.0,0.0)

eta0 = sqrt(mu/epsilon)


rhsHV(:,:) = dcmplx(0.0,0.0)


! yz-plane
matrices%khat(1) = 0.0_dp
matrices%khat(2) = -sin(theta_inc)
matrices%khat(3) = -cos(theta_inc)

k=matrices%khat * mesh%k



call inttri(P0_tri,w0_tri,5)

allocate(shape_tri(3,size(w0_tri)))
shape_tri(1,:) = 1.0_dp - P0_tri(1,:) - P0_tri(2,:)
shape_tri(2,:) = P0_tri(1,:)
shape_tri(3,:) = P0_tri(2,:)

allocate(Pt(3,size(w0_tri)))
allocate(wt(size(w0_tri)))



do test_T = 1, mesh%N_tri

   T_coord = mesh%coord(:,mesh%etopol(:,test_T))
   call gradshape_tri(T_dN,T_nxdN, T_nvec, T_coord)

   call linmap_tri(Pt, wt, T_coord, P0_tri, W0_tri)
   
   alok(:,:) = dcmplx(0.0,0.0)
   alokH(:,:) = dcmplx(0.0,0.0)
   alokV(:,:) = dcmplx(0.0,0.0)
   alokHV(:,:) = dcmplx(0.0,0.0)


   
   do i1 = 1,size(w0_tri)
      r=Pt(:,i1)

      tap = -(r(1)**2.0_dp + r(2)**2.0_dp)/g**2.0_dp
      E0= xhat * exp(dcmplx(0.0, dot_product(k,r))) * exp(tap)
      H0 = crossCC(dcmplx(matrices%khat, 0.0), E0) * sqrt(epsilon/mu)

      H0V = xhat * exp(dcmplx(0.0, dot_product(k,r))) * sqrt(epsilon/mu) * exp(tap)
      E0V = sqrt(mu/epsilon) * crossCC(dcmplx(-matrices%khat, 0.0), H0V)

      !print*, dot_product(E0,E0), dot_product(E0V,E0V)
      
      alok(1,:) = alok(1,:) + E0(1) * shape_tri(:,i1) * wt(i1)
      alok(2,:) = alok(2,:) + E0(2) * shape_tri(:,i1) * wt(i1)
      alok(3,:) = alok(3,:) + E0(3) * shape_tri(:,i1) * wt(i1)
      alokH(1,:) = alokH(1,:) + H0(1) * shape_tri(:,i1) * wt(i1)
      alokH(2,:) = alokH(2,:) + H0(2) * shape_tri(:,i1) * wt(i1)
      alokH(3,:) = alokH(3,:) + H0(3) * shape_tri(:,i1) * wt(i1)

      alokV(1,:) = alokV(1,:) + E0V(1) * shape_tri(:,i1) * wt(i1)
      alokV(2,:) = alokV(2,:) + E0V(2) * shape_tri(:,i1) * wt(i1)
      alokV(3,:) = alokV(3,:) + E0V(3) * shape_tri(:,i1) * wt(i1)
      alokHV(1,:) = alokHV(1,:) + H0V(1) * shape_tri(:,i1) * wt(i1)
      alokHV(2,:) = alokHV(2,:) + H0V(2) * shape_tri(:,i1) * wt(i1)
      alokHV(3,:) = alokHV(3,:) + H0V(3) * shape_tri(:,i1) * wt(i1)

   end do
  

   do te = 1, 3
      test = mesh%etopol_edges(te,test_T)

      if(mesh%boundary(test) == 0) then
         signt = RWG_sign(mesh, test, test_T,te)
      
         el = norm(mesh%coord(:,mesh%edges(1,test)), mesh%coord(:,mesh%edges(2,test)))
         
         ti = mesh%Eori(1,te)
         tj = mesh%Eori(2,te)
         
         rhsHV(test,1) = rhsHV(test,1) - 1.0_dp/eta0 * signt * el * & 
              (sum(T_nxdN(:,tj)*alok(:,ti)) - sum(T_nxdN(:,ti)*alok(:,tj)))

         rhsHV(mesh%N_edge+test,1)=rhsHV(mesh%N_edge+test,1)-eta0*signt*el * & 
              (sum(T_nxdN(:,tj)*alokH(:,ti)) - sum(T_nxdN(:,ti)*alokH(:,tj)))


         rhsHV(test,2) = rhsHV(test,2) - 1.0_dp/eta0 * signt * el * & 
              (sum(T_nxdN(:,tj)*alokV(:,ti)) - sum(T_nxdN(:,ti)*alokV(:,tj)))

         rhsHV(mesh%N_edge+test,2)=rhsHV(mesh%N_edge+test,2)-eta0*signt*el * & 
              (sum(T_nxdN(:,tj)*alokHV(:,ti)) - sum(T_nxdN(:,ti)*alokHV(:,tj)))


         
      end if
   end do

end do
!print*,matrices%rhs
end subroutine rhs_tapered_HV

end module rhs_pmchwt
