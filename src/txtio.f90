module txtio
  use common
  implicit none
  
contains
subroutine strip_string(strin,strout,strlen)

  character(*), intent(in) :: strin
  character(*), intent(out) :: strout
  integer, intent(out) :: strlen
  character, dimension(*), parameter :: comchars = (/ '!', '#' /)
  integer, parameter :: cclen = size(comchars)
  integer :: osl, i, ci
  
  osl = len(strin)
  write(strout,'(A)') strin
  
  do i=1,cclen
    ci = index(strout, comchars(i))
    if(ci == 0) cycle
    write(strout,'(A)') strout(1:ci-1)
  end do
  
  write(strout,'(A)') trim(adjustl(strout))
  strlen = len_trim(strout)
    
end subroutine strip_string

! Read mesh geometry from external OFF file
subroutine read_off(fn,mesh)

  type (mesh_struct) :: mesh
  character(98), intent(in) :: fn
  real(dp), dimension(:,:), allocatable :: XT
  integer, dimension(:,:), allocatable :: IT
  integer :: nnod, ntri
  integer :: outstat
  integer :: j1, j2, fu, slen, cstat
  real(dp) :: r
  character(len=1024) :: line, sl

  outstat = -1
  
  ! File unit and unit opening
  open(newunit=fu, file=trim(fn), action='read', status='old')
  
  ! Scan for keyword 'OFF'
  do
    read(fu,'(A)',IOSTAT=cstat) line
    if(cstat /= 0) then
      write(error_unit,*) "File ended while scanning keyword 'OFF'"
      stop
    end if
    call strip_string(line,sl,slen)
    j2 = index(sl,"OFF")
    if(j2==1) then
      exit
    end if
  end do
  
  ! Scan for no. of vertices, and faces
  do
    read(fu,'(A)',IOSTAT=cstat) line
    if(cstat /= 0) then
      write(error_unit,*) "File ended while scanning for no. of vertices and faces"
      stop
    end if
    call strip_string(line,sl,slen)
    if(slen>0) then
      read(sl,*,IOSTAT=cstat) nnod, ntri
      if(cstat /= 0 .or. nnod <= 0 .or. ntri <= 0) then
        write(error_unit,*) "Wrong format when scanning for no. of vertices and faces"
        stop
      end if
      exit
    end if
  end do
  
  ! Allocate arrays
  allocate(XT(nnod,3), IT(ntri,3), STAT=cstat)
  if(cstat /= 0) then
    write(error_unit,*) "Error in memory allocation of vertices and faces"
    stop
  end if
  
  ! Read vertices
  do j1=1,nnod
    read(fu,*,IOSTAT=cstat) XT(j1,:)
    if(cstat /= 0) then
      write(error_unit,*) "Error reading vertice no. ", j1
      stop
    end if
  end do
    
  ! Read faces. Note that we only read triangles, and discard other nodes
  do j1=1,ntri
    read(fu,*,IOSTAT=cstat) j2, IT(j1,:)
    if(cstat /= 0) then
      write(error_unit,*) "Error reading face no. ", j1
      stop
    end if
    IT(j1,1) = IT(j1,1)+1
    IT(j1,2) = IT(j1,2)+1
    IT(j1,3) = IT(j1,3)+1
  end do
  
  close(fu)
  
  outstat = 0
  
  allocate(mesh%coord(3,nnod))
  mesh%coord = transpose(XT)
  print *,'   Number of nodes      =',size(mesh%coord,2)
  mesh%N_node = size(mesh%coord,2)
  
  allocate(mesh%etopol(3,ntri))
  mesh%etopol = transpose(IT)
  print *,'   Number of elements   =',size(mesh%etopol,2)
  mesh%N_tri = size(mesh%etopol,2)

end subroutine read_off

subroutine write_output(fn, mueller_mat, crs)
  CHARACTER(LEN=98) :: fn
  real(dp) :: mueller_mat(:,:)
  real(dp) :: crs(:)

  integer :: i, j, N
  N = size(mueller_mat,2)
  ! output data into a file 
   open(1, file = fn, status = 'replace')  
   do i=1, size(mueller_mat,1) 
      write(1,*) mueller_mat(i,:)   
   end do  

   !write cross sections
   do i=1, size(crs,1) 
      write(1,*) crs(i)   
   end do  

   
   close(1) 
end subroutine write_output
  
end module txtio
