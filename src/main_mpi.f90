program main_mpi
use common
use geometry
use mpi
use io
use field_pmchwt
use possu
use solver
use rhs_pmchwt
implicit none


type (mesh_struct) :: mesh
type (data) :: matrices

integer :: ierr, rc, my_id, N_procs, Eori(2,3), t1,t2, rate

integer :: restart, maxit, mueller, ave, halton_init, i1
double precision :: k, tol, khat(3), E0(3), cell_size, phi, theta, epsr, epsi
double complex :: eps_r

CHARACTER(LEN=28) :: meshname, fname, mueller_out, J_out, arg_name, arg
integer :: num_args, i_arg
double precision, dimension(:,:), allocatable :: mueller_mat, mueller_mat_ave, r
double complex, dimension(:,:), allocatable :: EH, rhsHV
double precision :: avelength, maxlength, minlength, p1(3),p2(3), theta_inc, crs(3)

call MPI_INIT(ierr)


if (ierr .ne. MPI_SUCCESS) then
   print *,'Error starting MPI program. Terminating.'
   call MPI_ABORT(MPI_COMM_WORLD, rc, ierr)
end if

call MPI_COMM_RANK (MPI_COMM_WORLD, my_id, ierr)
call MPI_COMM_SIZE (MPI_COMM_WORLD, N_procs, ierr)

if(my_id == 0) then 

   print *,'*************************************************************'
   print *,'**                                                         **'
   print *,'**               SIE-PMCHWT-mpi v. 0.1                     **'
   print *,'**                                                         **'
   print *,'*************************************************************'


! Default arguments
   meshname = 'mesh.h5'
   mueller_out = 'mueller.h5'
   J_out = 'J.h5'
   fname = 'out.h5'
   k = 2*pi
   khat = [0.0,0.0,1.0]!/sqrt(2.0) 
   E0 = [1,0,0]
   mueller = 0 
   phi = 0.0
   theta = 0.0
   ave = 0
   halton_init = 0
   tol = 1e-5 
   maxit = 50  
   restart = 4
   epsr = 2.55
   epsi = 0.34

   num_args = command_argument_count()
   do i_arg = 1,num_args,2
      call get_command_argument(i_arg,arg_name)
  
      select case(arg_name)
      
      case('-mesh')
         call get_command_argument(i_arg+1,arg)
         meshname = arg
         print*, 'mesh file is:', meshname
      case('-S_out')
         call get_command_argument(i_arg+1,arg)
         mueller_out = arg
         print*, 'Mueller matrix is written in the file:', mueller_out
      case('-output_file')
         call get_command_argument(i_arg+1,arg)
         fname = arg
         print*, 'Output is written in the file:', fname  
      case('-J_out')
         call get_command_argument(i_arg+1,arg)
         J_out = arg
         print*, 'Solution is written in the file:', J_out
      case('-k')
         call get_command_argument(i_arg+1,arg)
         read(arg,*) k 
      case('-eps_r')
         call get_command_argument(i_arg+1,arg)
         read(arg,*) epsr 
      case('-eps_i')
         call get_command_argument(i_arg+1,arg)
         read(arg,*) epsi 
      case('-mueller')
         call get_command_argument(i_arg+1,arg)
         read(arg,*) mueller
      case('-phi')
         call get_command_argument(i_arg+1,arg)
         read(arg,*) phi
      case('-theta')
         call get_command_argument(i_arg+1,arg)
         read(arg,*) theta
      case('-ave')
         call get_command_argument(i_arg+1,arg)
         read(arg,*) ave
      case('-halton_int')
         call get_command_argument(i_arg+1,arg)
         read(arg,*) halton_init
      case('-tol')
         call get_command_argument(i_arg+1,arg)
         read(arg,*) tol
      case('-maxit')
         call get_command_argument(i_arg+1,arg)
         read(arg,*) maxit
      case('-restart')
         call get_command_argument(i_arg+1,arg)
         read(arg,*) restart
      
      case('-help')
         print*, 'Command line parameters' 
         print*, '-mesh mesh.h5      "Read mesh from file"' 
         print*, '-S_out mueller.h5  "Output file: Mueller matrix"'
         print*, '-J_out J.h5        "Output file: Solution coefficients"'
         print*, '-k 1.0             "Wavenumber"'
         print*, '-mueller 1         "Compute mueller matrix (1) yes (0) no"'
         print*, '-phi 0.0           "Incident angel phi"'
         print*, '-theta 0.0         "Incident angel theta"'
         print*, '-ave 0             "Orientation averaging (number of orientations)"'
         print*, '-halton_init 0     "Orientation averaging (beginning of Halton sequence)"'
         print*, '-tol 1e-5          "GMRES tolerance"'
         print*, '-restart 4         "GMRES restart"'
         print*, '-maxit 50          "Maximum number of GMRES iterations"'
         stop
      case default 
         print '(a,a,/)', 'Unrecognized command-line option: ', arg_name
         stop
      end select
   end do

   mesh%tol = tol
   mesh%restart = restart
   mesh%maxit = maxit
   matrices%khat = khat
   matrices%E0 = dcmplx(E0, 0.0)
   mesh%k = k
   mesh%eps_r = dcmplx(epsr, epsi)

   theta_inc = 0.0!pi/4.0 ! [0 pi/2]
   
   call read_mesh(mesh, meshname)
   !call read_field_points(matrices)
  
end if


call MPI_Bcast(mueller,1,MPI_INTEGER,0,MPI_COMM_WORLD,ierr)
call MPI_Bcast(ave,1,MPI_INTEGER,0,MPI_COMM_WORLD,ierr)
call MPI_Bcast(halton_init,1,MPI_INTEGER,0,MPI_COMM_WORLD,ierr)

call MPI_Bcast(mesh%N_tri,1,MPI_INTEGER,0,MPI_COMM_WORLD,ierr)
call MPI_Bcast(mesh%tol,1,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,ierr)
call MPI_Bcast(mesh%restart,1,MPI_INTEGER,0,MPI_COMM_WORLD,ierr)
call MPI_Bcast(mesh%maxit,1,MPI_INTEGER,0,MPI_COMM_WORLD,ierr)
call MPI_Bcast(mesh%k,1,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,ierr)
call MPI_Bcast(mesh%N_node,1,MPI_INTEGER,0,MPI_COMM_WORLD,ierr)
call MPI_Bcast(matrices%khat,3,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,ierr)
call MPI_Bcast(matrices%E0,3,MPI_DOUBLE_COMPLEX,0,MPI_COMM_WORLD,ierr)
call MPI_Bcast(mesh%eps_r,1,MPI_DOUBLE_COMPLEX,0,MPI_COMM_WORLD,ierr)
call MPI_Bcast(theta_inc,1,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,ierr)

if(my_id .ne. 0) then
   allocate(mesh%coord(3,mesh%N_node))
   allocate(mesh%etopol(3,mesh%N_tri))
end if


!call MPI_BARRIER(MPI_COMM_WORLD,ierr)

call MPI_Bcast(mesh%coord,size(mesh%coord),MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,ierr)
call MPI_Bcast(mesh%etopol,size(mesh%etopol),MPI_INTEGER,0,MPI_COMM_WORLD,ierr)

!call MPI_Bcast(matrices%field_points,size(matrices%field_points),MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,ierr)

Eori(:,1) = [3,2]
Eori(:,2) = [1,3]
Eori(:,3) = [2,1]

!Eori(:,1) = [1,2]
!Eori(:,2) = [2,3]
!Eori(:,3) = [3,1]


mesh%Eori = Eori


call find_edges(mesh)
!print*, size(mesh%etopol_edges,2)
!print*, int((mesh%etopol_edges(:,:)))

if(my_id == 0) then
   print*, '   Number of edges      =', mesh%N_edge
   print*, '   k  = ', real(k)
!end if


   maxlength = 0.0
   avelength = 0.0

   p1 = mesh%coord(:,mesh%edges(1,1))
   p2 = mesh%coord(:,mesh%edges(2,1))
   minlength = norm(p1,p2)
    do i1=1,mesh%N_edge
       p1 = mesh%coord(:,mesh%edges(1,i1))
       p2 = mesh%coord(:,mesh%edges(2,i1))
       if (norm(p1,p2) > maxlength) then
          maxlength = norm(p1,p2)
       end if
       if (norm(p1,p2) < minlength) then
          minlength = norm(p1,p2)
       end if
       avelength = avelength + norm(p1,p2)

    end do

    avelength = avelength/dble(mesh%N_edge)
    print*, '   Ave elem. size', real((2*pi/k)/avelength)**(-1.0), 'wavelengths'
    print*, '   Max elem. size', real((2*pi/k)/maxlength)**(-1.0), 'wavelengths'
    print*, '   Min elem. size', real((2*pi/k)/minlength)**(-1.0), 'wavelengths'

end if

allocate(matrices%rhs(2*mesh%N_edge))
allocate(matrices%x(2*mesh%N_edge))
allocate(matrices%Ax(2*mesh%N_edge))
allocate(rhsHV(2*mesh%N_edge,2))
!call rhs_efie(matrices, mesh)
!call rhs_mfie(matrices,mesh)
!call rhs_tapered(matrices,mesh)


call rhs_tapered_HV(matrices, mesh, theta_inc, rhsHV)

print*, 'Build matrix...'
!print*, (matrices%rhs)
call system_clock(t1,rate)
call build_matrix_pmchwt_mpi(matrices,mesh,my_id,N_procs)
call system_clock(t2,rate)

!call build_matrix_efie(matrices,mesh)
!call build_matrix_mfie(matrices,mesh)

print*,'Done in', real(T2-T1)/real(rate), 'seconds'

if(my_id == 0) then
   print*, 'Invert matrix...'

   !matrices%A = Cinv(matrices%A)
   !matrices%x = matmul(matrices%A,matrices%rhs)

   !call gmres_full_mpi(matrices, mesh)
   !print*, matrices%rhs
   print*,'Done'
end if

if(mueller == 0) then
   matrices%rhs = rhsHV(:,1)
   call gmres_full_mpi(matrices, mesh)

   call compute_reflectance(matrices,mesh, 32, 90)
   matrices%E_H = matrices%E_field
   deallocate(matrices%E_field)
   deallocate(matrices%field_points)

   matrices%rhs = rhsHV(:,2)
   call gmres_full_mpi(matrices, mesh)
   call compute_reflectance(matrices,mesh, 32, 90)
   matrices%E_V = matrices%E_field
   
if (my_id == 0) then
   
   !call compute_rcs(matrices,mesh)
   !fname = "rcs.h5"
   !call real_write2file(matrices%rcs,fname)

   !fname = "x.h5"
   !call cmplx_vec_write2file(matrices%x,fname)

   !fname = "Efield_H.h5"
   !call write2file(matrices%E_H,fname)
   !fname = "Efield_V.h5"
   !call write2file(matrices%E_V,fname)
   !fname ="angles.h5"
   !call real_write2file(matrices%field_points,fname)
   
   call write2file_surf(matrices%E_H, matrices%E_V, matrices%field_points,fname)
   
   call surface_fields(matrices, mesh, EH, r)
   fname = "EH.h5"
   call write2file(EH,fname)
   fname ="r.h5"
   call real_write2file(r,fname)

end if

end if


if (mueller==1) then
   if(ave == 0) then
      mueller_mat = compute_mueller(matrices, mesh, pi/180.0*phi, pi/180.0*theta, 360, pi/180.0 * 0.0, my_id)
   else
      call orientation_ave(mesh,matrices,361,ave,halton_init, my_id, mueller_mat, crs)
   end if
  
   if(my_id == 0) then
      !call real_write2file(mueller_mat,mueller_out)
      call write2file_mueller(mueller_mat, crs, mueller_out)
   end if
end if



call MPI_FINALIZE(ierr)


end program main_mpi

