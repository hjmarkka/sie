module interpolation
use octtree
use common
use integration_points
!use translation
use mlfma_pmchwt
use io
implicit none

integer :: upsm = 0
integer :: upsn = 0

contains

subroutine interpolation_coeff_pole(L1, theta1, phi1, coeff)
integer :: L1, L2
integer :: M,N 

real(dp), dimension(:), allocatable :: P_theta, w_theta, theta, ph, phi, wj, vi,coeff, P_th1 
integer :: i1, i2, j1, level, t,p, acc, ind, s, ind1, ind2, tind, pind, pind2, trans_ind, las, tt, ss
real(dp) :: phi1, theta1, box_dl, f, vi_th, wj_phi
integer, dimension(:), allocatable :: ind_phi, ind_theta

p = 2
M = L1+upsm+1
N= 2*(L1+upsn+1)

call gaussint2(P_theta, w_theta, M)

allocate(theta(3*M+2), ph(N), phi(3*N), vi(2*p), wj(2*p), P_th1(M+2))
allocate(ind_phi(2*p), ind_theta(2*p))



P_theta = pi - acos(P_theta)  !! thtea

P_th1 = [0.0d0, P_theta, pi]

!theta = [P_theta, P_theta, P_theta]
theta = [P_theta-pi, P_th1, P_theta+pi]
M = M + 2

allocate(coeff(M*N))
coeff(:) = 0.0d0

do i1 = 1,N
   ph(i1) = dble(i1-1) * 2*pi / dble(N) + pi/dble(N)
end do
phi=[ph-2*pi, ph, ph+2*pi]
phi=[ph-2*pi, ph, ph+2*pi]

ss = N+1 
do s = 1, N
   if(phi1 < ph(s)) then
      ss=s
      exit
   end if
end do
s = ss-1

tt = M
do t = 1, M
   !if(theta1 > P_theta(t)) then
    if(theta1  < P_th1(t)) then  
      tt  = t
      exit
   end if
end do
t = tt-1

f = 0.0
las = 1
do i1 = t-p+1, t+p
   vi_th = 1.0d0   
   do i2 = t-p+1, t+p
      if(i1 .ne. i2) then
         vi_th = vi_th*(theta1 - theta(i2+M-2)) / (theta(i1+M-2) - theta(i2+M-2))        
      end if
   end do
   f = f + vi_th

   vi(las) = vi_th
   ind_theta(las) = i1
   las = las + 1
end do




f = 0.0
las = 1
do i1 = s-p+1, p+s
   wj_phi = 1.0d0   
   do i2 = s-p+1, p+s
      if(i1 .ne. i2) then
         wj_phi = wj_phi*(phi1 - phi(i2+N)) / (phi(i1+N) - phi(i2+N))
      end if
   end do

   f = f + wj_phi

   wj(las) = wj_phi
   ind_phi(las) = i1
   las = las + 1
end do

f = 0.0
do i1 = 1, 2*p !phi
   pind = ind_phi(i1)
   if(pind > N) pind = pind-N
   if(pind < 1) pind = N+pind
   
   do i2 = 1, 2*p ! theta
      tind = ind_theta(i2)

      !trans_ind = (pind-1)*M + tind
      trans_ind = (tind-1)*N + pind
      
      if(tind > M) then
         tind = 2*M-tind
         pind2 = pind + N/2 ! ind_phi = N-ind_phi
         if(pind2 > N) pind2 = pind2-N
         
         !trans_ind = (pind2-1)*M + tind
          trans_ind = (tind-1)*N + pind2
      end if
      
      if(tind < 1) then
         tind = 2-tind
         pind2 = pind + N/2 
         if(pind2 > N) pind2 = pind2-N
         !trans_ind = (pind2-1)*M + tind
         trans_ind = (tind-1)*N + pind2
      end if
      
      !print*, trans_ind, tind, pind
      coeff(trans_ind) = wj(i1) * vi(i2)
      f = f + wj(i1) * vi(i2) !* S(trans_ind)
    
   end do
end do
   
end subroutine interpolation_coeff_pole

subroutine interpolation_coeff(L1, p, theta1, phi1, coeff, val, spind)
integer :: L1, L2
integer :: M,N 

real(dp), dimension(:), allocatable :: P_theta, w_theta, theta, ph, phi, wj, vi,coeff  
integer :: i1, i2, j1, level, t,p, acc, ind, s, ind1, ind2, tind, pind, pind2, trans_ind, las, tt, ss
real(dp) :: phi1, theta1, box_dl, f, vi_th, wj_phi, val(4*p**2)
integer, dimension(:), allocatable :: ind_phi, ind_theta
integer :: spind(4*p**2)

M = upsm+L1+1
N= 2*(upsn+L1+1)


call gaussint2(P_theta, w_theta, M)

allocate(theta(3*M), ph(N), phi(3*N), vi(2*p), wj(2*p))
allocate(ind_phi(2*p), ind_theta(2*p))
allocate(coeff(M*N))
coeff(:) = 0.0_dp

P_theta = acos(P_theta)
w_theta = w_theta
!theta = [P_theta, P_theta, P_theta]
theta = [P_theta-pi, P_theta, P_theta+pi]

do i1 = 1,N
   ph(i1) = dble(i1-1) * 2_dp*pi / dble(N) + pi/dble(N)
end do
!phi=[ph, ph,  ph]
phi=[ph-2_dp*pi, ph, ph+2_dp*pi]

ss = N+1 
do s = 1, N
   if(phi1 < ph(s)) then
      ss=s-1
      exit
   end if
end do
s = ss-1

tt = M +1
do t = 1, M
   if(theta1 > P_theta(t)) then
      tt  = t
      exit
   end if
end do
t = tt-1

f = 0.0
las = 1

do i1 = t-p+1, t+p
   vi_th = 1.0_dp   
   do i2 = t-p+1, t+p
      if(i1 .ne. i2) then
         vi_th = vi_th*(theta1 - theta(i2+M)) / (theta(i1+M) - theta(i2+M))
 !        print*, theta1, theta(i2+M), theta(i1+M)
      end if
   end do
   f = f + vi_th

   vi(las) = vi_th
   ind_theta(las) = i1
   las = las + 1
end do

!print*, vi
!stop

f = 0.0
las = 1
do i1 = s-p+1, p+s
   wj_phi = 1.0_dp   
   do i2 = s-p+1, p+s
      if(i1 .ne. i2) then
         wj_phi = wj_phi*(phi1 - phi(i2+N)) / (phi(i1+N) - phi(i2+N))
      end if
   end do

   f = f + wj_phi

   wj(las) = wj_phi
   ind_phi(las) = i1
   las = las + 1
end do

f = 0.0
las = 1
do i1 = 1, 2*p !phi
   pind = ind_phi(i1)
   if(pind > N) pind = pind-N
   if(pind < 1) pind = N+pind
   
   do i2 = 1, 2*p ! theta
      tind = ind_theta(i2)

      trans_ind = (pind-1)*M + tind
      
      if(tind > M) then
         tind = 2*M-tind+1
         pind2 = pind + N/2 ! ind_phi = N-ind_phi
         if(pind2 > N) pind2 = pind2-N
         trans_ind = (pind2-1)*M + tind
      end if
      
      if(tind < 1) then
         tind = 1-tind
         pind2 = pind + N/2
         if(pind2 > N) pind2 = pind2-N
         trans_ind = (pind2-1)*M + tind
      end if
      
      !print*, trans_ind, tind, pind
      coeff(trans_ind) = wj(i1) * vi(i2) 

      f = f + wj(i1) * vi(i2) !* S(trans_ind)

      spind(las) = trans_ind    
      val(las) = wj(i1) * vi(i2)
      
      las = las + 1
   end do
end do
deallocate(theta, ph, phi, vi, wj)
deallocate(ind_phi, ind_theta)

end subroutine interpolation_coeff


subroutine interpolation_coeff_theta(L1, p, theta1, pind, val, spind)
integer :: L1, L2
integer :: M,N 

real(dp), dimension(:), allocatable :: P_theta, w_theta, theta, vi,coeff  
integer :: i1, i2, j1, level, t,p, acc, ind, s, ind1, ind2, tind, pind, pind2, trans_ind, las, tt, ss
real(dp) :: theta1, box_dl, f, vi_th, val(2*p)
integer, dimension(:), allocatable :: ind_theta
integer :: spind(2*p)

M = L1+1
N= 2*(L1+1)


call gaussint2(P_theta, w_theta, M)

allocate(theta(3*M), vi(2*p))
allocate(ind_theta(2*p))
allocate(coeff(M*N))
coeff(:) = 0.0_dp

P_theta = acos(P_theta)
w_theta = w_theta
!theta = [P_theta, P_theta, P_theta]
theta = [P_theta-pi, P_theta, P_theta+pi]

tt = M +1
do t = 1, M
   if(theta1 > P_theta(t)) then
      tt  = t
      exit
   end if
end do
t = tt-1


las = 1

do i1 = t-p+1, t+p
   vi_th = 1.0_dp   
   do i2 = t-p+1, t+p
      if(i1 .ne. i2) then
         vi_th = vi_th*(theta1 - theta(i2+M)) / (theta(i1+M) - theta(i2+M))
 !        print*, theta1, theta(i2+M), theta(i1+M)
      end if
   end do
 
   vi(las) = vi_th
   ind_theta(las) = i1
   las = las + 1
end do

las = 1

   
do i2 = 1, 2*p ! theta
   tind = ind_theta(i2)

   trans_ind = (pind-1)*M + tind
      
   if(tind > M) then
      tind = 2*M-tind+1
      pind2 = pind + N/2 ! ind_phi = N-ind_phi
      if(pind2 > N) pind2 = pind2-N
      trans_ind = (pind2-1)*M + tind
   end if
      
   if(tind < 1) then
      tind = 1-tind
      pind2 = pind + N/2
      if(pind2 > N) pind2 = pind2-N
      trans_ind = (pind2-1)*M + tind
   end if
      
   !print*, trans_ind, tind, pind
   !coeff(trans_ind) = vi(i2) 

   spind(las) = trans_ind    
   val(las) = vi(i2)
      
   las = las + 1
  
end do
deallocate(theta, vi)
deallocate(ind_theta)

end subroutine interpolation_coeff_theta


subroutine interpolation_coeff_phi(L1, p, tind, phi1, M, val, spind)
integer :: L1, L2
integer :: M,N 

real(dp), dimension(:), allocatable :: ph, phi, wj  
integer :: i1, i2, j1, level, t,p, acc, ind, s, ind1, ind2, tind, pind, pind2, trans_ind, las, tt, ss
real(dp) :: phi1, box_dl, wj_phi, val(2*p)
integer, dimension(:), allocatable :: ind_phi
integer :: spind(2*p)

!M = L1+1
N= 2*(L1+1)

allocate(ph(N), phi(3*N), wj(2*p))
allocate(ind_phi(2*p))

do i1 = 1,N
   ph(i1) = dble(i1-1) * 2_dp*pi / dble(N) + pi/dble(N)
end do
!phi=[ph, ph,  ph]
phi=[ph-2_dp*pi, ph, ph+2_dp*pi]

ss = N+1 
do s = 1, N
   if(phi1 < ph(s)) then
      ss=s-1
      exit
   end if
end do
s = ss-1


las = 1
do i1 = s-p+1, p+s
   wj_phi = 1.0_dp   
   do i2 = s-p+1, p+s
      if(i1 .ne. i2) then
         wj_phi = wj_phi*(phi1 - phi(i2+N)) / (phi(i1+N) - phi(i2+N))
      end if
   end do

   wj(las) = wj_phi
   ind_phi(las) = i1
   las = las + 1
end do

las = 1
do i1 = 1, 2*p !phi
   pind = ind_phi(i1)
   if(pind > N) pind = pind-N
   if(pind < 1) pind = N+pind
   
   trans_ind = (pind-1)*M + tind
      
   
   !print*, trans_ind, tind, pind
   !coeff(trans_ind) = wj(i1) 
   spind(las) = trans_ind    
   val(las) = wj(i1) 
      
   las = las + 1

end do
deallocate(ph, phi, wj)
deallocate(ind_phi)

end subroutine interpolation_coeff_phi


subroutine build_interpolation_matrix_mlfma(otree, mesh)
type (level_struct) :: otree(:)
type (mesh_struct) :: mesh

integer :: level, level1, level0, L1, L0, acc, M0, M1, N0, N1, LL0, LL1, p
real(dp), dimension(:,:), allocatable :: P0, P1
real(dp), dimension(:), allocatable :: w0, w1
integer :: i1
real(dp) :: vec(3), theta, phi, box_dl0, box_dl1
real(dp), dimension(:), allocatable :: coeff, val
integer, dimension(:), allocatable :: ind
CHARACTER(LEN=28) :: fname
real(dp) :: mem
acc = mesh%acc
p = mesh%p

allocate(val(4*p**2))
allocate(ind(4*p**2))
mem = 0.0_dp
do level = size(otree), 4, -1

   level0 = level-1
   box_dl0 =  otree(level0)%tree(1)%dl
   L0 = rokhlinorder2(real(sqrt(mesh%eps_r)) * mesh%k*box_dl0, acc)
   LL0 = sampleorder(real(sqrt(mesh%eps_r)) * mesh%k*box_dl0, acc)
   
   M0 = upsm+LL0+1
   N0= 2*(upsn+LL0+1)

   level1 = level
   box_dl1 =  otree(level1)%tree(1)%dl

   L1 = rokhlinorder2(real(sqrt(mesh%eps_r)) * mesh%k*box_dl1, acc)
   LL1 = sampleorder(real(sqrt(mesh%eps_r)) * mesh%k*box_dl1, acc)
   
   M1 = upsm+LL1+1
   N1= 2*(upsn+LL1+1)

  ! print*, 'Min distance 2*kd0 =', 2*mesh%k*box_dl1
   call sample_points(P0,w0,M0,N0)
   call sample_points(P1,w1,M1,N1)


   allocate(otree(level)%Intermat_sparse(4*p**2 * size(w0)))
   allocate(otree(level)%Intermat_ind(4*p**2 * size(w0),2))

   mem = mem + (4.0_dp*p**2_dp * size(w0) * 64.0_dp /1024.0_dp/1024.0_dp/8.0_dp + &
        4.0_dp*p**2_dp * size(w0) * 32.0_dp / 8.0_dp /1024.0_dp/1024.0_dp * 2.0_dp)
   
   
   !allocate(otree(level)%Intermat(M0*N0,M1*N1 + 2*N1)) !poles
!   allocate(otree(level)%Intermat(M0*N0,M1*N1))
   allocate(otree(level)%P(3,M1*N1))
   allocate(otree(level)%w(M1*N1))

   otree(level)%P = P1
   otree(level)%w = w1
   otree(level)%L = L1
   
   print*, 'level =', level1, 'Rokhlin order L =', L1, '#sample points', size(w1)

   do i1 = 1, size(w0)

      vec = cart2sph(P0(:,i1))

      theta = vec(2)
      phi = vec(3)
      if(phi < 0.0_dp) phi = 2_dp*pi+phi 
        ! print*, theta
      call interpolation_coeff(LL1, p, theta, phi, coeff, val, ind)

     ! otree(level)%Intermat(i1,:) = coeff
      deallocate(coeff)


      otree(level)%Intermat_sparse((i1-1)*4*p**2+1 : i1*4*p**2) = val
      otree(level)%Intermat_ind((i1-1)*4*p**2+1 : i1*4*p**2, 1) = i1
      otree(level)%Intermat_ind((i1-1)*4*p**2+1 : i1*4*p**2, 2) = ind
   end do

   deallocate(P0,P1,w0,w1)
end do


level1 = 3
box_dl1 =  otree(level1)%tree(1)%dl

L1 =rokhlinorder2(real(sqrt(mesh%eps_r)) * mesh%k*box_dl1, acc)
LL1 = sampleorder(real(sqrt(mesh%eps_r)) * mesh%k*box_dl1, acc)
   
M1 = upsm+LL1+1
N1= 2*(upsn+LL1+1)

call sample_points(P1,w1,M1,N1)
   
allocate(otree(level1)%P(3,M1*N1))
allocate(otree(level1)%w(M1*N1))
otree(level1)%P = P1
otree(level1)%w = w1
otree(level1)%L = L1

print*, 'level =', level1, 'Rokhlin order L =', L1, '#sample points', size(w1)
!print*, size(Intermat,1), size(Intermat,2)



!fname="mat2.h5"
!call real_write2file(otree(level1+1)%Intermat,fname)
print*, 'Size of interpolators', floor(mem), 'Mb'

end subroutine build_interpolation_matrix_mlfma


subroutine build_interpolation_matrix_mlfma2(otree, mesh)
type (level_struct) :: otree(:)
type (mesh_struct) :: mesh

integer :: level, level1, level0, L1, L0, acc, M0, M1, N0, N1, LL0, LL1, p, las
real(dp), dimension(:,:), allocatable :: P0, P1
real(dp), dimension(:), allocatable :: w0, w1, w_theta0, w_theta1, P_theta0, P_theta1
integer :: i1, i2
real(dp) :: vec(3), theta, phi, box_dl0, box_dl1
real(dp), dimension(:), allocatable :: coeff, val
integer, dimension(:), allocatable :: ind
CHARACTER(LEN=28) :: fname
real(dp) :: mem

acc = mesh%acc
p = mesh%p

allocate(val(2*p))
allocate(ind(2*p))
mem = 0.0_dp
do level = size(otree), 4, -1

   level0 = level-1
   box_dl0 =  otree(level0)%tree(1)%dl
   L0 = rokhlinorder2(real(sqrt(mesh%eps_r)) * mesh%k*box_dl0, acc)
  
   M0 = L0+1
   N0= 2*(L0+1)

   level1 = level
   box_dl1 =  otree(level1)%tree(1)%dl

   L1 = rokhlinorder2(real(sqrt(mesh%eps_r)) * mesh%k*box_dl1, acc)
   
   M1 = L1+1
   N1= 2*(L1+1)

  ! print*, 'Min distance 2*kd0 =', 2*mesh%k*box_dl1
   call sample_points(P0,w0,M0,N0)
   call sample_points(P1,w1,M1,N1)

   
   call gaussint2(P_theta0, w_theta0, M0)   
   P_theta0 = acos(P_theta0)

  


   
   allocate(otree(level)%Intermat_th_sparse(2*p * N1*M0))
   allocate(otree(level)%Intermat_th_ind(2*p * N1*M0,2))
 
   allocate(otree(level)%Intermat_phi_sparse(2*p * N0*M0))
   allocate(otree(level)%Intermat_phi_ind(2*p * N0*M0,2))
   
   !allocate(otree(level)%Intermat(M0*N0,M1*N1 + 2*N1)) !poles
   !allocate(otree(level)%Intermat(M0*N0,M1*N1))

!   allocate(otree(level)%P(3,M1*N1))
!   allocate(otree(level)%w(M1*N1))

   otree(level)%P = P1
   otree(level)%w = w1
   otree(level)%L = L1
   
   print*, 'level =', level1-1, 'Rokhlin order L =', L1, '#sample points', size(w1)

   las = 1
   do i1 = 1,N1 ! phi coarse grid
      phi = dble(i1-1) * 2.0_dp*pi / dble(N1) + pi/dble(N1) 

      do i2 = 1, M0 ! dense grid
         theta = P_theta0(i2)
         call interpolation_coeff_theta(L1, p, theta, i1, val, ind)
         
         otree(level)%Intermat_th_sparse((las-1)*2*p+1 : las*2*p) = val
         otree(level)%Intermat_th_ind((las-1)*2*p+1 : las*2*p, 1) = las
         otree(level)%Intermat_th_ind((las-1)*2*p+1 : las*2*p, 2) = ind

         las = las + 1
      end do
   end do

   allocate(otree(level)%iao_th(M0*N1+1))
   allocate(otree(level)%ao_th(M0*N1 * 2*p))
   allocate(otree(level)%jao_th(M0*N1 * 2*p))

   call coocsr(M0*N1, 2*p*M0*N1, otree(level)%Intermat_th_sparse, otree(level)%Intermat_th_ind(:,1) &
        , otree(level)%Intermat_th_ind(:,2), otree(level)%ao_th, otree(level)%jao_th, otree(level)%iao_th)

   
   las = 1
   do i1 = 1,N0 ! phi dense grid
      phi = dble(i1-1) * 2.0_dp*pi / dble(N0) + pi/dble(N0) 

      do i2 = 1, M0 ! dense grid
         theta = P_theta0(i2)
         call interpolation_coeff_phi(L1, p, i2, phi, M0, val, ind)
         
         otree(level)%Intermat_phi_sparse((las-1)*2*p+1 : las*2*p) = val
         otree(level)%Intermat_phi_ind((las-1)*2*p+1 : las*2*p, 1) = las
         otree(level)%Intermat_phi_ind((las-1)*2*p+1 : las*2*p, 2) = ind

         las = las + 1
      end do
   end do

   allocate(otree(level)%iao(M0*N0+1))
   allocate(otree(level)%ao(M0*N0 * 2*p))
   allocate(otree(level)%jao(M0*N0 * 2*p))

   call coocsr(M0*N0, 2*p*M0*N0, otree(level)%Intermat_phi_sparse, otree(level)%Intermat_phi_ind(:,1) &
        , otree(level)%Intermat_phi_ind(:,2), otree(level)%ao, otree(level)%jao, otree(level)%iao)


   
   deallocate(P0,P1,w0,w1, P_theta0, w_theta0)
end do



level1 = 3
box_dl1 =  otree(level1)%tree(1)%dl

L1 =rokhlinorder2(real(sqrt(mesh%eps_r)) * mesh%k*box_dl1, acc)
   
M1 = L1+1
N1= 2*(L1+1)

call sample_points(P1,w1,M1,N1)
   
!allocate(otree(level1)%P(3,M1*N1))
!allocate(otree(level1)%w(M1*N1))

otree(level1)%P = P1
otree(level1)%w = w1
otree(level1)%L = L1

print*, 'level =', level1-1, 'Rokhlin order L =', L1, '#sample points', size(w1)
!print*, size(Intermat,1), size(Intermat,2)



!fname="mat2.h5"
!call real_write2file(otree(level1+1)%Intermat,fname)
print*, 'Size of interpolators', floor(mem), 'Mb'

end subroutine build_interpolation_matrix_mlfma2


end module interpolation
