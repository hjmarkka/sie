module solver_mlfma
use common
use field_pmchwt
use io
use integration_points
use rhs_pmchwt
use gmres
use omp_lib

implicit none

contains
function compute_mueller_ave(matrices, mesh, otree, phi, theta, np, mp, crs) result(mueller)
type (data) :: matrices
type (mesh_struct) :: mesh
type (level_struct) :: otree(:)
real(dp) :: phi, theta, phi_sca, crs(3)
integer :: np, mp, my_id, i1
real(dp) :: inc_vec(3,3), theta_sca, R , abcd2(2,2), Csca
real(dp) :: obs_point(3), sca_vec(3,3), ksca(3), abcd(2,2), mat(2,2), Cabs1, Cabs2, Cabs , Cext1, Cext2, Cext
integer :: T1, T2, rate, n, m, las, ind1, ind2, cont, ierr, N_procs, m_start, m_stop
real(dp), dimension(:,:), allocatable :: mueller
complex(dp) :: E(3), S(mp*np,5), A, F(mp*np,5)



allocate(mueller(mp*np,17))
mueller(:,:) = 0.0_dp

R  = 100000.0_dp * 2.0_dp*pi/mesh%k 

inc_vec = sph_unit_vectors(theta,phi)

abcd(1,1) = 1.0_dp!inc_vec(:,2)
abcd(1,2) = 0.0_dp!inc_vec(:,2)
abcd(2,1) = 0.0_dp!inc_vec(:,3)
abcd(2,2) = 1.0_dp!inc_vec(:,3)

matrices%khat = inc_vec(:,1)
matrices%E0 = inc_vec(:,2)

call rhs(matrices, mesh)

print*,'Solving...'
call system_clock(T1,rate)
call gmres_mlfma(matrices,mesh, otree)
Cabs1 = absorption_cross_section(matrices, mesh)

call system_clock(T2)
print*,'Matmul done in', real(T2-T1)/real(rate), 'seconds'
call system_clock(T1,rate)

F(:,:) = dcmplx(0.0,0.0)

!$omp parallel default(private) &
!$omp firstprivate(mp,np,abcd,R, phi,theta) &
!$omp shared(matrices, mesh, F)
!$omp do 
do m = 1,mp

   phi_sca = 2.0_dp*pi*dble(m-1)/dble(mp)
   mat(1,1) = cos(phi_sca)
   mat(1,2) = sin(phi_sca)
   mat(2,1) = sin(phi_sca)
   mat(2,2) = -cos(phi_sca)
   abcd2 = matmul(abcd,mat)

  

   do n = 1,np
         
      theta_sca = pi*dble(n-1)/dble(np) + pi/2.0_dp/dble(np)
      obs_point = sph2cart(R,theta_sca,phi_sca)
      obs_point = rotation(obs_point,2,theta)
      obs_point = rotation(obs_point,3,phi)
    
    
      E = farfield_efie(matrices, mesh, obs_point)
    !  E = fields(matrices, mesh, obs_point)
    
      sca_vec = sph_unit_vectors(theta_sca,phi_sca)
      sca_vec(:,2) =  rotation(sca_vec(:,2),2,theta)
      sca_vec(:,2) =  rotation(sca_vec(:,2),3,phi)

      sca_vec(:,3) =  rotation(sca_vec(:,3),2,theta)
      sca_vec(:,3) =  rotation(sca_vec(:,3),3,phi)

      A = cdexp(dcmplx(0.0, mesh%k*(R))) / (mesh%k*R) 

      las = (m-1)*np + n
      F(las,1) = 1.0_dp/A  * sum(E * sca_vec(:,2))
      F(las,3) = 1.0_dp/A  * sum(E * sca_vec(:,3))
     

   end do

end do
!$omp end do
!$omp end parallel

!E = fields(matrices, mesh, matrices%khat*R)
E = farfield_efie(matrices, mesh, matrices%khat*R)

 A = cdexp(dcmplx(0.0, mesh%k*(R))) / R
 
 Cext1 = vlen(imag(E/A) * 4.0_dp*pi / mesh%k)

call system_clock(T2)
print*,'Fields done in', real(T2-T1)/real(rate), 'seconds'

matrices%E0 = inc_vec(:,3)

call rhs(matrices, mesh)

!print*,'Solving...'
call system_clock(T1,rate)
call gmres_mlfma(matrices,mesh,otree)
Cabs2 = absorption_cross_section(matrices, mesh)
!print*, 'Cabs2 =', Cabs2
call system_clock(T2)
!print*,'Done in', real(T2-T1)/real(rate), 'seconds'

!print*,'Compute fields...'
call system_clock(T1,rate)

!$omp parallel default(private) &
!$omp firstprivate(mp,np,abcd,R, phi,theta) &
!$omp shared(matrices, mesh, F)
!$omp do 
do m = 1,mp

   phi_sca = 2.0_dp*pi*dble(m-1)/dble(mp)
   mat(1,1) = cos(phi_sca)
   mat(1,2) = sin(phi_sca)
   mat(2,1) = sin(phi_sca)
   mat(2,2) = -cos(phi_sca)
   abcd2 = matmul(abcd,mat)

   do n = 1,np
        
      theta_sca = pi*dble(n-1)/dble(np) + pi/2.0_dp/dble(np)
     
      obs_point = sph2cart(R,theta_sca,phi_sca)
      obs_point = rotation(obs_point,2,theta)
      obs_point = rotation(obs_point,3,phi)
       
      E = farfield_efie(matrices, mesh, obs_point)     
     ! E = fields(matrices, mesh, obs_point)

      sca_vec = sph_unit_vectors(theta_sca,phi_sca)
      sca_vec(:,2) =  rotation(sca_vec(:,2),2,theta)
      sca_vec(:,2) =  rotation(sca_vec(:,2),3,phi)

      sca_vec(:,3) =  rotation(sca_vec(:,3),2,theta)
      sca_vec(:,3) =  rotation(sca_vec(:,3),3,phi)

      ksca = obs_point/R * mesh%k

      A = cdexp(dcmplx(0.0, mesh%k*(R) )) / (mesh%k*R) 

      las = (m-1)*np + n
      F(las,2) = 1.0_dp/A  * sum(E * sca_vec(:,2))
      F(las,4) = 1.0_dp/A  * sum(E * sca_vec(:,3))
      F(las,5) = theta_sca

   end do
end do
!$omp end do
!$omp end parallel

 !E = fields(matrices, mesh, matrices%khat*R)
 E = farfield_efie(matrices, mesh, matrices%khat*R)

 A = cdexp(dcmplx(0.0, mesh%k*(R))) / R
 
 Cext2 = vlen(imag(E/A) * 4.0_dp*pi / mesh%k)

call system_clock(T2)
!print*,'Done in', real(T2-T1)/real(rate), 'seconds'
print*,'Fields done in', real(T2-T1)/real(rate), 'seconds'


do m = 1,mp
   phi_sca = 2.0_dp*pi*dble(m-1)/dble(mp)
   mat(1,1) = cos(phi_sca)
   mat(1,2) = sin(phi_sca)
   mat(2,1) = sin(phi_sca)
   mat(2,2) = -cos(phi_sca)
   abcd2 = matmul(abcd,mat)

   ind1 = (m-1)*np + 1
   ind2 = (m)*(np)

   S(ind1:ind2,2) =  -dcmplx(0.0, 1.0) * (F(ind1:ind2,1) * abcd2(1,1) + F(ind1:ind2,2) * abcd2(2,1))
   S(ind1:ind2,3) =  dcmplx(0.0, 1.0) * (F(ind1:ind2,1) * abcd2(1,2) + F(ind1:ind2,2) * abcd2(2,2))
   S(ind1:ind2,4) =  dcmplx(0.0, 1.0) * (F(ind1:ind2,3) * abcd2(1,1) + F(ind1:ind2,4) * abcd2(2,1))
   S(ind1:ind2,1) =  -dcmplx(0.0, 1.0) * (F(ind1:ind2,3) * abcd2(1,2) + F(ind1:ind2,4) * abcd2(2,2)) 

   S(ind1:ind2,5) = F(ind1:ind2,5)

end do



mueller(:,1) = real(S(:,5))*180.0_dp/pi

mueller(:,2) = 0.5_dp * (abs(S(:,1))**2.0_dp + abs(S(:,2))**2.0_dp + abs(S(:,3))**2.0_dp + abs(S(:,4))**2.0_dp)
mueller(:,3) = 0.5_dp * (abs(S(:,2))**2.0_dp - abs(S(:,1))**2.0_dp + abs(S(:,4))**2.0_dp - abs(S(:,3))**2.0_dp)
mueller(:,4) = -(real(S(:,2)*conjg(S(:,3)) + S(:,1)*conjg(S(:,4))))
mueller(:,5) = imag(S(:,2)*conjg(S(:,3)) - S(:,1)*conjg(S(:,4)))

mueller(:,6) = 0.5_dp * (abs(S(:,2))**2.0_dp - abs(S(:,1))**2.0_dp + abs(S(:,3))**2.0_dp - abs(S(:,4))**2.0_dp)
mueller(:,7) = 0.5_dp * (abs(S(:,1))**2.0_dp + abs(S(:,2))**2.0_dp - abs(S(:,3))**2.0_dp - abs(S(:,3))**2.0_dp)
mueller(:,8) = real(S(:,2)*conjg(S(:,3)) - S(:,1)*conjg(S(:,4)))
mueller(:,9) = imag(S(:,2)*conjg(S(:,3)) + S(:,1)*conjg(S(:,4)))

mueller(:,10) = real(S(:,2)*conjg(S(:,4)) + S(:,1)*conjg(S(:,3)))
mueller(:,11) = -(real(S(:,2)*conjg(S(:,4)) - S(:,1)*conjg(S(:,3))))
mueller(:,12) = -(real(S(:,1)*conjg(S(:,2)) + S(:,3)*conjg(S(:,4))))
mueller(:,13) = -(imag(S(:,2)*conjg(S(:,1)) + S(:,4)*conjg(S(:,3))))

mueller(:,14) = -(imag(S(:,4)*conjg(S(:,2)) + S(:,1)*conjg(S(:,3))))
mueller(:,15) = -(imag(S(:,4)*conjg(S(:,2)) - S(:,1)*conjg(S(:,3))))
mueller(:,16) = -(imag(S(:,1)*conjg(S(:,2)) - S(:,3)*conjg(S(:,4))))
mueller(:,17) = -(real(S(:,1)*conjg(S(:,2)) - S(:,3)*conjg(S(:,4))))


Cabs = (Cabs1 + Cabs2)/2.0_dp
Cext = (Cext1 + Cext2)/2.0_dp

print*, 'Cabs =', Cabs
print*, 'Cext =', Cext

crs(1) = Cabs
crs(2) = Cext

Csca = 0._dp
do m = 1, mp*np

   Csca = Csca + mueller(m,2)*sin(mueller(m,1)/180.0_dp*pi)/dble(mp)/dble(np) * pi * 2.0_dp* pi/mesh%k**2.0_dp 
   !Csca = Csca + sin(mueller(m,1)/180d0*pi)/mp/(np-1) *pi
end do
crs(3) = Csca
print*, 'Csca =', Csca
end function compute_mueller_ave

!____________________________________________________________________________
!
!
!
!____________________________________________________________________________-

function compute_mueller(matrices, mesh, otree, phi, theta, np, phi_sca) result(mueller)
type (data) :: matrices
type (mesh_struct) :: mesh
type (level_struct) :: otree(:)
real(dp) :: phi, theta, phi_sca
integer :: np, my_id
real(dp) :: inc_vec(3,3), theta_sca, R, abcd(2,2), mat(2,2) 
real(dp) :: obs_point(3), sca_vec(3,3), abcd2(2,2)
integer :: T1, T2, rate, n, ierr, cont
real(dp), dimension(:,:), allocatable :: mueller
complex(dp) :: E(3), S(np,5), A, F(np,5)



allocate(mueller(np,17))
mueller(:,:) = 0.0_dp


R  = 100000.0_dp * 2.0_dp*pi/mesh%k 

inc_vec = sph_unit_vectors(theta,phi)

abcd(1,1) = 1.0_dp!inc_vec(:,2)
abcd(1,2) = 0.0_dp!inc_vec(:,2)
abcd(2,1) = 0.0_dp!inc_vec(:,3)
abcd(2,2) = 1.0_dp!inc_vec(:,3)


mat(1,1) = cos(phi_sca)
mat(1,2) = sin(phi_sca)
mat(2,1) = sin(phi_sca)
mat(2,2) = -cos(phi_sca)
abcd2 = matmul(abcd,mat)



matrices%khat = inc_vec(:,1)
matrices%E0 = inc_vec(:,2)

call rhs(matrices, mesh)

print*,'Solving...'
call system_clock(T1,rate)
!matrices%x = matmul(matrices%A, matrices%rhs)
call gmres_mlfma(matrices, mesh,otree)
call system_clock(T2)
print*,'Done in', real(T2-T1)/real(rate), 'seconds'


do n = 1,np

   theta_sca = pi*dble(n-1)/dble(np) + pi/2.0_dp/dble(np)
 
   obs_point = sph2cart(R,theta_sca,phi_sca)
   obs_point = rotation(obs_point,2,theta)
   obs_point = rotation(obs_point,3,phi)

 
   E = fields(matrices, mesh, obs_point)
 
     
   sca_vec = sph_unit_vectors(theta_sca,phi_sca)
   sca_vec(:,2) =  rotation(sca_vec(:,2),2,theta)
   sca_vec(:,2) =  rotation(sca_vec(:,2),3,phi)

   sca_vec(:,3) =  rotation(sca_vec(:,3),2,theta)
   sca_vec(:,3) =  rotation(sca_vec(:,3),3,phi)

   A = cdexp(dcmplx(0.0, mesh%k*(R) )) / (mesh%k*R) 

   F(n+1,1) = 1.0_dp/A  * sum(E * sca_vec(:,2)) !f11
   F(n+1,3) = 1.0_dp/A  * sum(E * sca_vec(:,3)) !ff21 
end do



matrices%E0 = inc_vec(:,3)
call rhs(matrices, mesh)


print*,'Solving...'
call system_clock(T1,rate)
call gmres_mlfma(matrices, mesh,otree)
call system_clock(T2)
print*,'Done in', real(T2-T1)/real(rate), 'seconds'



do n = 1,np

   theta_sca = pi*dble(n-1)/dble(np) + pi/2.0_dp/dble(np)
  
   obs_point = sph2cart(R,theta_sca,phi_sca)
   obs_point = rotation(obs_point,2,theta)
   obs_point = rotation(obs_point,3,phi)

   E = fields(matrices, mesh, obs_point)

  
  
   sca_vec = sph_unit_vectors(theta_sca,phi_sca)
   sca_vec(:,2) =  rotation(sca_vec(:,2),2,theta)
   sca_vec(:,2) =  rotation(sca_vec(:,2),3,phi)

   sca_vec(:,3) =  rotation(sca_vec(:,3),2,theta)
   sca_vec(:,3) =  rotation(sca_vec(:,3),3,phi)

   A = cdexp(dcmplx(0.0, mesh%k*(R) )) / (mesh%k*R) 

   F(n+1,2) = 1.0_dp/A  * sum(E * sca_vec(:,2)) !f12
   F(n+1,4) = 1.0_dp/A  * sum(E * sca_vec(:,3)) !f22
   S(n+1,5) = theta_sca
end do



S(:,2) =  -dcmplx(0.0, 1.0) * (F(:,1) * abcd2(1,1) + F(:,2) * abcd2(2,1))
S(:,3) =  dcmplx(0.0, 1.0) * (F(:,1) * abcd2(1,2) + F(:,2) * abcd2(2,2))
S(:,4) =  dcmplx(0.0, 1.0) * (F(:,3) * abcd2(1,1) + F(:,4) * abcd2(2,1))
S(:,1) =  -dcmplx(0.0, 1.0) * (F(:,3) * abcd2(1,2) + F(:,4) * abcd2(2,2))

mueller(:,1) = real(S(:,5))*180.0_dp/pi

mueller(:,2) = 0.5_dp * (abs(S(:,1))**2.0_dp + abs(S(:,2))**2.0_dp + abs(S(:,3))**2.0_dp + abs(S(:,4))**2.0_dp)
mueller(:,3) = 0.5_dp * (abs(S(:,2))**2.0_dp - abs(S(:,1))**2.0_dp + abs(S(:,4))**2.0_dp - abs(S(:,3))**2.0_dp)
mueller(:,4) = -(real(S(:,2)*conjg(S(:,3)) + S(:,1)*conjg(S(:,4))))
mueller(:,5) = imag(S(:,2)*conjg(S(:,3)) - S(:,1)*conjg(S(:,4)))

mueller(:,6) = 0.5_dp * (abs(S(:,2))**2.0_dp - abs(S(:,1))**2.0_dp + abs(S(:,3))**2.0_dp - abs(S(:,4))**2.0_dp)
mueller(:,7) = 0.5_dp * (abs(S(:,1))**2.0_dp + abs(S(:,2))**2.0_dp - abs(S(:,3))**2.0_dp - abs(S(:,3))**2.0_dp)
mueller(:,8) = real(S(:,2)*conjg(S(:,3)) - S(:,1)*conjg(S(:,4)))
mueller(:,9) = imag(S(:,2)*conjg(S(:,3)) + S(:,1)*conjg(S(:,4)))

mueller(:,10) = real(S(:,2)*conjg(S(:,4)) + S(:,1)*conjg(S(:,3)))
mueller(:,11) = -(real(S(:,2)*conjg(S(:,4)) - S(:,1)*conjg(S(:,3))))
mueller(:,12) = -(real(S(:,1)*conjg(S(:,2)) + S(:,3)*conjg(S(:,4))))
mueller(:,13) = -(imag(S(:,2)*conjg(S(:,1)) + S(:,4)*conjg(S(:,3))))

mueller(:,14) = -(imag(S(:,4)*conjg(S(:,2)) + S(:,1)*conjg(S(:,3))))
mueller(:,15) = -(imag(S(:,4)*conjg(S(:,2)) - S(:,1)*conjg(S(:,3))))
mueller(:,16) = -(imag(S(:,1)*conjg(S(:,2)) - S(:,3)*conjg(S(:,4))))
mueller(:,17) = -(real(S(:,1)*conjg(S(:,2)) - S(:,3)*conjg(S(:,4))))

!call write2file(S)
!print*, my_id


end function compute_mueller

!______ Orientation averaging routine_____________1

subroutine orientation_ave(mesh,matrices, otree, N_theta,M,halton_init, mueller_ave, crs)
type (data) :: matrices
type (mesh_struct) :: mesh
type (level_struct) :: otree(:)
real(dp), dimension(:,:), allocatable :: mueller, mueller_ave
integer :: M1,M2,M,N, i, N_theta, i1, halton_init, my_id, ierr
real(dp) :: vec(3), Cabs_ave, Cabs, crs(3), Cext_ave, Csca_ave, crs2(3)
 
!N = M
!N = int(floor(sqrt(dble(M))*2))
N = 64

!if(my_id == 0) then
allocate(mueller(N_theta*N,17))
allocate(mueller_ave(N_theta,17))
mueller_ave(:,:) = 0.0_dp 

Cabs_ave = 0.0_dp
Cext_ave = 0.0_dp
Csca_ave = 0.0_dp
crs(:) = 0.0_dp

 
do i = 1,M

   vec(2) = acos(2.0_dp*halton_seq(halton_init+i, 2)-1.0_dp)
   vec(3) = halton_seq(halton_init+i, 3)*2.0_dp*pi

   mueller = compute_mueller_ave(matrices, mesh, otree, vec(3), vec(2), N_theta, N, crs2) 
!print*,'kkkkkkkk'
   !if(my_id == 0) then
   do i1 = 1,N
      mueller_ave = mueller_ave + mueller(N_theta*(i1-1)+1:N_theta*i1,:)/dble(N*M)
     
   end do
   Cabs_ave = Cabs_ave + crs2(1)/dble(M)
   Cext_ave = Cext_ave + crs2(2)/dble(M)
   Csca_ave = Csca_ave + crs2(3)/dble(M)
   crs = crs + crs2/dble(M)
   print*,'Orientation averaging', i,'/',M
   !end if 
end do


Print*, 'Cabs =', Cabs_ave
Print*, 'Cext =', Cext_ave
Print*, 'Csca =', Csca_ave


end subroutine orientation_ave

end module solver_mlfma
