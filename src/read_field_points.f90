subroutine read_field_points(matrices)
use common
use hdf5
implicit none

type (data) :: matrices 

CHARACTER(LEN=18), PARAMETER :: file = "field_points.h5" ! File name
CHARACTER(LEN=16), PARAMETER :: dataset1 = "coord"     ! Dataset name


integer(HID_T) :: file_id 
integer(HID_T) :: dataset1_id
integer(HID_T) :: dataspace_id
integer(HSIZE_T), dimension(2) :: coord_dims, dims_out

integer :: error

double precision, dimension(:,:), allocatable :: coord

call h5open_f(error) ! initialize interface
call h5fopen_f(file, H5F_ACC_RDWR_F, file_id, error) ! open file


call h5dopen_f(file_id, dataset1, dataset1_id, error) ! open dataset

call h5dget_space_f(dataset1_id, dataspace_id, error) 
call H5sget_simple_extent_dims_f(dataspace_id, dims_out, coord_dims, error)

allocate(coord(coord_dims(1), coord_dims(2)))
call h5dread_f(dataset1_id, H5T_NATIVE_DOUBLE, coord, coord_dims, error)

call h5dclose_f(dataset1_id, error) ! close dataset

call h5fclose_f(file_id, error) ! close file
call h5close_f(error) ! close inteface

matrices%field_points = coord

end subroutine read_field_points
