module translation
use common
use sfunctions
use octtree
use integration_points
!use radiation_patterns
use cbessel
implicit none

contains

function rokhlinorder(level, acc) result(L)
integer :: level, acc, L
integer :: T(15,10)

T(:,:) = 0

T(1,:) = [21181, 22315, 22348, 22375, 22398, 22421, 22440, 22460, 22477, 22493]
T(2,:) = [10631, 11167, 11194, 11214, 11233, 11250, 11266, 11282, 11296, 11309]
T(3,:) = [5350,  5593,  5613,  5630,  5643,  5659,  5669,  5682,  5692,  5702]
T(4,:) = [2778,  2805,  2821,  2834,  2844,  2854,  2864,  2874,  2883,  2891]
T(5,:) = [1390,  1410,  1420,  1430,  1440,  1447,  1456,  1463,  1470,  1477]
T(6,:) = [697,   710,   720,   727,   733,   740,   746,   753,   759,   763]
T(7,:) = [348,   358,   365,   372,   378,   384,   388,   394,   398,   402]
T(8,:) = [174,   184,   190,   194,   198,   204,   210,   217,   223,   227]
T(9,:) = [89,    95,    99,   105,   112,   121,   131,   141,   148,   148]
T(10,:) = [46,    50,    56,    65,    75,    88,   101,   115,   131,   144]
T(11,:) = [23,    29,    39,    49,    65,    78,    92,   108,   124,   138]
T(12,:) = [13,    19,    32,    46,    62,    75,    91,   105,   121,   137]
T(13,:) = [7,    16,    29,    45,    59,    75,    91,   105,   121,   137]
T(14,:) = [6,    16,    29,    45,    59,    75,    91,   105,   121,   137]
T(15,:) = [3,    16,    29,    45,    59,    75,    91,   105,   121,   137]

L = T(12-level+1, acc)

end function rokhlinorder


function rokhlinorder2(ka, acc) result(L)
integer :: acc, L
integer :: T(15,10)
double precision :: ka
!L = floor(1.73*ka + 2.16*acc**2.0/3.0 * ka**1.0/3.0)
!L = floor(ka + 6.0d0*ka**1.0d0/3.0d0)
!L = floor(ka + acc*ka**(1.0/3.0))
L = floor(ka + 1.8*acc**(2.0/3.0) * ka**(1.0/3.0))
!if(L<6) L = 6
end function rokhlinorder2

function sampleorder(ka, acc) result(L)
integer :: acc, L
integer :: T(15,10)
double precision :: ka
!L = floor(1.73*ka + 2.16*acc**2.0/3.0 * ka**1.0/3.0)
!L = floor(ka + 6.0d0*ka**1.0d0/3.0d0)
!L = floor(2.0*(ka + acc*ka**(1.0/3.0)))
L = floor(ka + 1.8*acc**(2.0/3.0) * ka**(1.0/3.0))
!if(L<6) L = 6
end function sampleorder


subroutine radiation_pattern(otree, mesh, matrices)
type (level_struct), dimension(:) :: otree
type (mesh_struct) :: mesh
type (data) :: matrices
integer :: level

integer :: cube, tri, L, acc, M, N, ele, i1, box, loc, i2, be, bi, bj, basis
double precision :: rpc(3), rp(3), B_coord(3,3), khat(3), el, signb
double precision, dimension(:,:), allocatable :: P, P0_tri
double precision, dimension(:), allocatable :: w, w0_tri
double complex, dimension(:), allocatable :: f
double precision :: B_dN(3,3), B_nxdN(3,3), B_nvec(3), alok(3), dyad(3,3), eye(3,3), vec(3), vec2(3,3)
double complex, dimension(3) :: intN 
double precision, dimension(:,:), allocatable :: Pt, shape_tri
double precision, dimension(:), allocatable :: wt
double complex :: a(3)


level = size(otree)
acc = 3
!L = rokhlinorder(level, acc)

L = rokhlinorder2(mesh%k*otree(level)%tree(1)%dl, acc)
M = L+1
N= 2*(L+1)

call inttri(P0_tri,w0_tri,5)
call sample_points(P,w,M,N)

otree(level)%P = P
otree(level)%w = w
otree(level)%L = L

allocate(shape_tri(3,size(w0_tri)))
allocate(Pt(3,size(w0_tri)))
allocate(wt(size(w0_tri)))

shape_tri(1,:) = 1.0d0 - P0_tri(1,:) - P0_tri(2,:)
shape_tri(2,:) = P0_tri(1,:)
shape_tri(3,:) = P0_tri(2,:)

eye(:,:) = 0.0d0
eye(1,1) = 1.0d0
eye(2,2) = 1.0d0
eye(3,3) = 1.0d0

do box = 1, size(otree(level)%tree)
print*, box
   allocate(otree(level)%tree(box)%rad_pattern_theta(otree(level)%tree(box)%N_source*3,size(w)))
   allocate(otree(level)%tree(box)%rad_pattern_phi(otree(level)%tree(box)%N_source*3,size(w)))
 !  allocate(otree(level)%tree(box)%rad_pattern_3(otree(level)%tree(box)%N_source*3,size(w)))

   allocate(otree(level)%tree(box)%in_pattern_theta(size(w)))
   allocate(otree(level)%tree(box)%in_pattern_phi(size(w)))
 !  allocate(otree(level)%tree(box)%in_pattern_3(size(w)))
   
   otree(level)%tree(box)%in_pattern_theta(:) = dcmplx(0.0,0.0)
   otree(level)%tree(box)%in_pattern_phi(:) = dcmplx(0.0,0.0)
!   otree(level)%tree(box)%in_pattern_3(:) = dcmplx(0.0,0.0)
   
   rpc = otree(level)%tree(box)%cp
   do loc = 1, otree(level)%tree(box)%N_source
      
      ele = otree(level)%tree(box)%sources(loc)

      B_coord = mesh%coord(:,mesh%etopol(:,ele))
      call gradshape_tri(B_dN,B_nxdN, B_nvec, B_coord)

      call linmap_tri(Pt, wt, B_coord, P0_tri, w0_tri)

        
      do be = 1,3
         basis = mesh%etopol_edges(be,ele)
        
         el = norm(mesh%coord(:,mesh%edges(1,basis)), mesh%coord(:,mesh%edges(2,basis)))

         bi = mesh%Eori(1,be)
         bj = mesh%Eori(2,be)

          
         if(mesh%boundary(basis) == 0) then
            signb = RWG_sign(mesh, basis, ele,be)
           
            do i1 = 1, size(w)
               khat = P(:,i1)
               
               intN(:) = dcmplx(0.0,0.0)
      
               do i2 = 1, size(w0_tri)
           
                  rp = Pt(:,i2)         
                  intN = intN + shape_tri(:,i2) * exp(dcmplx(0.0,-1.0) * mesh%k &
                       * dot_product(khat, rp-rpc)) * wt(i2)

               end do
              
               dyad = eye - kkdyad(khat)

           
               a =  matmul(dyad, signb * el * (B_nxdN(:,bj)*intN(bi) - B_nxdN(:,bi)*intN(bj)))

              
               vec =  cart2sph(khat)
               vec2 = sph_unit_vectors(vec(2),vec(3))
               
               otree(level)%tree(box)%rad_pattern_theta((be-1)*otree(level)%tree(box)%N_source + loc,i1) = &
                    dot_product(vec2(:,2), a)
               otree(level)%tree(box)%rad_pattern_phi((be-1)*otree(level)%tree(box)%N_source + loc,i1) = &
                    dot_product(vec2(:,3), a)
         !       otree(level)%tree(box)%rad_pattern_3((be-1)*otree(level)%tree(box)%N_source + loc,i1) = a(3)

             !  print*,'r', dot_product(vec2(:,1), a)
             !  print*,'theta', dot_product(vec2(:,2), a)
             !  print*,'phi', dot_product(vec2(:,3), a)
               
            end do
            
         end if
      end do
   end do
end do
!print*, otree(level)%tree(49)%rad_pattern_theta

print*, 'radiation pattern done'
end subroutine radiation_pattern

!*****************************************************************************
subroutine radiation_pattern_edge(otree, mesh, matrices)
type (level_struct), dimension(:) :: otree
type (mesh_struct) :: mesh
type (data) :: matrices
integer :: level

integer :: cube, tri, L, acc, M, N, ele, i1, box, loc, i2, be, bi, bj, basis, bt
double precision :: rpc(3), rp(3), B_coord(3,3), khat(3), el, signb
double precision, dimension(:,:), allocatable :: P, P0_tri
double precision, dimension(:), allocatable :: w, w0_tri
double complex, dimension(:), allocatable :: f
double precision :: B_dN(3,3), B_nxdN(3,3), B_nvec(3), alok(3), dyad(3,3), eye(3,3), vec(3), vec2(3,3)
double complex, dimension(3) :: intN 
double precision, dimension(:,:), allocatable :: Pt, shape_tri
double precision, dimension(:), allocatable :: wt
double complex :: a(3)
integer :: basis_tri(2)

level = size(otree)

acc = 4
!L = rokhlinorder(level, acc)
L = rokhlinorder2(mesh%k*otree(level)%tree(1)%dl, acc)
M = L+1
N= 2*(L+1)

call inttri(P0_tri,w0_tri,5)
!P = otree(level)%P
!w = otree(level)%w
call sample_points(P,w,M,N)


otree(level)%P = P
otree(level)%w = w
otree(level)%L = L





allocate(shape_tri(3,size(w0_tri)))
allocate(Pt(3,size(w0_tri)))
allocate(wt(size(w0_tri)))

shape_tri(1,:) = 1.0d0 - P0_tri(1,:) - P0_tri(2,:)
shape_tri(2,:) = P0_tri(1,:)
shape_tri(3,:) = P0_tri(2,:)

eye(:,:) = 0.0d0
eye(1,1) = 1.0d0
eye(2,2) = 1.0d0
eye(3,3) = 1.0d0

do box = 1, size(otree(level)%tree)

   allocate(otree(level)%tree(box)%rad_pattern_theta(otree(level)%tree(box)%N_source,size(w)))
   allocate(otree(level)%tree(box)%rad_pattern_phi(otree(level)%tree(box)%N_source,size(w)))

   otree(level)%tree(box)%rad_pattern_theta(:,:) = dcmplx(0.0,0.0)
   otree(level)%tree(box)%rad_pattern_phi(:,:) = dcmplx(0.0,0.0)
   
   allocate(otree(level)%tree(box)%in_pattern_theta(size(w)))
   allocate(otree(level)%tree(box)%in_pattern_phi(size(w)))
 
   
   otree(level)%tree(box)%in_pattern_theta(:) = dcmplx(0.0,0.0)
   otree(level)%tree(box)%in_pattern_phi(:) = dcmplx(0.0,0.0)

   
   rpc = otree(level)%tree(box)%cp
   do loc = 1, otree(level)%tree(box)%N_source
      
      basis = otree(level)%tree(box)%sources(loc)
      basis_tri = mesh%edgesT(:,basis)
      

      do bt = 1, 2

         B_coord = mesh%coord(:,mesh%etopol(:,basis_tri(bt)))
         
         call gradshape_tri(B_dN,B_nxdN, B_nvec, B_coord)        
         call linmap_tri(Pt, wt, B_coord, P0_tri, w0_tri)

         do be = 1, 3                                  
            if(basis == mesh%etopol_edges(be,basis_tri(bt))) exit
         end do

         el = norm(mesh%coord(:,mesh%edges(1,basis)), mesh%coord(:,mesh%edges(2,basis)))

         bi = mesh%Eori(1,be)
         bj = mesh%Eori(2,be)
          
         if(mesh%boundary(basis) == 0) then
            signb = RWG_sign(mesh, basis, basis_tri(bt),be)
           
            do i1 = 1, size(w)
               khat = P(:,i1)
               
               intN(:) = dcmplx(0.0,0.0)
      
               do i2 = 1, size(w0_tri)
           
                  rp = Pt(:,i2)         
                  intN = intN + shape_tri(:,i2) * exp(dcmplx(0.0,-1.0) * mesh%k &
                       * dot_product(khat, rp-rpc)) * wt(i2)

               end do
              
               dyad = eye - kkdyad(khat)

           
               a =  matmul(dyad, signb * el * (B_nxdN(:,bj)*intN(bi) - B_nxdN(:,bi)*intN(bj)))

              
               vec =  cart2sph(khat)
               vec2 = sph_unit_vectors(vec(2),vec(3))
               
               otree(level)%tree(box)%rad_pattern_theta(loc,i1) = otree(level)%tree(box)%rad_pattern_theta(loc,i1) + &
                    dot_product(vec2(:,2), a)
               otree(level)%tree(box)%rad_pattern_phi(loc,i1) = otree(level)%tree(box)%rad_pattern_phi(loc,i1) + &
                    dot_product(vec2(:,3), a)
        
            end do
            
         end if
      end do
   end do
end do
!print*, otree(level)%tree(49)%rad_pattern_theta

print*, 'radiation pattern done'
end subroutine radiation_pattern_edge

function translator(cp1, cp2, k, a, P, w, L) result(T)
  
  double precision :: cp1(3), cp2(3), k ,a
  complex(8), dimension(:), allocatable   :: T
  double precision, dimension(:,:) :: P
  double precision, dimension(:) :: w
  double precision, dimension(:), allocatable :: tt
  integer :: L, M, N, acc, i1
  double precision :: D(3)
   
  D = cp1 - cp2

  allocate(tt(size(w)))
  do i1 = 1,size(w)
     tt(i1) = dot_product(D,P(:,i1)) / vlen(D)
  end do

  allocate(T(size(w)))
  T = rokhlin(tt,L,vlen(D),dcmplx(k,0.0))

end function translator

function translator_pole(cp1, cp2, k, a, P, w, L) result(T)
  
  double precision :: cp1(3), cp2(3), k ,a
  complex(8), dimension(:), allocatable   :: T
  double precision, dimension(:,:) :: P
  double precision, dimension(:) :: w
  double precision, dimension(2) :: tt
  integer :: L, M, N, acc, i1
  double precision :: D(3), khat(3)
   
  D = cp1 - cp2
  khat = [0.0,0.0,1.0]
 
  tt(1) = dot_product(D,khat) / vlen(D)
  tt(2) = dot_product(D,-khat) / vlen(D)

  allocate(T(2))
  T = rokhlin(tt,L,vlen(D),dcmplx(k,0.0))

end function translator_pole



function rokhlin(t,L,D,k) result(y)
double precision, dimension(:) :: t
integer :: L, i1, n, m, kode, nz, ierr
double precision :: D
double complex :: k
double complex, dimension(size(t)) :: y, s
double complex, dimension(L+1, size(t)) :: p
double complex, dimension(L+1) :: h, jn, yn, h2 
real(8) :: fnu

h = sphankel(L,k*D)
!call sphj2(L, kd, L, jn, yn)
!call cspherebessel(L,k*D, jn, yn)
!h = jn + dcmplx(0.0,1.0)*yn

!kode = 1
!fnu = 0.5
!m = 1
!call cbesh(k*D, fnu, kode, m, L+1, h2, nz, ierr)

!h2 = sqrt(0.5*pi/(k*D))*h2
p = legendre(L,t)

!print*,h
!print*, '********'
!print*, h2
!h=h2
!stop
s(:) = dcmplx(0.0,0.0)

do n = 0, L
   do i1 = 1, size(t)
      s(i1) = s(i1) + (2.0*n+1.0) * dcmplx(0.0,1.0)**(n) * h(n+1) * p(n+1,i1)
   end do
end do

y =  dcmplx(0.0,1.0)*k / ((4*pi)**2)*s



end function rokhlin



subroutine matvec_mlfma(otree, mesh, matrices)
type (level_struct), dimension(:) :: otree
type (mesh_struct) :: mesh
type (data) :: matrices

integer :: i1, tind, bind, level, acc, L, M, N, ele, box, loc, be, basis, Ns, parent, test
integer :: i2, box2, near_box, i3, neighbour
complex(8), dimension(mesh%N_edge) :: Ax
double precision, dimension(:,:), allocatable :: P
double precision, dimension(:), allocatable :: w
complex(8), dimension(:), allocatable :: rad_theta, rad_phi, rad_3
double precision :: cp_box(3), cp_box2(3), box_dl, eta1
complex(8), dimension(:), allocatable :: T, F_th, F_phi, F_3
complex(8) :: no

eta1 = sqrt(mu/epsilon)
!************ sparse matvec multiplication *****************************

Ax(:) = dcmplx(0.0,0.0)
do i1 = 1, size(matrices%sparse_M)
   tind = matrices%ind_M(i1,1)
   bind = matrices%ind_M(i1,2)

   Ax(tind) =  Ax(tind) + matrices%sparse_M(i1) * matrices%x(bind)
end do


!*********************** Aggregation *************************************

level = size(otree)
box_dl =  otree(level)%tree(1)%dl
!acc = 4
!L = rokhlinorder(level, acc)
!L = rokhlinorder2(mesh%k*box_dl, acc)
!print*, 'level',l
!M = L+1
!N= 2*(L+1)

!call sample_points(P,w,M,N)

P = otree(level)%P 
w = otree(level)%w 
L = otree(level)%L 

allocate(rad_theta(size(w)))
allocate(rad_phi(size(w)))
!allocate(rad_3(size(w)))


allocate(F_th(size(w)))
allocate(F_phi(size(w)))
!allocate(F_3(size(w)))



do box = 1, size(otree(level)%tree)
  
   Ns = otree(level)%tree(box)%N_source
   cp_box =  otree(level)%tree(box)%cp ! from
   box_dl =  otree(level)%tree(box)%dl
   
   rad_theta(:) = dcmplx(0.0,0.0)
   rad_phi(:) = dcmplx(0.0,0.0)
   !   rad_3(:) = dcmplx(0.0,0.0)

   do loc = 1, Ns
      
      ele = otree(level)%tree(box)%sources(loc)
      do be = 1,3
         basis = mesh%etopol_edges(be,ele)
        
         rad_theta = rad_theta + otree(level)%tree(box)%rad_pattern_theta((be-1)*Ns + loc,:) * matrices%x(basis)
         rad_phi = rad_phi + otree(level)%tree(box)%rad_pattern_phi((be-1)*Ns + loc,:) * matrices%x(basis)
         !rad_3 = rad_3 + otree(level)%tree(box)%rad_pattern_3((be-1)*Ns + loc,:) * matrices%x(basis)
         !if(basis == 700) print*, box, 'box B'
      end do
   end do

   !**************** Transalation *******************************************
   
   
   parent = otree(level)%tree(box)%parent
   do i1 = 1, 27 ! near neighbours of parent cube
      neighbour = otree(level-1)%tree(parent)%near_neighbours(i1)
     
      if(neighbour == 0) exit
      
      do i2 = 1, 8 ! children of near neighbours
         box2 = otree(level-1)%tree(neighbour)%children(i2) ! level
         if(box2 == 0) exit

          ! ***check if near box ****
         near_box = 0
         do i3 = 1,27
            if(box2 == otree(level)%tree(box)%near_neighbours(i3)) then
               near_box = 1
            end if
         end do

         if(near_box == 0) then
            !print*, box2, box
            cp_box2 =  otree(level)%tree(box2)%cp !to
            T = translator(cp_box2, cp_box, mesh%k, box_dl, P, w, L)
            !print*, box_dl, vlen(cp_box-cp_box2)
            
            F_th = T*rad_theta
            F_phi = T*rad_phi
            !F_3 = T*rad_3

            otree(level)%tree(box2)%in_pattern_theta = otree(level)%tree(box2)%in_pattern_theta + F_th
            otree(level)%tree(box2)%in_pattern_phi = otree(level)%tree(box2)%in_pattern_phi + F_phi
            !otree(level)%tree(box2)%in_pattern_3 = otree(level)%tree(box2)%in_pattern_3 + F_3
            
            deallocate(T)
         end if
         
      end do
      
   end do
   !****************************************************
end do
   


!******************** disaggregation *************************************

no = mesh%k**2 /(dcmplx(0.0,1.0) * mesh%k) * eta1! (4.0*pi))**2 !/ eta1

do box = 1, size(otree(level)%tree)
   Ns = otree(level)%tree(box)%N_source
   cp_box =  otree(level)%tree(box)%cp ! from
   box_dl =  otree(level)%tree(box)%dl
    
   do loc = 1, Ns
      
      ele = otree(level)%tree(box)%sources(loc)
      do be = 1,3
         test = mesh%etopol_edges(be,ele)
       
         do i1 = 1, size(w)
            Ax(test) =  Ax(test) + conjg(otree(level)%tree(box)%rad_pattern_theta((be-1)*Ns + loc,i1)) &
                * otree(level)%tree(box)%in_pattern_theta(i1) * w(i1) * no
            !print*, otree(level)%tree(box)%in_pattern_theta(i1)
         
            Ax(test) =  Ax(test) + conjg(otree(level)%tree(box)%rad_pattern_phi((be-1)*Ns + loc,i1)) &
                 * otree(level)%tree(box)%in_pattern_phi(i1) * w(i1) * no

         !   Ax(test) =  Ax(test) + conjg(otree(level)%tree(box)%rad_pattern_3((be-1)*Ns + loc,i1)) &
         !        * otree(level)%tree(box)%in_pattern_3(i1) * w(i1) * no

            
                 
         end do
     
      end do
   end do
end do

matrices%Ax = Ax

end subroutine matvec_mlfma



subroutine matvec_fmm(otree, mesh, matrices)
type (level_struct), dimension(:) :: otree
type (mesh_struct) :: mesh
type (data) :: matrices

integer :: i1, tind, bind, level, acc, L, M, N, ele, box, loc, be, basis, Ns, parent, test
integer :: i2, box2, near_box, i3, neighbour
complex(8), dimension(mesh%N_edge) :: Ax
double precision, dimension(:,:), allocatable :: P
double precision, dimension(:), allocatable :: w
complex(8), dimension(:), allocatable :: rad_theta, rad_phi, rad_3
double precision :: cp_box(3), cp_box2(3), box_dl, eta1, signb
complex(8), dimension(:), allocatable :: T, F_th, F_phi, F_3
complex(8) :: no

eta1 = sqrt(mu/epsilon)
!************ sparse matvec multiplication *****************************

Ax(:) = dcmplx(0.0,0.0)
do i1 = 1, size(matrices%sparse_M)
   tind = matrices%ind_M(i1,1)
   bind = matrices%ind_M(i1,2)

   Ax(tind) =  Ax(tind) + matrices%sparse_M(i1) * matrices%x(bind)
end do


!*********************** Aggregation *************************************

level = size(otree)
box_dl =  otree(level)%tree(1)%dl

!acc = 4
!L = rokhlinorder(level, acc)
!L = rokhlinorder2(mesh%k*box_dl, acc)
!print*, 'level',l
!M = L+1
!N= 2*(L+1)

!call sample_points(P,w,M,N)

P = otree(level)%P 
w = otree(level)%w 
L = otree(level)%L

allocate(rad_theta(size(w)))
allocate(rad_phi(size(w)))

allocate(F_th(size(w)))
allocate(F_phi(size(w)))



do box = 1, size(otree(level)%tree)
  
   Ns = otree(level)%tree(box)%N_source
   cp_box =  otree(level)%tree(box)%cp ! from
   box_dl =  otree(level)%tree(box)%dl
   
   rad_theta(:) = dcmplx(0.0,0.0)
   rad_phi(:) = dcmplx(0.0,0.0)
  
   do loc = 1, Ns
      
      ele = otree(level)%tree(box)%sources(loc)
      do be = 1,3
         basis = mesh%etopol_edges(be,ele)
         !signb = RWG_sign(mesh, basis, ele,be)
         
         rad_theta = rad_theta + otree(level)%tree(box)%rad_pattern_theta((be-1)*Ns + loc,:) * matrices%x(basis)
         rad_phi = rad_phi + otree(level)%tree(box)%rad_pattern_phi((be-1)*Ns + loc,:) * matrices%x(basis)
       
      end do
   end do

   !**************** Transalation *******************************************
   
   
      
   do box2 = 1, size(otree(level)%tree) 
   
       
      ! ***check if near box ****
      near_box = 0
      do i3 = 1,27
         if(box2 == otree(level)%tree(box)%near_neighbours(i3)) then
            near_box = 1
         end if
      end do

      if(near_box == 0) then
         !print*, box2, box
         cp_box2 =  otree(level)%tree(box2)%cp !to
         T = translator(cp_box2, cp_box, mesh%k, box_dl, P, w, L)
         !print*, box_dl, vlen(cp_box-cp_box2)
            
         F_th = T*rad_theta
         F_phi = T*rad_phi
         !F_3 = T*rad_3

         otree(level)%tree(box2)%in_pattern_theta = otree(level)%tree(box2)%in_pattern_theta + F_th
         otree(level)%tree(box2)%in_pattern_phi = otree(level)%tree(box2)%in_pattern_phi + F_phi
         !otree(level)%tree(box2)%in_pattern_3 = otree(level)%tree(box2)%in_pattern_3 + F_3
            
         deallocate(T)
      end if
         
   end do
      
  
   !****************************************************
end do
   


!******************** disaggregation *************************************

no = mesh%k**2 /(dcmplx(0.0,1.0) * mesh%k) * eta1! (4.0*pi))**2 !/ eta1

do box = 1, size(otree(level)%tree)
   Ns = otree(level)%tree(box)%N_source
   cp_box =  otree(level)%tree(box)%cp ! from
   box_dl =  otree(level)%tree(box)%dl
    
   do loc = 1, Ns
      
      ele = otree(level)%tree(box)%sources(loc)
      do be = 1,3
         test = mesh%etopol_edges(be,ele)
         !signb = RWG_sign(mesh, test, ele,be)
         
         do i1 = 1, size(w)
            Ax(test) =  Ax(test) + conjg(otree(level)%tree(box)%rad_pattern_theta((be-1)*Ns + loc,i1)) &
                * otree(level)%tree(box)%in_pattern_theta(i1) * w(i1) * no
            !print*, otree(level)%tree(box)%in_pattern_theta(i1)
         
            Ax(test) =  Ax(test) + conjg(otree(level)%tree(box)%rad_pattern_phi((be-1)*Ns + loc,i1)) &
                 * otree(level)%tree(box)%in_pattern_phi(i1) * w(i1) * no

         !   Ax(test) =  Ax(test) + conjg(otree(level)%tree(box)%rad_pattern_3((be-1)*Ns + loc,i1)) &
         !        * otree(level)%tree(box)%in_pattern_3(i1) * w(i1) * no

            
                 
         end do
     
      end do
   end do
end do

matrices%Ax = Ax

end subroutine matvec_fmm

!*********************************************************************



subroutine matvec_fmm_edge(otree, mesh, matrices)
type (level_struct), dimension(:) :: otree
type (mesh_struct) :: mesh
type (data) :: matrices

integer :: i1, tind, bind, level, acc, L, M, N, ele, box, loc, be, basis, Ns, parent, test
integer :: i2, box2, near_box, i3, neighbour, las
complex(8), dimension(mesh%N_edge) :: Ax
double precision, dimension(:,:), allocatable :: P
double precision, dimension(:), allocatable :: w
complex(8), dimension(:), allocatable :: rad_theta, rad_phi
double precision :: cp_box(3), cp_box2(3), box_dl, eta1, signb
complex(8), dimension(:), allocatable :: T, F_th, F_phi
complex(8) :: no

eta1 = sqrt(mu/epsilon)
!************ sparse matvec multiplication *****************************

Ax(:) = dcmplx(0.0,0.0)
do i1 = 1, size(matrices%sparse_M)
   tind = matrices%ind_M(i1,1)
   bind = matrices%ind_M(i1,2)

   Ax(tind) =  Ax(tind) + matrices%sparse_M(i1) * matrices%x(bind)
end do


!*********************** Aggregation *************************************

level = size(otree)
box_dl =  otree(level)%tree(1)%dl
!acc = 4
!L = rokhlinorder(level, acc)
!L = rokhlinorder2(mesh%k*box_dl, acc)
!print*, 'level',l
!M = L+1
!N= 2*(L+1)

!call sample_points(P,w,M,N)

P = otree(level)%P 
w = otree(level)%w 
L = otree(level)%L

allocate(rad_theta(size(w)))
allocate(rad_phi(size(w)))

allocate(F_th(size(w)))
allocate(F_phi(size(w)))

do box = 1, size(otree(level)%tree)
   otree(level)%tree(box)%in_pattern_theta(:) = dcmplx(0.0,0.0)
   otree(level)%tree(box)%in_pattern_phi(:) = dcmplx(0.0,0.0)
end do

do box = 1, size(otree(level)%tree)
  
   Ns = otree(level)%tree(box)%N_source
   cp_box =  otree(level)%tree(box)%cp ! from
   box_dl =  otree(level)%tree(box)%dl
   
   rad_theta(:) = dcmplx(0.0,0.0)
   rad_phi(:) = dcmplx(0.0,0.0)
  
   do loc = 1, Ns
      
      basis = otree(level)%tree(box)%sources(loc)
   
      rad_theta = rad_theta + otree(level)%tree(box)%rad_pattern_theta(loc,:) * matrices%x(basis)
      rad_phi = rad_phi + otree(level)%tree(box)%rad_pattern_phi(loc,:) * matrices%x(basis)
      
   end do

   !**************** Transalation *******************************************
   
   
   las = 1   
   do box2 = 1, size(otree(level)%tree) 
   
       
      ! ***check if near box ****
      near_box = 0
      do i3 = 1,27
         if(box2 == otree(level)%tree(box)%near_neighbours(i3)) then
            near_box = 1
         end if
      end do

      if(near_box == 0) then
         !print*, box2, box
         cp_box2 =  otree(level)%tree(box2)%cp !to
         !T = translator(cp_box2, cp_box, mesh%k, box_dl, P, w, L)

         T = otree(level)%tree(box)%translator(:,las)
         !print*,  otree(level)%tree(box)%translator_rec_ind(las), box2
         !print*, box_dl, vlen(cp_box-cp_box2)
            
         F_th = T*rad_theta
         F_phi = T*rad_phi
         !F_3 = T*rad_3

         otree(level)%tree(box2)%in_pattern_theta = otree(level)%tree(box2)%in_pattern_theta + F_th
         otree(level)%tree(box2)%in_pattern_phi = otree(level)%tree(box2)%in_pattern_phi + F_phi
         !otree(level)%tree(box2)%in_pattern_3 = otree(level)%tree(box2)%in_pattern_3 + F_3
          las = las +1  
         deallocate(T)
      end if
         
   end do
      
  
   !****************************************************
end do
   


!******************** disaggregation *************************************

no = mesh%k**2 /(dcmplx(0.0,1.0) * mesh%k) * eta1! (4.0*pi))**2 !/ eta1

do box = 1, size(otree(level)%tree)
   Ns = otree(level)%tree(box)%N_source
   cp_box =  otree(level)%tree(box)%cp ! from
   box_dl =  otree(level)%tree(box)%dl
    
   do loc = 1, Ns
      
      test = otree(level)%tree(box)%sources(loc)
          
      do i1 = 1, size(w)
         
         Ax(test) =  Ax(test) + conjg(otree(level)%tree(box)%rad_pattern_theta(loc,i1)) &
              * otree(level)%tree(box)%in_pattern_theta(i1) * w(i1) * no
         
         Ax(test) =  Ax(test) + conjg(otree(level)%tree(box)%rad_pattern_phi(loc,i1)) &
              * otree(level)%tree(box)%in_pattern_phi(i1) * w(i1) * no
         
      end do
      
   end do
end do

matrices%Ax = Ax

end subroutine matvec_fmm_edge


!*******************************************************************************

subroutine compute_translators(otree, mesh, matrices)
type (level_struct), dimension(:) :: otree
type (mesh_struct) :: mesh
type (data) :: matrices

integer :: i1, level, acc, L, M, N, box, las
integer ::  box2, near_box

double precision, dimension(:,:), allocatable :: P
double precision, dimension(:), allocatable :: w
complex(8), dimension(:), allocatable :: T
double precision :: cp_box(3), cp_box2(3), box_dl



level = size(otree)
box_dl =  otree(level)%tree(1)%dl

P = otree(level)%P 
w = otree(level)%w 
L = otree(level)%L

allocate(T(size(w)))

do box = 1, size(otree(level)%tree)


   allocate(otree(level)%tree(box)%translator(size(w),size(otree(level)%tree)))
   allocate(otree(level)%tree(box)%translator_rec_ind(size(otree(level)%tree)))
   
   cp_box =  otree(level)%tree(box)%cp ! from
   box_dl =  otree(level)%tree(box)%dl
   
   las = 1    
   do box2 = 1, size(otree(level)%tree) 
        
      ! ***check if near box ****
      near_box = 0
      do i1 = 1,27
         if(box2 == otree(level)%tree(box)%near_neighbours(i1)) then
            near_box = 1
         end if
      end do

      if(near_box == 0) then
         !print*, box2, box
         cp_box2 =  otree(level)%tree(box2)%cp !to
         T = translator(cp_box2, cp_box, mesh%k, box_dl, P, w, L)
    
         otree(level)%tree(box)%translator(:,las) = T
         otree(level)%tree(box)%translator_rec_ind(las) = box2
         las = las + 1      
      end if
         
   end do
      
  
   !****************************************************
end do
   
end subroutine compute_translators


!*******************************************************************************


end module translation
