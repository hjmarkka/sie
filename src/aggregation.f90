module aggregation
  use octtree


contains

  
subroutine aggregate(otree, mesh, matrices)
type (level_struct), dimension(:) :: otree
type (mesh_struct) :: mesh
type (data) :: matrices

integer :: i1, tind, bind, level, acc, L, M, N, ele, box, loc, be, basis, Ns, parent, test
integer :: i2, box2, near_box, i3, neighbour, las
complex(8), dimension(mesh%N_edge) :: Ax
double precision, dimension(:,:), allocatable :: P
double precision, dimension(:), allocatable :: w
complex(8), dimension(:), allocatable :: rad_theta, rad_phi
double precision :: cp_box(3), cp_box2(3), box_dl, eta1, signb
complex(8), dimension(:) :: Fth, Fphi
complex(8) :: no


level = size(otree)
box_dl =  otree(level)%tree(1)%dl

P = otree(level)%P 
w = otree(level)%w 
L = otree(level)%L

allocate(rad_theta(size(w)))
allocate(rad_phi(size(w)))


! Finest level
do box = 1, size(otree(level)%tree)
  
   Ns = otree(level)%tree(box)%N_source
   cp_box =  otree(level)%tree(box)%cp ! from
   box_dl =  otree(level)%tree(box)%dl
   
   rad_theta(:) = dcmplx(0.0,0.0)
   rad_phi(:) = dcmplx(0.0,0.0)
  
   do loc = 1, Ns
      
      basis = otree(level)%tree(box)%sources(loc)
   
      rad_theta = rad_theta + otree(level)%tree(box)%rad_pattern_theta(loc,:) * matrices%x(basis)
      rad_phi = rad_phi + otree(level)%tree(box)%rad_pattern_phi(loc,:) * matrices%x(basis)
      
   end do

    otree(level)%tree(box)%out_pattern_theta = rad_theta
    otree(level)%tree(box)%out_pattern_phi = rad_phi
    
end do


do level = size(otree), 4, -1

   do box2 = 1, size(otree(level-1)%tree)
      otree(level-1)%tree(box2)%out_pattern_theta(:) = dcmplx(0.0,0.0)
      otree(level-1)%tree(box2)%out_pattern_phi(:) = dcmplx(0.0,0.0)
      
   end do
   
   do box = 1, size(otree(level)%tree)
      Ns = otree(level)%tree(box)%N_source
      cp_box =  otree(level)%tree(box)%cp ! from box
      box_dl =  otree(level)%tree(box)%dl

      box2 = otree(level)%tree(box)%parent
         
      cp_box2 =  otree(level-1)%tree(box2)%cp ! parent box
      box_dl2 =  otree(level-1)%tree(box2)%dl

      !Interpolation to level-1
      Fth = matmul(otree(level)%Intermat, otree(level)%tree(box)%out_pattern_theta) 
      Fphi = matmul(otree(level)%Intermat, otree(level)%tree(box)%out_pattern_phi)

      r = cp_box2-cp_box

      !Shift to the center of the parent box
      do i1 = 1, size(w)
         otree(level-1)%tree(box2)%out_pattern_theta =  otree(level-1)%tree(box2)%out_pattern_theta + &
              exp(cdmplx(0.0,1.0)*dot_product(otree(level-1)%P(:,i1),r)) * Fth(i1)
         otree(level-1)%tree(box2)%out_pattern_phi =  otree(level-1)%tree(box2)%out_pattern_phi + &
              exp(cdmplx(0.0,1.0)*dot_product(otree(level-1)%P(:,i1),r)) * Fphi(i1)
         
      end do
      
   end do
end do

end subroutine aggregate


!********************************************************************************

subroutine compute_translators_mlfma(otree, mesh, matrices)
type (level_struct), dimension(:) :: otree
type (mesh_struct) :: mesh
type (data) :: matrices

integer :: i1, level, acc, L, M, N, box, las, i2, i3
integer ::  box2, near_box, parent, neighbour

double precision, dimension(:,:), allocatable :: P
double precision, dimension(:), allocatable :: w
complex(8), dimension(:), allocatable :: T
double precision :: cp_box(3), cp_box2(3), box_dl


do level = size(otree), 3, -1

   level = size(otree)
   box_dl =  otree(level)%tree(1)%dl

   P = otree(level)%P 
   w = otree(level)%w 
   L = otree(level)%L

   allocate(T(size(w)))


   do box = 1, size(otree(level)%tree)

      allocate(otree(level)%tree(box)%translator(size(w),189))
      allocate(otree(level)%tree(box)%translator_rec_ind(189))

      cp_box =  otree(level)%tree(box)%cp ! from
      box_dl =  otree(level)%tree(box)%dl
      parent = otree(level)%tree(box)%parent

      las = 1
      do i1 = 1, 27 ! near neighbours of parent cube
         neighbour = otree(level-1)%tree(parent)%near_neighbours(i1)
     
         if(neighbour == 0) exit
      
         do i2 = 1, 8 ! children of near neighbours
            box2 = otree(level-1)%tree(neighbour)%children(i2) ! level
            if(box2 == 0) exit

            ! ***check if near box ****
            near_box = 0
            do i3 = 1,27
               if(box2 == otree(level)%tree(box)%near_neighbours(i3)) then
                  near_box = 1
               end if
            end do

            if(near_box == 0) then
   
               cp_box2 =  otree(level)%tree(box2)%cp !to
               T = translator(cp_box2, cp_box, mesh%k, box_dl, P, w, L)
    
               otree(level)%tree(box)%translator(:,las) = T
               otree(level)%tree(box)%translator_rec_ind(las) = box2
               las = las + 1      
            end if
         end do
      end do
   end do
      
   deallocate(T)
   !****************************************************
end do
   
end subroutine compute_translators_mlfma


  !******************** disaggregation *************************************

subroutine dissaggrgation(otree, mesh, matrices)
type (level_struct), dimension(:) :: otree
type (mesh_struct) :: mesh
type (data) :: matrices

complex(8), :: no
real(8) :: eta1, cp_box(3), cp_box2(3), box_dl, box2_dl
complex(8), dimension(mesh%N_edge) :: Ax
integer :: i1, tind, bind, box, box2, Ns, loc, test


eta1 = sqrt(mu/epsilon)
!************ sparse matvec multiplication *****************************

Ax(:) = dcmplx(0.0,0.0)
do i1 = 1, size(matrices%sparse_M)
   tind = matrices%ind_M(i1,1)
   bind = matrices%ind_M(i1,2)

   Ax(tind) =  Ax(tind) + matrices%sparse_M(i1) * matrices%x(bind)
end do


!*********************** Downward pass **************************

do level = 3, size(otree)-1

   do box = 1, size(otree(level)%tree)
      cp_box = otree(level)%tree(box)%cp ! from
      box_dl =  otree(level)%tree(box)%dl

      do i1 = 1, 8
         box2 = otree(level+1)%tree(box)%children(i1) ! level
         if(box2 == 0) exit
         cp_box2 = otree(level+1)%tree(box2)%cp ! from
         box2_dl =  otree(level+1)%tree(box2)%dl


      end do
      
   end do
end do


!**************** Finest level *********************************
no = mesh%k**2 /(dcmplx(0.0,1.0) * mesh%k) * eta1! (4.0*pi))**2 !/ eta1

do box = 1, size(otree(level)%tree)
   Ns = otree(level)%tree(box)%N_source
   cp_box =  otree(level)%tree(box)%cp ! from
   box_dl =  otree(level)%tree(box)%dl
    
   do loc = 1, Ns
      
      test = otree(level)%tree(box)%sources(loc)
          
      do i1 = 1, size(w)
         
         Ax(test) =  Ax(test) + conjg(otree(level)%tree(box)%rad_pattern_theta(loc,i1)) &
              * otree(level)%tree(box)%in_pattern_theta(i1) * w(i1) * no
         
         Ax(test) =  Ax(test) + conjg(otree(level)%tree(box)%rad_pattern_phi(loc,i1)) &
              * otree(level)%tree(box)%in_pattern_phi(i1) * w(i1) * no
         
      end do
      
   end do
end do

matrices%Ax = Ax

end subroutine dissaggrgation

end module aggregation
