module mlfma_pmchwt
  use common
  use octtree
  !use interpolation
  use integration_points
  use sfunctions
  !use cbessel
  use omp_lib
  implicit none

contains


function rokhlinorder2(ka, acc) result(L)
integer :: acc, L
integer :: T(15,10)
real(dp) :: ka
L = floor(ka + 1.8_dp*acc**(2.0_dp/3.0_dp) * ka**(1.0_dp/3.0_dp)) 
end function rokhlinorder2

function sampleorder(ka, acc) result(L)
integer :: acc, L
integer :: T(15,10)
real(dp) :: ka
L = floor(ka + 1.8_dp*acc**(2.0_dp/3.0_dp) * ka**(1.0_dp/3.0_dp)) 
end function sampleorder


function rokhlin(t,L,D,k) result(y)
real(dp), dimension(:) :: t
integer :: L, i1, n, m, kode, nz, ierr
real(dp) :: D
complex(dp) :: k
complex(dp), dimension(size(t)) :: y, s
complex(dp), dimension(L+1, size(t)) :: p
complex(dp), dimension(L+1) :: h, jn, yn, h2 
real(dp) :: fnu

h = sphankel(L,k*D)

!kode = 1
!fnu = 0.5
!m = 1
!call cbesh(k*D, fnu, kode, m, L+1, h2, nz, ierr)

!print*, h
!h = sqrt(0.5*pi/(k*D))*h2
!Print*, '*********'
!print*, h
p = legendre(L,t)
s(:) = dcmplx(0.0,0.0)

do n = 0, L
   do i1 = 1, size(t)
      s(i1) = s(i1) + (2.0_dp*dble(n)+1.0_dp) * dcmplx(0.0,1.0)**dble(n) * h(n+1) * p(n+1,i1)
   end do
end do

y =  dcmplx(0.0,1.0)*k / ((4.0_dp*pi)**2.0_dp)*s

end function rokhlin


function translator(cp1, cp2, k, a, P, w, L) result(T)
  
  real(dp) :: cp1(3), cp2(3) ,a
  complex(dp) :: k
  complex(dp), dimension(:), allocatable   :: T
  real(dp), dimension(:,:) :: P
  real(dp), dimension(:) :: w
  real(dp), dimension(:), allocatable :: tt
  integer :: L, M, N, acc, i1
  real(dp) :: D(3)
   
  D = cp1 - cp2

  allocate(tt(size(w)))
  do i1 = 1,size(w)
     tt(i1) = dot_product(D,P(:,i1)) / vlen(D)
  end do

  allocate(T(size(w)))
  T = rokhlin(tt,L,vlen(D),k)

end function translator


subroutine allocate_memory_mlfma(otree, mesh, matrices)
type (level_struct), dimension(:) :: otree
type (mesh_struct) :: mesh
type (data) :: matrices

integer :: level, L, box
real(dp) :: mem
real(dp), dimension(:), allocatable :: w

mem = 0

do level = size(otree), 3, -1
   
   w = otree(level)%w 
   L = otree(level)%L

   do box = 1, size(otree(level)%tree)
      
      allocate(otree(level)%tree(box)%out_pattern_theta(size(w)))
      allocate(otree(level)%tree(box)%out_pattern_phi(size(w)))

      allocate(otree(level)%tree(box)%out_Mpattern_theta(size(w)))
      allocate(otree(level)%tree(box)%out_Mpattern_phi(size(w)))

      allocate(otree(level)%tree(box)%out_pattern2_theta(size(w)))
      allocate(otree(level)%tree(box)%out_pattern2_phi(size(w)))

      allocate(otree(level)%tree(box)%out_Mpattern2_theta(size(w)))
      allocate(otree(level)%tree(box)%out_Mpattern2_phi(size(w)))
      
  !    allocate(otree(level)%tree(box)%out_pattern_pole_x(2))
  !    allocate(otree(level)%tree(box)%out_pattern_pole_y(2))
      
      allocate(otree(level)%tree(box)%in_pattern_theta(size(w)))
      allocate(otree(level)%tree(box)%in_pattern_phi(size(w)))

      allocate(otree(level)%tree(box)%in_Mpattern_theta(size(w)))
      allocate(otree(level)%tree(box)%in_Mpattern_phi(size(w)))

      allocate(otree(level)%tree(box)%in_pattern2_theta(size(w)))
      allocate(otree(level)%tree(box)%in_pattern2_phi(size(w)))

      allocate(otree(level)%tree(box)%in_Mpattern2_theta(size(w)))
      allocate(otree(level)%tree(box)%in_Mpattern2_phi(size(w)))

      mem = mem + size(w)*64.0_dp*2.0_dp * 16.0_dp / 8.0_dp / 1024.0_dp / 1024.0_dp 
   end do
end do

print*, 'Size of in and out patterns', floor(mem), 'Mb'
end subroutine allocate_memory_mlfma


!****************************************************************************

function interpolate(otree, level, vec_in) result(vec)
  type (level_struct), dimension(:) :: otree
  complex(dp), dimension(:) :: vec_in
  integer :: level, s, ss
  complex(dp), dimension(size(otree(level-1)%w)) :: vec
  complex(dp), dimension(2*(otree(level)%L+1) * (otree(level-1)%L + 1)) :: tmp
  
  s = 2*(otree(level)%L+1) * (otree(level-1)%L + 1)
  ss = size(otree(level-1)%w) 
  !allocate(tmp(s))
  !print*, s
!  tmp = sp_matmul(otree(level)%Intermat_th_sparse, otree(level)%Intermat_th_ind, &
!           vec_in, s)
  !print*, ss
  call amux(s, vec_in, tmp, otree(level)%ao_th, otree(level)%jao_th, otree(level)%iao_th)

  
!  vec = sp_matmul(otree(level)%Intermat_phi_sparse, otree(level)%Intermat_phi_ind, &
!           tmp, ss)

  call amux(ss, tmp, vec, otree(level)%ao, otree(level)%jao, otree(level)%iao)

  !deallocate(tmp)
end function interpolate


function anterpolate(otree, level, vec_in) result(vec)
  type (level_struct), dimension(:) :: otree
  complex(dp), dimension(:) :: vec_in
  integer :: level, s, ss, row1, row2
  complex(dp), dimension(size(otree(level+1)%w)) :: vec
  complex(dp), dimension(2*(otree(level+1)%L+1) * (otree(level)%L + 1)) :: tmp
  
  s = 2*(otree(level+1)%L+1) * (otree(level)%L + 1)
  ss = size(otree(level+1)%w)

  row1 = size(otree(level)%w)
 ! allocate(tmp(s))
  !print*, s
  
!  tmp = sp_matmul_T(otree(level+1)%Intermat_phi_sparse, otree(level+1)%Intermat_phi_ind, &
!       vec_in, s)
!   print*, row1, size(otree(level+1)%iao), size(vec_in), size(otree(level+1)%iao_th),  s, size(tmp)

  call atmux(row1, vec_in, tmp, otree(level+1)%ao, otree(level+1)%jao, otree(level+1)%iao)

  !print*, ss
!  vec = sp_matmul_T(otree(level+1)%Intermat_th_sparse, otree(level+1)%Intermat_th_ind, &
!           tmp, ss)

   call atmux(s, tmp, vec, otree(level+1)%ao_th, otree(level+1)%jao_th, otree(level+1)%iao_th)

  
!  deallocate(tmp)
end function anterpolate




!*****************************************************************************
subroutine radiation_pattern_mlfma(otree, mesh, matrices)
type (level_struct), dimension(:) :: otree
type (mesh_struct) :: mesh
type (data) :: matrices
integer :: level

integer :: cube, tri, L, acc, M, N, ele, i1, box, loc, i2, be, bi, bj, basis, bt
real(dp) :: rpc(3), rp(3), B_coord(3,3), khat(3), el, signb, khat_pole(3)
real(dp), dimension(:,:), allocatable :: P, P0_tri
real(dp), dimension(:), allocatable :: w, w0_tri
!double complex, dimension(:), allocatable :: f
real(dp) :: B_dN(3,3), B_nxdN(3,3), B_nvec(3), alok(3), dyad(3,3), eye(3,3), vec(3), vec2(3,3)
complex(dp), dimension(3) :: intN, intN2, intNr2, intN_pole0, intN_pole180 
real(dp), dimension(:,:), allocatable :: Pt, shape_tri
real(dp), dimension(:), allocatable :: wt
complex(dp) :: a(3), a0(3), a180(3), a2(3), a3(3)
integer :: basis_tri(2)
real(dp) :: mem
khat_pole = [0.0_dp,0.0_dp,1.0_dp]

level = size(otree)

call inttri(P0_tri,w0_tri,5)
P = otree(level)%P
w = otree(level)%w

allocate(shape_tri(3,size(w0_tri)))
allocate(Pt(3,size(w0_tri)))
allocate(wt(size(w0_tri)))

shape_tri(1,:) = 1.0_dp - P0_tri(1,:) - P0_tri(2,:)
shape_tri(2,:) = P0_tri(1,:)
shape_tri(3,:) = P0_tri(2,:)

eye(:,:) = 0.0_dp
eye(1,1) = 1.0_dp
eye(2,2) = 1.0_dp
eye(3,3) = 1.0_dp

mem = 0
!$omp parallel default(private) &
!$omp firstprivate(level, eye, shape_tri, P0_tri, w0_tri, P, w, khat_pole) &
!$omp shared(otree, mesh, matrices, mem)
!$omp do reduction(+:mem)
do box = 1, size(otree(level)%tree)

   allocate(otree(level)%tree(box)%rad_pattern_theta(otree(level)%tree(box)%N_source,size(w)))
   allocate(otree(level)%tree(box)%rad_pattern_phi(otree(level)%tree(box)%N_source,size(w)))

   allocate(otree(level)%tree(box)%rad_pattern2_theta(otree(level)%tree(box)%N_source,size(w)))
   allocate(otree(level)%tree(box)%rad_pattern2_phi(otree(level)%tree(box)%N_source,size(w)))

!   allocate(otree(level)%tree(box)%rec_pattern_theta(otree(level)%tree(box)%N_source,size(w)))
!   allocate(otree(level)%tree(box)%rec_pattern_phi(otree(level)%tree(box)%N_source,size(w)))

   allocate(otree(level)%tree(box)%rec_pattern2_theta(otree(level)%tree(box)%N_source,size(w)))
   allocate(otree(level)%tree(box)%rec_pattern2_phi(otree(level)%tree(box)%N_source,size(w)))

   mem = mem + otree(level)%tree(box)%N_source*size(w)*64.0*2.0/1024.0/1024.0/8.0*6.0
   !allocate(otree(level)%tree(box)%rad_pattern_pole_x(otree(level)%tree(box)%N_source,2))
   !allocate(otree(level)%tree(box)%rad_pattern_pole_y(otree(level)%tree(box)%N_source,2))
   
   otree(level)%tree(box)%rad_pattern_theta(:,:) = dcmplx(0.0,0.0)
   otree(level)%tree(box)%rad_pattern_phi(:,:) = dcmplx(0.0,0.0)

   otree(level)%tree(box)%rad_pattern2_theta(:,:) = dcmplx(0.0,0.0)
   otree(level)%tree(box)%rad_pattern2_phi(:,:) = dcmplx(0.0,0.0)

   otree(level)%tree(box)%rec_pattern2_theta(:,:) = dcmplx(0.0,0.0)
   otree(level)%tree(box)%rec_pattern2_phi(:,:) = dcmplx(0.0,0.0)
   
   !otree(level)%tree(box)%rad_pattern_pole_x(:,:) = dcmplx(0.0,0.0)
   !otree(level)%tree(box)%rad_pattern_pole_y(:,:) = dcmplx(0.0,0.0)
   
   rpc = otree(level)%tree(box)%cp
   do loc = 1, otree(level)%tree(box)%N_source
      
      basis = otree(level)%tree(box)%sources(loc)
      basis_tri = mesh%edgesT(:,basis)
      

      do bt = 1, 2

         B_coord = mesh%coord(:,mesh%etopol(:,basis_tri(bt)))
         
         call gradshape_tri(B_dN,B_nxdN, B_nvec, B_coord)        
         call linmap_tri(Pt, wt, B_coord, P0_tri, w0_tri)

         do be = 1, 3                                  
            if(basis == mesh%etopol_edges(be,basis_tri(bt))) exit
         end do

         el = norm(mesh%coord(:,mesh%edges(1,basis)), mesh%coord(:,mesh%edges(2,basis)))

         bi = mesh%Eori(1,be)
         bj = mesh%Eori(2,be)
          
         if(mesh%boundary(basis) == 0) then
            signb = RWG_sign(mesh, basis, basis_tri(bt),be)
           
            do i1 = 1, size(w)
               khat = P(:,i1)
               
               intN(:) = dcmplx(0.0,0.0)
               intN2(:) = dcmplx(0.0,0.0)
               intNr2(:) = dcmplx(0.0,0.0)
            !   intN_pole0(:) = dcmplx(0.0,0.0)
            !   intN_pole180(:) = dcmplx(0.0,0.0)
               
               do i2 = 1, size(w0_tri)
           
                  rp = Pt(:,i2)         
                  intN = intN + shape_tri(:,i2) * exp(dcmplx(0.0,-1.0) * mesh%k &
                       * dot_product(khat, rp-rpc)) * wt(i2)
                   intN2 = intN2 + shape_tri(:,i2) * exp(dcmplx(0.0,-1.0) * sqrt(mesh%eps_r) * mesh%k &
                        * dot_product(khat, rp-rpc)) * wt(i2)
                   intNr2 = intNr2 + shape_tri(:,i2) * exp(dcmplx(0.0,1.0) * sqrt(mesh%eps_r) * mesh%k &
                        * dot_product(khat, rp-rpc)) * wt(i2)
                   
    !              intN_pole0 = intN_pole0 + shape_tri(:,i2) * exp(dcmplx(0.0,-1.0) * mesh%k &
    !                   * dot_product(khat_pole, rp-rpc)) * wt(i2)
    !              intN_pole180 = intN_pole180 + shape_tri(:,i2) * exp(dcmplx(0.0,-1.0) * mesh%k &
    !                   * dot_product(-khat_pole, rp-rpc)) * wt(i2)
                  
               end do
              
               dyad = eye - kkdyad(khat)

           
               a =  matmul(dyad, signb * el * (B_nxdN(:,bj)*intN(bi) - B_nxdN(:,bi)*intN(bj)))
               a2 =  matmul(dyad, signb * el * (B_nxdN(:,bj)*intN2(bi) - B_nxdN(:,bi)*intN2(bj)))
               a3 =  matmul(dyad, signb * el * (B_nxdN(:,bj)*intNr2(bi) - B_nxdN(:,bi)*intNr2(bj)))
               
               vec =  cart2sph(khat)
               vec2 = sph_unit_vectors(vec(2),vec(3))
               
               otree(level)%tree(box)%rad_pattern_theta(loc,i1) = otree(level)%tree(box)%rad_pattern_theta(loc,i1) + &
                    dot_product(vec2(:,2), a)
               otree(level)%tree(box)%rad_pattern_phi(loc,i1) = otree(level)%tree(box)%rad_pattern_phi(loc,i1) + &
                    dot_product(vec2(:,3), a)

               otree(level)%tree(box)%rad_pattern2_theta(loc,i1) = otree(level)%tree(box)%rad_pattern2_theta(loc,i1) + &
                    dot_product(vec2(:,2), a2)
               otree(level)%tree(box)%rad_pattern2_phi(loc,i1) = otree(level)%tree(box)%rad_pattern2_phi(loc,i1) + &
                    dot_product(vec2(:,3), a2)

               otree(level)%tree(box)%rec_pattern2_theta(loc,i1) = otree(level)%tree(box)%rec_pattern2_theta(loc,i1) + &
                    dot_product(vec2(:,2), a3)
               otree(level)%tree(box)%rec_pattern2_phi(loc,i1) = otree(level)%tree(box)%rec_pattern2_phi(loc,i1) + &
                    dot_product(vec2(:,3), a3)
               
            end do

            ! ******** poles ************
     !       a0 =  matmul(dyad, signb * el * (B_nxdN(:,bj)*intN_pole0(bi) - B_nxdN(:,bi)*intN_pole0(bj)))
     !       a180 =  matmul(dyad, signb * el * (B_nxdN(:,bj)*intN_pole180(bi) - B_nxdN(:,bi)*intN_pole180(bj)))
     !       
     !       otree(level)%tree(box)%rad_pattern_pole_x(loc,:) = otree(level)%tree(box)%rad_pattern_pole_x(loc,:) &
     !            + [a0(1), a180(1)]
     !       otree(level)%tree(box)%rad_pattern_pole_y(loc,:) = otree(level)%tree(box)%rad_pattern_pole_y(loc,:) &
     !            + [a0(2), a180(2)]
            !****************************
         end if
      end do
   end do
end do
!$omp end do
!$omp end parallel



print*,'Size of the radiation patterns', floor(mem), 'Mb'
print*, 'radiation pattern done'
end subroutine radiation_pattern_mlfma
  
subroutine aggregate(otree, mesh, matrices)
type (level_struct), dimension(:) :: otree
type (mesh_struct) :: mesh
type (data) :: matrices

integer :: i1, tind, bind, level, acc, L, M, N, ele, box, loc, be, basis, Ns, parent, test
integer :: i2, box2, near_box, i3, neighbour, las, N0
real(dp), dimension(:,:), allocatable :: P
real(dp), dimension(:), allocatable :: w
complex(dp), dimension(:), allocatable :: rad_theta, rad_phi, radM_theta, radM_phi
complex(dp), dimension(:), allocatable :: rad2_theta, rad2_phi, radM2_theta, radM2_phi
real(dp) :: cp_box(3), cp_box2(3), box_dl, eta1, signb, r(3), phi, khat_pole(3)
complex(dp), dimension(:), allocatable :: Fth, Fphi, Mth, Mphi, Gth_poles, Gphi_poles
complex(dp), dimension(:), allocatable :: Fth2, Fphi2, Mth2, Mphi2
complex(dp) :: no, rad_x(2), rad_y(2)
CHARACTER(LEN=28) :: fname, fname2

khat_pole = [0.0_dp,0.0_dp,1.0_dp]

level = size(otree)
box_dl =  otree(level)%tree(1)%dl

P = otree(level)%P 
w = otree(level)%w 
L = otree(level)%L

allocate(rad_theta(size(w)))
allocate(rad_phi(size(w)))

allocate(radM_theta(size(w)))
allocate(radM_phi(size(w)))

allocate(rad2_theta(size(w)))
allocate(rad2_phi(size(w)))

allocate(radM2_theta(size(w)))
allocate(radM2_phi(size(w)))

do level = size(otree), 3, -1
   !$omp parallel default(private) &
   !$omp firstprivate(level) &
   !$omp shared(otree, mesh, matrices)
   !$omp do 
   do box2 = 1, size(otree(level)%tree)
      otree(level)%tree(box2)%out_pattern_theta(:) = dcmplx(0.0,0.0)
      otree(level)%tree(box2)%out_pattern_phi(:) = dcmplx(0.0,0.0)
      
      otree(level)%tree(box2)%in_pattern_theta(:) = dcmplx(0.0,0.0)
      otree(level)%tree(box2)%in_pattern_phi(:) = dcmplx(0.0,0.0)

      otree(level)%tree(box2)%out_Mpattern_theta(:) = dcmplx(0.0,0.0)
      otree(level)%tree(box2)%out_Mpattern_phi(:) = dcmplx(0.0,0.0)
      
      otree(level)%tree(box2)%in_Mpattern_theta(:) = dcmplx(0.0,0.0)
      otree(level)%tree(box2)%in_Mpattern_phi(:) = dcmplx(0.0,0.0)

      otree(level)%tree(box2)%out_pattern2_theta(:) = dcmplx(0.0,0.0)
      otree(level)%tree(box2)%out_pattern2_phi(:) = dcmplx(0.0,0.0)
      
      otree(level)%tree(box2)%in_pattern2_theta(:) = dcmplx(0.0,0.0)
      otree(level)%tree(box2)%in_pattern2_phi(:) = dcmplx(0.0,0.0)

      otree(level)%tree(box2)%out_Mpattern2_theta(:) = dcmplx(0.0,0.0)
      otree(level)%tree(box2)%out_Mpattern2_phi(:) = dcmplx(0.0,0.0)
      
      otree(level)%tree(box2)%in_Mpattern2_theta(:) = dcmplx(0.0,0.0)
      otree(level)%tree(box2)%in_Mpattern2_phi(:) = dcmplx(0.0,0.0)



      
    !  otree(level)%tree(box2)%out_pattern_pole_x(:) = dcmplx(0.0,0.0)
    !  otree(level)%tree(box2)%out_pattern_pole_y(:) = dcmplx(0.0,0.0)
   end do
   !$omp end do
   !$omp end parallel
end do


! Finest level
level = size(otree)
!$omp parallel default(private) &
!$omp firstprivate(level) &
!$omp shared(matrices, mesh, otree)
!$omp do
do box = 1, size(otree(level)%tree)
  
   Ns = otree(level)%tree(box)%N_source
   cp_box =  otree(level)%tree(box)%cp ! from
   box_dl =  otree(level)%tree(box)%dl
   
   rad_theta(:) = dcmplx(0.0,0.0)
   rad_phi(:) = dcmplx(0.0,0.0)

   radM_theta(:) = dcmplx(0.0,0.0)
   radM_phi(:) = dcmplx(0.0,0.0)

   rad2_theta(:) = dcmplx(0.0,0.0)
   rad2_phi(:) = dcmplx(0.0,0.0)

   radM2_theta(:) = dcmplx(0.0,0.0)
   radM2_phi(:) = dcmplx(0.0,0.0)

   
   !rad_x(:) = dcmplx(0.0,0.0)
   !rad_y(:) = dcmplx(0.0,0.0)
 
   !print*, size(otree(level)%tree(box)%rad_pattern_theta)
   do loc = 1, Ns
      
      basis = otree(level)%tree(box)%sources(loc)
   
      rad_theta = rad_theta + otree(level)%tree(box)%rad_pattern_theta(loc,:) * matrices%x(basis)
      rad_phi = rad_phi + otree(level)%tree(box)%rad_pattern_phi(loc,:) * matrices%x(basis)

      radM_theta = radM_theta + otree(level)%tree(box)%rad_pattern_theta(loc,:) * matrices%x(basis+mesh%N_edge)
      radM_phi = radM_phi + otree(level)%tree(box)%rad_pattern_phi(loc,:) * matrices%x(basis+mesh%N_edge)

      rad2_theta = rad2_theta + otree(level)%tree(box)%rad_pattern2_theta(loc,:) * matrices%x(basis)
      rad2_phi = rad2_phi + otree(level)%tree(box)%rad_pattern2_phi(loc,:) * matrices%x(basis)

      radM2_theta = radM2_theta + otree(level)%tree(box)%rad_pattern2_theta(loc,:) * matrices%x(basis+mesh%N_edge)
      radM2_phi = radM2_phi + otree(level)%tree(box)%rad_pattern2_phi(loc,:) * matrices%x(basis+mesh%N_edge)


      
      ! Poles
   !   rad_x = rad_x + otree(level)%tree(box)%rad_pattern_pole_x(loc,:) * matrices%x(basis)
   !   rad_y = rad_y + otree(level)%tree(box)%rad_pattern_pole_y(loc,:) * matrices%x(basis)
      
   end do

    otree(level)%tree(box)%out_pattern_theta = rad_theta
    otree(level)%tree(box)%out_pattern_phi = rad_phi

    otree(level)%tree(box)%out_Mpattern_theta = radM_theta
    otree(level)%tree(box)%out_Mpattern_phi = radM_phi

    otree(level)%tree(box)%out_pattern2_theta = rad2_theta
    otree(level)%tree(box)%out_pattern2_phi = rad2_phi

    otree(level)%tree(box)%out_Mpattern2_theta = radM2_theta
    otree(level)%tree(box)%out_Mpattern2_phi = radM2_phi

    
   ! otree(level)%tree(box)%out_pattern_pole_x = rad_x
   ! otree(level)%tree(box)%out_pattern_pole_y = rad_y
end do
!$omp end do
!$omp end parallel

deallocate(rad_theta)
deallocate(rad_phi)

deallocate(radM_theta)
deallocate(radM_phi)

deallocate(rad2_theta)
deallocate(rad2_phi)

deallocate(radM2_theta)
deallocate(radM2_phi)

deallocate(P,w)

do level = size(otree), 4, -1
   !print*, level

   allocate(Fth(size(otree(level-1)%w)))
   allocate(Fphi(size(otree(level-1)%w)))

   allocate(Mth(size(otree(level-1)%w)))
   allocate(Mphi(size(otree(level-1)%w)))

   allocate(Fth2(size(otree(level-1)%w)))
   allocate(Fphi2(size(otree(level-1)%w)))

   allocate(Mth2(size(otree(level-1)%w)))
   allocate(Mphi2(size(otree(level-1)%w)))

   
 !  N0 = 2*(otree(level)%L + 1)
 !  allocate(Gth_poles(size(otree(level)%Intermat,2)))
 !  allocate(Gphi_poles(size(otree(level)%Intermat,2)))

   !$omp parallel default(private) &
   !$omp firstprivate(level) &
   !$omp shared(matrices, mesh, otree)
   !$omp do schedule(guided)
   do box = 1, size(otree(level)%tree)
      cp_box =  otree(level)%tree(box)%cp ! from box

      box2 = otree(level)%tree(box)%parent       
      cp_box2 =  otree(level-1)%tree(box2)%cp ! parent box
 
      !Interpolation to level-1
     ! Fth = matmul(otree(level)%Intermat, otree(level)%tree(box)%out_pattern_theta) 
    !  Fphi = matmul(otree(level)%Intermat, otree(level)%tree(box)%out_pattern_phi)

    !  Mth = matmul(otree(level)%Intermat, otree(level)%tree(box)%out_Mpattern_theta) 
    !  Mphi = matmul(otree(level)%Intermat, otree(level)%tree(box)%out_Mpattern_phi)

   !   Fth2 = matmul(otree(level)%Intermat, otree(level)%tree(box)%out_pattern2_theta) 
   !   Fphi2 = matmul(otree(level)%Intermat, otree(level)%tree(box)%out_pattern2_phi)

   !   Mth2 = matmul(otree(level)%Intermat, otree(level)%tree(box)%out_Mpattern2_theta) 
   !   Mphi2 = matmul(otree(level)%Intermat, otree(level)%tree(box)%out_Mpattern2_phi)


!      Fth = sp_matmul(otree(level)%Intermat_sparse, otree(level)%Intermat_ind, &
!           otree(level)%tree(box)%out_pattern_theta, size(Fth))
!      Fphi = sp_matmul(otree(level)%Intermat_sparse, otree(level)%Intermat_ind, &
!           otree(level)%tree(box)%out_pattern_phi, size(Fphi))
      
!      Mth = sp_matmul(otree(level)%Intermat_sparse, otree(level)%Intermat_ind, &
!           otree(level)%tree(box)%out_Mpattern_theta, size(Mth))
!      Mphi = sp_matmul(otree(level)%Intermat_sparse, otree(level)%Intermat_ind, &
!           otree(level)%tree(box)%out_Mpattern_phi, size(Mphi))

!      Fth2 = sp_matmul(otree(level)%Intermat_sparse, otree(level)%Intermat_ind, &
!           otree(level)%tree(box)%out_pattern2_theta, size(Fth2))
!      Fphi2 = sp_matmul(otree(level)%Intermat_sparse, otree(level)%Intermat_ind, &
!           otree(level)%tree(box)%out_pattern2_phi, size(Fphi2))
      
!      Mth2 = sp_matmul(otree(level)%Intermat_sparse, otree(level)%Intermat_ind, &
 !          otree(level)%tree(box)%out_Mpattern2_theta, size(Mth2))
!      Mphi2 = sp_matmul(otree(level)%Intermat_sparse, otree(level)%Intermat_ind, &
!           otree(level)%tree(box)%out_Mpattern2_phi, size(Mphi2))


      Fth = interpolate(otree, level, otree(level)%tree(box)%out_pattern_theta)
      Fphi = interpolate(otree, level, otree(level)%tree(box)%out_pattern_phi)

      Mth = interpolate(otree, level, otree(level)%tree(box)%out_Mpattern_theta)
      Mphi = interpolate(otree, level, otree(level)%tree(box)%out_Mpattern_phi)

      Fth2 = interpolate(otree, level, otree(level)%tree(box)%out_pattern2_theta)
      Fphi2 = interpolate(otree, level, otree(level)%tree(box)%out_pattern2_phi)

      Mth2 = interpolate(otree, level, otree(level)%tree(box)%out_Mpattern2_theta)
      Mphi2 = interpolate(otree, level, otree(level)%tree(box)%out_Mpattern2_phi)

!******************************************************************************************
      
      
!      fname = 'rad1.h5'
!      fname2 = 'rad2.h5'
!      call cmplx_vec_write2file(otree(level)%tree(box)%out_pattern_theta,fname)
!      call cmplx_vec_write2file(Fth,fname2)
!      stop
      !********************************************************************************
      ! Interpolation wiht poles
   !   do i1 = 1, N0
   !      phi = dble(i1-1) * 2*pi/dble(N0) + pi/dble(N0)
         !North pole
   !      Gth_poles(i1) = cos(phi)*otree(level)%tree(box)%out_pattern_pole_x(1) &
   !           + sin(phi) * otree(level)%tree(box)%out_pattern_pole_y(1)
   !      Gphi_poles(i1) = -sin(phi)*otree(level)%tree(box)%out_pattern_pole_x(1) &
   !           + cos(phi) * otree(level)%tree(box)%out_pattern_pole_y(1)
         !South pole
   !      Gth_poles(N0 + size(otree(level)%w) + i1) = -(cos(phi)*otree(level)%tree(box)%out_pattern_pole_x(2) &
   !           + sin(phi) * otree(level)%tree(box)%out_pattern_pole_y(2))
   !      Gphi_poles(N0 + size(otree(level)%w) + i1) = (-sin(phi)*otree(level)%tree(box)%out_pattern_pole_x(2) &
   !           + cos(phi) * otree(level)%tree(box)%out_pattern_pole_y(2))
         
   !   end do

   !   Gth_poles(N0+1: N0 + size(otree(level)%w)) = otree(level)%tree(box)%out_pattern_theta
   !   Gphi_poles(N0+1: N0 + size(otree(level)%w)) = otree(level)%tree(box)%out_pattern_phi
      
      !Interpolation to level-1
   !   Fth = matmul(otree(level)%Intermat, Gth_poles) 
   !   Fphi = matmul(otree(level)%Intermat, Gphi_poles)

   !   fname = 'rad1.h5'
   !   fname2 = 'rad2.h5'
   !   call cmplx_vec_write2file(Gth_poles,fname)
   !   call cmplx_vec_write2file(Fth,fname2)
   !   stop
      
      !**************************************************************************

      !Fth = otree(level)%tree(box)%out_pattern_theta 
      !Fphi = otree(level)%tree(box)%out_pattern_phi


      
      r = cp_box2-cp_box

      !Shift to the center of the parent box
      !print*, size(otree(level-1)%w), size(otree(level-1)%tree(box2)%out_pattern_theta), size(Fth)




      do i1 = 1, size(otree(level-1)%w)
         Fth(i1) = exp(dcmplx(0.0,1.0)*mesh%k*dot_product(otree(level-1)%P(:,i1),r)) * Fth(i1)
         Fphi(i1) = exp(dcmplx(0.0,1.0)*mesh%k*dot_product(otree(level-1)%P(:,i1),r)) * Fphi(i1) 
                    
         Mth(i1) = exp(dcmplx(0.0,1.0)*mesh%k*dot_product(otree(level-1)%P(:,i1),r)) * Mth(i1)    
         Mphi(i1) =exp(dcmplx(0.0,1.0)*mesh%k*dot_product(otree(level-1)%P(:,i1),r)) * Mphi(i1) 

         ! medium 2
       
         Fth2(i1) = exp(dcmplx(0.0,1.0)*sqrt(mesh%eps_r)*mesh%k*dot_product(otree(level-1)%P(:,i1),r)) * Fth2(i1)
         Fphi2(i1) = exp(dcmplx(0.0,1.0)*sqrt(mesh%eps_r)*mesh%k*dot_product(otree(level-1)%P(:,i1),r)) * Fphi2(i1) 
         
         Mth2(i1) = exp(dcmplx(0.0,1.0)*sqrt(mesh%eps_r)*mesh%k*dot_product(otree(level-1)%P(:,i1),r)) * Mth2(i1)        
         Mphi2(i1) = exp(dcmplx(0.0,1.0)*sqrt(mesh%eps_r)*mesh%k*dot_product(otree(level-1)%P(:,i1),r)) * Mphi2(i1)
         
      end do

      !$OMP CRITICAL
      otree(level-1)%tree(box2)%out_pattern_theta =  otree(level-1)%tree(box2)%out_pattern_theta + Fth
      otree(level-1)%tree(box2)%out_pattern_phi =  otree(level-1)%tree(box2)%out_pattern_phi + Fphi
      otree(level-1)%tree(box2)%out_Mpattern_theta =  otree(level-1)%tree(box2)%out_Mpattern_theta + Mth  
      otree(level-1)%tree(box2)%out_Mpattern_phi =  otree(level-1)%tree(box2)%out_Mpattern_phi + Mphi
          
      ! medium 2
      otree(level-1)%tree(box2)%out_pattern2_theta =  otree(level-1)%tree(box2)%out_pattern2_theta + Fth2
      otree(level-1)%tree(box2)%out_pattern2_phi =  otree(level-1)%tree(box2)%out_pattern2_phi + Fphi2
      otree(level-1)%tree(box2)%out_Mpattern2_theta =  otree(level-1)%tree(box2)%out_Mpattern2_theta + Mth2 
      otree(level-1)%tree(box2)%out_Mpattern2_phi =  otree(level-1)%tree(box2)%out_Mpattern2_phi + Mphi2
      !$OMP END CRITICAL


 !'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''     
 !     !$OMP CRITICAL
 !     do i1 = 1, size(otree(level-1)%w)
 !        otree(level-1)%tree(box2)%out_pattern_theta(i1) =  otree(level-1)%tree(box2)%out_pattern_theta(i1) + &
 !             exp(dcmplx(0.0,1.0)*mesh%k*dot_product(otree(level-1)%P(:,i1),r)) * Fth(i1)
 !        otree(level-1)%tree(box2)%out_pattern_phi(i1) =  otree(level-1)%tree(box2)%out_pattern_phi(i1) + &
 !             exp(dcmplx(0.0,1.0)*mesh%k*dot_product(otree(level-1)%P(:,i1),r)) * Fphi(i1) 

 !        otree(level-1)%tree(box2)%out_Mpattern_theta(i1) =  otree(level-1)%tree(box2)%out_Mpattern_theta(i1) + &
 !             exp(dcmplx(0.0,1.0)*mesh%k*dot_product(otree(level-1)%P(:,i1),r)) * Mth(i1)
 !        otree(level-1)%tree(box2)%out_Mpattern_phi(i1) =  otree(level-1)%tree(box2)%out_Mpattern_phi(i1) + &
 !             exp(dcmplx(0.0,1.0)*mesh%k*dot_product(otree(level-1)%P(:,i1),r)) * Mphi(i1) 

         ! medium 2
 !        otree(level-1)%tree(box2)%out_pattern2_theta(i1) =  otree(level-1)%tree(box2)%out_pattern2_theta(i1) + &
 !             exp(dcmplx(0.0,1.0)*sqrt(mesh%eps_r)*mesh%k*dot_product(otree(level-1)%P(:,i1),r)) * Fth2(i1)
 !        otree(level-1)%tree(box2)%out_pattern2_phi(i1) =  otree(level-1)%tree(box2)%out_pattern2_phi(i1) + &
 !             exp(dcmplx(0.0,1.0)*sqrt(mesh%eps_r)*mesh%k*dot_product(otree(level-1)%P(:,i1),r)) * Fphi2(i1) 

 !        otree(level-1)%tree(box2)%out_Mpattern2_theta(i1) =  otree(level-1)%tree(box2)%out_Mpattern2_theta(i1) + &
 !             exp(dcmplx(0.0,1.0)*sqrt(mesh%eps_r)*mesh%k*dot_product(otree(level-1)%P(:,i1),r)) * Mth2(i1)
 !        otree(level-1)%tree(box2)%out_Mpattern2_phi(i1) =  otree(level-1)%tree(box2)%out_Mpattern2_phi(i1) + &
 !             exp(dcmplx(0.0,1.0)*sqrt(mesh%eps_r)*mesh%k*dot_product(otree(level-1)%P(:,i1),r)) * Mphi2(i1)
         
 !     end do
 !     !$OMP END CRITICAL

!''''''''''''''''''''''''''''''''''''''''''''''''

      
      ! Shift poles
      !NOrth pole
!      otree(level-1)%tree(box2)%out_pattern_pole_x(1) =  otree(level-1)%tree(box2)%out_pattern_pole_x(1) + &
!           exp(dcmplx(0.0,1.0)*mesh%k*dot_product(khat_pole,r)) * otree(level)%tree(box)%out_pattern_pole_x(1)
!      otree(level-1)%tree(box2)%out_pattern_pole_y(1) =  otree(level-1)%tree(box2)%out_pattern_pole_y(1) + &
!           exp(dcmplx(0.0,1.0)*mesh%k*dot_product(khat_pole,r)) * otree(level)%tree(box)%out_pattern_pole_y(1)
     ! !South pole!
!      otree(level-1)%tree(box2)%out_pattern_pole_x(2) =  otree(level-1)%tree(box2)%out_pattern_pole_x(2) + &
!           exp(dcmplx(0.0,1.0)*mesh%k*dot_product(-khat_pole,r)) * otree(level)%tree(box)%out_pattern_pole_x(2)
!      otree(level-1)%tree(box2)%out_pattern_pole_y(2) =  otree(level-1)%tree(box2)%out_pattern_pole_y(2) + &
!           exp(dcmplx(0.0,1.0)*mesh%k*dot_product(-khat_pole,r)) * otree(level)%tree(box)%out_pattern_pole_y(2)
      
   end do
   !$omp end do
   !$omp end parallel

   
   deallocate(Fth, Fphi, Mth, Mphi)
   deallocate(Fth2, Fphi2, Mth2, Mphi2)
   !   deallocate(Gth_poles, Gphi_poles)

end do


end subroutine aggregate


!********************************************************************************

subroutine compute_translators_mlfma(otree, mesh, matrices)
type (level_struct), dimension(:) :: otree
type (mesh_struct) :: mesh
type (data) :: matrices

integer :: i1, level, acc, L, M, N, box, las, i2, i3
integer ::  box2, near_box, parent, neighbour

real(dp), dimension(:,:), allocatable :: P
real(dp), dimension(:), allocatable :: w
complex(dp), dimension(:), allocatable :: T, Tpole, T2
real(dp) :: cp_box(3), cp_box2(3), box_dl


do level = size(otree), 3, -1
   box_dl =  otree(level)%tree(1)%dl

   P = otree(level)%P 
   w = otree(level)%w 
   L = otree(level)%L

   allocate(T(size(w)), T2(size(w)))
 

   do box = 1, size(otree(level)%tree)

      allocate(otree(level)%tree(box)%translator(size(w),189))
      allocate(otree(level)%tree(box)%translator2(size(w),189))
      allocate(otree(level)%tree(box)%translator_rec_ind(189))
   
      
      allocate(otree(level)%tree(box)%out_pattern_theta(size(w)))
      allocate(otree(level)%tree(box)%out_pattern_phi(size(w)))

      allocate(otree(level)%tree(box)%out_Mpattern_theta(size(w)))
      allocate(otree(level)%tree(box)%out_Mpattern_phi(size(w)))

      allocate(otree(level)%tree(box)%out_pattern2_theta(size(w)))
      allocate(otree(level)%tree(box)%out_pattern2_phi(size(w)))

      allocate(otree(level)%tree(box)%out_Mpattern2_theta(size(w)))
      allocate(otree(level)%tree(box)%out_Mpattern2_phi(size(w)))
      
  !    allocate(otree(level)%tree(box)%out_pattern_pole_x(2))
  !    allocate(otree(level)%tree(box)%out_pattern_pole_y(2))
      
      allocate(otree(level)%tree(box)%in_pattern_theta(size(w)))
      allocate(otree(level)%tree(box)%in_pattern_phi(size(w)))

      allocate(otree(level)%tree(box)%in_Mpattern_theta(size(w)))
      allocate(otree(level)%tree(box)%in_Mpattern_phi(size(w)))

      allocate(otree(level)%tree(box)%in_pattern2_theta(size(w)))
      allocate(otree(level)%tree(box)%in_pattern2_phi(size(w)))

      allocate(otree(level)%tree(box)%in_Mpattern2_theta(size(w)))
      allocate(otree(level)%tree(box)%in_Mpattern2_phi(size(w)))

      
      
      cp_box =  otree(level)%tree(box)%cp ! from
      box_dl =  otree(level)%tree(box)%dl
      parent = otree(level)%tree(box)%parent

      las = 1
      do i1 = 1, 27 ! near neighbours of parent cube
         neighbour = otree(level-1)%tree(parent)%near_neighbours(i1)
     
         if(neighbour == 0) exit
      
         do i2 = 1, 8 ! children of near neighbours
            box2 = otree(level-1)%tree(neighbour)%children(i2) ! level
            if(box2 == 0) exit

            ! ***check if near box ****
            near_box = 0
            do i3 = 1,27
               if(box2 == otree(level)%tree(box)%near_neighbours(i3)) then
                  near_box = 1
               end if
            end do

            if(near_box == 0) then
   
               cp_box2 =  otree(level)%tree(box2)%cp !to
               T = translator(cp_box2, cp_box, dcmplx(mesh%k,0.0), box_dl, P, w, L)
               T2 = translator(cp_box2, cp_box, mesh%k*sqrt(mesh%eps_r), box_dl, P, w, L)
               
               otree(level)%tree(box)%translator(:,las) = T 
               otree(level)%tree(box)%translator2(:,las) = T2
               
               otree(level)%tree(box)%translator_rec_ind(las) = box2

               las = las + 1
             
            end if
         end do
      end do
   end do

   
   deallocate(T, T2, w, P)
   !****************************************************
end do
   
end subroutine compute_translators_mlfma



subroutine compute_translators_mlfma2(otree, mesh, matrices)
type (level_struct), dimension(:) :: otree
type (mesh_struct) :: mesh
type (data) :: matrices

integer :: i1, level, acc, L, M, N, box, las
integer ::  box_x, box_y, box_z
real(dp) :: mem

real(dp), dimension(:,:), allocatable :: P
real(dp), dimension(:), allocatable :: w
complex(dp), dimension(:), allocatable :: T, Tpole, T2
real(dp) :: cp_box(3), cp_box2(3), box_dl

mem = 0
cp_box = [0.0_dp, 0.0_dp, 0.0_dp]
do level = size(otree), 3, -1
   box_dl =  otree(level)%tree(1)%dl

   P = otree(level)%P 
   w = otree(level)%w 
   L = otree(level)%L

   allocate(T(size(w)), T2(size(w)))

   allocate(otree(level)%translators(size(w), 7**3))
   allocate(otree(level)%translators2(size(w), 7**3 ))

   mem = mem + 2.0_dp*(7.0_dp**3.0_dp * size(w)/1024.0_dp/8.0_dp * 64.0_dp * 2.0_dp) / 1024.0_dp
   las = 1
   do box_x = -3,3
      do box_y = -3,3
         do box_z = -3,3

            cp_box2(1) = box_x * box_dl
            cp_box2(2) = box_y * box_dl
            cp_box2(3) = box_z * box_dl
            !T(:) = dcmplx(0.0,0.0)
            !T2(:) = dcmplx(0.0,0.0)
            if(vlen(cp_box2) > box_dl*1.9_dp) then
               T = translator(cp_box2, cp_box, dcmplx(mesh%k,0.0), box_dl, P, w, L)
               T2 = translator(cp_box2, cp_box, mesh%k*sqrt(mesh%eps_r), box_dl, P, w, L)
            end if
               otree(level)%translators(:,las) = T 
               otree(level)%translators2(:,las) = T2
            
               las = las + 1
          !  end if
         end do
      end do
   end do

   
   deallocate(T, T2, w, P)
   !****************************************************   
end do

print*, 'Size of the translators', floor(mem), 'Mb'
end subroutine compute_translators_mlfma2

function translator_index(r, box_dl) result(ind)
  real(dp) :: box_dl, r(3)
  integer :: ind

  integer :: box_x, box_y, box_z, las, ix, iy, iz
  real(dp) :: cp_box(3)
  
  las = 1
  ix = 0
  do box_x = -3, 3
     cp_box(1) = box_x * box_dl
     ix = 0
     if(cp_box(1) < r(1) + box_dl/2.0_dp .and. cp_box(1) > r(1) - box_dl/2.0_dp) ix = 1
     do box_y = -3,3
        cp_box(2) = box_y * box_dl
        iy = 0     
        if(cp_box(2) < r(2) + box_dl/2.0_dp .and. cp_box(2) > r(2) - box_dl/2.0_dp) iy = 1
        do box_z = -3,3
           cp_box(3) = box_z * box_dl
           iz = 0
           if(cp_box(3) < r(3) + box_dl/2.0_dp .and. cp_box(3) > r(3) - box_dl/2.0_dp) iz = 1
           
            if(vlen(cp_box) > box_dl*1.9) then

               if(ix*iy*iz == 1) ind = las
               las = las + 1
            end if
         end do
      end do
   end do
   
end function translator_index


function translator_index2(r, box_dl) result(ind)
  real(dp) :: box_dl, r(3)
  integer :: ind

  integer :: box_x, box_y, box_z, las, ix, iy, iz
  real(dp) :: cp_box(3)
  
  las = 1
  ix = 0
  do box_x = -3, 3
     cp_box(1) = box_x * box_dl
     if(cp_box(1) < r(1) + box_dl/2.0_dp .and. cp_box(1) > r(1) - box_dl/2.0_dp) ix = box_x+4
  end do

     
  do box_y = -3,3
     cp_box(2) = box_y * box_dl   
     if(cp_box(2) < r(2) + box_dl/2.0_dp .and. cp_box(2) > r(2) - box_dl/2.0_dp) iy = box_y+4
  end do
  
  do box_z = -3,3
     cp_box(3) = box_z * box_dl

     if(cp_box(3) < r(3) + box_dl/2.0_dp .and. cp_box(3) > r(3) - box_dl/2.0_dp) iz = box_z+4

  end do
            

  ind = iz + 7 * (iy-1) + 7*7*(ix-1) 
            
   
end function translator_index2

function translator_index3(r, box_dl) result(ind)
  real(dp) :: box_dl, r(3)
  integer :: ind

  integer :: box_x, box_y, box_z, las, ix, iy, iz
  real(dp) :: cp_box(3)
  
  las = 1
  ix = 0

  box_x = nint(r(1)/box_dl)
  ix = box_x+4
  
  box_y = nint(r(2)/box_dl)
  iy = box_y+4
     
  box_z = nint(r(3)/box_dl) 
  iz = box_z+4
       
  ind = iz + 7 * (iy-1) + 7*7*(ix-1) 
            
   
end function translator_index3




  !******************** disaggregation *************************************

subroutine disaggregation(otree, mesh, matrices)
type (level_struct), dimension(:) :: otree
type (mesh_struct) :: mesh
type (data) :: matrices

complex(dp) :: no1, no2, no3, no4, no12, no22, no32
real(dp) :: eta1, cp_box(3), cp_box2(3), box_dl, box2_dl, r(3)
complex(dp), dimension(2*mesh%N_edge) :: Ax
integer :: i1, box, box2, Ns, loc, test, i2, level, N0, ithre, nthreads
complex(dp), dimension(:), allocatable :: Fth, Fphi, Gth, Gphi, an_th, an_phi, Fth2, Fphi2
complex(dp), dimension(:), allocatable :: Mth, Mphi, Mth2, Mphi2
complex(dp), dimension(:,:), allocatable :: AxN
complex(dp) :: eta2
integer(ip) :: ii1
integer :: tind, bind, t1, t2, rate

eta1 = sqrt(mu/epsilon)
eta2 = sqrt(mu/(epsilon*mesh%eps_r))

call system_clock(t1,rate)

!*********************** Downward pass **************************

do level = 3, size(otree)-1
   !print*, 'dis', level, size(otree(level)%w)

   
   allocate(Fth(size(otree(level)%w)), Fphi(size(otree(level)%w)))
   allocate(Mth(size(otree(level)%w)), Mphi(size(otree(level)%w)))

   allocate(Fth2(size(otree(level)%w)), Fphi2(size(otree(level)%w)))
   allocate(Mth2(size(otree(level)%w)), Mphi2(size(otree(level)%w)))

 
  ! N0 = 2*(otree(level+1)%L + 1)
  ! allocate(Gth(size(otree(level+1)%Intermat,2)), Gphi(size(otree(level+1)%Intermat,2)))

   
   !$omp parallel default(private) &
   !$omp firstprivate(level) &
   !$omp shared(matrices, mesh, otree)
   !$omp do schedule(guided)
   do box = 1, size(otree(level)%tree)
      cp_box = otree(level)%tree(box)%cp ! from

      do i1 = 1, 8
         box2 = otree(level)%tree(box)%children(i1) ! level
         if(box2 == 0) exit
         cp_box2 = otree(level+1)%tree(box2)%cp ! to

         r = cp_box2-cp_box
         
         !Shift to the center of a child box
         do i2 = 1, size(otree(level)%w)
            Fth(i2) = exp(dcmplx(0.0,1.0)*mesh%k*dot_product(otree(level)%P(:,i2),r)) * &
                 otree(level)%tree(box)%in_pattern_theta(i2) 
            Fphi(i2) = exp(dcmplx(0.0,1.0)*mesh%k*dot_product(otree(level)%P(:,i2),r)) * &
                 otree(level)%tree(box)%in_pattern_phi(i2)

            Mth(i2) = exp(dcmplx(0.0,1.0)*mesh%k*dot_product(otree(level)%P(:,i2),r)) * &
                 otree(level)%tree(box)%in_Mpattern_theta(i2) 
            Mphi(i2) = exp(dcmplx(0.0,1.0)*mesh%k*dot_product(otree(level)%P(:,i2),r)) * &
                 otree(level)%tree(box)%in_Mpattern_phi(i2)

            !***********  medium 2 **************************

            Fth2(i2) = exp(dcmplx(0.0,1.0)*sqrt(mesh%eps_r)*mesh%k*dot_product(otree(level)%P(:,i2),r)) * &
                 otree(level)%tree(box)%in_pattern2_theta(i2) 
            Fphi2(i2) = exp(dcmplx(0.0,1.0)*sqrt(mesh%eps_r)*mesh%k*dot_product(otree(level)%P(:,i2),r)) * &
                 otree(level)%tree(box)%in_pattern2_phi(i2)

            Mth2(i2) = exp(dcmplx(0.0,1.0)*sqrt(mesh%eps_r)*mesh%k*dot_product(otree(level)%P(:,i2),r)) * &
                 otree(level)%tree(box)%in_Mpattern2_theta(i2) 
            Mphi2(i2) = exp(dcmplx(0.0,1.0)*sqrt(mesh%eps_r)*mesh%k*dot_product(otree(level)%P(:,i2),r)) * &
                 otree(level)%tree(box)%in_Mpattern2_phi(i2)
         end do
    
   
         
!         otree(level+1)%tree(box2)%in_pattern_theta = otree(level+1)%tree(box2)%in_pattern_theta + &
!              sp_matmul_T(otree(level+1)%Intermat_sparse, otree(level+1)%Intermat_ind, Fth, size(otree(level+1)%w))

!         otree(level+1)%tree(box2)%in_pattern_phi = otree(level+1)%tree(box2)%in_pattern_phi + &
!              sp_matmul_T(otree(level+1)%Intermat_sparse, otree(level+1)%Intermat_ind, Fphi, size(otree(level+1)%w))

!         otree(level+1)%tree(box2)%in_Mpattern_theta = otree(level+1)%tree(box2)%in_Mpattern_theta + &
!              sp_matmul_T(otree(level+1)%Intermat_sparse, otree(level+1)%Intermat_ind, Mth, size(otree(level+1)%w))

!         otree(level+1)%tree(box2)%in_Mpattern_phi = otree(level+1)%tree(box2)%in_Mpattern_phi + &
!              sp_matmul_T(otree(level+1)%Intermat_sparse, otree(level+1)%Intermat_ind, Mphi, size(otree(level+1)%w))

         !************ medium 2 **************************

!          otree(level+1)%tree(box2)%in_pattern2_theta = otree(level+1)%tree(box2)%in_pattern2_theta + &
!              sp_matmul_T(otree(level+1)%Intermat_sparse, otree(level+1)%Intermat_ind, Fth2, size(otree(level+1)%w))

!         otree(level+1)%tree(box2)%in_pattern2_phi = otree(level+1)%tree(box2)%in_pattern2_phi + &
!              sp_matmul_T(otree(level+1)%Intermat_sparse, otree(level+1)%Intermat_ind, Fphi2, size(otree(level+1)%w))

!         otree(level+1)%tree(box2)%in_Mpattern2_theta = otree(level+1)%tree(box2)%in_Mpattern2_theta + &
!              sp_matmul_T(otree(level+1)%Intermat_sparse, otree(level+1)%Intermat_ind, Mth2, size(otree(level+1)%w))

!         otree(level+1)%tree(box2)%in_Mpattern2_phi = otree(level+1)%tree(box2)%in_Mpattern2_phi + &
!              sp_matmul_T(otree(level+1)%Intermat_sparse, otree(level+1)%Intermat_ind, Mphi2, size(otree(level+1)%w))
         

         !**********************************************************
         otree(level+1)%tree(box2)%in_pattern_theta = otree(level+1)%tree(box2)%in_pattern_theta + &
              anterpolate(otree, level, Fth)
         otree(level+1)%tree(box2)%in_pattern_phi = otree(level+1)%tree(box2)%in_pattern_phi + &
              anterpolate(otree, level, Fphi)

         otree(level+1)%tree(box2)%in_Mpattern_theta = otree(level+1)%tree(box2)%in_Mpattern_theta + &
              anterpolate(otree, level, Mth)
         otree(level+1)%tree(box2)%in_Mpattern_phi = otree(level+1)%tree(box2)%in_Mpattern_phi + &
              anterpolate(otree, level, Mphi)


         otree(level+1)%tree(box2)%in_pattern2_theta = otree(level+1)%tree(box2)%in_pattern2_theta + &
              anterpolate(otree, level, Fth2)
         otree(level+1)%tree(box2)%in_pattern2_phi = otree(level+1)%tree(box2)%in_pattern2_phi + &
              anterpolate(otree, level, Fphi2)

         otree(level+1)%tree(box2)%in_Mpattern2_theta = otree(level+1)%tree(box2)%in_Mpattern2_theta + &
              anterpolate(otree, level, Mth2)
         otree(level+1)%tree(box2)%in_Mpattern2_phi = otree(level+1)%tree(box2)%in_Mpattern2_phi + &
              anterpolate(otree, level, Mphi2)

         
         ! poles

 !        Gth = matmul((transpose(otree(level+1)%Intermat)),Fth)
 !        Gphi = matmul((transpose(otree(level+1)%Intermat)),Fphi)
       
 !        otree(level+1)%tree(box2)%in_pattern_theta =  otree(level+1)%tree(box2)%in_pattern_theta &
 !             + Gth(N0+1:N0+size(otree(level+1)%w))
 !        otree(level+1)%tree(box2)%in_pattern_phi =  otree(level+1)%tree(box2)%in_pattern_phi &
 !             + Gphi(N0+1:N0+size(otree(level+1)%w))
          
         !********************
      end do
   end do
   !$omp end do
   !$omp end parallel
   
   deallocate(Fth, Fphi, Mth, Mphi)
   deallocate(Fth2, Fphi2, Mth2, Mphi2)
 
 !  deallocate(Gth, Gphi)
end do


!**************** Finest level *********************************
level = size(otree)

! ******* PMCHWT ***********
if(mesh%form == 1) then
   no1 = (mesh%k)**2 * 1.0_dp /(dcmplx(0.0,1.0) * mesh%k) 
   no2 = dcmplx(0.0,-1.0) * mesh%k * eta1
   no3 = dcmplx(0.0,-1.0) * mesh%k / eta1
   no12 = (sqrt(mesh%eps_r) * mesh%k)**2 * eta2 / (eta1 * dcmplx(0.0,1.0) * sqrt(mesh%eps_r)*mesh%k)
   no4 =  (sqrt(mesh%eps_r) * mesh%k)**2 * eta1 / (eta2 * dcmplx(0.0,1.0) * sqrt(mesh%eps_r)*mesh%k)
   no22 = dcmplx(0.0,-1.0) *sqrt(mesh%eps_r) * mesh%k * eta1
   no32 =  dcmplx(0.0,-1.0) * sqrt(mesh%eps_r) * mesh%k / eta1
else
   ! ******** CTF ************************

   no1 = (mesh%k)**2 * 1.0_dp /(dcmplx(0.0,1.0) * mesh%k) 
   no2 = dcmplx(0.0,-1.0) * mesh%k * eta1
   no3 = dcmplx(0.0,-1.0) * mesh%k / eta1
   no12 = (sqrt(mesh%eps_r) * mesh%k)**2 * 1.0_dp / (dcmplx(0.0,1.0) * sqrt(mesh%eps_r)*mesh%k)
   no4 =  (sqrt(mesh%eps_r) * mesh%k)**2 * 1.0_dp / (dcmplx(0.0,1.0) * sqrt(mesh%eps_r)*mesh%k)
   no22 = dcmplx(0.0,-1.0) *sqrt(mesh%eps_r) * mesh%k * eta2
   no32 = dcmplx(0.0,-1.0) * sqrt(mesh%eps_r) * mesh%k / eta2

end if

nthreads = 1

Ax(:) = dcmplx(0.0,0.0)
!$omp parallel default(shared) 
!$ nthreads = omp_get_num_threads()
!$omp end parallel

allocate(AxN(2*mesh%N_edge, nthreads))
AxN(:,:) = dcmplx(0.0,0.0)
ithre = 1
!$omp parallel default(private) &
!$omp firstprivate(level, no1, no2, no3, no12, no4, no22, no32) &
!$omp shared(matrices, mesh, otree, AxN)
!$omp do schedule(guided)
do box = 1, size(otree(level)%tree)
   Ns = otree(level)%tree(box)%N_source
   cp_box =  otree(level)%tree(box)%cp ! from
   box_dl =  otree(level)%tree(box)%dl
   !$ ithre = omp_get_thread_num() + 1
   
   do loc = 1, Ns
      
      test = otree(level)%tree(box)%sources(loc)
          
      do i1 = 1, size(otree(level)%w)

         ! ****************** L_1(J) ***********************************************
         AxN(test,ithre) =  AxN(test,ithre) + conjg(otree(level)%tree(box)%rad_pattern_theta(loc,i1)) &
              * otree(level)%tree(box)%in_pattern_theta(i1) * no1
         
         AxN(test,ithre) =  AxN(test,ithre) + conjg(otree(level)%tree(box)%rad_pattern_phi(loc,i1)) &
              * otree(level)%tree(box)%in_pattern_phi(i1) * no1 

         ! **************** L_1(M) ************************************************

         AxN(test+mesh%N_edge,ithre) =  AxN(test+mesh%N_edge,ithre) + conjg(otree(level)%tree(box)%rad_pattern_theta(loc,i1)) &
              * otree(level)%tree(box)%in_Mpattern_theta(i1) * no1
         
         AxN(test+mesh%N_edge,ithre) =  AxN(test+mesh%N_edge,ithre) + conjg(otree(level)%tree(box)%rad_pattern_phi(loc,i1)) &
              * otree(level)%tree(box)%in_Mpattern_phi(i1) * no1 

         ! **************** K_1(J) *******-k x theta = -phi ************-k x phi = theta ****************
         
         AxN(test+mesh%N_edge,ithre) =  AxN(test+mesh%N_edge,ithre) - conjg(-otree(level)%tree(box)%rad_pattern_phi(loc,i1)) &
              * otree(level)%tree(box)%in_pattern_theta(i1) * no2
         
         AxN(test+mesh%N_edge,ithre) =  AxN(test+mesh%N_edge,ithre) - conjg(otree(level)%tree(box)%rad_pattern_theta(loc,i1)) &
              * otree(level)%tree(box)%in_pattern_phi(i1) * no2

         !**************** K_1 (M) ********************************************

         AxN(test,ithre) =  AxN(test,ithre) + conjg(-otree(level)%tree(box)%rad_pattern_phi(loc,i1)) &
              * otree(level)%tree(box)%in_Mpattern_theta(i1) * no3 
         
         AxN(test,ithre) =  AxN(test,ithre) + conjg(otree(level)%tree(box)%rad_pattern_theta(loc,i1)) &
              * otree(level)%tree(box)%in_Mpattern_phi(i1) * no3

         !*************************** Medium 2 ************************************************************************

         ! ****************** L_2(J) ***********************************************
         AxN(test,ithre) =  AxN(test,ithre) + (otree(level)%tree(box)%rec_pattern2_theta(loc,i1)) &
              * otree(level)%tree(box)%in_pattern2_theta(i1) * no12
         
         AxN(test,ithre) =  AxN(test,ithre) + (otree(level)%tree(box)%rec_pattern2_phi(loc,i1)) &
              * otree(level)%tree(box)%in_pattern2_phi(i1) * no12 

         ! **************** L_2(M) ************************************************

         AxN(test+mesh%N_edge,ithre) =  AxN(test+mesh%N_edge,ithre) + (otree(level)%tree(box)%rec_pattern2_theta(loc,i1)) &
              * otree(level)%tree(box)%in_Mpattern2_theta(i1) * no4
         
         AxN(test+mesh%N_edge,ithre) =  AxN(test+mesh%N_edge,ithre) + (otree(level)%tree(box)%rec_pattern2_phi(loc,i1)) &
              * otree(level)%tree(box)%in_Mpattern2_phi(i1) * no4 

         ! **************** K_2(J) *******-k x theta = -phi ************-k x phi = theta ****************
         
         AxN(test+mesh%N_edge,ithre) =  AxN(test+mesh%N_edge,ithre) - (-otree(level)%tree(box)%rec_pattern2_phi(loc,i1)) &
              * otree(level)%tree(box)%in_pattern2_theta(i1) * no22
         
         AxN(test+mesh%N_edge,ithre) =  AxN(test+mesh%N_edge,ithre) - (otree(level)%tree(box)%rec_pattern2_theta(loc,i1)) &
              * otree(level)%tree(box)%in_pattern2_phi(i1) * no22

         !**************** K_2 (M) ********************************************

         AxN(test,ithre) =  AxN(test,ithre) + (-otree(level)%tree(box)%rec_pattern2_phi(loc,i1)) &
              * otree(level)%tree(box)%in_Mpattern2_theta(i1) * no32 
         
         AxN(test,ithre) =  AxN(test,ithre) + (otree(level)%tree(box)%rec_pattern2_theta(loc,i1)) &
              * otree(level)%tree(box)%in_Mpattern2_phi(i1) * no32



         
      end do
      
   end do
end do
!$omp end do
!$omp end parallel

call system_clock(t2,rate)
print*,'Dissregation done in', real(T2-T1)/real(rate), 'seconds'

!************ sparse matvec multiplication *****************************
!ithre=1
!!$omp parallel default(private) &
!!$omp shared(matrices, AxN)
!!$omp do 
!do ii1 = 1_ip, size(matrices%sparse_M, kind=kind(ii1))
!   !$ ithre = omp_get_thread_num() + 1
!   tind = matrices%ind_M(ii1,1_ip)
!   bind = matrices%ind_M(ii1,2_ip)

!   AxN(tind,ithre) =  AxN(tind,ithre) + matrices%sparse_M(ii1) * matrices%x(bind)
!end do
!!$omp end do
!!$omp end parallel
call system_clock(t1,rate)
call cmplx_amux(2*mesh%N_edge, matrices%x, Ax, matrices%ao, matrices%jao, matrices%iao)


do ithre = 1, nthreads
  Ax(:) = Ax(:) + AxN(:, ithre)
enddo
deallocate(AxN)

call system_clock(t2,rate)
print*,'Near zone done in', real(T2-T1)/real(rate), 'seconds'


!*********************** Downward pass **************************



matrices%Ax = Ax
print*, 'disaggregation done'
end subroutine disaggregation



subroutine matvec_mlfma_pmchwt(otree, mesh, matrices)
type (level_struct), dimension(:) :: otree
type (mesh_struct) :: mesh
type (data) :: matrices

integer :: i1, tind, bind, level, acc, L, M, N, ele, box, loc, be, basis, Ns, parent, test
integer :: i2, box2, near_box, i3, neighbour, las, trans_ind, t1, t2, rate
complex(dp), dimension(mesh%N_edge) :: Ax
real(dp) :: cp_box1(3), cp_box2(3), box_dl, r(3)
complex(dp), dimension(:), allocatable :: F_th, F_phi,F2_th, F2_phi, M_th, M_phi,M2_th, M2_phi
complex(dp), dimension(2) :: F_x, F_y
complex(dp) :: no

call system_clock(t1,rate)
call aggregate(otree, mesh, matrices)
call system_clock(t2,rate)
print*,'Aggregation done in', real(T2-T1)/real(rate), 'seconds'

!*********************** Translate *************************************
call system_clock(t1,rate)
do level = 3, size(otree)
   print*, 'level',level-1, 'size w', size(otree(level)%w)

   allocate(F_th(size(otree(level)%w)))
   allocate(F_phi(size(otree(level)%w)))

   allocate(M_th(size(otree(level)%w)))
   allocate(M_phi(size(otree(level)%w)))
   
   allocate(F2_th(size(otree(level)%w)))
   allocate(F2_phi(size(otree(level)%w)))

   allocate(M2_th(size(otree(level)%w)))
   allocate(M2_phi(size(otree(level)%w)))


   !$omp parallel default(private) &
   !$omp firstprivate(level) &
   !$omp shared(matrices, mesh, otree)
   !$omp do schedule(guided)
   do box = 1, size(otree(level)%tree)
      cp_box1 = otree(level)%tree(box)%cp
      box_dl = otree(level)%tree(box)%dl
      parent = otree(level)%tree(box)%parent
      las = 1
      do i1 = 1, 27 ! near neighbours of parent cube
         neighbour = otree(level-1)%tree(parent)%near_neighbours(i1)
     
         if(neighbour == 0) exit
      
         do i2 = 1, 8 ! children of near neighbours
            box2 = otree(level-1)%tree(neighbour)%children(i2) ! level
            if(box2 == 0) exit

            ! ***check if near box ****
            near_box = 0
            do i3 = 1,27
               if(box2 == otree(level)%tree(box)%near_neighbours(i3)) then
                  near_box = 1
               end if
            end do

            if(near_box == 0) then
               !print*, box, box2,  otree(level)%tree(box)%translator_rec_ind(las)
               cp_box2 = otree(level)%tree(box2)%cp
               r = -(cp_box2 - cp_box1)
               !T = otree(level)%tree(box)%translator(:,las)
       
               trans_ind = translator_index3(r, box_dl)
              
              ! print*, '**************'
               F_th = otree(level)%tree(box2)%out_pattern_theta * otree(level)%translators(:,trans_ind) &
                    * otree(level)%w
               F_phi = otree(level)%tree(box2)%out_pattern_phi * otree(level)%translators(:,trans_ind) &
                    * otree(level)%w

               M_th = otree(level)%tree(box2)%out_Mpattern_theta * otree(level)%translators(:,trans_ind) &
                    * otree(level)%w
               M_phi = otree(level)%tree(box2)%out_Mpattern_phi * otree(level)%translators(:,trans_ind) &
                    * otree(level)%w
               
               F2_th = otree(level)%tree(box2)%out_pattern2_theta * otree(level)%translators2(:,trans_ind) &
                    * otree(level)%w
               F2_phi = otree(level)%tree(box2)%out_pattern2_phi * otree(level)%translators2(:,trans_ind) &
                    * otree(level)%w

               M2_th = otree(level)%tree(box2)%out_Mpattern2_theta * otree(level)%translators2(:,trans_ind) &
                    * otree(level)%w
               M2_phi = otree(level)%tree(box2)%out_Mpattern2_phi * otree(level)%translators2(:,trans_ind) &
                    * otree(level)%w

               
               otree(level)%tree(box)%in_pattern_theta = otree(level)%tree(box)%in_pattern_theta + F_th    
               otree(level)%tree(box)%in_pattern_phi = otree(level)%tree(box)%in_pattern_phi + F_phi

               otree(level)%tree(box)%in_Mpattern_theta = otree(level)%tree(box)%in_Mpattern_theta + M_th         
               otree(level)%tree(box)%in_Mpattern_phi = otree(level)%tree(box)%in_Mpattern_phi + M_phi
      
               otree(level)%tree(box)%in_pattern2_theta = otree(level)%tree(box)%in_pattern2_theta + F2_th  
               otree(level)%tree(box)%in_pattern2_phi = otree(level)%tree(box)%in_pattern2_phi + F2_phi
          
               otree(level)%tree(box)%in_Mpattern2_theta = otree(level)%tree(box)%in_Mpattern2_theta + M2_th       
               otree(level)%tree(box)%in_Mpattern2_phi = otree(level)%tree(box)%in_Mpattern2_phi + M2_phi     
         
                         
            end if
         end do
      end do
   end do
   !$omp end do
   !$omp end parallel 


   deallocate(F_th, F_phi, M_th, M_phi)
   deallocate(F2_th, F2_phi, M2_th, M2_phi)
   !****************************************************
end do
call system_clock(t2,rate)
print*,'Translations done in', real(T2-T1)/real(rate), 'seconds' 


!******************** disaggregation + near zone interactions*************************************

call disaggregation(otree, mesh, matrices)

!! output: matrices%Ax 

end subroutine matvec_mlfma_pmchwt



end module mlfma_pmchwt
