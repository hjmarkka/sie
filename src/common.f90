module common
use iso_fortran_env
use omp_lib  
implicit none

!integer, parameter :: dp = selected_real_kind(15, 307)
!integer, parameter :: ip = selected_int_kind(18)
integer, parameter :: dp = selected_real_kind(real64)
integer, parameter :: ip = selected_int_kind(int64)
real(dp), parameter :: pi = 4.0_dp*atan(1.0_dp)
real(dp), parameter :: epsilon = 8.854187817_dp*(10_dp**(-12.0_dp))
real(dp), parameter :: mu = 4.0_dp*pi*10.0_dp**(-7.0_dp)
integer, parameter :: nthreads = 8


type mesh_struct
   integer, dimension(:,:), allocatable :: etopol, etopol_edges, edges, edgesT
   real(dp), dimension(:,:), allocatable :: coord
   integer, dimension(:), allocatable :: boundary
   integer :: N_node, N_tri, N_edge, Eori(2,3), restart, maxit, N1, N2, N_loc
   real(dp) :: k, tol, near_dist
   complex(dp) :: eps_r
   integer :: acc, p, form
end type mesh_struct

type data
   complex(dp), dimension(:,:), allocatable :: A, E_field, AJ_loc, AM_loc, E_H, E_V
   complex(dp), dimension(:), allocatable :: x, rhs, Ax
   complex(dp), dimension(:), allocatable :: sparse_M
   integer, dimension(:,:), allocatable :: ind_M
   real(dp), dimension(:,:), allocatable :: field_points, rcs
   real(dp) :: khat(3)
   complex(dp) :: E0(3), force(3), torque(3)
   complex(dp), dimension(:), allocatable :: ao
   integer, dimension(:), allocatable :: jao
   integer(ip), dimension(:), allocatable :: iao

end type data

contains

!**************************************************************

function crossRR(a,b) result(c)
real(dp), intent(in) :: a(3), b(3)
real(dp) :: c(3)
c(1) = a(2)*b(3) - a(3)*b(2)
c(2) = -a(1)*b(3) + a(3)*b(1)
c(3) = a(1)*b(2) - a(2)*b(1)

end function crossRR

!***********************************************************

function crossRC(a,b) result(c)
complex(dp), intent(in) :: b(3)
real(dp), intent(in) :: a(3)
complex(dp) :: c(3)
c(1) = a(2)*b(3) - a(3)*b(2)
c(2) = -a(1)*b(3) + a(3)*b(1)
c(3) = a(1)*b(2) - a(2)*b(1)

end function crossRC

!*************************************************************************
function crossCC(a,b) result(c)
complex(dp), intent(in) :: a(3), b(3)
complex(dp) :: c(3)
c(1) = a(2)*b(3) - a(3)*b(2)
c(2) = -a(1)*b(3) + a(3)*b(1)
c(3) = a(1)*b(2) - a(2)*b(1)

end function crossCC

!*************************************************************************
function kkdyad(k) result(dyad)
real(dp) :: dyad(3,3) 
real(dp) :: k(3)

dyad(1,1) = k(1)*k(1)
dyad(1,2) = k(1)*k(2)
dyad(1,3) = k(1)*k(3)

dyad(2,1) = k(2)*k(1)
dyad(2,2) = k(2)*k(2)
dyad(2,3) = k(2)*k(3)

dyad(3,1) = k(3)*k(1)
dyad(3,2) = k(3)*k(2)
dyad(3,3) = k(3)*k(3)

end function kkdyad
!*************************************************************************  
function norm(r, rp) result(c)
implicit none
real(dp), dimension(3), intent(in) :: r, rp
real(dp) :: c
c = sqrt((r(1)-rp(1))**2.0_dp + (r(2)-rp(2))**2.0_dp + (r(3)-rp(3))**2.0_dp)
!norm=sqrt(dot_product(r-rp,r-rp))
end function norm

!*************************************************************************
function vlen(r) result(c)
implicit none
real(dp), dimension(3), intent(in) :: r
real(dp) :: c
c = sqrt((r(1))**2.0_dp + (r(2))**2.0_dp + (r(3))**2.0_dp)
!norm=sqrt(dot_product(r-rp,r-rp))
end function vlen

!**********************************************************************

subroutine linmap_line(P,w,p1,p2,P0,w0)
real(dp), intent(in) :: P0(:), w0(:)
real(dp), intent(in) :: p1(3), p2(3)
real(dp) :: P(3,size(w0)), w(size(w0))
integer :: i1

do i1 = 1,size(w0)
   P(:,i1) = p2*P0(i1) + p1 *(1.0_dp-P0(i1))

   w(i1) = norm(p2,p1)*w0(i1)
end do

end subroutine linmap_line

subroutine linmap_tri(P, W, tri_coord, P0, W0)
implicit none
real(dp), intent(in) :: tri_coord(3,3)
real(dp), intent(in) :: P0(:,:)
real(dp), intent(in) :: W0(:)

real(dp) :: P(3,size(W0)), W(size(W0))

real(dp), dimension(3) :: a, b, ww
real(dp) :: ww1
integer i1, n

n=size(W0)

a=tri_coord(:,2)-tri_coord(:,1);
b=tri_coord(:,3)-tri_coord(:,1);

ww(1) = (a(1)*a(1) +  a(2)*a(2) + a(3)*a(3)) * b(1) 
ww(2) = (a(1)*a(1) +  a(2)*a(2) + a(3)*a(3)) * b(2) 
ww(3) = (a(1)*a(1) +  a(2)*a(2) + a(3)*a(3)) * b(3) 

ww1=sqrt((ww(1)*b(1)+ww(2)*b(2)+ww(3)*b(3))-((a(1)*b(1)+a(2)*b(2)+a(3)*b(3))*(a(1)*b(1)+a(2)*b(2)+a(3)*b(3))));

do i1 = 1,n 
P(:,i1) = tri_coord(:,1) + a * P0(1,i1) + b * P0(2,i1)
end do

W = W0 * ww1 

end subroutine linmap_tri

!*****************************************************************************


function Gr(rf,rp,k) result(Green)
real(dp), intent(in) :: rf(3), rp(3)
complex(dp), intent(in) :: k
real(dp) :: R
complex(dp) :: Green
R=norm(rf,rp)
Green = cdexp(dcmplx(0.0,1.0) * k*R) / (4.0_dp*pi*R)
end function Gr

!************************************************************************

function grad_G(rf,rp,k) result(gradG) 
real(dp), intent(in) :: rf(3), rp(3)
complex(dp), intent(in) :: k
complex(dp) :: gradG(3)  

real(dp) :: R
R=norm(rf,rp)
gradG = cdexp(dcmplx(0.0,1.0)*k*R) / (4.0_dp*pi*R**3.0_dp) * (-1.0_dp + dcmplx(0.0,1.0)*k*R) * (rf-rp)

end function grad_G

!*****************************************************************************


function n_gradGr(rf,rp,k,nvec) result(Green)
real(dp), intent(in) :: rf(3), rp(3), nvec(3)
complex(dp), intent(in) :: k
real(dp) :: R, r_rp(3)

double complex :: Green, G

r_rp = rf-rp
R=norm(rf,rp)
G = cdexp(dcmplx(0.0,1.0)*k*R) / (4.0_dp*pi*R*R*R) * (-1.0_dp + dcmplx(0.0,1.0)*k*R)

Green = G*sum(nvec*r_rp)

end function n_gradGr

!************************************************************************


function tri_n_vectors(tri_coord) result(nvec)
real(dp), intent(in) :: tri_coord(3,3)
real(dp) :: nvec(3)

real(dp) :: a1(3), a2(3), n(3), len_n

a1 = tri_coord(:,2) - tri_coord(:,1)
a2 = tri_coord(:,3) - tri_coord(:,1)

n=crossRR(a1,a2)
len_n = sqrt(n(1)*n(1) + n(2)*n(2) + n(3)*n(3)) 

nvec = n/len_n

end function tri_n_vectors


!*********************************************************************************


function same_edge(fnodes,fnodes2) result(a)
integer :: fnodes(2), fnodes2(2)
integer :: a, i1, i2

a=0
do i1 = 1,2
   do i2 = 1,2
     if(fnodes(i1) == fnodes2(i2)) then
        a = a+1
     end if
   end do
end do

end function same_edge

!***********************************************************************

subroutine gradshape_tri(gradS, ngradS, nhat, tri_coord)

real(dp), intent(in) :: tri_coord(3,3)
real(dp) :: a(3), b(3), c(3), n(3), nL, nhat(3)
real(dp) :: s1(3), s2(3), s3(3), gradS(3,3), ngradS(3,3)

a = tri_coord(:,2) - tri_coord(:,1)
b = tri_coord(:,3) - tri_coord(:,1)
c = tri_coord(:,3) - tri_coord(:,2)

n = crossRR(a,b)
nL = vlen(n)
!A = nL/2.0

nhat = n/nL

s1 = crossRR(nhat,c)
s2 = crossRR(nhat,b)
s3 = crossRR(nhat,a)

gradS(:,1) = s1/nL
gradS(:,2) = -s2/nL
gradS(:,3) = s3/nL

ngradS(:,1) = crossRR(nhat,s1)/nL
ngradS(:,2) = -crossRR(nhat,s2)/nL
ngradS(:,3) = crossRR(nhat,s3)/nL

end subroutine gradshape_tri

function RWG_sign(mesh, edge, tri, tk) result(sign)
type (mesh_struct) :: mesh
integer, intent(in) :: edge, tri, tk
real(dp) :: sign
integer :: enodes(2), enodes2(2)

enodes = mesh%edges(:,edge)
enodes2 = mesh%etopol(mesh%Eori(:,tk),tri)

if(enodes(1)  == enodes2(1)) then
   sign = 1.0_dp
else
   sign = -1.0_dp
end if

end function RWG_sign

function tri_area(coord) result(c)
implicit none
real(dp), dimension(3,3), intent(in) :: coord
real(dp) :: c, tri1, tri2, tri3, tri4


tri1 = vlen(crossRR(coord(:,2)-coord(:,1),coord(:,3)-coord(:,1))) / 2.0_dp

c = tri1
!norm=sqrt(dot_product(r-rp,r-rp))
end function tri_area

function near_zone(T_node, B_node) result(near)
integer, intent(in) :: T_node(3), B_node(3)
integer :: near, i1,i2

near = 0
do i1 = 1,3
   do i2 = 1,3
      if(T_node(i1) == B_node(i2)) then
         near = 1
      end if
   end do
end do

end function near_zone



function unique(invec) result(outvec)
integer :: invec(:)
integer, allocatable :: outvec(:)

integer :: i1, i2, las

las = 1
do i1 = 1,size(invec)
   do i2 = i1+1, size(invec)
       if(invec(i1) == invec(i2)) then 
          invec(i2) = 0
       end if
   end do  
end do

las = 0
do i1 = 1,size(invec)
   if(invec(i1) .ne. 0) then
      las = las + 1
   end if
end do

allocate(outvec(las))



las = 1
do i1 = 1,size(invec)
   if(invec(i1) .ne. 0) then
      outvec(las) = invec(i1)
      las = las + 1
   end if
end do




end function unique
!____________________________


function sph_unit_vectors(theta,phi) result(vec)
real(dp), intent(in) :: theta, phi
real(dp) :: vec(3,3)

vec(:,1) = [sin(theta)*cos(phi), sin(theta)*sin(phi), cos(theta)] ! r_hat
vec(:,2) = [cos(theta)*cos(phi), cos(theta)*sin(phi), -sin(theta)] ! theta_hat
vec(:,3) = [-sin(phi), cos(phi), dble(0.0)] ! phi_hat

end function sph_unit_vectors

function sph2cart(r,theta,phi) result(x)
real(dp), intent(in) :: r, theta, phi
real(dp) :: x(3)

x(1) = r*sin(theta)*cos(phi)
x(2) = r*sin(theta)*sin(phi)
x(3) = r*cos(theta)

end function sph2cart



function cart2sph(x) result(vec)
real(dp), intent(in) :: x(3)
real(dp) :: vec(3)

vec(1) = sqrt(x(1)**2 + x(2)**2 + x(3)**2) ! r
vec(2) = acos(x(3)/vec(1)) ! theta
vec(3) = atan2(x(2),x(1))   

end function cart2sph





function rotation(x, axis, angle) result(xp)
real(dp) :: x(3), angle, xp(3)
integer :: axis

if(axis == 1) then
   xp(1) = x(1)
   xp(2) = cos(angle)*x(2) - sin(angle)*x(3)
   xp(3) = sin(angle)*x(2) + cos(angle)*x(3)
end if

if(axis == 2) then
   xp(1) = cos(angle)*x(1) + sin(angle)*x(3)
   xp(2) = x(2)
   xp(3) = -sin(angle)*x(1) + cos(angle)*x(3)
end if

if(axis == 3) then
   xp(1) = cos(angle)*x(1) - sin(angle)*x(2)
   xp(2) = sin(angle)*x(1) + cos(angle)*x(2)
   xp(3) = x(3)
end if

end function rotation

function halton_seq(index, base) result(num)
integer :: base, index, i
real(dp) :: num, f
num = 0.0_dp

f = 1.0_dp/dble(base)
i = index

do while(i>0) 
   num = num + f * mod(i,base)
   i = floor(dble(i)/dble(base))
   f = f / dble(base)
end do

end function halton_seq


function sp_matmul(val, ind, vec1, N) result(vec)

  real(dp), dimension(:) :: val
  integer, dimension(:,:) :: ind
  complex(dp), dimension(:) :: vec1
  complex(dp), dimension(N) :: vec
  
  integer :: N, i1, ind1, ind2

  vec(:) = dcmplx(0.0,0.0)
  
  do i1 = 1, size(val)
     ind1 = ind(i1,1)
     ind2 = ind(i1,2)
     
     vec(ind1) = vec(ind1) + val(i1) * vec1(ind2)
  end do
  
end function sp_matmul

function sp_matmul_T(val, ind, vec1, N) result(vec)

  real(dp), dimension(:) :: val
  integer, dimension(:,:) :: ind
  complex(dp), dimension(:) :: vec1
  complex(dp), dimension(N) :: vec
  
  integer :: N, i1, ind1, ind2

  vec(:) = dcmplx(0.0,0.0)
  
  do i1 = 1, size(val)
     ind1 = ind(i1,1)
     ind2 = ind(i1,2)
     
     vec(ind2) = vec(ind2) + val(i1) * vec1(ind1)
  end do
  
end function sp_matmul_T



subroutine coocsr(nrow, nnz, a, ir, jc, ao, jao, iao)

!*****************************************************************************80
!
!! COOCSR converts COO to CSR.
!
!  Discussion:
!
!    This routine converts a matrix that is stored in COO coordinate format
!    a, ir, jc into a CSR row general sparse ao, jao, iao format.
!
!  Modified:
!
!    07 January 2004
!
!  Author:
!
!    Youcef Saad
!
!  Parameters:
!
!    Input, integer ( kind = 4 ) NROW, the row dimension of the matrix.
!
!    Input, integer ( kind = 4 ) NNZ, the number of nonzero elements.
!
! a,
! ir,
! jc    = matrix in coordinate format. a(k), ir(k), jc(k) store the nnz
!         nonzero elements of the matrix with a(k) = actual real value of
!         the elements, ir(k) = its row number and jc(k) = its column
!        number. The order of the elements is arbitrary.
!
! on return:
!
! ir       is destroyed
!
!    Output, real AO(*), JAO(*), IAO(NROW+1), the matrix in CSR
!    Compressed Sparse Row format.
!
  implicit none

  integer :: nrow

  real(dp) :: a(:)
  real(dp) :: ao(:)
  integer ::  i
  integer ::  iad
  integer :: iao(nrow+1)
  integer :: ir(:)
  integer :: j
  integer :: jao(:)
  integer :: jc(:)
  integer :: k
  integer :: k0
  integer :: nnz
  real(dp) :: x

  iao(1:nrow+1) = 0
!
!  Determine the row lengths.
!
  do k = 1, nnz
    iao(ir(k)) = iao(ir(k)) + 1
  end do
!
!  The starting position of each row.
!
  k = 1
  do j = 1, nrow+1
     k0 = iao(j)
     iao(j) = k
     k = k + k0
  end do
!
!  Go through the structure once more.  Fill in output matrix.
!
  do k = 1, nnz
     i = ir(k)
     j = jc(k)
     x = a(k)
     iad = iao(i)
     ao(iad) = x
     jao(iad) = j
     iao(i) = iad + 1
  end do
!
!  Shift back IAO.
!
  do j = nrow, 1, -1
    iao(j+1) = iao(j)
  end do
  iao(1) = 1

  return
end subroutine coocsr


subroutine cmplx_coocsr(nrow, nnz, a, ir, jc, ao, jao, iao)

!*****************************************************************************80
!
!! COOCSR converts COO to CSR.
!
!  Discussion:
!
!    This routine converts a matrix that is stored in COO coordinate format
!    a, ir, jc into a CSR row general sparse ao, jao, iao format.
!
!  Modified:
!
!    07 January 2004
!
!  Author:
!
!    Youcef Saad
!
!  Parameters:
!
!    Input, integer ( kind = 4 ) NROW, the row dimension of the matrix.
!
!    Input, integer ( kind = 4 ) NNZ, the number of nonzero elements.
!
! a,
! ir,
! jc    = matrix in coordinate format. a(k), ir(k), jc(k) store the nnz
!         nonzero elements of the matrix with a(k) = actual real value of
!         the elements, ir(k) = its row number and jc(k) = its column
!        number. The order of the elements is arbitrary.
!
! on return:
!
! ir       is destroyed
!
!    Output, real AO(*), JAO(*), IAO(NROW+1), the matrix in CSR
!    Compressed Sparse Row format.
!
  implicit none

  integer :: nrow

  complex(dp) :: a(:)
  complex(dp) :: ao(:)
  integer ::  i
  integer(ip) ::  iad
  integer(ip) :: iao(nrow+1)
  integer :: ir(:)
  integer :: j
  integer :: jao(:)
  integer :: jc(:)
  integer(ip) :: k
  integer(ip) :: k0
  integer(ip) :: nnz
  complex(dp) :: x

  iao(1:nrow+1) = 0
!
!  Determine the row lengths.
!
  do k = 1_ip, nnz
    iao(ir(k)) = iao(ir(k)) + 1
  end do
!
!  The starting position of each row.
!
  k = 1_ip
  do j = 1, nrow+1
     k0 = iao(j)
     iao(j) = k
     k = k + k0
  end do
!
!  Go through the structure once more.  Fill in output matrix.
!
  do k = 1_ip, nnz
     i = ir(k)
     j = jc(k)
     x = a(k)
     iad = iao(i)
     ao(iad) = x
     jao(iad) = j
     iao(i) = iad + 1
  end do
!
!  Shift back IAO.
!
  do j = nrow, 1, -1
    iao(j+1) = iao(j)
  end do
  iao(1) = 1

  return
end subroutine cmplx_coocsr

subroutine amux(n, x, y, a, ja, ia)

!*****************************************************************************80
!
!! AMUX multiplies a CSR matrix A times a vector.
!
!  Discussion:
!
!    This routine multiplies a matrix by a vector using the dot product form.
!    Matrix A is stored in compressed sparse row storage.
!
!  Modified:
!
!    07 January 2004
!
!  Author:
!
!    Youcef Saad
!
!  Parameters:
!
!    Input, integer ( kind = 4 ) N, the row dimension of the matrix.
!
!    Input, real X(*), and array of length equal to the column dimension 
!    of A.
!
!    Input, real A(*), integer ( kind = 4 ) JA(*), IA(NROW+1), the matrix in CSR
!    Compressed Sparse Row format.
!
!    Output, real Y(N), the product A * X.
!
  implicit none

  integer :: n

  real(dp) :: a(:)
  integer :: i
  integer :: ia(:)
  integer :: ja(:)
  integer :: k
  complex(dp) :: t
  complex(dp) :: x(:)
  complex(dp) :: y(n)

  do i = 1, n
!
!  Compute the inner product of row I with vector X.
!
    t = 0.0_dp
    do k = ia(i), ia(i+1)-1
      t = t + a(k) * x(ja(k))
    end do

    y(i) = t

  end do

  return
end subroutine amux


subroutine cmplx_amux(n, x, y, a, ja, ia)
  implicit none

  integer :: n

  complex(dp) :: a(:)
  integer(ip) :: i
  integer(ip) :: ia(:)
  integer :: ja(:)
  integer(ip) :: k
  complex(dp) :: t
  complex(dp) :: x(:)
  complex(dp) :: y(n)

  !$omp parallel default(private) &
  !$omp firstprivate(n) &
  !$omp shared(y, ia, ja, x, a)
  !$omp do schedule(guided)
  do i = 1, n
    t = 0.0_dp
    do k = ia(i), ia(i+1_ip)-1_ip
      t = t + a(k) * x(ja(k))
    end do
    y(i) = t
 end do
 !$omp end do
 !$omp end parallel

 
  return
end subroutine cmplx_amux


subroutine atmux(n, x, y, a, ja, ia)

!*****************************************************************************80
!
!! ATMUX computes A' * x for a CSR matrix A.
!
!  Discussion:
!
!    This routine multiplies the transpose of a matrix by a vector when the
!    original matrix is stored in compressed sparse row storage. Can also be
!    viewed as the product of a matrix by a vector when the original
!    matrix is stored in the compressed sparse column format.
!
!  Modified:
!
!    07 January 2004
!
!  Author:
!
!    Youcef Saad
!
!  Parameters:
!
!    Input, integer ( kind = 4 ) N, the row dimension of the matrix.
!
!    Input, real X(*), an array whose length is equal to the
!    column dimension of A.
!
!    Output, real Y(N), the product A' * X.
!
!    Input, real A(*), integer ( kind = 4 ) JA(*), IA(N+1), the matrix in CSR
!    Compressed Sparse Row format.
!
  implicit none

  integer :: n

  real(dp) :: a(:)
  integer :: i
  integer :: ia(:)
  integer :: ja(:)
  integer :: k
  complex(dp) :: x(:)
  complex(dp) :: y(:)

  y(:) = 0.0_dp

  do i = 1, n
    do k = ia(i), ia(i+1)-1
      y(ja(k)) = y(ja(k)) + x(i) * a(k)
    end do
  end do

  return
end subroutine atmux

end module common
