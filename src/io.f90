module io
use common
use hdf5

implicit none

contains 

subroutine write2file(A,fname)
complex(dp), intent(in) :: A(:,:)
CHARACTER(LEN=28), intent(in) :: fname
CHARACTER(LEN=28) :: filename
!CHARACTER(LEN=7), PARAMETER :: filename = "A.h5" ! File name
CHARACTER(LEN=3), PARAMETER :: dsetname1 = "A_r" ! Dataset name
CHARACTER(LEN=3), PARAMETER :: dsetname2 = "A_i" ! Dataset name

INTEGER(HID_T) :: file_id       ! File identifier
INTEGER(HID_T) :: dset_id1       ! Dataset identifier
INTEGER(HID_T) :: dset_id2       ! Dataset identifier
INTEGER(HID_T) :: dspace_id     ! Dataspace identifier


INTEGER(HSIZE_T), DIMENSION(2) :: dims  ! Dataset dimensions
INTEGER     ::    rank = 2                       ! Dataset rank

INTEGER     ::   error ! Error flag
!INTEGER     :: i, j

filename = fname
dims = (/size(A,1),size(A,2)/)
     
CALL h5open_f(error)

!
! Create a new file using default properties.
!
CALL h5fcreate_f(filename, H5F_ACC_TRUNC_F, file_id, error)

!
! Create the dataspace.
!
CALL h5screate_simple_f(rank, dims, dspace_id, error)

!
! Create and write dataset using default properties.
!
CALL h5dcreate_f(file_id, dsetname1, H5T_NATIVE_DOUBLE, dspace_id, &
                       dset_id1, error, H5P_DEFAULT_F, H5P_DEFAULT_F, &
                       H5P_DEFAULT_F)

CALL h5dwrite_f(dset_id1, H5T_NATIVE_DOUBLE, real(A), dims, error)

CALL h5dcreate_f(file_id, dsetname2, H5T_NATIVE_DOUBLE, dspace_id, &
                       dset_id2, error, H5P_DEFAULT_F, H5P_DEFAULT_F, &
                       H5P_DEFAULT_F)

CALL h5dwrite_f(dset_id2, H5T_NATIVE_DOUBLE, imag(A), dims, error)

!
! End access to the dataset and release resources used by it.
!
CALL h5dclose_f(dset_id1, error)
CALL h5dclose_f(dset_id2, error)
!
! Terminate access to the data space.
!
CALL h5sclose_f(dspace_id, error)

!
! Close the file.
!
CALL h5fclose_f(file_id, error)

!
! Close FORTRAN interface.
!
CALL h5close_f(error)

end subroutine write2file

!_______________________________________________________________________

subroutine real_write2file(A,fname)
real(dp), intent(in) :: A(:,:)
CHARACTER(LEN=28), intent(in) :: fname
CHARACTER(LEN=28) :: filename 

CHARACTER(LEN=7), PARAMETER :: dsetname1 = "mueller" ! Dataset name


INTEGER(HID_T) :: file_id       ! File identifier
INTEGER(HID_T) :: dset_id1       ! Dataset identifier
INTEGER(HID_T) :: dspace_id     ! Dataspace identifier


INTEGER(HSIZE_T), DIMENSION(2) :: dims  ! Dataset dimensions
INTEGER     ::    rank = 2                       ! Dataset rank

INTEGER     ::   error ! Error flag
!INTEGER     :: i, j

dims = (/size(A,1),size(A,2)/)
filename = fname  
 
CALL h5open_f(error)

!
! Create a new file using default properties.
!
CALL h5fcreate_f(filename, H5F_ACC_TRUNC_F, file_id, error)

!
! Create the dataspace.
!
CALL h5screate_simple_f(rank, dims, dspace_id, error)

!
! Create and write dataset using default properties.
!
CALL h5dcreate_f(file_id, dsetname1, H5T_NATIVE_DOUBLE, dspace_id, &
                       dset_id1, error, H5P_DEFAULT_F, H5P_DEFAULT_F, &
                       H5P_DEFAULT_F)

CALL h5dwrite_f(dset_id1, H5T_NATIVE_DOUBLE, A, dims, error)


!
! End access to the dataset and release resources used by it.
!
CALL h5dclose_f(dset_id1, error)

!
! Terminate access to the data space.
!
CALL h5sclose_f(dspace_id, error)

!
! Close the file.
!
CALL h5fclose_f(file_id, error)

!
! Close FORTRAN interface.
!
CALL h5close_f(error)

end subroutine real_write2file
!_---______________________________________________________________

subroutine cmplx_vec_write2file(vec,fname)
complex(dp), intent(in) :: vec(:)
CHARACTER(LEN=28), intent(in) :: fname
CHARACTER(LEN=28) :: filename

!CHARACTER(LEN=7), PARAMETER :: filename = "J.h5" ! File name
CHARACTER(LEN=3), PARAMETER :: dsetname1 = "J_r" ! Dataset name
CHARACTER(LEN=3), PARAMETER :: dsetname2 = "J_i" ! Dataset name
!INTEGER, PARAMETER :: NX = 1128

INTEGER(HID_T) :: file_id       ! File identifier
INTEGER(HID_T) :: dset_id1       ! Dataset identifier
INTEGER(HID_T) :: dset_id2       ! Dataset identifier
INTEGER(HID_T) :: dspace_id     ! Dataspace identifier


INTEGER(HSIZE_T), DIMENSION(1) :: dims           ! Dataset dimensions
INTEGER     ::    rank = 1                       ! Dataset rank

INTEGER     ::   error ! Error flag
!INTEGER     :: i, j

filename = fname
dims = (/size(vec)/)
     
CALL h5open_f(error)

! Create a new file using default properties.
CALL h5fcreate_f(filename, H5F_ACC_TRUNC_F, file_id, error)

! Create the dataspace.
CALL h5screate_simple_f(rank, dims, dspace_id, error)

! Create and write dataset using default properties.

CALL h5dcreate_f(file_id, dsetname1, H5T_NATIVE_DOUBLE, dspace_id, &
                       dset_id1, error, H5P_DEFAULT_F, H5P_DEFAULT_F, &
                       H5P_DEFAULT_F)

CALL h5dwrite_f(dset_id1, H5T_NATIVE_DOUBLE, real(vec), dims, error)

CALL h5dcreate_f(file_id, dsetname2, H5T_NATIVE_DOUBLE, dspace_id, &
                       dset_id2, error, H5P_DEFAULT_F, H5P_DEFAULT_F, &
                       H5P_DEFAULT_F)

CALL h5dwrite_f(dset_id2, H5T_NATIVE_DOUBLE, imag(vec), dims, error)


! End access to the dataset and release resources used by it.
CALL h5dclose_f(dset_id1, error)
CALL h5dclose_f(dset_id2, error)

! Terminate access to the data space.
CALL h5sclose_f(dspace_id, error)


! Close the file.
CALL h5fclose_f(file_id, error)

! Close FORTRAN interface.
CALL h5close_f(error)

end subroutine cmplx_vec_write2file

!________________________________________________________________________

subroutine real_vec_write2file(vec)
real(dp), intent(in) :: vec(:)

CHARACTER(LEN=7), PARAMETER :: filename = "rcs.h5" ! File name
CHARACTER(LEN=3), PARAMETER :: dsetname1 = "rcs" ! Dataset name

INTEGER(HID_T) :: file_id       ! File identifier
INTEGER(HID_T) :: dset_id1       ! Dataset identifier
INTEGER(HID_T) :: dspace_id     ! Dataspace identifier


INTEGER(HSIZE_T), DIMENSION(1) :: dims     ! Dataset dimensions
INTEGER     ::    rank = 1                       ! Dataset rank

INTEGER     ::   error ! Error flag

dims  =  (/size(vec)/)

CALL h5open_f(error)

!
! Create a new file using default properties.
!
CALL h5fcreate_f(filename, H5F_ACC_TRUNC_F, file_id, error)

!
! Create the dataspace.
!
CALL h5screate_simple_f(rank, dims, dspace_id, error)

!
! Create and write dataset using default properties.
!
CALL h5dcreate_f(file_id, dsetname1, H5T_NATIVE_DOUBLE, dspace_id, &
                       dset_id1, error, H5P_DEFAULT_F, H5P_DEFAULT_F, &
                       H5P_DEFAULT_F)

CALL h5dwrite_f(dset_id1, H5T_NATIVE_DOUBLE, vec, dims, error)

!
! End access to the dataset and release resources used by it.
!
CALL h5dclose_f(dset_id1, error)

!
! Terminate access to the data space.
!
CALL h5sclose_f(dspace_id, error)

!
! Close the file.
!
CALL h5fclose_f(file_id, error)

!
! Close FORTRAN interface.
!
CALL h5close_f(error)

end subroutine real_vec_write2file



subroutine write2file_mueller(A, crs, fname)
real(dp), intent(in) :: A(:,:)
real(dp), intent(in) :: crs(3)
CHARACTER(LEN=98), intent(in) :: fname
CHARACTER(LEN=98) :: filename 

CHARACTER(LEN=7), PARAMETER :: dsetname1 = "mueller" ! Dataset name
CHARACTER(LEN=14), PARAMETER :: dsetname2 = "cross_sections" ! Dataset name

INTEGER(HID_T) :: file_id       ! File identifier
INTEGER(HID_T) :: dset_id1, dset_id2      ! Dataset identifier
INTEGER(HID_T) :: dspace_id , dspace_id2    ! Dataspace identifier


INTEGER(HSIZE_T), DIMENSION(2) :: dims! Dataset dimensions
INTEGER(HSIZE_T), DIMENSION(1) :: dims2
INTEGER     ::    rank = 2                       ! Dataset rank
INTEGER     ::    rank2 = 1  

INTEGER     ::   error ! Error flag
!INTEGER     :: i, j

dims = (/size(A,1),size(A,2)/)
dims2 = 3
filename = fname  
!print*,crs
!crs=[Cext,Csca,Cabs,Csca_int]

CALL h5open_f(error)

!
! Create a new file using default properties.
!
CALL h5fcreate_f(filename, H5F_ACC_TRUNC_F, file_id, error)

!
! Create the dataspace.
!
CALL h5screate_simple_f(rank, dims, dspace_id, error)

!
! Create and write dataset using default properties.
!
CALL h5dcreate_f(file_id, dsetname1, H5T_NATIVE_DOUBLE, dspace_id, &
                       dset_id1, error, H5P_DEFAULT_F, H5P_DEFAULT_F, &
                       H5P_DEFAULT_F)

CALL h5dwrite_f(dset_id1, H5T_NATIVE_DOUBLE, A, dims, error)


!
! End access to the dataset and release resources used by it.
!
CALL h5dclose_f(dset_id1, error)

!
! Terminate access to the data space.
!
CALL h5sclose_f(dspace_id, error)

!****************************************************

!
! Create the dataspace.
!
CALL h5screate_simple_f(rank2, dims2, dspace_id2, error)

!
! Create and write dataset using default properties.
!
CALL h5dcreate_f(file_id, dsetname2, H5T_NATIVE_DOUBLE, dspace_id2, &
                       dset_id2, error, H5P_DEFAULT_F, H5P_DEFAULT_F, &
                       H5P_DEFAULT_F)

CALL h5dwrite_f(dset_id2, H5T_NATIVE_DOUBLE, crs, dims2, error)

!
! End access to the dataset and release resources used by it.
!
CALL h5dclose_f(dset_id2, error)

!
! Terminate access to the data space.
!
CALL h5sclose_f(dspace_id2, error)


!
! Close the file.
!
CALL h5fclose_f(file_id, error)

!
! Close FORTRAN interface.
!
CALL h5close_f(error)

end subroutine write2file_mueller
!_---______________________________________________________________



subroutine write2file_surf(E_H, E_V, angles,filename)
  complex(dp), intent(in) :: E_H(:,:), E_V(:,:)
  real(dp), intent(in) :: angles(:,:)
  
CHARACTER(LEN=28), intent(in) :: filename

CHARACTER(LEN=28), PARAMETER :: dsetname1 = "EH_r" ! Dataset name
CHARACTER(LEN=28), PARAMETER :: dsetname2 = "EH_i" ! Dataset name
CHARACTER(LEN=28), PARAMETER :: dsetname3 = "EV_r" ! Dataset name
CHARACTER(LEN=28), PARAMETER :: dsetname4 = "EV_i" ! Dataset name
CHARACTER(LEN=28), PARAMETER :: dsetname5 = "angles" ! Dataset name


INTEGER(HID_T) :: file_id       ! File identifier
INTEGER(HID_T) :: dset_id1       ! Dataset identifier
INTEGER(HID_T) :: dset_id2       ! Dataset identifier
INTEGER(HID_T) :: dset_id3       ! Dataset identifier
INTEGER(HID_T) :: dset_id4       ! Dataset identifier
INTEGER(HID_T) :: dset_id5       ! Dataset identifier

INTEGER(HID_T) :: dspace_id     ! Dataspace identifier
INTEGER(HID_T) :: dspace_id2     ! Dataspace identifier

INTEGER(HSIZE_T), DIMENSION(2) :: dims, dims2  ! Dataset dimensions
INTEGER     ::    rank = 2                       ! Dataset rank

INTEGER     ::   error ! Error flag
!INTEGER     :: i, j


dims = (/size(E_H,1),size(E_H,2)/)
dims2 = (/size(angles,1),size(angles,2)/)   

CALL h5open_f(error)

!
! Create a new file using default properties.
!
CALL h5fcreate_f(filename, H5F_ACC_TRUNC_F, file_id, error)

!
! Create the dataspace.
!
CALL h5screate_simple_f(rank, dims, dspace_id, error)

!
! Create and write dataset using default properties.
!
CALL h5dcreate_f(file_id, dsetname1, H5T_NATIVE_DOUBLE, dspace_id, &
                       dset_id1, error, H5P_DEFAULT_F, H5P_DEFAULT_F, &
                       H5P_DEFAULT_F)
CALL h5dwrite_f(dset_id1, H5T_NATIVE_DOUBLE, real(E_H), dims, error)
CALL h5dclose_f(dset_id1, error)

CALL h5dcreate_f(file_id, dsetname2, H5T_NATIVE_DOUBLE, dspace_id, &
                       dset_id2, error, H5P_DEFAULT_F, H5P_DEFAULT_F, &
                       H5P_DEFAULT_F)
CALL h5dwrite_f(dset_id2, H5T_NATIVE_DOUBLE, imag(E_H), dims, error)
CALL h5dclose_f(dset_id2, error)

!''''''

!
! Create and write dataset using default properties.
!
CALL h5dcreate_f(file_id, dsetname3, H5T_NATIVE_DOUBLE, dspace_id, &
                       dset_id3, error, H5P_DEFAULT_F, H5P_DEFAULT_F, &
                       H5P_DEFAULT_F)
CALL h5dwrite_f(dset_id3, H5T_NATIVE_DOUBLE, real(E_V), dims, error)
CALL h5dclose_f(dset_id3, error)

CALL h5dcreate_f(file_id, dsetname4, H5T_NATIVE_DOUBLE, dspace_id, &
                       dset_id4, error, H5P_DEFAULT_F, H5P_DEFAULT_F, &
                       H5P_DEFAULT_F)
CALL h5dwrite_f(dset_id4, H5T_NATIVE_DOUBLE, imag(E_V), dims, error)
CALL h5dclose_f(dset_id4, error)

CALL h5sclose_f(dspace_id, error)

!''''''
!
! Create the dataspace.
!
CALL h5screate_simple_f(rank, dims2, dspace_id2, error)

CALL h5dcreate_f(file_id, dsetname5, H5T_NATIVE_DOUBLE, dspace_id2, &
                       dset_id5, error, H5P_DEFAULT_F, H5P_DEFAULT_F, &
                       H5P_DEFAULT_F)
CALL h5dwrite_f(dset_id5, H5T_NATIVE_DOUBLE, angles, dims2, error)
CALL h5dclose_f(dset_id5, error)




! Terminate access to the data space.
!
CALL h5sclose_f(dspace_id2, error)

!
! Close the file.
!
CALL h5fclose_f(file_id, error)

!
! Close FORTRAN interface.
!
CALL h5close_f(error)

end subroutine write2file_surf



end module 

