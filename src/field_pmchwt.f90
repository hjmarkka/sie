module field_pmchwt
use common
use integration_points
use omp_lib
implicit none
 

contains

function absorption_cross_section(matrices, mesh) result(Cabs)
type (mesh_struct) :: mesh
type (data) :: matrices
real(dp) :: Cabs

integer :: ele, i1, be, basis, bi, bj, be2, basis2, bk, bl
real(dp), allocatable :: P0_tri(:,:), Pt(:,:), shape_tri(:,:)
real(dp), allocatable :: w0_tri(:),wt(:)
real(dp) :: T_coord(3,3), T_dN(3,3), T_nxdN(3,3), At, alok(3,3), T_nvec(3), el, signt, el2, signt2
complex(dp) :: a(3), b(3), a1(3), a2(3), a3(3), a4(3), J(3), M(3)
real(dp) :: alok2(3)

call inttri(P0_tri,w0_tri,5)

allocate(shape_tri(3,size(w0_tri)))
shape_tri(1,:) = 1.0_dp - P0_tri(1,:) - P0_tri(2,:)
shape_tri(2,:) = P0_tri(1,:)
shape_tri(3,:) = P0_tri(2,:)

allocate(Pt(3,size(w0_tri)))
allocate(wt(size(w0_tri)))

Cabs = 0.0_dp
!$omp parallel default(private) &
!$omp firstprivate(shape_tri, P0_tri, w0_tri) &
!$omp shared(matrices, mesh, Cabs)
!$omp do reduction(+:Cabs)
do ele = 1, mesh%N_tri

   T_coord = mesh%coord(:,mesh%etopol(:,ele))
   call gradshape_tri(T_dN,T_nxdN, T_nvec, T_coord)
   At = tri_area(T_coord)

   call linmap_tri(Pt, wt, T_coord, P0_tri, W0_tri)

   alok(:,:) = 0.0_dp
   alok2(:) = 0.0_dp
   do i1 = 1, size(w0_tri)
      alok(:,1) = alok(:,1) + shape_tri(:,i1) * shape_tri(1,i1) * wt(i1)
      alok(:,2) = alok(:,2) + shape_tri(:,i1) * shape_tri(2,i1) * wt(i1)
      alok(:,3) = alok(:,3) + shape_tri(:,i1) * shape_tri(3,i1) * wt(i1)
      
       alok2 = alok2 + shape_tri(:,i1) * wt(i1)
   end do
  
    do be = 1, 3
      basis = mesh%etopol_edges(be,ele)
      signt = RWG_sign(mesh, basis, ele,be)
      
      el = norm(mesh%coord(:,mesh%edges(1,basis)), mesh%coord(:,mesh%edges(2,basis)))

      bi = mesh%Eori(1,be)
      bj = mesh%Eori(2,be)

      do be2 = 1,3
         basis2 = mesh%etopol_edges(be2,ele)
         signt2 = RWG_sign(mesh, basis2, ele,be2)
         
         el2 = norm(mesh%coord(:,mesh%edges(1,basis2)), mesh%coord(:,mesh%edges(2,basis2)))
         
         bk = mesh%Eori(1,be2)
         bl = mesh%Eori(2,be2)

         
       
         a1 = crossRR(T_nxdN(:,bj), T_nxdN(:,bl)) * alok(bi,bk)
         a2 = crossRR(T_nxdN(:,bj), T_nxdN(:,bk)) * alok(bi,bl)
         a3 = crossRR(T_nxdN(:,bi), T_nxdN(:,bl)) * alok(bj,bk) 
         a4 = crossRR(T_nxdN(:,bi), T_nxdN(:,bk)) * alok(bj,bl)

         a = (a1 - a2 - a3 + a4) * signt * signt2 * el *el2 * conjg(matrices%x(basis)) * (matrices%x(mesh%N_edge + basis2)) 
         
          Cabs = Cabs - real(dot_product(T_nvec,a))
         

!         J = el * signt * (T_nxdN(:,bj) * alok2(bi) - T_nxdN(:,bi) * alok2(bj)) * matrices%x(basis)
!         M = el2 * signt2 * (T_nxdN(:,bl) * alok2(bk) - T_nxdN(:,bk) * alok2(bl)) * matrices%x(mesh%N_edge+basis2)

         !Cabs = Cabs + real(dot_product(J, crossRC(T_nvec,M)) - dot_product(M, crossRC(T_nvec,J))) / 2.0d0
!         Cabs = Cabs - real(dot_product(T_nvec, crossCC(conjg(J),M)))
      end do
   end do
 
  
end do
!$omp end do
!$omp end parallel
Cabs = Cabs * sqrt(mu/epsilon) 


end function absorption_cross_section

function fields(matrices, mesh, r) result(E)
type (mesh_struct) :: mesh
type (data) :: matrices

integer :: test_T, i1, te, test, ti, tj
real(dp) :: T_coord(3,3), T_dN(3,3), T_nxdN(3,3), T_nvec(3), r(3), rp(3), rhat(3)
real(dp), allocatable :: P0_tri(:,:), Pt(:,:), shape_tri(:,:)
real(dp), allocatable :: w0_tri(:),wt(:)
complex(dp) :: alok(3), E(3), a(3), b(3), c(3), a2(3), b2(3), c2(3), H(3), alok2(3,3), EH(3,2)
real(dp) :: signt, el, At

call inttri(P0_tri,w0_tri,3)

allocate(shape_tri(3,size(w0_tri)))
shape_tri(1,:) = 1.0_dp - P0_tri(1,:) - P0_tri(2,:)
shape_tri(2,:) = P0_tri(1,:)
shape_tri(3,:) = P0_tri(2,:)

allocate(Pt(3,size(w0_tri)))
allocate(wt(size(w0_tri)))

E(:) = dcmplx(0.0,0.0)
H(:) =dcmplx(0.0,0.0)


do test_T = 1, mesh%N_tri

   T_coord = mesh%coord(:,mesh%etopol(:,test_T))
   call gradshape_tri(T_dN,T_nxdN, T_nvec, T_coord)
   At = tri_area(T_coord)

   call linmap_tri(Pt, wt, T_coord, P0_tri, W0_tri)

   alok(:) = dcmplx(0.0,0.0)
   alok2(:,:) = dcmplx(0.0,0.0)

   do i1 = 1,size(w0_tri)
      rp=Pt(:,i1)
      rhat = (r - rp)/norm(r,rp)

      alok = alok + Gr(r,rp,dcmplx(mesh%k,0.0)) * shape_tri(:,i1) * wt(i1)
      
      alok2(:,1) = alok2(:,1) + grad_G(r,rp,dcmplx(mesh%k,0.0)) * shape_tri(1,i1)* wt(i1)
      alok2(:,2) = alok2(:,2) + grad_G(r,rp,dcmplx(mesh%k,0.0)) * shape_tri(2,i1)* wt(i1)
      alok2(:,3) = alok2(:,3) + grad_G(r,rp,dcmplx(mesh%k,0.0)) * shape_tri(3,i1)* wt(i1)

   end do

   do te = 1, 3
      test = mesh%etopol_edges(te,test_T)
      signt = RWG_sign(mesh, test, test_T,te)
      
      el = norm(mesh%coord(:,mesh%edges(1,test)), mesh%coord(:,mesh%edges(2,test)))

      ti = mesh%Eori(1,te)
      tj = mesh%Eori(2,te)

      a =  signt * el * (T_nxdN(:,tj)*alok(ti) - T_nxdN(:,ti)*alok(tj)) * matrices%x(test)
      
      b = signt * el * (alok2(:,1) + alok2(:,2) + alok2(:,3)) * matrices%x(test)

      c = signt * el * (crossRC(T_nxdN(:,tj),alok2(:,ti)) - crossRC(T_nxdN(:,ti),alok2(:,tj))) * matrices%x(mesh%N_edge+test)

      a2 =  signt * el * (T_nxdN(:,tj)*alok(ti) - T_nxdN(:,ti)*alok(tj)) * matrices%x(mesh%N_edge+test)
      
      b2 = signt * el * (alok2(:,1) + alok2(:,2) + alok2(:,3)) * matrices%x(mesh%N_edge+test)

      c2 = signt * el * (crossRC(T_nxdN(:,tj),alok2(:,ti)) - crossRC(T_nxdN(:,ti),alok2(:,tj))) * matrices%x(test)
     
       E = E - sqrt(mu/epsilon) * dcmplx(0.0,mesh%k)*a + &
            1.0_dp/At * sqrt(mu/epsilon)/dcmplx(0.0,mesh%k)*b - c
          
       H = H - 1.0_dp/sqrt(mu/epsilon) * dcmplx(0.0,mesh%k)*a2 + &
            1.0_dp/At * 1.0_dp/sqrt(mu/epsilon)/dcmplx(0.0,mesh%k)*b2 + c2

       
   end do

end do

deallocate(P0_tri, Pt, shape_tri, w0_tri,wt)

EH(:,1) = E
EH(:,2) = H

end function fields





subroutine surface_fields(matrices, mesh, EH, r)
type (mesh_struct) :: mesh
type (data) :: matrices

integer :: test_T, i1, te, test, ti, tj
real(dp) :: T_coord(3,3), T_dN(3,3), T_nxdN(3,3), T_nvec(3), rp(3), rhat(3)
real(dp), allocatable :: P0_tri(:,:), Pt(:,:), shape_tri(:,:)
real(dp), allocatable :: w0_tri(:),wt(:), r(:,:)
complex(dp) :: alok(3), E(3), a(3), b(3), c(3), a2(3), b2(3), c2(3), H(3), alok2(3,3)
real(dp) :: signt, el, At
complex(dp), allocatable :: EH(:,:)
call inttri(P0_tri,w0_tri,1)

allocate(shape_tri(3,size(w0_tri)))
shape_tri(1,:) = 1.0_dp - P0_tri(1,:) - P0_tri(2,:)
shape_tri(2,:) = P0_tri(1,:)
shape_tri(3,:) = P0_tri(2,:)

allocate(Pt(3,size(w0_tri)))
allocate(wt(size(w0_tri)))

E(:) = dcmplx(0.0,0.0)
H(:) =dcmplx(0.0,0.0)

allocate(EH(6,mesh%N_tri))
allocate(r(3,mesh%N_tri))

do test_T = 1, mesh%N_tri

   T_coord = mesh%coord(:,mesh%etopol(:,test_T))
   call gradshape_tri(T_dN,T_nxdN, T_nvec, T_coord)
   At = tri_area(T_coord)

   call linmap_tri(Pt, wt, T_coord, P0_tri, W0_tri)

   rp=Pt(:,1)
   
   do te = 1, 3
      test = mesh%etopol_edges(te,test_T)
      signt = RWG_sign(mesh, test, test_T,te)
      
      el = norm(mesh%coord(:,mesh%edges(1,test)), mesh%coord(:,mesh%edges(2,test)))

      ti = mesh%Eori(1,te)
      tj = mesh%Eori(2,te)

      a =  signt * el * (T_nxdN(:,tj)*shape_tri(ti,1) - T_nxdN(:,ti)*shape_tri(tj,1)) * matrices%x(test)
      

      a2 =  signt * el * (T_nxdN(:,tj)*shape_tri(ti,1) - T_nxdN(:,ti)*shape_tri(tj,1)) * matrices%x(mesh%N_edge+test)
      
     
      EH(1:3,test_T) = EH(1:3,test_T) + a
      EH(4:6,test_T) = EH(4:6,test_T) + a2
      
   end do

   r(:,test_T) = rp
end do

deallocate(P0_tri, Pt, shape_tri,  w0_tri,wt)

end subroutine surface_fields


function farfield_efie(matrices, mesh, r) result(E)


type (mesh_struct) :: mesh
type (data) :: matrices

integer :: test_T, i1, te, test, ti, tj
real(dp) :: T_coord(3,3), T_dN(3,3), T_nxdN(3,3), T_nvec(3), r(3), rp(3), rhat(3)
real(dp), allocatable :: P0_tri(:,:), Pt(:,:), shape_tri(:,:)
real(dp), allocatable :: w0_tri(:),wt(:)
complex(dp) :: alok(3), E(3), b(3), b2(3), E2(3), codE(3), coE(3), a1(3),a2(3)
real(dp) :: signt, el, T_area, I(3,3)


I(:,:) = 0.0_dp
I(1,1) = 1.0_dp
I(2,2) = 1.0_dp
I(3,3) = 1.0_dp

call inttri(P0_tri,w0_tri,3)

allocate(Pt(3,size(w0_tri)))
allocate(wt(size(w0_tri)))

allocate(shape_tri(3,size(w0_tri)))
shape_tri(1,:) = 1.0_dp - P0_tri(1,:) - P0_tri(2,:)
shape_tri(2,:) = P0_tri(1,:)
shape_tri(3,:) = P0_tri(2,:)


E(:) = dcmplx(0.0,0.0)
rhat = r/vlen(r)
      
do test_T = 1, mesh%N_tri

   T_coord = mesh%coord(:,mesh%etopol(:,test_T))
   T_area = tri_area(T_coord)
   call gradshape_tri(T_dN,T_nxdN, T_nvec, T_coord)

   call linmap_tri(Pt, wt, T_coord, P0_tri, W0_tri)

   alok(:) = dcmplx(0.0,0.0)
   
   do i1 = 1,size(w0_tri)
      rp=Pt(:,i1)
      !rhat = (r-rp)/norm(r,rp)
      
      !alok = alok + exp(-dcmplx(0.0,1.0)*mesh%k * dot_product(rhat,rp)) * shape_tri(:,i1) * wt(i1)
      alok = alok + Gr(r,rp,dcmplx(mesh%k,0.0)) * shape_tri(:,i1) * wt(i1)
   end do

   do te = 1, 3
      test = mesh%etopol_edges(te,test_T)
      signt = RWG_sign(mesh, test, test_T,te)
      
      el = norm(mesh%coord(:,mesh%edges(1,test)), mesh%coord(:,mesh%edges(2,test)))

      ti = mesh%Eori(1,te)
      tj = mesh%Eori(2,te)

      a1 =  signt * el * (T_nxdN(:,tj)*alok(ti) - T_nxdN(:,ti)*alok(tj)) * matrices%x(test)
      a2 =  signt * el * (T_nxdN(:,tj)*alok(ti) - T_nxdN(:,ti)*alok(tj)) * matrices%x(test+mesh%N_edge)

       E = E + sqrt(mu/epsilon) * dcmplx(0.0,mesh%k)* matmul(I-kkdyad(rhat),a1) - &
             dcmplx(0.0,mesh%k)*crossRC(rhat,a2) 
      
      
   end do
  
end do

!E= E*1.0_dp/vlen(r) 

deallocate(P0_tri,w0_tri, Pt, wt)

end function farfield_efie

subroutine calc_fields(matrices,mesh) 
type (mesh_struct) :: mesh
type (data) :: matrices

integer :: i1
real(dp) :: r(3)

allocate(matrices%E_field(3,size(matrices%field_points,2)))

do i1 = 1, size(matrices%field_points,2)
   r = matrices%field_points(:,i1)

   matrices%E_field(:,i1) = fields(matrices, mesh, r)

end do

end subroutine


subroutine compute_reflectance(matrices,mesh, N_phi, N_theta) 
type (mesh_struct) :: mesh
type (data) :: matrices

integer :: i1, i2, N_phi, N_theta
real(dp) :: r(3), phi, theta, RR, per(3), par(3)
complex(dp) :: E(3)
allocate(matrices%E_field(3,N_phi*N_theta))
allocate(matrices%field_points(2,N_phi*N_theta))

RR = 2.0_dp*pi*100000.0_dp/mesh%k

do i1 = 1, N_phi
   phi = dble(i1-1) * 2.0_dp*pi/dble(N_phi)
   
   do i2 = 1, N_theta
      theta = dble(i2-1) * pi/2.0_dp/dble(N_theta) + pi/dble(N_theta)/4.0_dp 

      r(1) = RR*sin(theta)*cos(phi)
      r(2) = RR*sin(theta)*sin(phi)
      r(3) = RR*cos(theta)

      per = crossRR(matrices%khat,r)
      per = per/vlen(per)
      par = crossRR(per, r)
      par = par/vlen(par)
      !print*, vlen(per), vlen(par)
      !matrices%E_field(:, i2 + (i1-1)*N_theta ) = fields(matrices, mesh, r)
      E = fields(matrices, mesh, r)
      matrices%E_field(1, i2 + (i1-1)*N_theta ) = dot_product(E,per)
      matrices%E_field(2, i2 + (i1-1)*N_theta ) = dot_product(E,par)
      matrices%E_field(3, i2 + (i1-1)*N_theta ) = 0.0_dp
      
      matrices%field_points(:,i2 + (i1-1)*N_theta) =[phi,theta]
      
   end do
end do
end subroutine


!_______________________________________________________________
subroutine compute_rcs(matrices,mesh)
type (data) :: matrices
type (mesh_struct), intent(in) :: mesh
integer :: N_theta, i2
real(dp) :: r(3), r2(3), RR, lambda, theta, phi, phi2
complex(dp) :: E(3), E2(3)


N_theta = 180

allocate(matrices%rcs(N_theta+1,2))

lambda = (2.0_dp*pi)/mesh%k
RR = 100.0_dp*lambda

phi = 0.0_dp!pi/2.0
phi2 = pi/2.0_dp

do i2 = 0, N_theta

   theta = pi * dble(i2) / dble(N_theta) 
  
   r(1) = RR*sin(theta)*cos(phi)
   r(2) = RR*sin(theta)*sin(phi)
   r(3) = RR*cos(theta)

   r2(1) = RR*sin(theta)*cos(phi2)
   r2(2) = RR*sin(theta)*sin(phi2)
   r2(3) = RR*cos(theta)

   E = fields(matrices, mesh, r)
   E2 = fields(matrices, mesh, r2)

   matrices%rcs(i2+1,1) = 4*pi*RR**2/(lambda**2) * dble(dot_product(E,E))
    matrices%rcs(i2+1,2) = 4*pi*RR**2/(lambda**2) * dble(dot_product(E2,E2))  

end do


end subroutine compute_rcs



end module field_pmchwt
