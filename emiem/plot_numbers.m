function plot_numbers(point, ind, do_hold)
% function plot_numbers(point, ind, do_hold)
% point      - (N,number_of_points). N is 2 or 3.
% ind        - (1,number_of_points). label numbers.
% do_hold    - If 1, holds the picture first.

if nargin < 3
  do_hold = 1;
end

if do_hold
  release = ~ishold;
  hold on
end

N = size(point,1);
v = version;
if v(1) > '4'
  if N == 3
    h = text(point(1,:), point(2,:), point(3,:), int2str(ind(:)));
  else
    h = text(point(1,:), point(2,:), int2str(ind(:)));
  end
  set(h, 'HorizontalAlignment', 'center');
%  set(h,'FontSize',5);
else
  if N == 3
    for i = 1:length(ind)
      h = text(point(1,i), point(2,i), point(3,i), int2str(ind(i)));
      set(h, 'HorizontalAlignment', 'center');
%      set(h,'FontSize',5);
    end
  else
    for i = 1:length(ind)
      h = text(point(1,i), point(2,i), int2str(ind(i)));
      set(h, 'HorizontalAlignment', 'center');
%      set(h,'FontSize',5);
    end
  end
end

if do_hold
  if release
    hold off
  end
else
  % text() does not set axes properly in Matlab 5.3:
  x = point(1,:);
  y = point(2,:);
  z = point(3,:);
  if min(z) == max(z)
    axis([min(x) max(x) min(y) max(y)]);
  else
    axis([min(x) max(x) min(y) max(y) min(z) max(z)]);
  end
end
