function [E, eind] = add_edges(p1, p2, P, E)
% function [E, eind] = add_edges(p1, p2, P, E)
%
% Add new edges of (p1,p2) into geometry (P,E)
% P1 and p2, are indeces of the points P. They may also be vectors or matrices
% for several edges.
%
% Returns indeces of the new edges or similar old edges.
%

if nargin < 4
  E = zeros(3,0);
end

% Reshape point indeces into row vectors:
[morig,norig] = size(p1);
p1 = p1(:).';
p2 = p2(:).';

n = length(p1);
ne = size(E,2);

% New edges:
Eadd = calc_edge([p1; p2; zeros(1,n)], P);

if size(E,2) > 0
  % Add new edges:
  [E,eind] = add_indeces(E, Eadd, 1:2);
else
  E = Eadd;
  eind = 1:size(E,2);
end

if nargout > 1
  % Reshape edge indeces into original size:
  eind = reshape(eind, morig, norig);
end
