function plot_tetras(P,E,T,V,do_numbers,color,Vind)
% function plot_tetras(P,E,T,V,[do_numbers,color,Vind])
%
% Input:
% - P,E,T: Points, edges and triangles. See connect.m .
% - V: Tetrahedrons. See make_tetra.m .
% - do_numbers: If 1, plot tetrahedron numbers.   Default: 0.
% - color: Color.                                 Default: 'r'.
% - Vind: Only these tetrahedrons.                Default: 1:size(V,2).

% Default values:
if nargin < 7
  Vind = [];
  if nargin < 6
    color = [];
    if nargin < 5
      do_numbers = [];
    end
  end
end
if length(do_numbers) == 0
  do_numbers = 0;  
end
if length(color) == 0
  color = 'r-';
end
if length(Vind) == 0
  Vind = 1:size(V,2);
end

% Plot tetrahedron edges:
Eind = zeros(size(E,2),1);
Eind(V(5:10,Vind)) = ones(6,length(V(1,Vind)));
plot_edges(P, E, 0, color, find(Eind));

if do_numbers
  Pc = V(16:18,Vind);  % Center points of the tetrahedrons.
  plot_numbers(Pc, Vind);
end
