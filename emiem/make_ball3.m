function [P,E,T,B] = make_ball2(p,r,f,c,t,e)
% function [P,E,T,B] = make_ball2(p,r,f,c,t,e)
% Create a dome into point p having radius.
%
%  p - point (N,1) or [p,pv1,pv2] where pvi are plane vectors. Default [0;0;0].
%                                  Default plane: pv1 = [1;0;0], pv2 = [0;1;0].
%  r - radius.                                                 Default 1.
%  f - geodesic frequency.                                     Default 3.
%  c - class type (1 of 2).                                    Default 1.
%  t - polyhedron type 'i' for icosahedron                     Default 'i'.
%                      'o' for octahedron
%                      't' for tetrahedron
%  e - ellipse and specifies eccentricity ( 0.0 < e < 2.0)     Default 1.0
%  
% Usage:
%  make_ball2(p,r,n)            - Ball having origin p and radius r.

% Default values:
if nargin < 6
  e = [];
  if nargin < 5
    t = [];
    if nargin < 4
      c = [];
      if nargin < 3
	f = [];
	if nargin < 2
	  r = [];
	  if nargin < 1
	    p = [];
	  end
	end
      end
    end
  end
end

if length(p) == 0, p   = [0;0;0]; end
if length(r) == 0, r      = 1   ; end
if length(f) == 0, f      = 3   ; end
if length(c) == 0, c      = 1   ; end
if length(t) == 0, t      = 'i' ; end
if length(e) == 0, e      = 1.0 ; end

% Check dimensions:
[N,M] = size(p);        % Dimension.
if (N < 2) & (N > 3)
  error('N should be either 2 or 3!');
end

% Frequency:
if (ceil(f) ~= f) | (f <= 0)
  error('Frequency F should be an positive integer!');
end

% Class type:
if (ceil(c) ~= c) | (c <= 0) | (c >= 3)
  error('Class type C should be either 1 or 2!');
end
if (c == 2) & (ceil(f/2) ~= f/2)
  error('Class type C = 2 requires even frequency!');
end

% Polyhedron type:
if (t ~= 'i') & (t ~= 'o') & (t ~= 't')
  error('Polyhedron type T should be either ''i'', ''o'' or ''t''!');
end

% Eccentricity:
if (e <= 0) | ( e >= 2)
  error('Eccentricity E should be 0.0 < E < 2.0!');
end

% Unique name for files:
[dummy,ext] = fileparts(tempname);  % Temporary file name without directory.
ext = ext(3:end);                   % Remove "tp".
filename = ['dome' ext];
dxffile  = [filename '.dxf'];
txtfile  = [filename '.txt'];

% Create shell command:
cmd = sprintf('!dome -s -f%d -c%d -p%c -e%.5f %s > /dev/null',f,c,t,e,dxffile);

% Remove old file:
eval(['! rm -f ' dxffile]);

% Run dome program:
eval(cmd);

% Check if the output file exists:
if ~exist(dxffile)
  error(sprintf('Unable to create dome triangles with command:\n%s',cmd));
end

% Get the path into EMIEM toolbox:
empath = emiempath;

% Convert DXF file into triangle vertices:
cmd = sprintf('!%sdxf2etopol < %s > %s', empath,dxffile, txtfile);
eval(cmd)
eval(['! rm -f ' dxffile]);

% Check if the output file exists:
if ~exist(txtfile)
  error(sprintf('Unable to create dome triangles with command:\n%s',cmd));
end

% Load triangle vertex coordinates:
eval(sprintf('load %s', txtfile));
eval(['! rm -f ' txtfile]);
eval(sprintf('tp = %s;', filename));
nt = tp(end,1);
np = tp(end,2);
etopol = tp(1:nt,:);
coord = tp(nt+(1:np),:);

% Scale and move points:
coord = r*coord + p(:,ones(1,size(coord,1))).';

% Check the orientation of the first triangle for reorder_normal:
%ps1 = coord(etopol(1,:),:).';  % Triangle 1 vertices.
%N = cross(ps1(:,2)-ps1(:,1),ps1(:,3)-ps1(:,1));
%pc = sum(ps1,2)/3;  % Centroid
%if dot(N,pc-p) < 0
%  etopol(1,:) = etopol(1,[1 3 2]);
%end

% Create geometry:
[P,E,T,B] = etopol3geometry(coord, etopol);

% Reorder triangle normals:
%T = reorder_normal(T);
N = triangle_normals(P,T);
ind = find(dot(N, T(8:10,:) - p(:,ones(1,size(T,2)))) < 0);
if length(ind)
  T = reorder_triangle(T,ind);
end
