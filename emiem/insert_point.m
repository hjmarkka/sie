function [P,E,T,B,ind] = insert_point(p,P,E,T,B,tol,force)
% function [P,E,T,B,ind] = insert_point(p,P,E,T,B,[tol,force])
% Insert one point into geometry modifying points (P), edges (E),
% triangles (T) and bases (B). If the point is not close enough (tol) to a 
% old point creates new edges, triangles and bases. Return modified 
% geometry and index of the inserted point. Default tol is 1e-3.

if 0
  [P,E,T,B] = make_rectangle([0;0;1],[1;0;1],[1;1;1],[0;1;1],1);
%  [P1,E1,T1,B1] = make_triangle([0;0;1],[1;1;1],[0.25;0.25;2],1);
%  [P,E,T,B] = connect(P,E,T,B,P1,E1,T1,B1);
  for i = 1:200
    pp = [rand(2,1); 1];
    [P,E,T,B,pind] = insert_point(pp, P, E, T, B);
    plot_triangles(P,E,T); axis equal; view(2)
    drawnow
  end
end

% Default values:
if nargin < 7
  force = [];
  if nargin < 6
    tol = [];
  end
end
if length(force) == 0
  force = 1;
end
if length(tol) == 0
  tol = 1e-3;
end

% Get edge points and lengths:
pe1 = E(1,:);
pe2 = E(2,:);
Pe1 = P(:,pe1);
Pe2 = P(:,pe2);
l   = E(3,:);

% Find nearest geometry point:
PO = p(:,ones(1,size(P,2)))-P;
r = vlen(PO);
[pmin,pind] = min(r);

% Find nearest edge:
P21 = Pe2-Pe1;
PE = PO(:,pe1);
s = dot(P21,PE)./l;
ind = (s >= 0) & (s <= l);
re = zeros(size(l));
re(~ind) = min(r(pe1(~ind)),r(pe2(~ind)));
re(ind) = vlen(P21(:,ind)*diag(s(ind)./l(ind)) - PE(:,ind));
[emin,eind] = min(re);

% Special case: if the point is very close to the nearest point:
if pmin/l(eind) <= tol
% disp(['No points added, old point ', int2str(pind)]);
  ind = pind;
  return
end

% Check if the point is near enough to an edge:
if emin/l(eind) <= tol
% disp(['The point is on edge ', int2str(eind), ', splitting it']);
  [P,E,T,B,ind] = split_edge(eind,p,P,E,T,B);
else
  % Not on a edge, search the nearest triangle:

  % Find near triangles using the bounding boxes:
  [Tmin,Tmax] = Tboundingbox(P,E,T);
  near = near_boundingboxes(Tmin,Tmax,p,[], min(l) * tol);
  if length(find(near)) == 0
    % No near triangles found:
    if force
      error('Point is not inside any triangles!');
    else
      ind = [];
    end
    return
  end
  
  % Triangle points and area:
  Pt1 = P(:,T(4,:));
  Pt2 = P(:,T(5,:));
  Pt3 = P(:,T(6,:));
  A   = T(7,:);
  
  % Calculate triangle normal vectors:
  P21 = Pt2-Pt1;
  P31 = Pt3-Pt1;
  N = cross(P21,P31)*diag(1./(2*A));

  % Calculate projection point in triangle planes:
  pO = p(:,ones(1,size(T,2)));
  Q1 = pO-Pt1;
  d = dot(N,Q1);            % Distance from the triangle plane.
  rt = abs(d);
  
  % Minimum edge length of triangles:
%  let = l(T(1:3,:));
  let = [l(T(1,:)); l(T(2,:)); l(T(3,:))];
  lt = min(let);
  
  % Handle triangles if distance into the triangle plane is small:
  %tind = find(rt./lt < tol));
  tind = find(near(:) & (rt(:)./lt(:) < tol));
  
  if length(tind) == 0
    if force
      error('Point is not inside any triangles!');
    else
      ind = [];
    end
  else
    % Project point into triangle planes:
    N = N(:,tind);
    rt = rt(tind);
    Pp = pO(:,tind)-N*diag(rt);
  
    % (Outer) normal vectors of triangle edges 1 and 3 (not normalized):
    P21 = P21(:,tind);
    P31 = P31(:,tind);
    Q1  = Q1(:,tind);
    let = let(:,tind);
    M1 = cross(N,P21);
    M3 = cross(N,P31);
    
    % Signed distances from the edges:
    H1 = dot(M1,Q1);                     % dot(m1,p-p1);
    H3 = dot(M3,Q1);                     % dot(m3,p-p1);
    R1 = -H1./let(1,:);                  % r1 = dot(cross(N,-A/|A|)),p-p1);
    R3 = H3./let(3,:);                   % r3 = dot(cross(N,C/|C|),p-p1);
    R2 = (H1-H3+dot(M3,P21))./let(2,:);  % r2 = dot(cross(N,(A-C)/|D|),p-p2);
  
    % The projected point is inside the triangle if all the signs are negative:
    in = find((R1 < 0) & (R2 < 0) & (R3 < 0));

    if length(in) == 0
      if force
	error('Point is not inside any triangles!');
      else
	ind = [];
      end
    else
      % Select the nearest triangle:
      tind = tind(in);
      [rtmin,trind] = min(rt(in));
      tind = tind(trind);

%     disp(['The point is inside triangle ' int2str(tind) ', splitting it']);
      [P,E,T,B,ind] = split_triangle(tind, p, P, E, T, B);
    end
  end
end

