function [p,e,t,b,ein,tin,bin] = common_geometry(P1,E1,T1,B1,P2,E2,T2,B2,tol)
% function [p,e,t,b,ein,tin,bin]=common_geometry(P1,E1,T1,B1,P2,E2,T2,B2,[tol])
%
% Checks two geometries (P1,E1,T1,B1) and (P2,E2,T2,B2) for common points,
% edges, triangles and bases. Returns vectors P, C, T and B
% of the sizes of P2, E2, T2 and B2 having wither zeros (not common) or
% indeces into the common objects in P1, E1, T1 and B1.
%
% Also returns logical vectors EIN, TIN and BIN having value true if
% - ein: both edge points of E2 are in P1 but no similar edges in E1,
% - tin: all three triangle edges of T2 in E1 but no similar triangles in T1,
% - bin: basis edge and both triangles of B2 in E1 and T1 but no similar
%        bases in B1.

% Default values:
if nargin < 9
  tol = [];
  if nargin < 8
    B2 = [];
    if nargin < 7
      T2 = [];
      if nargin < 6
	E2 = [];
      end
    end
  end
end
if length(tol) == 0
  tol = 1e-3;
end
if length(B2) == 0
  B2 = zeros(3,0);
end
if length(T2) == 0
  T2 = zeros(10,0);
end
if length(E2) == 0
  E2 = zeros(3,0);
end

% Sizes:
np1 = size(P1,2);
ne1 = size(E1,2);
nt1 = size(T1,2);
nb1 = size(B1,2);

np2 = size(P2,2);
ne2 = size(E2,2);
nt2 = size(T2,2);
nb2 = size(B2,2);

% Tolerance for same points relative to the smallest edge:
ptol = min([E1(3,:) E2(3,:)]) * tol;
% ----------------------------------------------------------------------------
% Search for common points:
p = zeros(1,ne2);
for i = 1:np2
  PP = P2(:,i*ones(1,np1));
  ind = find(vlen(P1-PP) < ptol);
  if ind
    p(i) = ind(1);
  end
end

if nargout > 1
  % --------------------------------------------------------------------------
  % Both edge (E2) points should be common and similar edge in E1:
  P1E2 = p(E2(1,:));
  P2E2 = p(E2(2,:));
  ein = P1E2 & P2E2;
  
  % Search for common edges:
  e = zeros(1,ne2);
  for i = find(ein)
    ind = find(((E1(1,:) == P1E2(i)) & (E1(2,:) == P2E2(i))) | ...
	       ((E1(1,:) == P2E2(i)) & (E1(2,:) == P1E2(i))));
    if ind
      e(i) = ind(1);
    end
  end
  ein = ein & ~e;

  if nargout > 2
    % ------------------------------------------------------------------------
    % All triangle (T2) edges should be common and similar edge in T1:
    E1T2 = e(T2(1,:));
    E2T2 = e(T2(2,:));
    E3T2 = e(T2(3,:));
    tin = E1T2 & E2T2 & E3T2;

    % Search for common triangles:
    t = zeros(1,nt2);
    for i = find(tin)
      ET = [E1T2(i); E2T2(i); E3T2(i)];
      ET = ET(:,ones(1,nt1));
      ind = find(all(T1([1 2 3],:) == ET) | all(T1([2 3 1],:) == ET) | ...
		 all(T1([3 2 1],:) == ET) | all(T1([1 3 2],:) == ET) | ...
		 all(T1([2 1 3],:) == ET) | all(T1([3 1 2],:) == ET) );
      if ind
	t(i) = ind(1);
      end
    end
    tin = tin & ~t;

    if nargout > 3
      % ----------------------------------------------------------------------
      % Basis (B2) edge and both triangles should be common and similar in B1:
      EB2  = e(B2(1,:));
      T1B2 = t(B2(2,:));
      T2B2 = t(B2(3,:));
      bin = EB2 & T1B2 & T2B2;

      % Search for common bases:
      b = zeros(1,nb2);
      for i = find(bin)
	ind = find((B1(1,:) == EB2(i)) & ...
		   (((B1(2,:) == T1B2(i)) & (B1(3,:) == T2B2(i))) | ...
		    ((B1(2,:) == T2B2(i)) & (B1(3,:) == T1B2(i)))));
	if ind
	  b(i) = ind(1);
	end
      end
      bin = bin & ~b;
    end
  end
end

