function [E,T,B,Emap1,Emap2] = remove_edges(eind,E,T,B)
% function [E,T,B,Emap1,Emap2] = remove_edges(eind,E,T,B)
%
% Removes edges E(:,eind) and remaps edge indeces in T and B.

% Remove edges and get mappings between new and old edges:
[E,Emap1,Emap2] = remove_columns(eind,E);

% Fix the edge indeces in triangles:
if size(T,2)
  T(1:3,:) = reshape(Emap2(T(1:3,:)),3,size(T,2));
  if any(any(T(1:3,:) == 0))
    warning('Removed edges still in the triangles!');
  end
end

% Fix the edge indeces in bases:
if (nargin > 3) & size(B,2)
  B(1,:)   = Emap2(B(1,:));
  if any(B(1,:) == 0)
    warning('Removed edges still in the bases!');
  end
else
  B = [];
end
