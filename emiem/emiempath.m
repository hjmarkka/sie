function empath = emiempath
% function empath = emiempath
%
% Return path into emiem routines. The path is normally used for
% locating external emiem routines.

file = 'emiempath.m';
empath = which(file);
n = length(file);
empath = empath(1:end-n);
