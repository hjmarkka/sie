function [tind,alpha] = edge_triangles(eind,P,E,T)
% function [tind,alpha] = edge_triangles(eind,P,E,T)
%
% Searches all the triangles TIND of edge EIND and sorts them according to
% relative angles of the triangles around the edge.
%
% Input:  eind = index of the edge in E
%         P,E,T = see connect.m

if size(T,2) == 0
  tind = [];
  return
end

% Find all the triangles of edge EIND:
tind = find(any(T(1:3,:) == eind));
n = length(tind);

% No need to sort less than three triangles:
if n < 3
  return
end
% ----------------------------------------------------------------------------
% The two end points of the edge and the edge direction vector U:
p1 = E(1,eind);
p2 = E(2,eind);
l  = E(3,eind);
u  = (P(:,p2)-P(:,p1))/l;

% The three vertices of the triangles
pt1 = T(4,tind);
pt2 = T(5,tind);
pt3 = T(6,tind);

% Figure out the free vertices of the triangles:
ind1 = (pt1 ~= p1) & (pt1 ~= p2);
ind2 = (pt2 ~= p1) & (pt2 ~= p2);
ind3 = (pt3 ~= p1) & (pt3 ~= p2);
pind = zeros(1,n);
pind(ind1) = pt1(ind1);
pind(ind2) = pt2(ind2);
pind(ind3) = pt3(ind3);
% ----------------------------------------------------------------------------
% Triangle normals:
N = cross(u(:,ones(1,n)), P(:,pind)-P(:,p1(ones(1,n))));
A2 = vlen(N);
N = N./[A2;A2;A2];

% Vectors V are on the triangle planes and they are perpendicular to U:
V2 = cross(N, u(:,ones(1,n)));
v1 = V2(:,1);
% ----------------------------------------------------------------------------
% Angle between vectors v1 and v2 around a third vector u perpendicular
% to both of the other vectors can be calculated as:
%   tan(alpha) = dot(v2/|v2|, cross(u,v1/|v1|)) / dot(v2/|v2|, v1/|v1|)
%              = dot(v2, cross(u,v1)) / dot(v2, v1)
%              = dot(v2, N1) / dot(v2,v1)
alpha = atan2(dot(V2, N(:,ones(1,n))), dot(V2, v1(:,ones(1,n))));

% The first alpha is always zero:
alpha(1) = 0;

% atan2 gives -pi <= alpha <= pi, change to 0 <= alpha <= 2*pi:
ind = (alpha < 0);
alpha(ind) = 2*pi+alpha(ind);
% ----------------------------------------------------------------------------
% Sort triangles according to the angles:
[alpha,ind] = sort(alpha);
tind = tind(ind);
% ----------------------------------------------------------------------------
