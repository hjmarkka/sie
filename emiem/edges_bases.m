function [bedge,be] = edges_bases(edge, basis)
% function [bedge,be] = edges_bases(edge, basis)

nedge = size(edge,2);
nbasis = size(basis,2);

[bsort,bedge] = sort(basis(1,:));    % Sort bases according to edge indeces.
be = zeros(2,nedge);
k = 1;
i = 1;
while (k <= nbasis) & (i <= nedge)
  while i < bsort(k)
    be(:,i) = [0;0];
    i = i + 1;
  end
  if bsort(k) == i
    be(1,i) = k;
    while (k <= nbasis)
      if bsort(k) ~= i
	break
      end
      k = k+1;
    end
    be(2,i) = k-1;
    i = i + 1;
  end
end
