function T = reorder_normal(T,it,Tind)
% function T = reorder_normal(T,[it,Tind])
%
% Reorder triangle points so that the normal vectors are oriented equally
% for all triangles.
%
% T    - Triangle geometry. See help on function "connect". (10,nt) matrix.
% it   - Triangles are oriented equally to the triangle T(:,it). Default: 1.
% Tind - Reorder only triangles T(:,Tind). Default: 1:nt.

nt = size(T,2);

% Default values:
if nargin < 2
  it = 1;
end
if nargin < 3
  Tind = 1:nt;
end
n = length(Tind);

% Check that "it" is in Tind:
ind0 = find(Tind == it);
if any(length(ind0)) == 0
  error('Reference triangle "it" should be among "Tind"!');
end

Tstack = zeros(1,n);      % Stack for triangles waiting to be checked.
TE = [T(1:6,Tind); Tind]; % 3 edges, 3 points and triangle index.

% Put the first triangle into stack and remove it from the undone list.
Tstack(1) = it;
m = 1;
TE(:,ind0) = [];

k = 0;
while (k < n)
  % Take the last triangle from the stack:
  if m == 0
    s = 'Cannot reorder all triangles. Geometry may have several parts!';
    if size(TE,2) > 0
      s2 = sprintf('\nAt least triangle %d is on the other part!', TE(7,1));
      s = [s s2];
    end
    error(s);
  end
  ts = Tstack(m);  Tstack(:,m) = 0;  m = m - 1;
  Ts = T(1:6,ts);

  % Find neigbour triangles of this triangle:
%  disp(sprintf('Checking triangle %d',ts));
  [tind,eind] = connected_triangles(TE,Ts(1:3));

  % If the first points are the same, reorder:
  l = length(tind);
  for i = 1:l
    Te = TE(:,tind(i));
    if Ts(3+eind(2,i)) == Te(3+eind(1,i))
%      disp(sprintf('Reordering triangle %d',Te(7)));
      T(1:3,Te(7)) = T([1 3 2],Te(7));
      T(4:6,Te(7)) = T(3+[2 1 3],Te(7));
    end
  end

  % Put neighbour triangles into stack and remove from the undone list:
  Tstack(m+(1:l)) = TE(7,tind);  m = m + l;
  TE(:,tind) = [];
  
  k = k + 1;
end
