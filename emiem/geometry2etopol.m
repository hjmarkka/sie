function [coord, etopol] = geometry2etopol(P,E,T,B)
% function [coord, etopol] = geometry2etopol(P,E,T,B)
%
% Converts "geometry" style system into "etopol" system.

% Coord:
coord = P.';

% Etopol:
etopol = T(4:6,:).';
