function [in,s1,s2,s3,s4] = p_in_tetra(p, v, tol)
% function [in,s1,s2,s3,s4] = p_in_tetra(p, v, tol)
%
%  Checks if points P are inside tetrahedron V within relative tolerance TOL.
%
% Input:
% - p: (3,np) matrix of the 3 coordinates of the points.
% - v: (3,4)  matrix of the 3 coordinates of the 4 vertices of a tetrahedron.
% - tol: scalar, relative tolerance (tol * min(edge)) for the detection.
%        Default: 1e-6.
%
% Output:
% - in: scalar, 1 if point is inside the tetrahedron within the relative
%       toleranve tol * min(edge), 0 otherwise.
% - s1,s2,s3,s4: (1,np) vectors, signed distances from the tetrahedron triangle
%       planes. Negative if the point is on the "inside" side of the plane.

% ----------------------------------------------------------------------------
% Default values:
if (nargin < 3)
  tol = [];
end
if (length(tol) == 0)
  tol = 1e-6;
end
% ----------------------------------------------------------------------------
% Outer surface normals of the 4 triangles of the tetrahedron:
[N,vv] = tetrahedron_normals(v);

% Minimum edge length for tolerance checking:
lmin = min(vlen(vv));
ptol = tol * lmin;
% ----------------------------------------------------------------------------
% Signed distances from the triangle planes:
op = ones(1,size(p,2));
P2 = v(:,2*op);
P1 = v(:,  op);
s1 = N(:,1).' * (p-P2);
s2 = N(:,2).' * (p-P1);
s3 = N(:,3).' * (p-P1);
s4 = N(:,4).' * (p-P1);

% Point is inside if all signs are negative (within the tolerance):
in = (s1 < ptol) & (s2 < ptol) & (s3 < ptol) & (s4 < ptol);
% ----------------------------------------------------------------------------
