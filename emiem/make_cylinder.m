function [point,edge,triangle,basis,ps,R,nv] = ...
					   make_cylinder(p,r,n,m,m4,half,angle)
% function [point,edge,triangle,basis] = make_cylinder([p,r,n,m,m4,half,angle])
%
% Create cylinder from point p1 to p2 of radius r splitting circles
% into n and length in m pieces.
%                        m
%   r1 +-----------------------------------------+ r2
%      |                                         |
%   n  |                                         |
%   p1 +                                         + p2
%      |                                         |
%      |                                         |
%      +-----------------------------------------+
%
%  p - (N,m) points (col) vectors. N is 2 or 3.  Default [0;0;0],[1;0;0]
%  r - radix, (1,1), (1,2), (1,m) or (N,1), (N,2), (N,m)         Default 1.
%  n  - Number of circle subdivisions.                           Default 6.
%  m  - Number of length subdivisions.                           Default 1.
%  m4 - Number of rectangle subdivisions 2 or 4.                 Default 2.
%  half - If 1, creates also half basis on the boundaries.       Default 0.
%  angle - Angle of the cylinder.                                Default 2*pi.
%  
% Usage:
%  make_cylinder([p1 p2],r,n,m)       - Cylinder p1 to p2 with constant radius.
%  make_cylinder([p1 p2],[r1 r2],n,m) - Radius from r1 to r2.
%  make_cylinder(pm,rm,n)   - pm (1,m) with rm radix (1,m) for m subdivisions.
%
% Note:
%  - Point p may contain several (> 2) points. The cylinter can have curves.
%  - Radix r may also be a vector (or group of vectors) in that
%    case the cylinder end can have different angles.

% Default values:
if nargin < 7
  angle = [];
  if nargin < 6
    half = [];
    if nargin < 5
      m4 = [];
      if nargin < 4
	m = [];
	if nargin < 3
	  n = [];
	  if nargin < 2
	    r = [];
	    if nargin < 1
	      p = [];
	    end
	  end
	end
      end
    end
  end
end

if length(p)     == 0, p = [[0;0;0],[1;0;0]]; end
if length(r)     == 0, r     = 1   ; end
if length(n)     == 0, n     = 6   ; end
if length(m)     == 0, m     = 1   ; end
if length(m4)    == 0, m4    = 2   ; end
if length(half)  == 0, half  = 0   ; end
if length(angle) == 0, angle = 2*pi; end

% Check dimensions:
[N,M] = size(p);        % Dimension.
if (N < 2) & (N > 3)
  error('N should be either 2 or 3!');
end

% Check rectangle subdivisions:
if (m4 ~= 2) & (m4 ~= 4)
  warning('m4 should be either 2 or 4! Using 4');
  m4 = 4;
end

% Check number of subdivisions:
[nr,mr] = size(r);
if (mr > 2) & (M > 2) & (mr ~= M)
  error('p and r are of different size!');
end

% 2D -> 3D:
p0 = zeros(3,M);
p0(1:N,:) = p;

% Calculate points:
p1 = p0(:,1);
if M < 3
  if M == 1
    p2 = [1;0;0];
  elseif M == 2
    p2 = p0(:,2);
  end
  if mr > 2
    mp = mr;
  else
    mp = m+1;
  end
  ps = (p2-p1)*((0:mp-1)/(mp-1)) + p1*ones(1,mp);
else
  mp = M;
  ps = p0;
end

m = mp-1;
o3 = ones(3,1);

% Calculate radix:
r1 = r(:,1);
if mr < 3
  if mr == 1
    r2 = r1;
  elseif mr == 2
    r2 = r(:,2);
  end
  rs = (r2-r1)*((0:mp-1)/(mp-1)) + r1*ones(1,mp);
else
  rs = r;
end

% Special case: ps and one of two rs vectors given:
if (mr < 3) & (nr > 1) & (M > 2)
  lr1 = vlen(r1);
  lr2 = vlen(r2);
  vr1 = r1/lr1;
  vr2 = r2/lr2;
  rv = (vr2-vr1)*((0:mp-1)/(mp-1)) + vr1*ones(1,mp);
  rv = rv./(o3*vlen(rv));
  lr = (lr2-lr1)*((0:mp-1)/(mp-1)) + lr1*ones(1,mp);
  R = (o3*lr).*rv;
else
  if nr == 1
    % Radix -> points:
    R = [zeros(2,mp);rs];
  else
    % 2D -> 3D:
    R = zeros(3,mp);
    R(1:nr,:) = rs;
  end
end

% Calculate normal vectors:
pn = zeros(3,mp+1);
pn(:,2:mp) = ps(:,1:mp-1)-ps(:,2:mp);
if vlen(ps(:,1)-ps(:,mp)) < 1e-10
  pn(:,1) = pn(:,mp);
  pn(:,mp+1) = pn(:,2);
else
  pn(:,1) = pn(:,2);
  pn(:,mp+1) = pn(:,mp);
end
pn = pn./(o3*vlen(pn));
vn = (pn(:,1:mp)-pn(:,2:mp+1))/sqrt(2);
ind = vlen(vn) > 0;
nv = zeros(3,mp);
nv(:,~ind) = pn(:,~ind);
if any(ind)
  nv(:,ind) = -cross(vn(:,ind),R(:,ind));    % Sign was negative! MT 10.11.2000
end
ind = find(vlen(nv) == 0);
if any(ind)
  nv(:,ind) = cross(cross(pn(:,ind),pn(:,ind+1)),R(:,ind));
end
nv = nv./(o3*vlen(nv));

% Create disc:
[point,edge,triangle,basis] = calc_disc(ps,R,nv,n,m4,half,angle);

%plot_triangles(point,edge,triangle,basis); axis equal; pause
