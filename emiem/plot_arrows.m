function h = plot_arrows(P,F,scale,color)
% function h = plot_arrows(P,F,[scale,color])
%
% Plots Fa directed arrows into points Pa.
%
% Output:
%  h     - Handle into quiver3 graphics.
%
% Input:
%  P     - Points. (3,n) matrix.
%  F     - Vectors. Real (3,n) matrix.
%  scale - Scale factor. 0 = no automatic scaling. Default: 1.
%  color - Line style and color. Default: 'b'.

if nargin < 4
  color = 'b';
  if nargin < 3
    scale = 1;
  end
end

h0 = quiver3(P(1,:),P(2,:),P(3,:),F(1,:),F(2,:),F(3,:),scale,color,'linewidth',2);

if nargout > 0
  h = h0;
end
