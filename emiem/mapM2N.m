function [map,ind] = mapM2N(l,obj1,obj2)
%function [map,ind] = mapM2N(l,obj1,[obj2])
%
% Create M <-> N mapping from obj1 into obj2. Zeros in obj1 are not mapped.
%
% Input:
%   l    : Maximum index of objects 1.
%   obj1 : (1,n) row vector or (m,n) matrix of indeces of objects 1.
%   obj2 : (1,n) row vector of indeces of objects 2. If not given, obj2 = 1:n.
%
% Output:
%  map : sparse (n,l) matrix of mapping from obj1 to obj2.
%
% Optional non-sparse output:
%  map : (1,*) mapping between obj1 and obj2: The i'th object 1 maps into
%        map(ind(i):ind(i+1)-1).
%  ind : (1,n+1) starting indeces of map: The i'th object 1 has indeces
%         ind(i):ind(i+1)-1.
%
% Example: Mapping from points into triangles:
%   np = size(P,2);
%   TP = mapM2N(np,T(4:6,:));      % Triangles of point i: find(TP(:,i))
% or
%   [TP,tp] = mapM2N(np,T(4:6,:)); % Triangles of point i: TP(tp(i):tp(i+1)-1)

% Sizes:
[m,n] = size(obj1);
% ----------------------------------------------------------------------------
% Default values:
if nargin < 3
  obj2 = [];
end
if length(obj2) == 0
  obj2 = 1:n;
end
% ----------------------------------------------------------------------------
% Check sizes:
if size(obj2,2) ~= n
  error('Obj2 should be a row vector of the same size as obj1!')
end
% ----------------------------------------------------------------------------
% Zeros are mapped into l+1 and later removed:
obj1(obj1 == 0) = l+1;

if nargout == 1
  % Create sparse matrix:
  map = sparse(obj2(ones(1,m),:), obj1, 1, n, l+1);
  
  % Remove extra column where zeros are mapped:
  map(:,l+1) = [];
% ----------------------------------------------------------------------------
% Optional output:
else
  % [i,j] = find(TP.');
  [i,order] = sort(obj1(:));
  j = obj2(ones(1,m),:);
  map = j(order).';
  ind = [1 1+cumsum(sparse(ones(1,m*n), i.', 1, 1, l+1))];
  
  % Remove l+1 values where zeros are mapped:
  map(ind(l+1):ind(l+2)-1) = [];
  ind(l+2) = [];
end
% ----------------------------------------------------------------------------
