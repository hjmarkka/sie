function [ps,es,ts,bs] = scale_geometry(scale,point,edge,triangle,basis)
% function [ps,es,ts,bs] = scale_geometry(scale,point,edge,triangle,basis)
% Scales geometry: points, egde lengths, areas and centroids:
%
% scale                                        - Scaling factor.
% point,edge,triangle,basis         See "help connect".

%disp('Scaling geometry...');

% Scale points:
ps = scale*point;

if nargin > 2
  % Scale edge lengths:
  es = edge;
  es(3,:) = scale*es(3,:);

  if nargin > 3
    % Scale triangle areas and centroids:
    ts = triangle;
    ts(7,:)    = scale^2*ts(7,:);
    ts(8:10,:) = scale*ts(8:10,:);

    if nargin > 4
      % No scale needed for basis:
      bs = basis;
    end
  end
end
