function triangle = calc_triangle(triangle,point);
% function triangle = calc_triangle(triangle,point);
% 
% Calculate triangle area and centroid point. Works with multiple triangles.

P1 = point(:,triangle(4,:));
P2 = point(:,triangle(5,:));
P3 = point(:,triangle(6,:));

%ntriangles = size(triangle,2);
%for i = 1:ntriangles
%  triangle(7,i) = norm(cross(P2(:,i)-P1(:,i),P3(:,i)-P1(:,i)))/2;   % Area.
%end

triangle(7,:) = vlen(cross(P2-P1,P3-P1))/2;               % Area.
triangle(8:10,:) = (P1+P2+P3)/3;                          % Centroid point.
