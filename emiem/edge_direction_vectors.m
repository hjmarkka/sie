function [v,l,p1,p2,V,eenear] = edge_direction_vectors(P,E,eind,emap,enear)
% function [v,l,p1,p2,V,eenear] = edge_direction_vectors(P,E,eind,emap,enear)
%
% Calculate edge direction vectors. Optionally do it for subset (EIND) of
% the edges E(:,eind) and return these mapped with EMAP.

% Default values:
if nargin < 3
  eind = [];
end
if length(eind) == 0
  ne = size(E,2);
  eind = 1:ne;
end

% Edge direction vector (v) and length (l):
p1 = P(:,E(1,eind));
p2 = P(:,E(2,eind));
l = E(3,eind);
v = (p2-p1)./[l;l;l];

if nargout > 4
  eenear = emap(enear);
  V = v(:,eenear);
end
