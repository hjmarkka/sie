function [point,edge,triangle,basis] = make_rectangle(p1,p2,p3,p4,n1,n2,n,half)
% function [point,edge,triangle,basis] = make_rectangle(p1,p2,p3,p4,...
%                                             [n1,n2,n,half])
%
% Split rectangle into n1*n2 subrectangles and divide each subrange into
% triangles.
% 
%   p4 +-----------------------------------------+ p3
%      |                                         |
%      |                                         |
%      |                                         |
%      |                                         |
%   p1 +-----------------------------------------+ p2
%
%  p1, p2, p3, p4 - (N,1) point (col) vectors. N is 2 or 3.
%  n1   - Number of points between p1:p2.                          Default 1.
%  n2   - Number of points between p1:p4.                          Default 1.
%  n    - Nunber of triangles in one sub rectangle: 2 or 4.        Default 2.
%  half - If 1, creates also half basis on the boundaries.         Default 0.
%
% Usage:
%  make_rectangle(p1,p2,p3,p4,n)       - Divide both edges into n parts.
%  make_rectangle(p1,p2,p3,p4,n1,n2)   - Divide edges into n1 and n2 parts.
%  make_rectangle(p1,p2,p3,p4,n1,n2,4) - Split each rectangle into 4 triangles.
%  make_rectangle(p1,p2,p3,p4,[0 0.1 0.5 1],n2) - Use uneven divisions in n1.

N = size(p1,1);        % Dimension.

if (N < 2) & (N > 3)
  error('N should be either 2 or 3!');
end

if nargin < 5
  n1 = 1;
end
if nargin < 6
  n2 = n1;
end
if nargin < 7
  n = 2;
end
if nargin < 8
  half = 0;
end

% Check for vector values n1's:
if (length(n1) > 1) 
  if (n1(1) ~= 0) | (n1(length(n1)) ~= 1)
    error('n1(1) should be 0 and n1(last) should be 1!');
  end
  xn = n1;
  n1 = length(xn)-1;  
else
  xn = (0:n1)/n1;
end

if (length(n2) > 1) 
  if (n2(1) ~= 0) | (n2(length(n2)) ~= 1)
    error('n2(1) should be 0 and n2(last) should be 1!');
  end
  yn = n2;
  n2 = length(yn)-1;
else
  yn = (0:n2)/n2;
end

% Points in lower and upper lines:
%xl = (p2-p1)*xn + p1*ones(1,n1+1);
%xu = (p3-p4)*xn + p4*ones(1,n1+1);
xl = p2*xn + p1*(1-xn);
xu = p3*xn + p4*(1-xn);

% All the points:
point = zeros(N,(n1+1)*(n2+1));
pind  = zeros(n2+1,n1+1);
k = 0;
for j = 1:n2+1
  for i = 1:n1+1
    k = k+1;
%   point(:,k) = xl(:,i)+yn(j)*(xu(:,i)-xl(:,i)); % New point.
    point(:,k) = (1-yn(j))*xl(:,i) + yn(j)*xu(:,i); % New point.
    pind(j,i) = k;
  end
end

% Make triangles of all the sub rectangles:
triangle = [];
edge     = [];
for j = 1:n2
  for i = 1:n1
    [point,edge,triangle] = rec2tri(pind(j,i),pind(j,i+1),pind(j+1,i+1),...
	pind(j+1,i),n,point,edge,triangle);
  end
end

% Create basis edges:
basis = fix_edges(point,edge,triangle,half);
