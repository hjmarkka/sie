% Function program [P,E,T,B] = make_box(cp,l1,l2,l3,n1,n2,n3,m,dense,density)
% -----------------------------------------------------------------------
% Generates triangles on the surface of a box in 3D.
%
% INPUT  cp       : (3,1) vector, center of the box, [m]
%        l1,l2,l2 : lengths of the sides of the box, [m]
%        n1,n2,n3 : number of grid points for each side
%        m        : 2 or 4, default = 2
%        dense    : dense triangularization near the edges, default = 'n'
% OUTPUT P,T,E,B  : the triangularization
% -----------------------------------------------------------------------
% CALLS TO : make_rectangle.m, connect.m
% 12/09/01 : PYO / RNI
% 28/08/02 :           - input argument dense added
% -----------------------------------------------------------------------

function [P,E,T,B] = make_box(cp,l1,l2,l3,n1,n2,n3,m,dense,density)

if nargin < 8
  m = 2;
end
if nargin < 9
  dense = 'n';
end

cp = cp - [l1;l2;l3]/2;

p1 = [0 , 0, 0]' + cp;
p2 = [l1, 0, 0]' + cp;
p3 = [l1,l2, 0]' + cp;
p4 = [0 ,l2, 0]' + cp;
p5 = [0 , 0,l3]' + cp;
p6 = [l1, 0,l3]' + cp;
p7 = [l1,l2,l3]' + cp;
p8 = [0 ,l2,l3]' + cp;

if dense == 'y'
  n1 = make_dense(0,1,n1,3,density);
  n2 = make_dense(0,1,n2,3,density);
  n3 = make_dense(0,1,n3,3,density);
end

[P1,E1,T1,B1] = make_rectangle(p4,p3,p2,p1,n1,n2,m);
[P2,E2,T2,B2] = make_rectangle(p5,p6,p7,p8,n1,n2,m);
[P3,E3,T3,B3] = make_rectangle(p1,p2,p6,p5,n1,n3,m);
[P4,E4,T4,B4] = make_rectangle(p8,p7,p3,p4,n1,n3,m);
[P5,E5,T5,B5] = make_rectangle(p4,p1,p5,p8,n2,n3,m);
[P6,E6,T6,B6] = make_rectangle(p2,p3,p7,p6,n2,n3,m);

check = 0;
[P,E,T,B] = connect(P1,E1,T1,B1,P2,E2,T2,B2,check);
[P,E,T,B] = connect(P,E,T,B,P3,E3,T3,B3,check);
[P,E,T,B] = connect(P,E,T,B,P4,E4,T4,B4,check);
[P,E,T,B] = connect(P,E,T,B,P5,E5,T5,B5,check);
[P,E,T,B] = connect(P,E,T,B,P6,E6,T6,B6,check);  
% -------------------------------------------------------------------

