% FUNCTION program [cosa,sina] = SINCOS(angles)
% ----------------------------------------------------------------------------
% Program to calculate the cosines and sines of a set of (np) angles (given in
% radians). For real angles, the program rounds values which are within eps of
% 0 or 1 (necessary because, on the DEC stations, cos(pi/2) ~=0). For complex
% angles, the routine rounds the real parts before calculating the full cos
% and sin expressions
% INPUT  angles: (1,np)-matrix listing the np, possibly complex, angles
% OUTPUT cosa  : (1,np)-matrix listing cos(angle) for each of the (np) angles
%        sina  : (1,np)-matrix listing sin(angle) for each of the (np) angles
% ----------------------------------------------------------------------------
% CALLS TO : None
% 26/08/91 : Addie Lambert - Rolf Nevanlinna Institute
% ----------------------------------------------------------------------------

function [cosa,sina] = sincos(angles)

% round the real part of the angles to 0 and 1 if necessary
cosr = cos(real(angles));
sinr = sin(real(angles));

cosr_zero = find(abs(cosr) < eps);
if (length(cosr_zero) > 0) cosr(cosr_zero) = round(cosr(cosr_zero)); end;

cosr_one = find(abs(cosr) >1-eps);
if (length(cosr_one)  > 0) cosr(cosr_one)  = round(cosr(cosr_one));  end;

sinr_zero = find(abs(sinr) < eps);
if (length(sinr_zero) > 0) sinr(sinr_zero) = round(sinr(sinr_zero)); end;

sinr_one = find(abs(sinr) >1-eps);
if (length(sinr_one)  > 0) sinr(sinr_one)  = round(sinr(sinr_one));  end;

cosa = cosr;   sina = sinr;

% if any angles are complex, calculate full expression for these angles
im_ind = find(imag(angles));
if (size(im_ind) > 0)
  i = sqrt(-1);
  cosa(im_ind) = cosr(im_ind).* cosh(imag(angles(im_ind))) ...
            - i* sinr(im_ind).* sinh(imag(angles(im_ind)));
  sina(im_ind) = sinr(im_ind).* cosh(imag(angles(im_ind))) ...
            + i* cosr(im_ind).* sinh(imag(angles(im_ind)));
end
% ----------------------------------------------------------------------------
