#include <stdio.h>
#include <stdlib.h>
#include <strings.h>

#define MAX_LINE 1024

char s[MAX_LINE];

int readline()
{
  if (fgets(s, MAX_LINE, stdin) == NULL)
    return(1);
  else
    {
      /* printf("Luettu: %s", s); */
      return(0);
    }
}

int find_face()
{
  do {
    if (readline())
      return(1);
  } while (strncmp(s, "3DFACE", 5) != 0);
  /* printf("3DFACE L�YTYI!\n"); */
  return(0);
}

int main(void)
{
  int i;

  while (1)
    {
      /* Find 3DFACE: */
      if (find_face())
	break;
      
      /* Skip 4 lines: */
      for (i = 1; i <= 4; i++)
	if (readline())
	  break;
      
      for (i = 1; i <= 9; i++)
	{
	  /* Read tag: */
	  if (readline())
	    break;
	  /* Read coordinate: */
	  if (readline())
	    break;
	  /* Output coordinate: */
	  printf(s);
	}
      /* Break also outer loop: */
      if (i <= 9)
	break;

      /* Skip 6 lines: */
      for (i = 1; i <= 6; i++)
	if (readline())
	  break;
    }
  return(0);
}
