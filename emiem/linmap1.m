function [P,W] = linmap1(p1,p2,x,w)
% function [P,W] = linmap1(p1,p2,x,w)
%
% Map 1D Gaussian integration points and weights into an edge (p1,p2):
%
% Input:
% - p1,p2: (2,1) or (3,1) coordinates vectices of the edge.
% - x: (1,np) or (np,1) Gaussian quadrature points for the refence edge.
% - w: (np,1) Gaussian quadrature weights for the reference edge.
%
% Output:
% - P: (2,np) or (3,np) integration points for edge (p1,p2).
% - W: (np,1) integration weights for edge (p1,p2,p3).

x = x(:).';
%P = p1(:,ones(1,size(x,2))) + (p2-p1)*x;
P = p2*x + p1*(1-x);

if nargout > 1
  % Scale weights with edge length relation l/1:
  W = vlen(p2-p1) * w;
end
