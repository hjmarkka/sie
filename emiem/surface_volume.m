function V = surface_volume(P,E,T)
% function V = surface_volume(P,E,T)
%
% Calculates volume of a surface (P,E,T). The surface is assumed to be
% closed and orientable.

% V = \int 1 dv = 1/3 \int Div \vec{R} dv = 1/3 \int dot(\vec{n}(y),\vec{R}) ds
% where \vec{R} = y-x and x is a fixed point in R^3.
%
% \vec{n} is a constant for a triangle and so is dot(\vec{n}(y),\vec{R},) = w0.
% => Volume = 1/3 \sum w0*A,
% where A is the triangle area.

nt = size(T,2);

% Select a fixed point x near all the triangles:
x = mean(T(8:10,:),2);   % Mean of the centroids.

% w0 = dot(\vec{n},P1), where P1 is one of the triangle vertices:
[N,P1]  = triangle_normals(P,T);
w0 = dot(N, P1-x(:,ones(1,nt)));

% Volume = 1/3 \sum w0*A:
A = T(7,:);
V = w0*A.'/3;
