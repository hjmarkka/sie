% emiem - ElectroMagnetic Integral Equation Method routines
%
% See also:
%    emiemdata           - Documentation of the data structures.
%    emiemsolvers        - Documentation of the EM solvers.
% ----------------------------------------------------------------------------
% Model creation:
%   make_rectangle       - Create quadrilateral geometry.
%   make_triangle        - Create triangular geometry.
%   make_tetra           - Create one tetrahedron.
%   make_circle          - Create circular (disc) geometry.
%   make_cylinder        - Create cylinder geometry.
%   make_ball            - Create ball (sphere) geometry.
%   make_ball2           - Create ball (sphere) geometry using polyhedrons.
%   make_ball3           - Same as make_ball2 but faster.
%   make_hemisphere      - Create half ball (hemisphere) geometry.
%   make_hemisphere2     - Same as make_hemisphere but faster.
%   make_branch          - Create cylinder geometry with branch(es).
%   make_box             - Create a rectangular cube.
%   make_box2            - Same as make_box but with advances densities.
%   make_box3            - Same as make_box2 but with make_dense2.
%   make_dense           - Create dense points between two points.
%   make_dense2          - Same as make_dense + edge refinement.
%   border2triangle      - Generates triangles inside a given border.
%
% Model import and export:
%   etopol2geometry      - Convert "etopol" points+triangles into geometry.
%   etopol3geometry      - Same as above but faster and without half bases.
%   geometry2etopol      - Convert geometry into "etopol" points+triangles.
%   points2triangles     - Convert list of triangle vertices into etopol.
%   points3triangles     - Same as above but faster.
%
% Model testing:
%   check_geometry       - Check consistency of geometry.
%   check_tetra          - Same as above + checks tetrahedronds, too.
%   common_geometry      - Check common objects in two geometries.
%   compare_geometries   - Compare two geometries.
%
% Model modifying:
%   translate_geometry   - Translate (move) geometry.
%   rotate_geometry      - Rotate geometry.
%   scale_geometry       - Scale geometry.
%   mirror_geometry      - Mirror geometry.
%
%   reorder_normal       - Reorders triangle normal vectors.
%   reorder_triangle     - Toggles triangle normal vector directions.
%   reorder_surfaces     - Orientates triangles of all surfaces.
%   reorder_surfaces2    - Same as above but faster.
%
%   remove_points        - Removes group of points and remaps point indeces.
%   remove_edges         - Removes group of edges and remaps edge indeces.
%   remove_triangles     - Removes group of triangles (and edges/points/bases).
% 
%   move_point           - Move point(s) in geometry.
%   insert_point         - Insert one point into geometry.
%   improve_geometry     - Split group of triangles into 3 parts.
%   recalculate_geometry - Recalculate geometry lengths, areas and centroids.
%
%   create_rectangles    - Combines right-angled triangles into rectangles.
%
% Model combining:
%   connect              - Connects two geometries.
%   connect2             - Connects two geometries.
%   force_edges          - Forces edges (of another geometry) into geometry.
%   make_hole            - Remove common parts of two geometries.
%   merge_geometries     - Merge two geometries.
%   modify_geometry      - Modify geometry with intersections of another.
%
% Model visualization:
%   plot_points          - Plot 2D or 3D points.
%   plot_edges           - Plot edges. Lines between the two edge points.
%   plot_triangles       - Plot triangles. The 3 triangle edge lines.
%   plot_tetras          - Plot tetrahedrons. The 6 tetrahedron edge lines.
%   plot_bases           - Plot bases. Basis edges.
%   plot_numbers         - Plot numbers into 3D points.
%   plot_geometry        - Plot points, edges, triangles and bases with pause.
%   plot_simpleces       - Plot points, edges and triangles (0D-2D simpleces).
%   plot_quadlaterals    - Plot quadlaterals.
%   plot_elements        - Plot points, edges, triangles and quadlaterals.
%   plot_arrows          - Plot 3D arrows in 3D points.
%   plot_boundingboxes   - Plot 3D bounding boxes.
%   fill_triangles       - Draw 3D colored patches of triangles.
%
% Modeling subroutines (not intended to be used directly):
%   rec2tri              - Divides rectangle into 2 or 4 triangles.
%   add_edges            - Add new edges to (P,E).
%   add_triangle         - Adds one triangle and the 3 edges.
%   calc_edge            - Calculates edge length.
%   calc_triangle        - Calculates triangle area and centroid.
%   calc_tetra           - Calculates tetrahedron volumes and centroids.
%   calc_disc            - Creates circular elements.
%   fix_edges            - Creates all bases for edges.
%   connected_triangles  - Connected triangles of a triangle.
%   plane2cylinder       - Maps planar geometry into cylinder.
%   split_edge           - Split an edge.
%   split_triangle       - Split a triangle.
%   force_edge           - Force one edge into geometry.
%   geometry_tolerance   - Relative and absolute tolerances of geometry(ies).
%   rectangle_bases      - Convert triangle bases to rectangle bases.
% ----------------------------------------------------------------------------
% Vector algebra:
%   dot3                 - Faster vector dot product without complex conjugate.
%   cross3               - Faster 3D vector cross product.
%   vlen                 - Vector length for a "matrix" of vectors.
%   vangle               - (Smallest) angle between two vectors.
%   rotmat               - 2D and 3D rotation matrix.
%   vrotmat              - Rotation matrix for vector rotation.
%   trimap               - Map 3D triangle into local (u,v) plane.
%   trimap1              - Same as above but faster.
%   recdim               - Get edge lengths of rectangles.
%   triangle_calculus    - Triangle vector calculus.
%
% Surfaces and borders:
%   border_edges         - Returns indeces of border edges.
%   get_borders          - Get border points of geometry.
%   stretch_border       - Stretch border into 3D band geometry.
%   get_surfaces         - Returns indeces of surface triangles.
%   get_surfaces2        - Same as above but faster.
%
% Bounding boxes:
%   Eboundingbox         - Bounding boxes of edges.
%   Tboundingbox         - Bounding boxes of triangles.
%   Qboundingbox         - Bounding boxes of rectangles.
%   eboundingbox         - Bounding boxes of element (T and Q).
%   Vboundingbox         - Bounding boxes of tetrahedrons.
%   near_boundingboxes   - Check neighborhood using bounding boxes.
%
% Routines on simpleces:
%   p_in_tri             - Checks if points are inside a triangle.
%   p_in_tetra           - Checks if points are inside a tetrahedron.
%   inside_surface       - Checks if points are inside a closed 3D surface.
%   surface_points       - Finds nearest surface points.
%   surface_volume       - Calculates volume of a closed surface.
%   surface_solid_angle  - Calculates relative solid angle of a surface.
%   interpolate_vertices - Interpolation of values in triangle vertices.
%   interpolate_piecewise- Interpolation of piece-wise constant values.
%   edges_bases          - Create list of all the bases of edges.
%   edge_triangles       - Create ordered list of all the triangles of edges.
%   edge_direction_vectors - Calculates edge direction vectors.
%   triangle_area        - Calculates triangle areas.
%   tetra_volume         - Calculates tetrahedron volumes.
%   triangle_normals     - Oriented triangle plane normals.
%   tetrahedron_normals  - Oriented tetrahedron triangle (OUTER) normals.
%
%   intersections        - Intersections of two geometries:
%   intersect_PP         -   point/point
%   intersect_PE         -   point/edge
%   intersect_PT         -   point/triangle
%   intersect_EE         -   edge/edge
%   intersect_ET         -   edge/triangle
%   intersect_TT         -   triangle/triangle
%   point_on_edge        - Checks if points are inside edges.
%   edges_crosses        - Checks if edges cross.
%
% Misc. routines:
%   remove_columns       - Remove columns + mappings between old and new.
%   map_subset           - Bijective map of subset of indeces.
%   mapM2N               - Many-to-many mappings of integer indeces.
%   mapM3N               - Sama as above but faster.
%   common_indeces       - Tests for common indeces, fast if mex available.
% ----------------------------------------------------------------------------
% Numerical integration:
%   gaussint     - 1D Gaussian integration points+weights.
%   intrect2     - 2D Gaussian integration points+weights.
%   inttri       - Gaussian integration points+weights for triangle, n <= 19.
%   inttri2      - Gaussian integration points+weights for triangle.
%   inttri3      - Gaussian integration points+weights for triangle, n <= 30.
%   inttetra     - Gaussian integration points+weights for tetrahedron, n <= 8.
%   intpolar2    - Polar integration points+weights for a triangle.
%   intduffy     - Duffy's integration points+weights for a triangle.
%   intdamp      - 1D damped Gaussian integration points+weights.
%   intpist2     - Singular (polar) integration points+weights for a triangle.
%   trintsp2     - Singular (polar) integration points+weights for a triangle.
%   linmap1      - Linear mapping of points+weights into edge.
%   linmap       - Linear mapping of points+weights into triangle.
%   linmap2      - Linear mapping of points+weights into triangle.
%   linmap3      - Linear mapping of points+weights into tetrahedron.
%   recmap       - Linear mapping of points+weights into rectangle.
%
% Analytical integration:
%   int1tri      - 2D integrals of some function on a triangle.
%   int1rec      - 2D integrals of some function on a rectangle.
%   int1perR     - 2D integrals of some 1/r functions on a triangle.
%   intRnpoly    - 2D integrals of some r^n functions on a lateral polygon.
%   intRn        - 2D integrals of some r^n functions on a triangle.
%   intRnrec     - 2D integrals of some r^n functions on a rectangle.
%   intSNTRnNNN2 - 2D integrals of \int R^n N1^p N2^q N3^r ds over triangle.
%   intSNtetra   - 3D integrals of \int R^n N1^p N2^q N3^r N4^s dv over tetra.
%   int2tri      - 4D integrals of some functions on two triangles.
%   int2rec      - 4D integrals of some functions on two rectangles.
%   int2perR     - 4D integrals of some 1/r terms on a triangle.
%   intNd        - nD integrals of \int N1^p1 * ... * N_{n+1}^(p_{n+1}) dn.
% ----------------------------------------------------------------------------
% EM routines:
%   substrate_parameters - Set frequency/substrate dependent parameters.
%   default_substrate_parameters - Default parameters of empty space.
%   subpar               - Get frequency/substrate dependent parameters.
%   subpar2              - Same as above + get selected parameters.
%   elements_per_lambda  - Warn for too long edges
%   F_elements_per_lambda- Calculate lambda/max(edge) of geometry.
%   is_metallic_domain   - Checks is domain is metallic.
%   check_metal          - Check metallic domains of tdomain and tand (mtand).
%   check_metal2         - Check metallic domains of tdomain and fpar+spar.
%
%   box_resonances       - Resonance frequencies of a cube cavity.
%
%   normalize_fields     - Calculated normalized electric and magnetics fields.
%   denormalize_fields   - Calculated non-normalized electric+magnetics fields.
%
%   nonsingular_greens   - Green's function values with singular subtraction.
%   Gtaylorcoeff         - Taylor series coefficients of Green's function.
%
%   mie_field            - Calculate fields of sphere.
%
%   planewave_field      - Calculate fields of plane wave.
%   dipole_field         - Calculate fields of dipole(s).
%   dipole_field2        - Same as above but uses dipole2.m.
%   frill_field          - Calculate fields of magnetic frill.
%
%   dipole2              - Fields of dipole(s).
%   frillSDB             - Calculate singular integrals of magnetic frill.
%   frill_Bterm          - Setup singular boundary integral of magnetic frill.
%   frill_map            - Map 3D point to frill probe cylinder.
% ----------------------------------------------------------------------------
% External programs:
%   generator-$HOSTTYPE  - Triangle generator for border2triangle.
%   dxf2points           - Extract points from DXF files for make_ball2.
%   dxf2etopol           - Extract points from DXF files for make_ball3.
%
% MEX routines:
%   generator            - Triangle generator for border2triangle (MEX).
%   check_boundingboxes  - Testing of bounding boxes (MEX).
%   check_indeces        - Check for common indeces (MEX).
% ----------------------------------------------------------------------------
% Utility routines:
%   emiempath            - Return path of the EMIEM routines.
%   magDB                - Logarithmic magnitude of complex matrix.
%   econd                - Eigenvalue condition number.
%   econditer            - Eiganvalue condition number, calculated iteratively.
%   elapsed_time         - Show elapsed time.
% ----------------------------------------------------------------------------
