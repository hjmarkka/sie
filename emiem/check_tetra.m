function err = check_tetra(P,E,T,V,tol)
% function err = check_tetra(P,E,T,V,[tol])
%
% Check (logical) consistency of the geometry. Default tol = 1e-3.
%
% Note: Does not check for any geometrical problems!

% ---------------------------------------------------------------------------
% Default values:
if nargin < 5
  tol = [];
  if nargin < 4
    V = [];
    if nargin < 3
      T = [];
      if nargin < 2
        E = [];
        if nargin < 1
          P = [];
        end
      end
    end
  end
end
if length(P) == 0
  P = zeros(3,0);
end
if length(E) == 0
  E = zeros(3,0);
end
if length(T) == 0
  T = zeros(10,0);
end
if length(V) == 0
  V = zeros(18,0);
end
if length(tol) == 0
  tol = 1e-3;
end
% ---------------------------------------------------------------------------
% Check points, edges and triangles:
err = check_geometry(P,E,T,[], tol);

np = size(P,2);
ne = size(E,2);
nt = size(T,2);
nv = size(V,2);

% Quick return if no tetrahedrons:
if nv == 0
  return
end
% ---------------------------------------------------------------------------
% POINTS:

% Check that all the points are used by tetrahedrons:
VP = V(11:14,:);       % Tetrahedron points.
pind = zeros(1,np);
pind(VP) = 1;
ind = find(pind == 0);
for i = 1:length(ind)
  disp(sprintf('Point %d not used by any tetrahedron!',ind(i)));
  err = 1;
end
% ---------------------------------------------------------------------------
% EDGES:

% Check that all the edges are used by tetrahedrons:
VE = V(5:10,:);        % Tetrahedron edges.
eind = zeros(1,ne);
eind(VE) = 1;
ind = find(eind == 0);
for i = 1:length(ind)
  disp(sprintf('Edge %d not used by any tetrahedron!',ind(i)));
  err = 1;
end
% ---------------------------------------------------------------------------
% TRIANGLES:

% Check that all the triangles are used by tetrahedrons:
VT = abs(V(1:4,:));     % Tetrahedron triangles.
tind = zeros(1,nt);
tind(VT) = 1;
ind = find(tind == 0);
for i = 1:length(ind)
  disp(sprintf('Triangle %d not used by any tetrahedrons!',ind(i)));
  err = 1;
end
% ---------------------------------------------------------------------------
% TETRAHEDRONS:

% Check for duplicate tetrahedrons:
perms4 = perms(1:4).';
nperms4 = size(perms4,2);
perms6 = perms(1:6).';
nperms6 = size(perms6,2);
for i = 1:nv
  % Check with all the permutations of the tetrahedron triangles:
  ind = logical(zeros(1,nv));
  for k = nperms4
    ind = ind | (all(VT == VT(perms4(:,k), i*ones(1,nv))) & ((1:nv) ~= i));
  end
  ind = find(ind);
  for j = 1:length(ind)
    disp(sprintf('Duplicate tetrahedron (triangles) %d and %d!', i,ind(j)))
    err = 1;
  end

  % Check with all the permutations of the tetrahedron edges:
  ind = logical(zeros(1,nv));
  for k = nperms6
    ind = ind | (all(VE == VE(perms6(:,k), i*ones(1,nv))) & ((1:nv) ~= i));
  end
  ind = find(ind);
  for j = 1:length(ind)
    disp(sprintf('Duplicate tetrahedron (edges) %d and %d!', i,ind(j)))
    err = 1;
  end

  % Check with all the permutations of the tetrahedron points:
  ind = logical(zeros(1,nv));
  for k = nperms4
    ind = ind | (all(VP == VP(perms4(:,k), i*ones(1,nv))) & ((1:nv) ~= i));
  end
  ind = find(ind);
  for j = 1:length(ind)
    disp(sprintf('Duplicate tetrahedron (points) %d and %d!', i,ind(j)))
    err = 1;
  end
end
% ............................................................................
% Check that the tetrahedron edges and points match:
P1VE1 = E(1,VE(1,:));         % The two points of the tetrahderon edges.
P2VE1 = E(2,VE(1,:));
P1VE2 = E(1,VE(2,:));
P2VE2 = E(2,VE(2,:));
P1VE3 = E(1,VE(3,:));
P2VE3 = E(2,VE(3,:));
P1VE4 = E(1,VE(4,:));
P2VE4 = E(2,VE(4,:));
P1VE5 = E(1,VE(5,:));
P2VE5 = E(2,VE(5,:));
P1VE6 = E(1,VE(6,:));
P2VE6 = E(2,VE(6,:));
VP1 = VP(1,:);
VP2 = VP(2,:);
VP3 = VP(3,:);
VP4 = VP(4,:);
ind = find(~((((P1VE1==VP1)&(P2VE1==VP2)) | ((P1VE1==VP2)&(P2VE1==VP1))) & ...
	     (((P1VE2==VP1)&(P2VE2==VP3)) | ((P1VE2==VP3)&(P2VE2==VP1))) & ...
	     (((P1VE3==VP1)&(P2VE3==VP4)) | ((P1VE3==VP4)&(P2VE3==VP1))) & ...
	     (((P1VE4==VP2)&(P2VE4==VP3)) | ((P1VE4==VP3)&(P2VE4==VP2))) & ...
	     (((P1VE5==VP3)&(P2VE5==VP4)) | ((P1VE5==VP4)&(P2VE5==VP3))) & ...
	     (((P1VE6==VP2)&(P2VE6==VP4)) | ((P1VE6==VP4)&(P2VE6==VP2)))));
for i = 1:length(ind)
  disp(sprintf('Tetrahedron %d edges and points do not match!',ind(i)));
  err = 1;
end
% ............................................................................
% Check that the tetrahedron triangles and points match:

% Tetrahedron triangle vertex indeces:
tp = [2 3 4; 1 4 3; 1 2 4; 1 3 2].';

for i = 1:4
  P1VT = T(4,VT(i,:));     % The three points of the tetrahedron triangles.
  P2VT = T(5,VT(i,:));
  P3VT = T(6,VT(i,:));
  eval(sprintf('PT1 = VP%d;', tp(1,i)));
  eval(sprintf('PT2 = VP%d;', tp(2,i)));
  eval(sprintf('PT3 = VP%d;', tp(3,i)));
  sT = sign(V(i,:));       % Sign of the triangle (outer) normal.
  orient1 = (((P1VT==PT1)&(P2VT==PT2)&(P3VT==PT3)) | ...
             ((P1VT==PT2)&(P2VT==PT3)&(P3VT==PT1)) | ...
             ((P1VT==PT3)&(P2VT==PT1)&(P3VT==PT2)));
  orient2 = (((P1VT==PT1)&(P2VT==PT3)&(P3VT==PT2)) | ...
             ((P1VT==PT3)&(P2VT==PT2)&(P3VT==PT1)) | ...
             ((P1VT==PT2)&(P2VT==PT1)&(P3VT==PT3)));
  ind = find(~(((sT > 0) & orient1) | ((sT < 0) & orient2 )));
  for j = 1:length(ind)
    if orient1(j) | orient2(j)
      disp(sprintf('Tetrahedron %d triangle %d wrong orientation!',ind(j), i));
    else
      disp(sprintf('Tetrahedron %d triangle %d points do not match!',ind(j),i))
    end
    err = 1;
  end
end
% ............................................................................
% Check tetrahedron volumes:
V1 = V(15,:);
P1 = P(:,VP(1,:));
P2 = P(:,VP(2,:));
P3 = P(:,VP(3,:));
P4 = P(:,VP(4,:));
V2 = tetra_volume(P1,P2,P3,P4);
ind = find(abs(V1-V2)./V2 > tol);
for i = 1:length(ind)
  disp(sprintf('Tetrahedron %d volume wrong: %f / %f!',...
               ind(i),V1(ind(i)),V2(ind(i))));
  err = 1;
end
% ............................................................................
% Check tetrahedron centroids:
C1 = V(16:18,:);
C2 = (P1+P2+P3+P4)/4;
minL = min(E(3,:));                    % Relative to the minimum edge length.
ind = find(vlen(C1-C2)/minL > tol);
for i = 1:length(ind)
  c1 = C1(ind(i));
  c2 = C2(ind(i));
  disp(sprintf('Tetrahedron %d centroid wrong: [%f %f %f] / [%f %f %f]!', ...
	       ind(i), c1(1), c1(2), c1(3), c2(1), c2(2), c2(3)));
  err = 1;
end
% ............................................................................
for i = 1:nv
  pvi = [P1(:,i) P2(:,i) P3(:,i) P4(:,i)];
  
  % Check that no points P are inside tetrahedron V(:,i):
  pind = find(p_in_tetra(P, pvi, tol));
  for j = pind
    if ~any(VP(:,i) == j)
      disp(sprintf('Point %d is inside tetrahedron %d!', pind(1), i));
      err = 1;
    end
  end

  % Check that a tetrahedron centroid is inside a single tetrahedron:
  pind = find(p_in_tetra(C1, pvi, tol));
  for j = pind
    if j ~= i
      disp(sprintf('Centroid of tetrahedron %d is inside tetrahedron %d!',...
                   j,i));
      err = 1;
    end
  end
end
% ---------------------------------------------------------------------------

