function [P,E2,T2,B2,Q,Qind,Emap1,Emap2,Tmap1,Tmap2,Bmap1,Bmap2] = ...
    create_rectangles(P,E,T,B,tdomain,tol,rm_edges)
% function [P,E2,T2,B2,Q,Qind,Emap1,Emap2,Tmap1,Tmap2,Bmap1,Bmap2] = ...
%     create_rectangles(P,E,T,B,[tdomain,tol,rm_edges])
%
% Combines two adjacent right-angled triangles into rectangles. The triangles
% are combined only if they have the same properties (tdomain).
%
% Input:
%   - P,E,T,B: points, edges, triangles and bases. See connect.m
%   - tdomain: Triangle properties (*,nt). Default: zeros(1,nt).
%   - tol  : relative tolerance for right-angle and triangle normal detection.
%                                                     Default: 1e-12
%   - rm_edges: if 0 E2 = E otherwise edges inside rectangles are removed.
%               Default: 1.
%
% Output:
%   - P,E2,T2,B2: Edges, triangles and bases may change.
%   - Q: (12,nq) flat rectangles combined from two right-angled triangles.
%            [e1;e2;e3;e4; p1;p2;p3;p4; A; c1;c2;c3]
%             e3
%       p4 +------+ p3     where e1,e2,e3,e4 are the edges,
%          |\   t2|              p1,p2,p3,p4 are the vertices,
%       e4 |  \   | e2           A           is the area of the rectangle and
%          |t1  \ |              c1,c2,c3    is the centroid of the rectangle.
%       p1 +------+ p2
%             e1
%   - Qind: (2,nq)  The two indeces (it1,it2) of the combined triangles.
%                   Index it2 is negative if the orientation of the second
%                   triangle was different from the first triangle.
%   - Emap1: (1,ne2) Original edge indeces of the remaining edges.
%   - Emap2: (1,ne) New edge indeces of the old edges (or zeros).
%   - Tmap1: (1,nt2) Original triangle indeces of the remaining triangles.
%   - Tmap2: (1,nt) New triangle indeces of the old triangles (or zeros).
%   - Bmap1: (1,nb2) Original basis indeces of the remaining bases.
%   - Bmap2: (1,nb) New basis indeces of the old bases (or zeros).

if nargin < 7
  if nargin < 6
    if nargin < 5
      if nargin < 4
	B = [];
      end
      tdomain = [];
    end
    tol = [];
  end
  rm_edges = [];
end
if length(B) == 0
  B = zeros(3,0);
end
if length(tdomain) == 0
  tdomain = zeros(1,size(T,2));
end
if length(tol) == 0
  tol = 1e-12;
end
if length(rm_edges) == 0
  rm_edges = 1;
end
% ----------------------------------------------------------------------------
% Search all the right-angled triangles:

% Edge vectors:
PE = E(1:2,:);  % Indeces of the two edge points.
L  = E(3,:);    % Edge lenghts.
v  = P(:,PE(2,:))-P(:,PE(1,:));

% Triangle edge vectors:
ET = T(1:3,:);    % Triangle edges.
s1 = v(:,ET(1,:))/L(1);
s2 = v(:,ET(2,:))/L(2);
s3 = v(:,ET(3,:))/L(3);
PT = T(4:6,:);    % Triangle points.
A  = T(7,:);      % Triangle areas.

% Triangle is right-angled if any of the edges are perpendicular:
right_angled = abs([dot(s2,s3); dot(s3,s1); dot(s1,s2)]) <= tol;
tind = find(any(right_angled));

if length(tind) == 0
  % No right-angled triangles, fast exit:
  E2 = E;
  T2 = T;
  B2 = B;
  Q = zeros(12,0);
  Qind = zeros(2,0);
  Emap1 = 1:size(E,2);  Emap2 = Emap1;
  Tmap1 = 1:size(T,2);  Tmap2 = Tmap1;
  Bmap1 = 1:size(B,2);  Bmap2 = Bmap1;
  return
end

% A triangle cannot have two right-angles:
terr = find(sum(right_angled) > 1);
if terr
  error(sprintf('Triangle %d has two right-angles!', terr(1)));
end

% Edge index opposite to the right-angles:
et = [1 2 3] * right_angled;
eind = ET(sub2ind(size(ET),et(tind),tind));

% Triangle normals of the right-angled triangles:
N = triangle_normals(P,T,tind);
% ----------------------------------------------------------------------------
% Loop over the right-angled triangles:

Q = zeros(12,0);
Qind = zeros(2,0);
Tmap1 = 1:size(T,2);
eremove = logical(zeros(1,size(E,2)));
ind123 = [1 2 3 1 2 3];

for i = 1:length(tind)
  it = tind(i);
  
  % Do not check the triangle itself and triangle pairs twice:
  e = eind(i);
  eind(i) = 0;
  
  % Find other right-angled triangles of the opposite edge:
  tind2 = find(eind == e);

  % If more than two other triangles, this edge cannot be removed:
  % Triangle areas should be equal, normal vectors parallel and
  % triangle properties be the same:
  it2 = tind(tind2);              % Index of the second triangle(s).
  if (length(tind2) == 1) & ...
    ((abs(A(it2)-A(it))/A(it) <= sqrt(tol)) & ...
     (1-abs(N(:,i).' * N(:,tind2)) <= tol) & ...
     (all(tdomain(:,it*ones(1,length(tind2))) == tdomain(:,it2))))

    % Combine points of the two triangles:
    ind1 = ind123(et(it)+[2 3 1]);  % Order of the points of the 1. triangle.
    pind1 = PT(ind1,it).';          % The points of the 1. triangle in order.
    ind2 = ind123(et(it2)+2);       % The free point of the 2. triangle.
    p3 = PT(ind2,it2);              % Edge index of the free point.
    ps = [pind1(1:2) p3 pind1(3)];  % The 4 points of the rectangle.
    
    % Combine edges of the two triangles:
    ind1 = ind123(et(it )+[1 2]);   % The two free edges of the 1. triangle.
    eind1 = ET(ind1,it).';          % Indeces of the two free edges.
    ind2 = ind123(et(it2)+[1 2]);   % The two free edges of the 2. triangle.
    eind2 = ET(ind2,it2).';         % Indeces of the two free edges.
    if any(PE(:,eind2(2)) == ps(2))
      eind2 = eind2([2 1]);         % The order of edges of the 2. triangle.
    end
    es = [eind1(2) eind2 eind1(1)]; % The 4 edges of the rectangle.
    
    % Further test that the edge lenghts of the two triangles are the same:
    l = L(es);                      % Lengths of the 4 edges.
    if (abs(l(3)-l(1))/l(1) <= tol) & (abs(l(4)-l(2))/l(2) <= tol)
      eremove(e) = 1;                 % Remove this edge.
      
      % Combine the two triangles into one rectangle:
      Ar = l(1)*l(2);               % Area.
      Pr = sum(P(:,ps),2)/4;        % Centroid.
      Q = [Q [es ps Ar Pr.'].'];
      Qind = [Qind [it it2].'];
      
      % Remove the two triangles T2:
      Tmap1([it it2]) = 0;
    end
  end
end
% ----------------------------------------------------------------------------
% Purge removed triangles:
[T2,Tmap1,Tmap2] = remove_columns(Tmap1 == 0, T);

% Convert bases of triangles into bases of triangles and rectangles:
[B2,Bmap1,Bmap2] = rectangle_bases(T,B,Qind);

% Fix the triangle indeces in bases:
ind2 = find(B2(2,:) > 0);
B2(2,ind2) = Tmap2(B2(2,ind2));
ind3 = find(B2(3,:) > 0);
B2(3,ind3) = Tmap2(B2(3,ind3));

% Remove edges inside the rectangles:
if rm_edges
  [E2,T2,B2,Emap1,Emap2] = remove_edges(eremove, E, T2, B2);

  % Fix the edge indeces in rectangles:
  if size(Q,2)
    Q(1:4,:) = reshape(Emap2(Q(1:4,:)),4,size(Q,2));
    if any(any(Q(1:4,:) == 0))
      warning('Removed edges still in the rectangles!');
    end
  end
else
  E2 = E;
  Emap1 = 1:size(E,2);
  Emap2 = Emap1;
end
% ----------------------------------------------------------------------------
