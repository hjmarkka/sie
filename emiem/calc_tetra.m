function V = calc_tetra(V,P);
% function V = calc_tetra(V,P);
% 
% Calculate tetra's volume and center point. Works with multiple tetras.

P1 = P(:,V(11,:));
P2 = P(:,V(12,:));
P3 = P(:,V(13,:));
P4 = P(:,V(14,:));

V(15,:) = tetra_volume(P1,P2,P3,P4);                   % Volume.
V(16:18,:) = (P1+P2+P3+P4)/4;                          % Center point.
