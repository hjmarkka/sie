function [p,e,t,b]=connecte(p1,e1,t1,b1,p2,e2,t2,b2,check,tol)
% function [point,edge,triangle,basis] = connect(point1,edge1,triangle1,basis1,
%                                                point2,edge2,triangle2,basis2,
%                                                [check, tol])
%
% point:    (3,number_of_points)     x          - X coordinate of the point.
%                                    y          - Y coordinate of the point.
%                                    z          - Z coordinate of the point.
% edge:     (3,number_of_edges)      point1     - Index of the first point.
%                                    point2     - Index of the second point.
%                                    length     - Length of the edge.
% triangle: (10,number_of_triangles) edge1      - Index of the first edge.
%          p3 +                      edge2      - Index of the second edge.
%             |\                     edge3      - Index of the third edge.
%             | \                    point1     - Index of the first point.
%             |  \ e2                point2     - Index of the second point.
%          e3 |   \                  point3     - Index of the third point.
%             |    \                 area       - Area of the triangle.
%             |     \                centroid x - X coordinate of the centroid.
%          p1 +------+ p2            centroid y - Y coordinate of the centroid.
%                e1                  centroid z - Z coordinate of the centroid.
% basis:    (3,number_of_basis)      edge       - Index of the basis edge.
%                                    triangle1  - First triangle.
%                                    triangle2  - Second triangle.
%
% check:    0 or 1                   0 = No checks.
%                                    1 = Do checks. Default.
%
% tol:      scalar                   Tolerance. Default: 1/100.

%disp('Connecting two structures...');

if (nargin < 10) 
  tol = [];
end
if (length(tol) == 0)
  tol = 1/100;
end
if nargin < 9
  check = 1;
end

np1 = size(p1,2);
np2 = size(p2,2);
if np1 == 0
  p = p2; e = e2; t = t2; b = b2;
  return;
end
if np2 == 0
  p = p1; e = e1; t = t1; b = b1;
  return;
end

% Tolerance for same points == 1/100 of the smallest edge:
ptol = min([e1(3,:) e2(3,:)])*tol;

% ----------------------------------------------------------------------------
% Points (and point indeces in edges and triangles):

% Add number of point1 into point indeces of edge2 and triangle2:
ep2 = e2(1:2,:);
tp2 = t2(4:6,:);
ep2 = ep2 + np1;
tp2 = tp2 + np1;

% Search for common points:
pjoin = [];
for i = 1:np2
  P2 = p2(:,i*ones(1,np1));
  ind = find(vlen(p1-P2) < ptol);
  if length(ind) > 0
    if length(ind) > 1
      error(sprintf('Found multiple same point2(%d)!', i));
    end
    % Found same point. Check the point.
    if size(pjoin,2) > 0
      if length(find(pjoin(:,2) == ind)) > 0
	error(sprintf('Point1(%d) has multiple same point2!', ind));
      end
    end
    pjoin = [pjoin; [i ind]];
  end
end

% Join common points:
p2ind = 1:np2;
npjoin = size(pjoin,1);
if npjoin > 0
  for i = 1:npjoin
    pind2 = pjoin(i,1);
    pind1 = pjoin(i,2);
%   fprintf('  Joining point2(%d) and point1(%d)\n', pind2, pind1);
    ntmp = pind2 + np1;
    ind = find(ep2 == ntmp);  ep2(ind) = pind1*ones(size(ind));
    ind = find(tp2 == ntmp);  tp2(ind) = pind1*ones(size(ind));
  end
  
  % Remove common points:
  p2(:,pjoin(:,1)) = [];
  p2ind(pjoin(:,1)) = [];

  % Move larger point indeces one down:
  for i = 1:npjoin
    pind2 = pjoin(i,1);
    ntmp = pind2 + np1;
    ind = find(ep2 >= ntmp);         ep2(ind) = ep2(ind) - 1;
    ind = find(tp2 >= ntmp);         tp2(ind) = tp2(ind) - 1;
%    ind = find(pjoin(:,1) >= ntmp);  pjoin(ind,1) = pjoin(ind,1) - 1;
    pjoin(:,1) = pjoin(:,1) - 1;
  end
end

e2(1:2,:) = ep2;
t2(4:6,:) = tp2;

% Join points:
p = [p1 p2];

% ----------------------------------------------------------------------------
% Edges (and edge indeces in triangles and bases):

% Add number of edge1 into edge indeces of triangle2 and basis2:
ne1 = size(e1,2);
ne2 = size(e2,2);
te2 = t2(1:3,:);
if length(b2) > 0
  be2 = b2(1,:);
  be2 = be2 + ne1;
else
  be2 = [];
end
te2 = te2 + ne1;

% Search for same edges:
P11 = e1(1,:);
P12 = e1(2,:);
ejoin = [];
for i = 1:ne2
  P21 = e2(1,i);
  P22 = e2(2,i);
  ind1 = find((P11 == P21) & (P12 == P22));
  ind2 = find((P11 == P22) & (P12 == P21));
  ind = [ind1 ind2];
  if length(ind) > 0
    if length(ind) > 1
      error(sprintf('Found multiple same edge2(%d)!', i));
    end
    % Found same edge.
    if size(ejoin,2) > 0
      if length(find(ejoin(:,2) == ind)) > 0
	error(sprintf('Edge1(%d) has multiple same edge2!', ind));
      end
    end
    ejoin = [ejoin; [i ind]];
  end
end

% Join common edges:
e2ind = 1:ne2;
nejoin = size(ejoin,1);
if nejoin > 0
  for i = 1:nejoin
    eind2 = ejoin(i,1);
    eind1 = ejoin(i,2);
%   fprintf('  Joining edge2(%d) and edge1(%d)\n', eind2, eind1);
    ntmp = eind2+ne1;
    ind = find(te2 == ntmp);  te2(ind) = eind1*ones(size(ind));
    if length(be2) > 0
      ind = find(be2 == ntmp);  be2(ind) = eind1*ones(size(ind));
    end
  end
  
  % Remove common edges:
  e2(:,ejoin(:,1)) = [];
  e2ind(ejoin(:,1)) = [];

  % Move larger edge indeces one down:
  for i = 1:nejoin
    eind2 = ejoin(i,1);
    ntmp = eind2 + ne1;
    ind = find(te2 >= ntmp);  te2(ind) = te2(ind) - 1;
    if length(be2) > 0
      ind = find(be2 >= ntmp);  be2(ind) = be2(ind) - 1;
    end
%    ind = find(ejoin(:,1) >= ntmp);  ejoin(ind,1) = ejoin(ind,1) - 1;
    ejoin(:,1) = ejoin(:,1) - 1;
  end
end

t2(1:3,:) = te2;
if length(be2) > 0
  b2(1,:) = be2;
end

% Check edges. A point[2,1] cannot be in the middle of an edge[1,2]:
if check
  for k = 1:2
    if k == 1
      E = e2;
      P = p1;
      pind = 1:np1;
      eind = e2ind;
    else
      E = e1;
      P = p2;
      pind = p2ind;
      eind = 1:ne1;
    end
    if size(P,2) > 0
      oP = ones(1,size(P,2));
      for i = 1:size(E,2)
	P1 = p(:,E(1,i));
	P2 = p(:,E(2,i));
	P1P2 = P2-P1;
	mP1P2 = P1P2*oP;
	lP1P2 = vlen(P1P2);
	d = vlen(cross(mP1P2,P-P1*oP))/lP1P2;  % Distance between edge/points1.
	ind = find(d < ptol);
	if length(ind) > 0
	  % Check if the point is between points P1 and P2:
	  c = dot(P(:,ind)-P1*ones(1,length(ind)),mP1P2(:,ind))/dot(P1P2,P1P2);
	  ind2 = find((c < 1) & (c > 0));
	  if length(ind2) > 0
	    s = '12';
	    error(sprintf('Point%c(%d) in the middle of edge%c(%d)', ...
		s(k), pind(ind(ind2(1))), s(3-k), eind(i)));
	  end
	end
      end
    end
  end
end

% Join edges:
e = [e1 e2];
% ----------------------------------------------------------------------------
% Triangles (and triangle indeces in bases):

% Add number of triangle1 into triangle indeces of basis2:
nt1 = size(t1,2);
nt2 = size(t2,2);
if length(b2) > 0
  bt2 = b2(2:3,:);
  bt2 = bt2 + nt1;
  % Replace half basis zeros with -1:
  bt2(bt2 == nt1) = -1;
else
  bt2 = [];
end

% Search for same triangles:
P11 = t1(4,:);
P12 = t1(5,:);
P13 = t1(6,:);
tjoin = [];
for i = 1:nt2
  P21 = t2(4,i);
  P22 = t2(5,i);
  P23 = t2(6,i);
  ind1 = find((P11 == P21) & (P12 == P22) & (P13 == P23));
  ind2 = find((P11 == P22) & (P12 == P23) & (P13 == P21));
  ind3 = find((P11 == P23) & (P12 == P21) & (P13 == P22));
  ind4 = find((P11 == P21) & (P12 == P23) & (P13 == P22));
  ind5 = find((P11 == P22) & (P12 == P21) & (P13 == P23));
  ind6 = find((P11 == P23) & (P12 == P22) & (P13 == P21));
  ind = [ind1 ind2 ind3 ind4 ind5 ind6];
  if length(ind) > 0
    if length(ind) > 1
      error(sprintf('Found multiple same triangle2(%d)!', i));
    end
    % Found same triangle.
    if size(tjoin,2) > 0
      if length(find(tjoin(:,2) == ind)) > 0
	error(sprintf('Triangle1(%d) has multiple same triangle2!', ind));
      end
    end
    tjoin = [tjoin; [i ind]];
  end
end

% Join common triangles:
ntjoin = size(tjoin,1);
if ntjoin > 0
  for i = 1:ntjoin
    tind2 = tjoin(i,1);
    tind1 = tjoin(i,2);
%   fprintf('  Joining triangle2(%d) and triangle1(%d)\n', tind2, tind1);
    ntmp = tind2+nt1;
    if length(bt2) > 0
      ind = find(bt2 == ntmp);  bt2(ind) = tind1*ones(size(ind));
    end
  end
  
  % Remove common triangles:
  t2(:,tjoin(:,1)) = [];

  % Move larger edge indeces one down:
  for i = 1:ntjoin
    tind2 = tjoin(i,1);
    ntmp = tind2 + nt1;
    if length(bt2) > 0
      ind = find(bt2 >= ntmp);  bt2(ind) = bt2(ind) - 1;
    end
%    ind = find(tjoin(:,1) >= ntmp);  tjoin(ind,1) = tjoin(ind,1) - 1;
    tjoin(:,1) = tjoin(:,1) - 1;
  end
end

% Fix zeros of the half bases of base2:
if length(b2) > 0
  hind = find(bt2(2,:) == -1);     % Keep zeros as zeros!
  b2(2:3,:) = bt2;
  b2(3,hind) = zeros(size(hind));  % Copy zeros here!
end

% Join triangles:
t = [t1 t2];
% ----------------------------------------------------------------------------
% Bases:

nb1 = size(b1,2);
nb2 = size(b2,2);

% Search for same bases:
bjoin = [];
if b1
  be1  = b1(1,:);
  bt11 = b1(2,:);
  bt12 = b1(3,:);
  for i = 1:nb2
    be2  = b2(1,i);
    bt21 = b2(2,i);
    bt22 = b2(3,i);
    ind1 = find((be1 == be2) & (bt11 == bt21) & (bt12 == bt22));
    ind2 = find((be1 == be2) & (bt12 == bt21) & (bt11 == bt22));
    ind = [ind1 ind2];
    if length(ind) > 0
      if length(ind) > 1
	error(sprintf('Found multiple same basis2(%d)!', i));
      end
      % Found same basis.
      if size(bjoin,2) > 0
	if length(find(bjoin(:,2) == ind)) > 0
	  error(sprintf('Basis1(%d) has multiple same basis2!', ind));
	end
      end
      bjoin = [bjoin; [i ind]];
    end
  end
end

% Remove duplicate bases:
if bjoin
  b2(:,bjoin(:,1)) = [];
% for i = 1:size(bjoin,1)
%   fprintf('  Joining basis2(%d) and basis1(%d)\n',bjoin(i,1),bjoin(i,2));
% end
end

% Add new basis for every common edge with a new triangle:
b3 = [];
if nejoin > 0
  nb = 0;
  te1 = t1(1:3,:);
  te2 = t2(1:3,:);
  % Common triangles:
  tcommon = zeros(1,size(t,2));
  if tjoin
    tcommon(tjoin(:,2)) = 1;
  end
  for i = 1:nejoin
    eind1 = ejoin(i,2);

    % Find all triangles of this edge:
    it1 = find(any(te1 == eind1));
    it2 = find(any(te2 == eind1));

    % Check if this edge has a half basis in one or both of the bases:
    if size(b1,2) > 0
	ib1 = find((b1(1,:) == eind1) & (b1(3,:) == 0));
    else
      ib1 = [];
    end
    if size(b2,2) > 0
      ib2 = find((b2(1,:) == eind1) & (b2(3,:) == 0));
    else
      ib2 = [];
    end
    
    if length(ib1) > 0
      if length(ib2) > 0
	% Both bases have a half basis: join them
	b1(3,ib1(1)) = b2(2,ib2(1));   % Copy triangle index.
	b2(:,ib2(1)) = [];             % Remove basis 2.
%	fprintf('  Joining half basis1(%d)+basis2(%d): [%d %d]\n',...
%		ib1(1),ib2(1),b1(2,ib1(1)),b1(3,ib1(1)));
%	fprintf('  Removing half basis2(%d)\n',ib2(1));
      else
	% Half basis 1: check that the triangle is not common and
	% no bases exists for this edge (eind1) and the first triangle
	% (tind1) of the half basis 1 in bases 2:
	tind1 = b1(2,ib1(1));
	if tcommon(tind1) & any((b2(1,:) == eind1) & any(b2(2:3,:) == tind1))
	  % Basis 2 over the edge and first triangle: remove half basis 1:
	  b1(:,ib1(1)) = [];
%	  fprintf('  Removing half basis1(%d)\n',ib1(1));
	elseif it2
	  % Fill half basis 1 with a triangle from t2:
	  tind2 = nt1+it2(1);
	  b1(3,ib1(1)) = tind2;
%	  fprintf('  Joining triangle2(%d) into basis1(%d): [%d %d]\n',...
%		  tind2,ib1(1),tind1,tind2);
	end
      end
    elseif length(ib2) > 0
      % Half basis 2: check that the triangle is not common and
      % no bases exists for this edge (eind1) and the first triangle
      % (tind2) of the half basis 2 in bases 1:
      tind2 = b2(2,ib2(1));
      if tcommon(tind2) & any((b1(1,:) == eind1) & any(b1(2:3,:) == tind2))
	% Basis 1 over the edge and first triangle: remove half basis 2:
	b2(:,ib2(1)) = [];
%	fprintf('  Removing half basis2(%d)\n',ib2(1));
      elseif it1
	% Fill half basis 2 with a triangle from t1:
	tind1 = it1(1);
	b2(3,ib2(1)) = tind1;
%	fprintf('  Joining triangle1(%d) into basis2(%d): [%d %d]\n',...
%		tind1,ib2(1),tind2,tind1);
      end
    elseif it2
      % Add new basis if any new triangles and non-common it1:
      tind1 = find(~tcommon(it1));
      if tind1
	tind1 = it1(tind1(1));
	tind2 = nt1+it2(1);
	nb = nb+1;
	b3(:,nb) = [eind1 tind1 tind2].';
%	fprintf('  New basis(%d): [%d %d %d]\n',nb,eind1,tind1,tind2);
      end
    end
  end
end

% Join bases:
b = [b1 b2 b3];
% ----------------------------------------------------------------------------
%plot_geometry(p,e,t,b);

