function plot_points(point, do_numbers, color, Pind)
% function plot_points(point, [do_numbers], [color], [Pind])
%
% point      - (N,number_of_points). N is 2 or 3.
% do_numbers - If 1, plot point numbers also.
%                 2, plot lines also
%                 3, plot lines only    Defaule: 0.
% color      - Color.                   Default: 'r'.
% Pind       - Only these points.       Default: 1:size(point,2).

% Default values:
if nargin < 4
  Pind = [];
  if nargin < 3
    color = [];
    if nargin < 2
      do_numbers = [];
    end
  end
end
[N,np] = size(point);
if length(Pind) == 0
  Pind = 1:np;
end
if length(do_numbers) == 0
  do_numbers = 0;
end
if length(color) == 0
  if do_numbers > 1
    color = 'r-';
  elseif do_numbers == 1
    color = 'r.';
  else
    color = 'rx';
  end
end

if do_numbers > 1
  ind = [Pind Pind(1)];         % Connect end points.
else
  ind = Pind;
end

P1 = point(1,ind);
P2 = point(2,ind);
if N == 3
  P3 = point(3,ind);
  if any(color ~= 'i');
    plot3(P1,P2,P3,color(color ~= 'i'));
  end
else
  if any(color ~= 'i');
    plot(P1,P2,color(color ~= 'i'));
  end
end

if (do_numbers > 0) & (do_numbers < 3)
  if all(color == 'i');
    do_hold = 0;
  else
    do_hold = 1;
  end
  if N == 2
    plot_numbers([P1;P2], ind, do_hold);
  else
    plot_numbers([P1;P2;P3], ind, do_hold);
  end
end
