function [l3,u3,v3,A,u,v,w] = trimap(p1,p2,p3)
% function [l3,u3,v3,A,u,v,w] = trimap(p1,p2,p3)
%
% Map triangle (p1,p2,p3) into (u,v) plane:
%
%  v ^
%    |     p3    (u3,v3)
%    |        /\
%    |       /  \
%    |      /    \
%    |     /      \                        A  = triangle area.
%    |    /        \                       nv = triangle normal vector.
%    |   /          \
%    |  /            \
%    | /              \
% p1 |/(0,0)        p2 \ (l3,0)
%    +------------------------------>  u

p21 = p2-p1;
p31 = p3-p1;

l3 = vlen(p21);

if length(p1) == 3
  nv = cross(p21,p31);
else
  nv = cross2(p21,p31);
end
A2 = norm(nv);
A  = A2/2;

u  = p21/l3;
u3 = dot(u,p31);
v3 = A2/l3;

if length(p1) == 3
  nv = nv/A2;
  v = cross(nv,u);
else
  uv = [u; 0];
  nv = [0;0;1]*sign(nv);
  v = cross(nv,uv);
  v = v(1:2);
end
w = nv;
