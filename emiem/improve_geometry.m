function [P,E,T,B,Eflag,Tflag,Bflag] = improve_geometry(P,E,T,B,Tind,...
							Eflag,Tflag,Bflag)
%function [P,E,T,B,Eflag,Tflag,Bflag] = improve_geometry(P,E,T,B,Tind,...
%							Eflag,Tflag,Bflag)
% Splits triangles Tind into 3 parts. If optional argument Tflag given
% tries to keep shapes of the triangles similar.
%
% - Tind  (1,nt) vector. Indeces of triangles to be splitted.
% - Eflag (2,nt) vector
%           1: User given value. This value will be copied into the other
%              new edges. Default:  [1:size(E,2)]
%              -1 for new edges in the middle of the triangles.
%           2: zero if this edge is original edge, not splitted.
%              -1 for new edges in the middle of the triangles.
%              Otherwise index of this edges's pair edge for
%              edges splitted into 2 parts.
% - Tflag (2,nt) vector.
%           1: User given value. This value will be copied into the 4 or 2
%              new smaller triangles. Default:  [1:size(T,2)]
%           2: zero if this triangle is original triangle, not splitted.
%              -1 for triangles splitted into 4 parts.
%              Otherwise index of this triangle's pair triangle for
%              triangles splitted into 2 parts.
% - Bflag (2,nt) vector
%           1: User given value. This value will be copied into the other
%              new bases. Default:  [1:size(B,2)]
%              -1 for new bases in the middle of the triangles.
%           2: zero if this basis is original basis, not splitted.
%              -1 for new bases in the middle of the triangles.
%              Otherwise index of this bases's pair basis for
%              bases splitted into 2 parts.

ne = size(E,2);
nt = size(T,2);
nb = size(B,2);

% Default values:
if nargin < 8
  Bflag = [];
  if nargin < 7
    Tflag = [];
    if nargin < 6
      Eflag = [];
      if nargin < 5
	Tind = [];
      end
    end
  end
end
if length(Tind) == 0
  % Split all the triangles by default:
  Tind = 1:nt;
end

% Tflag(3,nt): user given flag, index of the pair, -1 if in Tind or
%              original triangle number of the triangles splitted into 4 parts.
if length(Tflag) == 0
  Tflag = [1:nt; zeros(2,nt)];
else
% Tflag = [Tflag; zeros(1,nt)];                 % Does not work yet!!!
  Tflag = [Tflag(1:nt); zeros(2,nt)];
end
Tflag(3,Tind) = -1;

% Eflag(2,ne): user given flag and index of the new splitted edge:
if length(Eflag) == 0
  Eflag = [1:ne; zeros(1,ne)];
else
  Eflag = [Eflag(1,:); zeros(1,ne)];
end

% Bflag(2,ne): user given flag and index of the new splitted basis:
if length(Bflag) == 0
  Bflag = [1:nb; zeros(1,nb)];
else
  Bflag = [Bflag(1,:); zeros(1,nb)];
end

% Loop over all the triangles to be splitted:
for i = 1:length(Tind)
  % Split the 3 edges of this triangle:
  [P,E,Eflag,Tflag] = split_triangle_edges(P,E,T,Tind(i),Eflag,Tflag);
end

% Loop over all the triangles needed to be splitted:
Esplit = splitted_edges(T,1:nt,Eflag);
Tsplit = find(any(Esplit > 0));
for i = 1:length(Tsplit)
  % Split triangle into 2 or 4 parts:
  [E,T,B,Eflag,Tflag,Bflag] = split_triangle(P,E,T,B,Tsplit(i),...
					     Eflag,Tflag,Bflag);
end

% Loop over all the splitted edges:
Esplit = find(Eflag(2,:) > 0);
for i = 1:length(Esplit)
  % Split all the bases of this splitted edge:
  [B,Bflag] = split_bases(E,T,B,Esplit(i),Eflag,Tflag,Bflag);
end
% ============================================================================
% Split the 3 edges of triangle t:

function [P,E,Eflag,Tflag] = split_triangle_edges(P,E,T,t,Eflag,Tflag)

%if splitted_into_2(t,Tflag)
%  t2 = Tflag(t);
%  disp(sprintf('Triangle %d splitted into 2 with %d', t, t2));
%  [Tflag,e1,e2,e3a,e3b] = join_triangles(P,E,T,B,t,t2,Tflag);

%  % Split the two other neighbour triangles:
%  disp(sprintf('Splitting 2 neighbour triangles of triangle %d', t));
%  [e1a,e1b,Tflag] = split_triangles_behind_edge(P,E,T,B,e1,t,Tflag);
%  [e2a,e2b,Tflag] = split_triangles_behind_edge(P,E,T,B,e2,t,Tflag);
%else
  % The 3 edges of the triangle:
  e = T(1:3,t).';

  % Split the 3 edges:
  [P,E,Eflag] = split_edges(P,E,e,Eflag);
  
  % Check if any of the connected triangles of edges have 2 splitted edges:

  % Get triangles behind the 3 edges:
  [Tn,En] = connected_triangles(T,T(:,t));

  % Split triangles behind the 3 edges:
  for j = 1:length(Tn)
    % No need to check, if already splitting the triangle into 4 parts:
    if ~Tflag(3,Tn(j)) & (length(find(splitted_edges(T,Tn(j),Eflag))) == 2)
      [P,E,Eflag,Tflag] = split_triangle_edges(P,E,T,Tn(j),Eflag,Tflag);
    end
  end
%end

Tflag(3,t) = -1;
% ============================================================================
% Splitted edges of triangles t:

function Tsplit = splitted_edges(T,t,Eflag)
nt = size(t,2);
Tsplit = reshape(Eflag(2, T(1:3,t)), 3, nt);
% ============================================================================
% Split edges e(1..n) into 2 edges. Store the second new edge into Eflag:

function [P,E,Eflag] = split_edges(P,E,e,Eflag)

% Check which edges have not already been splitted:
e2ind = Eflag(2,e);
esplit = find(e2ind == 0);

if length(esplit) > 0
  eind = e(esplit);
  
  % The 2 end points of the edges:
  p1ind = E(1,eind);       % Point indices.
  p2ind = E(2,eind);
  P1 = P(:,p1ind);         % Point coordinates.
  P2 = P(:,p2ind);
  
  % Create new points into the middle of the two end points:
  Pm = (P1+P2)/2;
  [P,pmind] = add_point(P,Pm);

  % Put the first new edge into the old place:
  E(2,eind) = pmind;   % Update the second end point of the first edge.
  E(:,eind) = calc_edge(E(:,eind),P);     % Update edge lenghts.

  % Add the second edge:
  [E,enew,Eflag] = add_edge(P,E,pmind,p2ind,Eflag,...
			    [Eflag(1,eind); zeros(size(eind))]);
    
  % Store the new edge into Eflag:
  Eflag(2,eind) = enew;
end
% ============================================================================
% Split triangle t into 2 or 4 parts and add new edges and bases into middle:

function [E,T,B,Eflag,Tflag,Bflag]=split_triangle(P,E,T,B,t,Eflag,Tflag,Bflag)

% Get splitted edges of the triangle t:
Esplit = splitted_edges(T,t,Eflag);

nsplit = sum(Esplit > 0);

if     nsplit == 1
  e = find(Esplit);
  [E,T,B,Eflag,Tflag,Bflag] = split_triangle_2(P,E,T,B,t,e,Eflag,Tflag,Bflag);
elseif nsplit == 3
  [E,T,B,Eflag,Tflag,Bflag] = split_triangle_4(P,E,T,B,t,Eflag,Tflag,Bflag);
else
  error(sprintf('nsplit (%d) should be 1 or 3 here!', nsplit));
end
% ============================================================================
% Split triangle t into 2 part and add a new edge and basis into middle:
%                       p3
%                       +
%                      /|\
%                     / | \
%                    /  |  \
%              e3   /   em  \ e2
%                  /    |    \
%                 /  t  | t2  \
%                /      |      \
%               +-------+-------+
%             p1    e1     enew    p2

function [E,T,B,Eflag,Tflag,Bflag] = split_triangle_2(P,E,T,B,t,e1ind,...
						      Eflag,Tflag,Bflag)

% Get triangle edge and point mappings:
[e1,enew,p1,pm,p2,p3,p1ind,p2ind,p3ind,e2,e3,e2ind,e3ind,e2new,e3new] = ...
     map_triangle(P,E,T,t,e1ind,Eflag);

% Create new edge in the middle:
[E,em,Eflag] = add_edge(P,E,pm,p3,Eflag,-[1;1]);

% Leave the first triangle in the old place. Update points and edges:
T(e2ind,  t) = em;
T(3+p2ind, t) = pm;
T(:,t) = calc_triangle(T(:,t),P);  % Update triangle centroid and area.

% Add the second triangle:
es = zeros(3,1);
ps = zeros(3,1);
es([e1ind e2ind e3ind]) = [enew e2 em];
ps([p1ind p2ind p3ind]) = [pm   p2 p3];
[T,t2,Tflag] = add_triangle(P,E,T,es,ps,Tflag,[Tflag(1,t);t;0]);

% Update bases having edge e2 for the new triangle t2:
B = update_bases(B, e2, t, t2);

% Add new basis in the middle:
[B,bnew,Bflag] = add_basis(B,em,t,t2,Bflag,-[1;1]);

% Update triangle pairs:
Tflag(2,t) = t2;
% ============================================================================
% Split triangle t into 4 part and add new edges and bases into middle:
%                       p3
%                       +
%                      / \
%                     /   \
%      e3 or e3new   /     \ e2new or e2
%                   /   t3  \ 
%                  /         \
%           pm(3) +---em(2)---+ pm(2)
%                / \         / \
%               /   \  t4   /   \
%    e3new     /   em(3)  em(1)  \ e2 or e2new
%    or e3    /   t   \   /   t2  \
%            /         \ /         \  
%           +-----------+-----------+ 
%         p1(1)   e1(1)  pm(1) enew(1) p2(1)

function [E,T,B,Eflag,Tflag,Bflag] = split_triangle_4(P,E,T,B,t,...
						      Eflag,Tflag,Bflag)

% Get triangle edge and point mappings:
e1ind = 1;
%t,T(1:6,t)
[e1,enew,p1,pm,p2,p3,p1ind,p2ind,p3ind,e2,e3,e2ind,e3ind,e2new,e3new] = ...
     map_triangle(P,E,T,t,1:3,Eflag);

% Create the 3 new edges in the middle:
[E,em,Eflag] = add_edge(P,E,pm([1 e2ind e3ind]),pm([e2ind e3ind 1]),...
			Eflag,-[1 1 1;1 1 1]);

% Check which splitted edges belong to which point:
if     any(E(1:2,e3) == p3)
  e3map = [e3 e3new];
elseif any(E(1:2,e3new) == p3)
  e3map = [e3new e3];
else
  keyboard
  error(sprintf('Edges (3) of triangle %d are wrong!',t));
end
if     any(E(1:2,e2) == p2(1))
  e2map = [e2 e2new];
elseif any(E(1:2,e2new) == p2(1))
  e2map = [e2new e2];
else
  keyboard
  error(sprintf('Edges (2) of triangle %d are wrong!',t));
end

% Leave the first triangle in the old place. Update points and edges:
T(  [e1ind e2ind e3ind],t) = [e1(1) em(3) e3map(2)].';
T(3+[p1ind p2ind p3ind],t) = [p1(1) pm(1) pm(e3ind)].';
T(:,t) = calc_triangle(T(:,t),P);  % Update triangle centroid and area.

% Update original triangle index:
Tflag(3,t) = t;

% Add triangle t2:
es = zeros(3,1);
ps = zeros(3,1);
es([e1ind e2ind e3ind]) = [enew(1)  e2map(1) em(1)];
ps([p1ind p2ind p3ind]) = [pm(1)    p2(1)    pm(e2ind)];
[T,t2,Tflag] = add_triangle(P,E,T,es,ps,Tflag,[Tflag(1,t);0;t]);

% Add triangle t3:
es([e1ind(1) e2ind e3ind]) = [em(2)      e2map(2)  e3map(1)];
ps([p1ind(1) p2ind p3ind]) = [pm(e3ind)  pm(e2ind)  p3(1)];
[T,t3,Tflag] = add_triangle(P,E,T,es,ps,Tflag,[Tflag(1,t);0;t]);

% Add triangle t4:
es = [em(1) em(2)     em(3)].';
ps = [pm(1) pm(e2ind) pm(e3ind)].';
[T,t4,Tflag] = add_triangle(P,E,T,es,ps,Tflag,[Tflag(1,t);0;t]);

% Update bases of splitted edges for the new triangles:
%if e2map(1) == e2
%  B = update_bases(B, e2, t, t3);
%else
%  B = update_bases(B, e2, t, t2);
%end
%if e2map(1) == e3
%  B = update_bases(B, e3, t, t3);
%end

% Add new bases in the middle:
[B,bnew,Bflag] = add_basis(B,em,[t2 t3 t],[t4 t4 t4],Bflag,-[1 1 1;1 1 1]);

% ============================================================================
% Get triangle edge and point mappings:

function [e1,enew,p1,pm,p2,p3,p1ind,p2ind,p3ind,...
	  e2,e3,e2ind,e3ind,e2new,e3new] = map_triangle(P,E,T,t,eind,Eflag);

% Triangle t point and edge indeces:
et = T(1:3,t);
pt = T(4:6,t);

% Get splitted edge points (p1,pm,p2) and edge indeces (e,e2):
e1 = et(eind);
enew = Eflag(2,e1);
p1 = E(1,e1);
pm = E(2,e1);
p2 = E(2,enew);
if any(E(2,e1) ~= E(1,enew))
  error(sprintf('Edge splitting wrong for edges %d and %d!',e1,enew));
end

% Figure out the third triangle point index:
p1ind = find(pt == p1(1));
p2ind = find(pt == p2(1));
p3ind = find((pt ~= p1(1)) & (pt ~= p2(1)));
p3 = pt(p3ind);

% Figure out the two other edges of the triangle:
ebind = find(et ~= e1(1));

% Select the edge which has point p1:
if     any(E(1:2,et(ebind(1))) == p1(1))
  e3ind = ebind(1);
  e2ind = ebind(2);
elseif any(E(1:2,et(ebind(2))) == p1(1))
  e3ind = ebind(2);
  e2ind = ebind(1);
elseif Eflag(2,et(ebind(1))) & any(E(1:2,Eflag(2,et(ebind(1)))) == p1(1))
  e3ind = ebind(1);
  e2ind = ebind(2);
elseif Eflag(2,et(ebind(2))) & any(E(1:2,Eflag(2,et(ebind(2)))) == p1(1))
  e3ind = ebind(2);
  e2ind = ebind(1);
else  
  error(sprintf('Could not find point %d from triangle %d!', p1(1), t));
end
e3 = et(e3ind);
e2 = et(e2ind);
if length(eind) > 1
  e3new = enew(e3ind);
  e2new = enew(e2ind);
else
  e2new = [];
  e3new = [];
end
% ============================================================================
% Update all the bases having edge e and triangle t into triangle t2:

function B = update_bases(B,e,t,tnew)
bind1 = find((B(1,:) == e) & (B(2,:) == t));
B(2,bind1) = tnew;

bind2 = find((B(1,:) == e) & (B(3,:) == t));
B(3,bind2) = tnew;
% ============================================================================
% Split bases of splitted edge into 2 parts:

function [B,Bflag] = split_bases(E,T,B,e,Eflag,Tflag,Bflag);

% The second edge of this splitted edge:
enew = Eflag(2,e);

% The 3 splitted edge points:
p1 = E(1,e);
pm = E(2,e);
p2 = E(2,enew);
if E(2,e) ~= E(1,enew)
  error(sprintf('Edge splitting wrong for edges %d and %d!',e,enew));
end

% Loop for all the bases of this edge:
Bind = find(B(1,:) == e);
for j = 1:length(Bind)
  % The 2 basis triangles:
  t1 = B(2,Bind(j));
  t2 = B(3,Bind(j));

  % Get the splitted triangle pair:
  t1map = triangle_pair(T,t1,e,enew,p1,pm,p2,Bind(j),Tflag);
  t2map = triangle_pair(T,t2,e,enew,p1,pm,p2,Bind(j),Tflag);

  % Setup correct triangles for the old basis:
  B(2:3,Bind(j)) = [t1map(1) t2map(1)].';

  % Add the new basis:
  [B,bnew,Bflag] = add_basis(B,enew,t1map(2),t2map(2),Bflag,-[1;1]);
end
% ============================================================================
function tmap = triangle_pair(T,t,e,enew,p1,pm,p2,b,Tflag)

if t == 0
  tmap = [0 0];
  return
end

% Try to figure out splitted triangle pairs:
if Tflag(2,t) > 0
  tnew = Tflag(2,t);      % Splitted into 2 parts.
else
  % Splitted into 4 parts. Get the 4 splitted triangles:
  ts = find(Tflag(3,:) == Tflag(3,t));
  t    = ts(any(T(1:3,ts) == e));
  tnew = ts(any(T(1:3,ts) == enew));
  if (length(t) ~= 1) | (length(tnew) ~= 1)
    error(sprintf('Could not figure out triangle %d pair for basis %d!',...
		  t, b));
  end
end

% Check triangle edges:
if     any(T(1:3,t) == e)
  temap = [t tnew];
elseif any(T(1:3,t) == enew)
  temap = [tnew t];
else
  error(sprintf('Triangle %d have no edges of basis %d!',t, b));
end
  
% Check triangle points:
if     any(T(4:6,t) == p1) & any(T(4:6,t) == pm)
  tpmap = [t tnew];
elseif any(T(4:6,t) == pm) & any(T(4:6,t) == p2)
  tpmap = [tnew t];
else
  keyboard
  error(sprintf('Triangle %d have no points of edge of basis %d!', t, b));
end

% Check edges/points mapppings:
if any(temap ~= tpmap)
  error(sprintf('Edge/points mappings of triangle %d for basis %d wrong!',...
		t, b));
end
tmap = temap;
% ============================================================================
function [P,pnew] = add_point(P,Pnew)

np = size(Pnew,2);
pnew = size(P,2) + (1:np);
P = [P Pnew];
% ============================================================================
function [E,enew,Eflag] = add_edge(P,E,p1,p2,Eflag,flag)

ne = length(p1);
if (nargout > 2) & (nargin < 6)
  flag = zeros(2,ne);
end

% Add edges:
enew = size(E,2) + (1:ne);
E = [E [p1; p2; zeros(1,ne)]];

% Calculate edge lengthts:
E(:,enew) = calc_edge(E(:,enew),P);

if nargout > 2
  % Copy the user given value int Eflag:
  Eflag(:,enew) = flag;
end
% ============================================================================
function [T,tnew,Tflag] = add_triangle(P,E,T,es,ps,Tflag,flag)

nt = size(es,2);
if (nargout > 2) & (nargin < 6)
  flag = zeros(3,nt);
end

% Check triangle edges and points:
if ~any(E(1:2,es(1)) == ps(1)) | ~any(E(1:2,es(1)) == ps(2))
  keyboard
  error(sprintf('Edge %d (%d %d) does not contain points (%d %d)!',...
		es(1), E(1,es(1)), E(2,es(1)), ps(1), ps(2)));
end
if ~any(E(1:2,es(2)) == ps(2)) | ~any(E(1:2,es(2)) == ps(3))
  keyboard
  error(sprintf('Edge %d (%d %d) does not contain points (%d %d)!',...
		es(2), E(1,es(2)), E(2,es(2)), ps(2), ps(3)));
end
if ~any(E(1:2,es(3)) == ps(3)) | ~any(E(1:2,es(3)) == ps(1))
  keyboard
  error(sprintf('Edge %d (%d %d) does not contain points (%d %d)!',...
		es(3), E(1,es(3)), E(2,es(3)), ps(3), ps(1)));
end

% Add edges:
tnew = size(T,2) + (1:nt);
T = [T [es; ps; zeros(4,nt)]];

% Calculate triangle centroids and area:
T(:,tnew) = calc_triangle(T(:,tnew),P);

if nargout > 2
  % Copy the user given value int Tflag:
  Tflag(:,tnew) = flag;
end
% ============================================================================
function [B,bnew,Bflag] = add_basis(B,e,t1,t2,Bflag,flag)

nb = length(e);
if (nargout > 2) & (nargin < 6)
  flag = zeros(2,nb);
end

% Add bases:
bnew = size(B,2) + (1:nb);
B = [B [e; t1; t2]];

if nargout > 2
  % Copy the user given value int Bflag:
  Bflag(:,bnew) = flag;
end
% ============================================================================
