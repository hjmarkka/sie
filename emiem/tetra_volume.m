function V = tetra_volume(p1,p2,p3,p4)
% function V = tetra_volume(p1,p2,p3,p4)
%
% Calculates volumes of tetrahedrons having vertices (p1,p2,p3,p4).

V = abs(dot3(p4-p1, cross3(p2-p1, p3-p1))) / 6;
