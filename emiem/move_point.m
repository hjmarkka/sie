function [point,edge,triangle,basis] = move_point(point,edge,triangle,basis,...
                                            point_to_move, new_coordinates)

%function [point,edge,triangle,basis]=move_point(point,edge,triangle,basis,...
%                                            point_to_move, new_coordinates)
%
% Moves point POINT_TO_MOVE into NEW_COORDINATES and updates the geometry
% (POINT, EDGE, TRIANGLE). Also works with multiple points.
%
% The geometry is assumed to need no changes. So only small changes are
% allowed.

% Move points:
point(:,point_to_move) = new_coordinates;


% Search edges having one of the points POINT_TO_MOVE:
n = length(point_to_move);
nedges = size(edge,2);
ps = point_to_move(ones(1,nedges),:).';        % Copy nedges lines.
eind = find(ones(1,n)*((edge(  ones(1,n),:) == ps) | ...
                       (edge(2*ones(1,n),:) == ps)));

% Update edge lengths:
edge(:,eind) = calc_edge(edge(:,eind),point);

% Search triangles having one of the modified edges:
m = length(eind);
ntriangles = size(triangle,2);
es = eind(ones(1,ntriangles),:).';             % Copy ntriangles lines.
tind = find(ones(1,m)*((abs(triangle(  ones(1,m),:)) == es) | ...
                       (abs(triangle(2*ones(1,m),:)) == es) | ...
                       (abs(triangle(3*ones(1,m),:)) == es)));

% Update triangle areas and centroids:
triangle(:,tind) = calc_triangle(triangle(:,tind),point);
