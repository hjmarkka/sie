function [p,e,t,b,Pind,Eind,Tind,Bind] = ...
			connect(p1,e1,t1,b1,p2,e2,t2,b2,check,tol,ptol)
% function [point,edge,triangle,basis] = connect(point1,edge1,triangle1,basis1,
%                                                point2,edge2,triangle2,basis2,
%                                                [check, tol, ptol])
% Connect two geometries with common points, edges, triangles and bases.
%
%
% point:    (3,np)         1 x          - X coordinate of the point.
%                          2 y          - Y coordinate of the point.
%                          3 z          - Z coordinate of the point.
% edge:     (3,ne)         1 point1     - Index of the first point.
%                          2 point2     - Index of the second point.
%                          3 length     - Length of the edge.
% triangle: (10,nt)        1 edge1      - Index of the first edge.
%          p3 +            2 edge2      - Index of the second edge.
%             |\           3 edge3      - Index of the third edge.
%             | \          4 point1     - Index of the first vertex.
%             |  \ e2      5 point2     - Index of the second vectex.
%          e3 |   \        6 point3     - Index of the third vertex.
%             |    \       7 area       - Area of the triangle.
%             |     \      8 centroid x - X coordinate of the centroid.
%          p1 +------+ p2  9 centroid y - Y coordinate of the centroid.
%                e1       10 centroid z - Z coordinate of the centroid.
% basis:    (3,nb)         1 edge       - Index of the basis edge.
%                          2 triangle1  - First triangle.
%                          3 triangle2  - Second triangle.
%
% check:    0 or 1         0 = No checks.
%                          1 = Do checks. Default.
%
% tol:      scalar         Relative tolerance. Default: 1/100.
% ptol:     scalar         Absolute tolerance. Default: tol * min. edge length.

% ----------------------------------------------------------------------------
% Default values:
if nargin < 11
  ptol = [];
  if nargin < 10
    tol = [];
    if nargin < 9
      check = 1;
    end
  end
end
if length(check) == 0
  check = 1;
end
if length(tol) == 0
  tol = 1/100;
end
% ----------------------------------------------------------------------------
% Fast exit, if no points:

np1 = size(p1,2);
np2 = size(p2,2);
ne1 = size(e1,2);
ne2 = size(e2,2);
nt1 = size(t1,2);
nt2 = size(t2,2);
nb1 = size(b1,2);
nb2 = size(b2,2);

if np1 == 0
  p = p2; e = e2; t = t2; b = b2;
  if nargout > 4
    Pind = [zeros(1,np2); 1:np2];
    Eind = [zeros(1,ne2); 1:ne2];
    Tind = [zeros(1,nt2); 1:nt2];
    Bind = [zeros(1,nb2); 1:nb2];
  end
  return;
end
if np2 == 0
  p = p1; e = e1; t = t1; b = b1;
  if nargout > 4
    Pind = [1:np1; zeros(1,np1)];
    Eind = [1:ne1; zeros(1,ne1)];
    Tind = [1:nt1; zeros(1,nt1)];
    Bind = [1:nb1; zeros(1,nb1)];
  end
  return;
end
% ----------------------------------------------------------------------------
% Absolute tolerance == 1/100 of the smallest edge:

if length(ptol) == 0
  lmin = inf;
  if ne1 | ne2
    if ne1
      lmin = min([lmin e1(3,:)]);
    end
    if ne2
      lmin = min([lmin e2(3,:)]);
    end
  else
    for k = 1:2
      if k == 1
	P = p1;
      else
	P = p2;
      end
      np = size(P,2);
      for i = 1:np
	lmin = min([lmin vlen(P(:,i*ones(1,np-1))-P(:,[1:i-1 i+1:np]))]);
      end
    end
  end
  ptol = lmin * tol;
end
% ----------------------------------------------------------------------------
% Points (and point indeces in edges and triangles):

% Search for common points:
pjoin = zeros(0,2);
for i = 1:np2
  P2 = p2(:,i*ones(1,np1));
  ind = find(vlen(p1-P2) < ptol);
  if length(ind) > 0
    if length(ind) > 1
      error(sprintf('Found multiple same point2(%d)!', i));
    end
    % Found same point:
    if size(pjoin,2) > 0
      if length(find(pjoin(:,2) == ind)) > 0
	error(sprintf('Point1(%d) has multiple same point2!', ind));
      end
    end
    pjoin = [pjoin; [i ind]];
  end
end

% Create mapping (pmap2) from p2 into combined points p:
pmap2 = zeros(1,np2);
pmap2(pjoin(:,1)) = pjoin(:,2);
p2new = find(pmap2 == 0);
pmap2(p2new) = np1 + (1:length(p2new));

% Fix the point indeces of the edges e2 and triangles t2:
if ne2
  e2(1:2,:) = reshape(pmap2(e2(1:2,:)), 2, ne2);
end
if nt2
  t2(4:6,:) = reshape(pmap2(t2(4:6,:)), 3, nt2);
end

% Join points:
p = [p1 p2(:,p2new)];

% Common points:
pcommon = zeros(1,size(p,2));
pcommon(pjoin(:,2)) = 1;

% Optional matrix of point indeces:
if nargout > 4
  Pind = zeros(2,size(p,2));
  Pind(1,1:np1) = 1:np1;
  Pind(2,pmap2) = 1:np2;
end

% If common points, recalculate geometry because some points p2 may have moved:
if size(p,2) < np1+np2
  % Recalculate edge lengths and triangle areas and centroids:
  [pdummy,e2,t2,b2] = recalculate_geometry(p,e2,t2,b2);
end
% ----------------------------------------------------------------------------
% Edges (and edge indeces in triangles and bases):

% Search for same edges:
ejoin = zeros(0,2);
if (ne1 > 0) & (ne2 > 0)
  % Check only edges having common end points:
  ind1 = find(all(reshape(pcommon(e1(1:2,:)),2,ne1)));
  ind2 = find(all(reshape(pcommon(e2(1:2,:)),2,ne2)));
  P11 = e1(1,ind1);
  P12 = e1(2,ind1);
  for i = 1:length(ind2)
    P21 = e2(1,ind2(i));
    P22 = e2(2,ind2(i));
    ind = find(((P11 == P21) & (P12 == P22)) | ((P11 == P22) & (P12 == P21)));
    if length(ind) > 0
      if length(ind) > 1
	error(sprintf('Found multiple same edge2(%d)!', ind2(i)));
      end
      % Found same edge:
      if size(ejoin,2) > 0
	if length(find(ejoin(:,2) == ind1(ind))) > 0
	  error(sprintf('Edge1(%d) has multiple same edge2!', ind1(ind)));
	end
      end
      ejoin = [ejoin; [ind2(i) ind1(ind)]];
    end
  end
end

% Create mapping (emap2) from e2 into combined edges e:
emap2 = zeros(1,ne2);
emap2(ejoin(:,1)) = ejoin(:,2);
e2new = find(emap2 == 0);
emap2(e2new) = ne1 + (1:length(e2new));

% Fix the edge indeces of the triangles t2 and bases b2:
if nt2
  t2(1:3,:) = reshape(emap2(t2(1:3,:)), 3, nt2);
end
if nb2
  b2(1  ,:) = reshape(emap2(b2(1  ,:)), 1, nb2);
end

% Join edges:
e = [e1 e2(:,e2new)];

% Common edges:
ecommon = zeros(1,size(e,2));
ecommon(ejoin(:,2)) = 1;

% Optional matrix of edge indeces:
if nargout > 5
  Eind = zeros(2,size(e,2));
  Eind(1,1:ne1) = 1:ne1;
  Eind(2,emap2) = 1:ne2;
end

% Check edges: a point[2,1] cannot be in the middle of an edge[1,2]
if check
  % Find non-common p1 points and e1 edges:
  p1new = find(pcommon(1:np1) == 0);
  e1new = find(ecommon(1:ne1) == 0);

  for k = 1:2
    if k == 1
      P = p1(:,p1new);
      E = e2(:,e2new);
      pind = p1new;
      eind = e2new;
    else
      P = p2(:,p2new);
      E = e1(:,e1new);
      pind = p2new;
      eind = e1new;
    end
    for i = 1:size(E,2)
      % Check which points P are on the edge E(:,i):
      [in,d,s] = p_on_edge(P, p(:,E(1:2,i)), tol);
      ind = find((d < ptol) & (0 < s) & (s < 1));
      if ind
	s = '12';
	error(sprintf('Point%c(%d) in the middle of edge%c(%d)', ...
		      s(k), pind(ind(1)), s(3-k), eind(i)));
      end
    end
  end
end
% ----------------------------------------------------------------------------
% Triangles (and triangle indeces in bases):

% Search for same triangles:
tjoin = zeros(0,2);
if (nt1 > 0) & (nt2 > 0)
  % Check only triangles having common edges:
  ind1 = find(all(reshape(ecommon(t1(1:3,:)),3,nt1)));
  ind2 = find(all(reshape(ecommon(t2(1:3,:)),3,nt2)));
  P11 = t1(4,ind1);
  P12 = t1(5,ind1);
  P13 = t1(6,ind1);
  for i = 1:length(ind2)
    P21 = t2(4,ind2(i));
    P22 = t2(5,ind2(i));
    P23 = t2(6,ind2(i));
    ind = find(((P11 == P21) & (P12 == P22) & (P13 == P23)) | ...
	       ((P11 == P22) & (P12 == P23) & (P13 == P21)) | ...
	       ((P11 == P23) & (P12 == P21) & (P13 == P22)) | ...
	       ((P11 == P21) & (P12 == P23) & (P13 == P22)) | ...
	       ((P11 == P22) & (P12 == P21) & (P13 == P23)) | ...
	       ((P11 == P23) & (P12 == P22) & (P13 == P21)));
    if length(ind) > 0
      if length(ind) > 1
	error(sprintf('Found multiple same triangle2(%d)!', ind2(i)));
      end
      % Found same triangle:
      if size(tjoin,2) > 0
	if length(find(tjoin(:,2) == ind1(ind))) > 0
	  error(sprintf('Triangle1(%d) has multiple same triangle2!', ...
			ind1(ind)));
	end
      end
      tjoin = [tjoin; [ind2(i) ind1(ind)]];
    end
  end
end

% Create mapping (tmap2) from t2 into combined triangle t:
tmap2 = zeros(1,nt2);
tmap2(tjoin(:,1)) = tjoin(:,2);
t2new = find(tmap2 == 0);
tmap2(t2new) = nt1 + (1:length(t2new));

% Fix the triangle indeces of bases b2:
if nb2
  % Remember half bases of b2 and replace zero triangles of the half basis:
  half2 = (b2(3,:) == 0);
  b2(3,half2) = 1;
  b2(2:3,:) = reshape(tmap2(b2(2:3,:)), 2, nb2);
  % Restore zero triangles of the half bases:
  b2(3,half2) = 0;
end

% Join triangles:
t = [t1 t2(:,t2new)];
if nargout > 6
  Tind = zeros(2,size(t,2));
  Tind(1,1:nt1) = 1:nt1;
  Tind(2,tmap2) = 1:nt2;
end

% Common triangles:
tcommon = zeros(1,size(t,2));
tcommon(tjoin(:,2)) = 1;

% Check triangles: point[2,1] cannot be inside triangle[1,2]
if check
  % Find non-common t1 triangles:
  t1new = find(tcommon(1:nt1) == 0);

  for k = 1:2
    if k == 1
      P = p1(:,p1new);
      T = t2(:,t2new);
      pind = p1new;
      tind = t2new;
    else
      P = p2(:,p2new);
      T = t1(:,t1new);
      pind = p2new;
      tind = t1new;
    end
    for i = 1:size(T,2)
      % Check which points P are inside triangle T(:,i):
      [in,N,h,H,s1,s2,s3] = p_in_tri(P, p(:,T(4:6,i)), tol);
      ind = find((abs(h) < ptol) & (s1 < 0) & (s2 < 0) & (s3 < 0));
      if ind
	s = '12';
	error(sprintf('Point%c(%d) is inside triangle%c(%d)', ...
		      s(k), pind(ind(1)), s(3-k), tind(i)));
      end
    end
  end
end
% ----------------------------------------------------------------------------
% Bases:

% Search for same bases:
bjoin = zeros(0,2);
b1keep = logical(ones(1,nb1));
b2keep = logical(ones(1,nb2));
if nb1 > 0
  half1 = (b1(3,:) == 0);
end
if (nb1 > 0) & (nb2 > 0)
  % Replace zero triangles of half bases with size(t,2)+1:
  ntp1 = size(t,2)+1;
  b1(3,half1) = ntp1;
  b2(3,half2) = ntp1;

  % Mark the zero triangles of the half bases as common triangles:
  tcommon = [tcommon 1];

  % Check only bases having common edges and triangles:
  ind1 = find(all([reshape(ecommon(b1(1,:)),1,nb1); ...
		   reshape(tcommon(b1(2:3,:)),2,nb1)]));
  ind2 = find(all([reshape(ecommon(b2(1,:)),1,nb2); ...
		   reshape(tcommon(b2(2:3,:)),2,nb2)]));
  be1  = b1(1,ind1);
  bt11 = b1(2,ind1);
  bt12 = b1(3,ind1);
  for i = 1:length(ind2)
    be2  = b2(1,ind2(i));
    bt21 = b2(2,ind2(i));
    bt22 = b2(3,ind2(i));
    ind = find(((be1 == be2) & (bt11 == bt21) & (bt12 == bt22)) | ...
	       ((be1 == be2) & (bt12 == bt21) & (bt11 == bt22)));
    if length(ind) > 0
      if length(ind) > 1
	error(sprintf('Found multiple same basis2(%d)!', ind2(i)));
      end
      % Found same basis:
      if size(bjoin,2) > 0
	if length(find(bjoin(:,2) == ind1(ind))) > 0
	  error(sprintf('Basis1(%d) has multiple same basis2!', ind1(ind)));
	end
      end
      bjoin = [bjoin; [ind2(i) ind1(ind)]];
    end
  end

  % Remove bases b2 having both triangles common (or half and one common):
  b2keep(ind2) = 0;
      
  % Remove non-common half bases of b1 having common triangles:
  b1keep(ind1) = ~half1(ind1);
  b1keep(bjoin(:,2)) = 1;

  % Restore zero triangles of the half bases:
  b1(3,half1) = 0;
  b2(3,half2) = 0;
end

% Create mapping (bmap2) from b2 into combined bases b:
bmap2 = zeros(1,nb2);
bmap2(bjoin(:,1)) = bjoin(:,2);
bmap2(~b2keep) = -1;
b2new = (bmap2 == 0) & b2keep;

% Add new basis for every common edge with non-common triangles:
b3 = zeros(3,0);
nejoin = size(ejoin,1);
if nejoin > 0
  te1 = t1(1:3,:);
  te2 = t2(1:3,:);
  nb3 = 0;
  ib1 = [];
  ib2 = [];
  for i = 1:nejoin
    eind1 = ejoin(i,2);

    % Find all triangles of this edge:
    it1 = find(any(te1 == eind1));
    it2 = tmap2(find(any(te2 == eind1)));

    % No need to create new bases, if the edge has any common triangles:
    if length(it1) & length(it2) & ~any(tcommon([it1 it2]))
      % Check if this edge has a half basis in one or both of the bases:
      if nb1 > 0
	ib1 = find((b1(1,:) == eind1) & half1 & b1keep);
      end
      if nb2 > 0
	ib2 = find((b2(1,:) == eind1) & half2 & b2new);
      end
      
      if length(ib1) > 0
	if length(ib2) > 0
	  % Both bases have a half basis: join them
	  b1(3,ib1(1)) = b2(2,ib2(1));   % Copy triangle index.
	  b2(:,ib2(1)) = 0;              % Remove basis 2.
	  bmap2(ib2(1)) = ib1(1);
	  b2new(ib2(1)) = 0;
%	  fprintf('  Joining half basis1(%d)+basis2(%d): [%d %d %d]\n',...
%		  ib1(1),ib2(1),eind1,b1(2,ib1(1)),b1(3,ib1(1)));
%	  fprintf('  Removing half basis2(%d)\n',ib2(1));
	else
	  % Only bases 1 has a half basis: add triangle from bases 2
	  b1(3,ib1(1)) = it2(1);
%	  fprintf('  Joining triangle2(%d) into basis1(%d): [%d %d %d]',...
%		  it2(1),ib1(1),eind1,b1(2,ib1(1)),it2(1));
	end
      elseif length(ib2) > 0
	% Only bases 2 has a half basis: add triangle from bases 1
	b2(3,ib2(1)) = it1(1);
%	fprintf('  Joining triangle1(%d) into basis2(%d): [%d %d %d]',...
%		it1(1),ib2(1),eind1,b2(2,ib2(1)),it1(1));
      else
	% New basis if non-common triangles:
	nb3 = nb3+1;
	b3(:,nb3) = [eind1,it1(1),it2(1)].';
%	fprintf('  New basis(%d): [%d %d %d]\n',nb3,eind1,it1(1),it2(1));
      end
    end
  end
end

% Complete mapping of b2:
nb1keep = length(find(b1keep));
bmap2(b2new) = nb1keep + (1:length(find(b2new)));

% Join bases:
b = [b1(:,b1keep) b2(:,b2new) b3];

if nargout > 7
  Bind = zeros(2,size(b,2));
  ind1 = 1:nb1;
  Bind(1,1:nb1keep) = ind1(b1keep);
  ind2 = 1:nb2;
  Bind(2,bmap2(bmap2 > 0)) = ind2(bmap2 > 0);
end
% ----------------------------------------------------------------------------
