function [in,N,h,H,r1,r2,r3] = p_in_tri(p,t,tol)
% function [in,N,h,H,r1,r2,r3] = p_in_tri(p,t,[tol])
%  Check if points P are inside triangle T
%  p = [x ...
%       y ...
%       z ...]
%  t = [x1 x2 x3
%       y1 y2 y3
%       z1 z2 z3]
% default tol = 1e-6

% Default values:
if (nargin < 3)
  tol = [];
end
if (length(tol) == 0)
  tol = 1e-6;
end

% Triangle vertices:
p1 = t(:,1);
p2 = t(:,2);
p3 = t(:,3);

% Some vector algebra:
A = p2-p1;
C = p3-p1;
a = dot(A,A);
b = dot(C,A);
c = dot(C,C);
d = a-2*b+c;

% Minimum edge length of the triangle for tolerance checking:
lmin = sqrt(min([a d c]));
ptol = tol*lmin;

n = size(p,2);
op = ones(1,n);
P1 = p-p1(:,op);

% Height vector:
N = cross(A,C);
lN = vlen(N);
N = N/lN;
h = N.'*P1;      % dot(P1,N(:,op);
H = N*h;

% Projection of vectors p1->p into triangle plane (to be sure):
Q1 = P1-H;
  
% Normal vectors of triangle edges (not normalized):
m1 = cross(N,A);
m3 = cross(N,C);

% Distances from the edges:
h1 = m1.'*Q1;                     % dot(m1,p-p1);
h3 = m3.'*Q1;                     % dot(m3,p-p1);
r1 = -h1/sqrt(a);                 % r1 = dot(cross(N,-A/sqrt(a)),p-p1);
r3 = h3/sqrt(c);                  % r3 = dot(cross(N,C/sqrt(c)),p-p1);
r2 = (h1 - h3 + m3.'*A)/sqrt(d);  % r2 = dot(cross(N,(A-C)/sqrt(d)),p-p2);

in = (abs(h) < ptol) & (r1 < ptol) & (r2 < ptol) & (r3 < ptol);


