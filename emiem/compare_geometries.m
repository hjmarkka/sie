function [same,Pmap,Emap,Tmap,Bmap] = ...
    compare_geometries(P1,E1,T1,B1,P2,E2,T2,B2,ptol)
%function [same,Pmap,Emap,Tmap,Bmap] = ...
%    compare_geometries(P1,E1,T1,B1,P2,E2,T2,B2,[ptol])
%
% Compares two geometries for common points, edges, triangles and bases.
%
% same = logical 4-vector, one if P, E, T and B are the same.
% Pmap,Emap,Tmap,Bmap = mapping from the first into the second geometry.

% Default values:
if nargin < 9
  ptol = [];
end
if length(ptol) == 0
  ptol = 0;
end

% Initialize output:
same = [1 1 1 1];
% ----------------------------------------------------------------------------
% Points:
np1 = size(P1,2);
np2 = size(P2,2);
Pmap = zeros(1,np1);
P2used = zeros(1,np2);
for i1 = 1:np1
  P1o = P1(:,i1 * ones(1,np2));
  ind = find(vlen(P1o-P2) <= ptol);
  if length(ind) == 1
    if nargout > 1
      Pmap(i1) = ind;
      P2used(ind) = P2used(ind) + 1;
    end
  elseif length(ind) > 1
    error(sprintf('Two points P2(:,[%d %d]) are the same as P1(:,%d)!', ...
                  ind(1), ind(2), i1));
  else
    same(1) = 0;
  end
end
ind = find(any(P2used > 1));
if length(ind)
  error(sprintf('Point P2(:,%d) is the same as two P1 points!', ind(1)));
elseif any(P2used == 0)
  same(1) = 0;
end
% ----------------------------------------------------------------------------
% Edges:
ne1 = size(E1,2);
ne2 = size(E2,2);
Emap = zeros(1,ne1);
E2used = zeros(1,ne2);
for i1 = 1:ne1
  E1o = reshape(Pmap(E1(1:2, i1 * ones(1,ne2))), 2, ne2);
  ind = find(all(E1o == E2(1:2,:),1) | all(E1o == E2([2 1],:),1));
  if length(ind) == 1
    if nargout > 2
      Emap(i1) = ind;
      E2used(ind) = E2used(ind) + 1;
    end
  elseif length(ind) > 1
    error(sprintf('Two edges E2(:,[%d %d]) are the same as E1(:,%d)!', ...
                  ind(1), ind(2), i1));
  else
    same(2) = 0;
  end
end
ind = find(any(E2used > 1));
if length(ind)
  error(sprintf('Edge E2(:,%d) is the same as two E1 edges!', ind(1)));
elseif any(E2used == 0)
  same(2) = 0;
end
% ----------------------------------------------------------------------------
% Triangles:
nt1 = size(T1,2);
nt2 = size(T2,2);
Tmap = zeros(1,nt1);
T2used = zeros(1,nt2);
for i1 = 1:nt1
  T1o = reshape(Emap(T1(1:3, i1 * ones(1,nt2))), 3, nt2);
  ind = find(all(T1o == T2([1 2 3],:),1) | all(T1o == T2([1 3 2],:),1) | ...
             all(T1o == T2([2 1 3],:),1) | all(T1o == T2([2 3 1],:),1) | ...
             all(T1o == T2([3 1 2],:),1) | all(T1o == T2([3 2 1],:),1));
  if length(ind) == 1
    if nargout > 3
      Tmap(i1) = ind;
      T2used(ind) = T2used(ind) + 1;
    end
  elseif length(ind) > 1
    error(sprintf('Two triangles T2(:,[%d %d]) are the same as T1(:,%d)!', ...
                  ind(1), ind(2), i1));
  else
    same(3) = 0;
  end
end
ind = find(any(T2used > 1));
if length(ind)
  error(sprintf('Triangle T2(:,%d) is the same as two T1 triangles!', ind(1)));
elseif any(T2used == 0)
  same(3) = 0;
end
% ----------------------------------------------------------------------------
% Bases:
nb1 = size(B1,2);
nb2 = size(B2,2);
Bmap = zeros(1,nb1);
B2used = zeros(1,nb2);
for i1 = 1:nb1
  BE1o = Emap(B1(1  , i1 * ones(1,nb2)));
  BT1o = reshape(Tmap(B1(2:3, i1 * ones(1,nb2))), 2, nb2);
  ind = find((BE1o == B2(1,:)) & ...
             (all(BT1o == B2(2:3,:),1) | all(BT1o == B2([3 2],:),1)));
  if length(ind) == 1
    if nargout > 4
      Bmap(i1) = ind;
      B2used(ind) = B2used(ind) + 1;
    end
  elseif length(ind) > 1
    error(sprintf('Two bases B2(:,[%d %d]) are the same as B1(:,%d)!', ...
                  ind(1), ind(2), i1));
  else
    same(4) = 0;
  end
end
ind = find(any(B2used > 1));
if length(ind)
  error(sprintf('Basis B2(:,%d) is the same as two B1 bases!', ind(1)));
elseif any(B2used == 0)
  same(4) = 0;
end
% ----------------------------------------------------------------------------
