function [lu,lv,A,u,v,w] = recdim(p1,p2,p3,p4)
% function [lu,lv,A,u,v,w] = recdim(p1,p2,p3,p4)
%
% Map lateral rectangle (p1,p2,p3,p4) into (u,v) plane:
%
%  v ^
%    | (0,lv)        (lu,lv)
%    +-----------------+
%    | p4          p3  |
%    |                 |
%    |                 |                   A  = rectangle area.
%    |                 |                   nv = rectangle normal vector.
%    |                 |
%    |                 |
%    |                 |
% p1 | (0,0)        p2 | (lu,0)
%    +-----------------+------------>  u

p21 = p2-p1;
p41 = p4-p1;

lu = vlen(p21);
lv = vlen(p41);

if nargout > 2
  A = lu*lv; 

  if nargout > 3
    u = p21/lu;
    v = p41/lv;
    if length(p1) == 3
      w = cross(u,v);
    else
      w = cross2(u,w);
    end
  end
end
