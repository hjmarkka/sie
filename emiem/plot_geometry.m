function plot_geometry(point,edge,triangle,basis,do_numbers,color,color2)
% function plot_geometry(point,edge,triangle,basis,do_numbers,color,color2)
% point      - (N,number_of_points). N is 2 or 3.
% edge       - (3,number_of_edges).
% triangle   - (10,numbers_of_triangles).
% basis      - (3,numbers_of_basis).
% do_numbers - If 1, plot edge numbers.


if nargin < 7
  color2 = 'b.';
  if nargin < 6
    color = 'r-';
    if nargin < 5
      do_numbers = 0;
    end
  end
end

clf;

plot_points(point,do_numbers,color2);
title('Points');
axis equal; view(2); pause

plot_edges(point,edge,do_numbers,color);
title('Edges');
axis equal; view(2); pause

plot_triangles(point,edge,triangle,do_numbers,color);
title('Triangles');
axis equal; view(2); pause

plot_basis(point,edge,triangle,basis,do_numbers,color);
title('Basis');
axis equal; view(2); pause

