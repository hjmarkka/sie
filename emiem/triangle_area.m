function A = triangle_area(p1,p2,p3)
% function A = triangle_area(p1,p2,p3)
%
% Calculates areas of triangles having vertices (p1,p2,p3).

A = 0.5 * vlen(cross3(p2-p1,p3-p1));
