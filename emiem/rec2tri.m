function [point,edge,triangle] = rec2tri(p1,p2,p3,p4,n,point,edge,triangle)
% function [point,edge,triangle] = rec2tri(p1,p2,p3,p4,n,point,edge,triangle)
% n == 2:    p4 +----+ p3
%               |   /|
%               |  / |
%               | /  |
%               |/   |
%            p1 +----+ p2
%
% n == 4:    p4 +----+ p3
%               |\  /|
%               | \/ |
%               | /\ |
%               |/  \|
%            p1 +----+ p2

if     n == 2
  [edge,triangle] = add_triangle(p4,p1,p3,point,edge,triangle);
  [edge,triangle] = add_triangle(p1,p2,p3,point,edge,triangle);
elseif n == 4
  % Add middle point!
  point5 = 0.5*(point(:,p3)+point(:,p1));
  point = [point point5];
  p5 = size(point,2);
  
  [edge,triangle] = add_triangle(p4,p1,p5,point,edge,triangle);
  [edge,triangle] = add_triangle(p1,p2,p5,point,edge,triangle);
  [edge,triangle] = add_triangle(p3,p4,p5,point,edge,triangle);
  [edge,triangle] = add_triangle(p2,p3,p5,point,edge,triangle);
else
  error('n should be either 2 or 4!');
end
