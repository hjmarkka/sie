function err = check_geometry(P,E,T,B,tol)
% function err = check_geometry(P,E,T,B,[tol])
%
% Check consistency of the geometry. Default tol = 1e-3.
%
% Note: Does not check for any geometrical problems!

% Default values:
if nargin < 5
  tol = [];
  if nargin < 4
    B = [];
  end
end
if length(tol) == 0
  tol = 1e-3;
end
if length(B) == 0
  B = zeros(3,1);
end

np = size(P,2);
ne = size(E,2);
nt = size(T,2);
nb = size(B,2);

err = 0;
% ---------------------------------------------------------------------------
% POINTS:

% Check for duplicate points:
for i = 1:np
  PP = P(:,i*ones(1,np));
  ind = find((vlen(P-PP) < 1e-6) & ((1:np) ~= i));
  for j = 1:length(ind)
    disp(sprintf('Duplicate points %d and %d!', i,ind(j)))
    err = 1;
  end
end

if (nargin > 1) & (size(E,2) > 0)
  % Check that all the points are used by edges:
  PE1 = E(1,:);
  PE2 = E(2,:);
  pind = zeros(1,np);
  pind(PE1) = 1;
  pind(PE2) = 1;
  ind = find(pind == 0);
  for i = 1:length(ind)
    disp(sprintf('Point %d not used by any edges!',ind(i)));
    err = 1;
  end
else
  return
end

if (nargin > 2) & (size(T,2) > 0)
  % Check that all the points are used by triangles:
  PT1 = T(4,:);
  PT2 = T(5,:);
  PT3 = T(6,:);
  pind = zeros(1,np);
  pind(PT1) = 1;
  pind(PT2) = 1;
  pind(PT3) = 1;
  ind = find(pind == 0);
  for i = 1:length(ind)
    disp(sprintf('Point %d not used by any triangles!',ind(i)));
    err = 1;
  end
end
% ---------------------------------------------------------------------------
% EDGES:

% Check for duplicate edges:
for i = 1:ne
  EE = E(1:2,i*ones(1,ne));
  ind = find((all(E(1:2,:) == EE) | all(E([2 1],:) == EE)) & ((1:ne) ~= i));
  for j = 1:length(ind)
    disp(sprintf('Duplicate edge %d and %d!', i,ind(j)))
    err = 1;
  end
end

% Check edge lengths:
L1 = E(3,:);
L2 = vlen(P(:,PE1) - P(:,PE2));
ind = find(abs(L1-L2)./L2 > tol);
for i = 1:length(ind)
  disp(sprintf('Edge %d length wrong: %f / %f!',ind(i),L1(ind(i)),L2(ind(i))));
  err = 1;
end

if (nargin > 2) & (size(T,2) > 0)
  % Check that all the edges are used by triangles:
  ET1 = T(1,:);
  ET2 = T(2,:);
  ET3 = T(3,:);
  eind = zeros(1,ne);
  eind(ET1) = 1;
  eind(ET2) = 1;
  eind(ET3) = 1;
  ind = find(eind == 0);
  for i = 1:length(ind)
    disp(sprintf('Edge %d not used by any triangles!',ind(i)));
    err = 1;
  end
else
  return
end
% ---------------------------------------------------------------------------
% TRIANGLES:

% Check for duplicate triangles:
for i = 1:nt
  TE = T(1:3,i*ones(1,nt));
  ind = find((all(T([1 2 3],:) == TE) | all(T([2 3 1],:) == TE) | ...
	      all(T([3 2 1],:) == TE) | all(T([1 3 2],:) == TE) | ...
	      all(T([2 1 3],:) == TE) | all(T([3 1 2],:) == TE) ) & ...
	     ((1:nt) ~= i));
  for j = 1:length(ind)
    disp(sprintf('Duplicate triangle (edges) %d and %d!', i,ind(j)))
    err = 1;
  end
  TP = T(4:6,i*ones(1,nt));
  ind = find((all(T(3+[1 2 3],:) == TP) | all(T(3+[2 3 1],:) == TP) | ...
	      all(T(3+[3 2 1],:) == TP) | all(T(3+[1 3 2],:) == TP) | ...
	      all(T(3+[2 1 3],:) == TP) | all(T(3+[3 1 2],:) == TP) ) & ...
	     ((1:nt) ~= i));
  for j = 1:length(ind)
    disp(sprintf('Duplicate triangle (points) %d and %d!', i,ind(j)))
    err = 1;
  end
end

% Check that the triangle edges and points match:
P1ET1 = PE1(:,ET1);         % The two points of the triangle edges.
P2ET1 = PE2(:,ET1);
P1ET2 = PE1(:,ET2);
P2ET2 = PE2(:,ET2);
P1ET3 = PE1(:,ET3);
P2ET3 = PE2(:,ET3);
ind = find(~((((P1ET1==PT1)&(P2ET1==PT2)) | ((P1ET1==PT2)&(P2ET1==PT1))) &...
	     (((P1ET2==PT2)&(P2ET2==PT3)) | ((P1ET2==PT3)&(P2ET2==PT2))) &...
	     (((P1ET3==PT3)&(P2ET3==PT1)) | ((P1ET3==PT1)&(P2ET3==PT3)))));
for i = 1:length(ind)
  disp(sprintf('Triangle %d edges and points do not match!',ind(i)));
  err = 1;
end

% Check triangle areas:
A1 = T(7,:);
P1 = P(:,PT1);
P2 = P(:,PT2);
P3 = P(:,PT3);
A2 = vlen(cross(P2-P1,P3-P1))/2;
ind = find(abs(A1-A2)./A2 > tol);
for i = 1:length(ind)
  disp(sprintf('Triangle %d area wrong: %f / %f!', ...
	       ind(i), A1(ind(i)), A2(ind(i))));
  err = 1;
end

% Check triangle centroids:
C1 = T(8:10,:);
C2 = (P1+P2+P3)/3;
minL = min(L1);                      % Relative to the minimum edge length.
ind = find(vlen(C1-C2)/minL > tol);
for i = 1:length(ind)
  c1 = C1(ind(i));
  c2 = C2(ind(i));
  disp(sprintf('Triangle %d centroid wrong: [%f %f %f] / [%f %f %f]!', ...
	       ind(i), c1(1), c1(2), c1(3), c2(1), c2(2), c2(3)));
  err = 1;
end

ptol = tol*minL;
for i = 1:nt
  pti = [P1(:,i) P2(:,i) P3(:,i)];
  
  % Check that no points P are inside triangle T(:,i):
  pind = find(p_in_tri(P, pti, tol));
  for j = pind
    if ~any(T(4:6,i) == j)
      disp(sprintf('Point %d is inside triangle %d!', pind(j), i));
      err = 1;
    end
  end

  % Check that a triangle center is inside a single triangle:
  pind = find(p_in_tri(C1, pti, tol));
  for j = pind
    if j ~= i
      disp(sprintf('Centroid of triangle %d is inside triangle %d!', j, i));
      err = 1;
    end
  end
end
% ---------------------------------------------------------------------------
% BASES:

if (nargin < 4) | (size(B,2) == 0)
  return;
end

% Check for duplicate bases:
for i = 1:nb
  BE = B(1,i);
  BB = B(2:3,i*ones(1,nb));
  ind = find(((1:nb) ~= i) & (B(1,:) == BE) & ...
	     (all(B(2:3,:) == BB) | all(B([3 2],:) == BB)));
  for j = 1:length(ind)
    disp(sprintf('Duplicate basis %d and %d!', i,ind(j)))
    err = 1;
  end
end

ind = find(B(3,:));      % Do not check half bases!
EB  = B(1,ind);
TB1 = B(2,ind);
TB2 = B(3,ind);

% Check that the triangles are not the same:
ind = find(TB1 == TB2);
for i = 1:length(ind)
  disp(sprintf('Basis %d triangles are the same!',ind(i)));
  err = 1;
end

% Check that the two triangles have the basis edges:
E1T1 = ET1(:,TB1);   % The three edges of the triangles.
E2T1 = ET2(:,TB1);
E3T1 = ET3(:,TB1);
E1T2 = ET1(:,TB2);
E2T2 = ET2(:,TB2);
E3T2 = ET3(:,TB2);

ind = find(~(((E1T1 == EB) | (E2T1 == EB) | (E3T1 == EB)) & ...
	     ((E1T2 == EB) | (E2T2 == EB) | (E3T2 == EB))));
for i = 1:length(ind)
  disp(sprintf('Basis %d edge and triangles do not match!',ind(i)));
  err = 1;
end
% ---------------------------------------------------------------------------
