function [P,E,T,V] = make_tetra(p1,p2,p3,p4)
%function [P,E,T,V] = make_tetra(p1,p2,p3,p4)
%
% Create a tetrahedron:
%
%                 + p4                    Edges: (orientation not fixed here!)
%                /|\                      e1 = between points p1 and p2
%               / | \                     e2 =                p1 and p3
%              /  |  \e6                  e3 =                p1 and p4
%             /   |   \                   e4 =                p2 and p3
%         e5 /    |e3  \                  e5 =                p3 and p4
%           /     |  . / p2               e6 =                p4 and p2
%          /      |   /                   
%         /  e4.  |  /e1                  Triangles: oppposite to the point
%        /  .     | /                     t1 = back  (p2,p3,p4)  (Note: _outer_
%       /.        |/                      t2 = left  (p1,p4,p3)   triangle
%   p3 +----------+ p1                    t3 = right (p1,p2,p4)   normal!)
%            e2                           t4 = below (p1,p3,p2)
%
% Input:
%  - p1,p2,p3,p4: - (3,1) coordinates of the vertices of the tetrahedron.
%
% Output:
%  - P,E,T: Points, edges and triangles. See connect.m .
%  - V: (18,1) column vector of the tetrahedron:
%        1: 4 - The 4 triangles of the tetrahedron. Triangle j is opposite to
%               the point j. Negative, if the triangle normal is not
%               the _outer_ normal of the tetrahedron!
%        5:10 - The 6 edges of the tetrahedron. The three first edges are the
%               "spanning" edges of the tetrahedron.
%       11:14 - The 4 points of the tetrahedron (p1,p2,p3,p4).
%       15    - Volume of the tetrahedron.
%       16:18 - Coordinates of the centroid of the tetrahedron.

% 14.10.2000 Matti Taskinen, Rolf Nevanlinna Institute, University of Helsinki
% 04.07.2007 Triangle and edge orietations changed. Matti Taskinen

% Points:
P = [p1 p2 p3 p4];

% Edges:
E = [1 2 0; 1 3 0; 1 4 0; 2 3 0; 3 4 0; 4 2 0].';
E = calc_edge(E,P);   % Edge length.

% Triangles:
T = [[4 5 6 2 3 4; 3 5 2 1 4 3; 1 6 3 1 2 4 ; 2 4 1 1 3 2].' ; zeros(4,4)];
T = calc_triangle(T,P);  % Triangle area and centroid.

% Tetras:
%    triangles edges        points   volume  center point
V = [1 2 3 4   1 2 3 4 5 6  1 2 3 4  0       0 0 0].';
V = calc_tetra(V,P);  % Tetrahedron volume and center point.
