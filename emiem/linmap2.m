function [P,W] = linmap2(p1,p2,p3,x,w)
% function [P,W] = linmap2(p1,p2,p3,x,w)
%
% Map 2D Gaussian integration points and weights into a triangle (p1,p2,p3).
%
% Input:
% - p1,p2,p3: (2,1) or (3,1) coordinates of the vertices triangle.
% - x: (2,np) Gaussian quadrature points for the refence triangle.
% - w: (np,1) Gaussian quadrature weights for the reference triangle.
%
% Output:
% - P: (2,np) or (3,np) integration points for triangle (p1,p2,p3).
% - W: (np,1) integration weights for triangle (p1,p2,p3).

P = p1(:,ones(1,size(x,2))) + [p2-p1 p3-p1] * x;

if nargout > 1
  % Scale weights with area relation A/(1/2):
  W = 2 * triangle_area(p1,p2,p3) * w;
end

