function [edge, triangle] = add_triangle(p1, p2, p3, point, edge, triangle)
% function [edge, triangle] = add_triangle(p1, p2, p3, point, edge, triangle)
%
% Triangle:  points P1, P2 and P3, edges e1, e2 and e3
%
%         P3
%         |\
%         | \
%         |  \
%         |   \
%         |    \ e2
%     e3  |     \
%         |      \
%         |       \
%         |        \
%         +---------+
%        P1    e1    P2

ps = [p1 p2 p3 p1];
t = zeros(10, 1);          % edges, points, area and centroid point.

% Add new edges:
for i = 1:length(ps)-1
  pa = ps(i);
  pb = ps(i+1);
  
  % Search for this edge:
  if length(edge) > 0
    ind = find((edge(1,:) == pa) & (edge(2,:) == pb));
    if length(ind) == 0
      ind = find((edge(1,:) == pb) & (edge(2,:) == pa));
    end
  else
    ind = [];
  end
  if length(ind) == 0
    % Add edge:  [points 1 and 2, length]
    e = [pa pb vlen(point(:,pa)-point(:,pb))].';
    if e(3) == 0
      error('Length of the edge is zero!');
    end
    edge = [edge e];
    ind = size(edge, 2);
  end

  % Triangle -> edges:
  t(i) = ind;
end

% Triangle -> points:
t(4:6) = ps(1:3);

% Add triangle:
t = calc_triangle(t,point);

triangle = [triangle t];
