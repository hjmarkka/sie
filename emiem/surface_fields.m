function [Ef,Hf] = surface_fields(epar,Nf,Jf,dJf,Kf,dKf,what)
% function [Ef,Hf] = surface_fields(epar,Nf,Jf,[dJf,Kf,dKf,what])
%
% Calculate electric and/or magnetic fields on the surface.
%
% Output:
%  Ef   - Electric field values in points Pf. (3,np) matrix.
%  Hf   - Magnetic field values in points Pf. (3,np) matrix.
%
% Input:
%  epar - [f eps mu], eps and mu may be omitted. Default: eps = eps0, mu = m0.
%  Nf   - Normal vectors of the surface in points Pf. (3,np) matrix.
%  Jf   - Electric current densities in points Pf. (3,np) matrix or empty [].
%  dJf  - Divergencies of electric currents in points Pf. (1,np) vector or [].
%  Kf   - Magnetic current densities in points Pf. (3,np) matrix or [].
%  dKf  - Divergencies of magnetic currents in points Pf. (1,np) vector or [].
%  what - logical (1,2) vector specifing whether to calculate electric
%            and/or magnetic fields Ef and Hf.
%            Default: if no Kf/dKf given: [1 0]
%                     if    Kf/dKf given: [1 1]

f = epar(1);
if length(epar) < 2
  eps = 8.85418782e-012;
else
  eps = epar(2);
end
if length(epar) < 3
  mu = 4*pi*1e-7;
else
  mu = epar(3);
end

w = 2*pi*f;
i = sqrt(-1);

% Default values:
if nargin < 6
  dKf = [];
  if nargin < 5
    Kf = [];
    if nargin < 4
      dJf = [];
    end
  end
end
      
[dum,nj]  = size(Jf);
[dum,nk]  = size(Kf);
ndj = length(dJf);
ndk = length(dKf);

doJ  = nj  > 0;
doK  = nk  > 0;
doDJ = ndj > 0;
doDK = ndk > 0;

What = zeros(1,2);
if (nargin < 7) | (length(what) == 0)
  What(1) = 1;
  if nargout > 1
    What(2) = 1;
  elseif doJ | dodK
    What(2) = 1;
  end
else
  What(1:length(what)) = what;
end
doE = What(1);
doH = What(2);

np = max([nj nk ndj ndk]);

% Ef = cross(Nf,Kf) - 1/(i*w*eps) * dJf * Nf;
% Hf = cross(Nf,Jf) + 1/(i*w*mu)  * dKf * Nf;

if doE
  Ef = zeros(3,np);
  if doK
    Ef = -cross(Nf,Kf);
  end
  if doDJ
    Ef = Ef + 1/(i*w*eps) * dJf([1 1 1],:) .* Nf;
  end
end
if doH
  Hf = zeros(3,np);
  if doJ
    Hf = -cross(Nf,Jf);
  end
  if doDK
    Hf = Hf - 1/(i*w*mu)  * dKf([1 1 1],:) .* Nf;
  end
end
