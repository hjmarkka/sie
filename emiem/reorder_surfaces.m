function [T,tdomain] = reorder_surfaces(P,T,tdomain)
% function [T,tdomain] = reorder_surfaces(P,T,tdomain)
%
% Reorder triangles T of (closed) surfaces so that the triangle normals
% point into the domains of lower number and tdomain's so that tdomain(2,:)
% is the domain of the lower number (and tdomain(3,:) is the larger number).
%
% An outer domain is assumed to be domain number 1 and it must exist.
%
% Parameters:
% - P,T = Points and triangles. See connect.m.
% - tdomain: (3,nt) = Triangle properties [METAL; domain1; domain2]
%       where METAL = 0, if triangle is non-metallic,
%                     1, if triangle is perfectly electric conducting (PEC),
%                     2, if triangle is perfectly magnetic conducting (PMC),
%                     3, if triangle is PEC and PMC.

% ----------------------------------------------------------------------------
nt = size(T,2);
tdone = zeros(1,nt);    % 1, if the orientation of the triangle has been fixed.

% Get all the domains from tdomain:
maxdomain = max(max(tdomain(2:3,:)));
domains = zeros(1,maxdomain);
domains(tdomain(2:3,:)) = 1;
domains = find(domains);

% Domain 1 is assumed to be the outer domain and it is required:
if domains(1) ~= 1
  error('Domain 1 must exist and it must be the outer domain!');
end

% Do not check the triangles of the open surfaces:
Topen = (tdomain(2,:) == tdomain(3,:));
tdone(Topen) = 1;
% ----------------------------------------------------------------------------
% Starting from the outer domain (1), loop over the domains:
for idomain = domains
  % Get all the triangles of this domain (except the open surfaces):
  Tind = find(any(tdomain(2:3,:) == idomain) & ~Topen);
  Td = T(:,Tind);

  % If all the triangles have been oriented, no need to go further:
  if any(tdone(Tind) == 0)
    % Get all the (closed) surfaces of this domain:
    [surfaces,sstart,Torient,is_orient,is_closed] = get_surfaces(Td);
    if ~is_orient
      error(sprintf('Surface of domain %d is not orientable!', idomain));
    end
    if ~is_closed
      error(sprintf('Surface of domain %d is not closed!', idomain));
    end
    nsurfaces = length(sstart)-1;
    Vsurface = zeros(1,nsurfaces);
    
    % Calculate volumes of all the surfaces of this domain:
    Tcheck = logical(zeros(size(Tind)));
    for isurface = 1:nsurfaces
      % Triangles of this surface:
      TSind = surfaces(sstart(isurface):sstart(isurface+1)-1); % Index into Td!
      
      % Check that a triangle cannot be on two surfaces of a domain:
      ind = find(Tcheck(TSind));
      if ind
	error(sprintf('Triangle %d cannot be on two surfaces of domain %d!',...
		      ind(1), idomain));
      end
      Tcheck(TSind) = isurface;
      
      % Reorder triangles of this surface into temporary TS:
      TS = reorder_triangle(Td(:,TSind), Torient(TSind) < 0);
      
      % Update the orientation of non-fixed triangles of this surface:
      non_fixed = (tdone(Tind(TSind)) == 0);
      T(:,Tind(TSind(non_fixed))) = TS(:,non_fixed);
      
      % Calculate signed volume:
      %   V = \int 1 dV = \int \Div f dV = \int dot(nv,f) dS
      % where nv = triangle outer normal and f = 1/3 * position vector.
      % Integration using one Gaussian point, triangle centroids:
      PC = TS(8:10,:);
      A = TS(7,:);
      N = triangle_normals(P,TS);
      Vsurface(isurface) = 1/3*(dot(N,PC) * A.');
    end

    % The surfaces of a domain are either outer surfaces (the domain is
    % inside the surface) or inner ones (the domain is outside the surface):
    outer_surface = logical(zeros(1,nsurfaces));
    if idomain == 1
      % The outer domain 1 has no outer surfaces, only inner ones.
    else
      % The other domains are assumed to have one outer surface and
      % the other possible surfaces inside it. The outer surface has the
      % largest volume:
      [dummy,outer] = max(abs(Vsurface));
      outer_surface(outer(1)) = 1;
    end
    
    % Loop over the surfaces of this domain and reorder non-fixed triangles:
    for isurface = 1:nsurfaces
      % Triangles of this surface:
      TSind = surfaces(sstart(isurface):sstart(isurface+1)-1); % Index into Td!
      
      % Get all the non-fixed triangles of this surface:
      non_fixed = find(tdone(Tind(TSind)) == 0);
      if non_fixed
	ind = Tind(TSind(non_fixed));

	% Check that the volume of the surface is negatice in the case of
	% an outer surface and positive in the case of an inner surface:
	if ( outer_surface(isurface) & (Vsurface(isurface) > 0)) | ...
	   (~outer_surface(isurface) & (Vsurface(isurface) < 0))   
	  % Change the orientation of the non-fixed triangles:
	  T = reorder_triangle(T, ind);
	end

	% Fix the orientation of the non-fixed triangles:
	tdone(ind) = 1;
	
	% Fix the order of the two domains of the non-fixed triangles:
	d1ind = (tdomain(2,ind) == idomain);
	other_domain = zeros(size(ind));
	other_domain( d1ind) = tdomain(3,ind( d1ind));
	other_domain(~d1ind) = tdomain(2,ind(~d1ind));
	tdomain(2,ind) = idomain;
	tdomain(3,ind) = other_domain;
      end
    end    
  end
end
% ----------------------------------------------------------------------------
% Check that all the triangles have now been fixed:
ind = find(tdone == 0);
if ind
  error(sprintf('Orientation of triangle %d not fixed by the surfaces!',...
		ind(1)));
end

% Check thet tdomain(2,:) is smaller or equal than tdomain(3,:):
ind = find(tdomain(2,:) > tdomain(3,:));
if ind
  error(sprintf('Wrong orientation of tdomain of triangle %d!', ind(1)));
end
% ----------------------------------------------------------------------------
