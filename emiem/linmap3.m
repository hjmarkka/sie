function [P,W] = linmap3(p1,p2,p3,p4,x,w)
% function [P,W] = linmap3(p1,p2,p3,p4,x,w)
%
% Map 3D Gaussian integration points/weights into a tetrahedron (p1,p2,p3,p4).
%
% Input:
% - p1,p2,p3,p4: (3,1) coordinates of the vertices tetrahedron.
% - x: (2,np) Gaussian quadrature points for the refence tetrahedron.
% - w: (np,1) Gaussian quadrature weights for the reference tetrahedron.
%
% Output:
% - P: (3,np) integration points for tetrahedron (p1,p2,p3).
% - W: (np,1) integration weights for tetrahedron (p1,p2,p3).

P = p1(:,ones(1,size(x,2))) + [p2-p1 p3-p1 p4-p1] * x;

if nargout > 1
  % Scale weights with volume relation V/(1/6):
  W = 6 * tetra_volume(p1,p2,p3,p4) * w;
end

