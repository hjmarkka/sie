function [in,Pclosest] = inside_surface(P,E,T,Pi,tol)
% function [in,Pclosest] = inside_surface(P,E,T,Pi,[tol])
%
% Returns 1 if point Pi is inside the geometry surface,
%         0 if on the surface and 
%        -1 if outside.

nt = size(T,2);
n = size(Pi,2);
ind = 1:n;
in = -2*ones(1,n);

if nargin < 5
  tol = [];
end

% Select a random point outside:
zmax = max([P(3,:) Pi(3,:)]);
zmin = min([P(3,:) Pi(3,:)]);
hz = zmax-zmin;
if hz == 0
  zmax = zmax+1;
else
  zmax = zmax + hz;
end
Pout = [Pi(1:2,:); zmax(ones(1,n))];
Pray = Pout-Pi;
Dray = [1e300;0];
Dray = Dray(:,ones(1,n)); % [r side]: Distance from closest triangle and side.
Pclosest = NaN*ones(3,n);

% Go through all the triangles:
for i = 1:nt
  t = T(:,i);
  pind = t(4:6);
  pt = P(:,pind);

  % Find points on the triangle surface, inside the triangle:
  [in_s,N,h,H] = p_in_tri(Pi(:,ind),pt,tol);
  l = sum(in_s);
  if l > 0
    in(ind(in_s)) = zeros(1,l);   % On the surface.
    ind = ind(~in_s);
    h = h(~in_s);
    H = H(:,~in_s);
%    disp(sprintf('On the surface of triangle %d',i));
  end
  
  % Determine intersection points (Px) of Pray and triangle plane:
  hray = -N.'*Pray(:,ind);
  hind = find(hray ~= 0);
  if length(hind) > 0
    h = h(hind);
    hray = hray(hind);
    ind_h = ind(hind);
    Hray = N*hray;
    Sray = Pray(:,ind_h)+Hray;
    s = h./hray;
    S = s([1 1 1],:).*Sray;
    Px = Pi(:,ind_h)-H(:,hind)+S;
    
    % Check if these are inside the triangle:
    in_x = p_in_tri(Px,pt,tol);
    ind_x = ind_h(in_x);
    if length(ind_x) > 0
%      disp(sprintf('Ray intersects with triangle %d',i));
      % Check if the normal vector is in the same side with normal vector.
      inside = (h(in_x) < 0);
      
      % Keep in memory the closest intersection and on what side it was:
      r = vlen(Px(:,in_x)-Pi(:,ind_x));
      dind = (r < Dray(1,ind_x));
      if sum(dind) > 0
	Dray(:,ind_x(dind)) = [r(dind); inside(dind)];
	Pclosest(:,ind_x(dind)) = Px(:,dind);
      end
    end
  end
end

% Find all the points not on the surface:
ind = find(in == -2);

% Check if the closest intersection is on the same side with normal vector:
in(ind) = 2*Dray(2,ind) - 1;       % 1 if insize, -1 outside.
