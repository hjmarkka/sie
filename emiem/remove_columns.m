function [A,Amap1,Amap2] = remove_columns(ind,A)
% function [A,Amap1,Amap2] = remove_columns(ind,A)
%
% Removes columns A(:,ind) and returns also mappings between
% from the new into the old columns (Amap1) and from the old into the
% new columns (Amap2).

n = size(A,2);

% Remove columns:
A(:,ind) = [];

% Original columns indeces of the new matrix:
Amap1 = 1:n;
Amap1(ind) = [];

% New column indeces of the old matrix (or zeros):
Amap2 = zeros(1,n);
Amap2(Amap1) = 1:size(A,2);
