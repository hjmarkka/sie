function [point,edge,triangle,basis] = recalculate_geometry(point,edge,...
							    triangle,basis);
%function [point,edge,triangle,basis] = recalculate_geometry(point,edge,...
%							    triangle,basis);
% Recalculate edge lengths, triangle areas and centroids.

% Recalculate edge lengths:
if size(edge,2)
  edge = calc_edge(edge,point);
end

% Recalculate triangle areas and centroids:
if size(triangle,2)
  triangle = calc_triangle(triangle,point);
end
