function [N,vv] = tetrahedron_normals(pv)
% function [N,vv] = tetrahedron_normals(pv)
%
% Calculates the _OUTER_ surface normals of the 4 tetrahedron triangles.
%
% See make_tetra.m for the tetrahedron data types.
%
% Input: 
% - pv: Either: (3,4) Coortinates of the 4 tetrahedron vertices.
%       Or    : (3,6) Edge vectors of the 6 tetrahedron edges.
%
% Output:
% - N : (3,4) Outer surface normal vectors of the 4 tetrahedron triangles.
% - vv: (3,6) Edge vectors of the 6 tetrahedron edges.

if size(pv,2) == 4
  % Tetrahedron vertices:
  p1 = pv(:,1);
  p2 = pv(:,2);
  p3 = pv(:,3);
  p4 = pv(:,4);

  % Tetrahedron edge vectors:
  vv = [p2-p1 p3-p1 p4-p1 p3-p2 p4-p3 p4-p2];
else
  vv = pv;
end

% Tetrahedron triangle _outer_ normals:
n1 = cross3(vv(:,4),vv(:,6));  n1 = n1/vlen(n1);
n2 = cross3(vv(:,3),vv(:,2));  n2 = n2/vlen(n2);
n3 = cross3(vv(:,1),vv(:,3));  n3 = n3/vlen(n3);
n4 = cross3(vv(:,2),vv(:,1));  n4 = n4/vlen(n4);
N = [n1 n2 n3 n4];
