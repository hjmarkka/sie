function [pm,em,tm,bm] = mirror_geometry(origin,normal,...
					 point,edge,triangle,basis)
%function [pm,em,tm,bm] = mirror_geometry(origin,normal,
%                                         point,edge,triangle,basis)
% Create (plane) mirror of a geometry:
%
% origin:   (3,1)                 - Point in the mirror plane.
% normal:   (3,1)                 - (Outer) normal vector of the mirror plane.
% point,edge,triangle,basis       - See "help connect".

%disp('Mirroring geometry...');

np = size(point,2);

% Normalize normal vector:
normal = normal./(ones(3,1)*vlen(normal));

% Calculate distance from all the points into the mirror plane:
V = point - origin(:,ones(1,np));
N = normal(:,ones(1,np));
H = dot(V,N);

% Mirror points:
pm = point - 2*[H;H;H].*N;

% Recalculate triangle centroids (and areas):
tm = calc_triangle(triangle, pm);

% Edges and bases are the same:
em = edge;
bm = basis;