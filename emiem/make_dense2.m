function x = make_dense2(p1,p2,n,ends,s)
% function x = make_dense2(p1,p2,n,ends,s)
%
% Create N+1 points from P1 to P2 having 1/x^S or -ln(x) density in
% one or both ends.
%
% p1   = (n,1) vector, starting point.  n = 1,2,3,...   Default: 0
% p2   = (n,1) vector, ending point.                    Default: 1
% n    = integer, number of subsections.                Default: 10
% ends = scalar 1/2/3, starting/ending/both ends.       Default: 1
% s    = scalar, 0 <= s < 1 or 'ln', singularity type.  Default: 1/2
%        if >= 1, adds 1/s edging segment.
%
% The density (0 < s < 1) is calculated according to one dimensional integral
% of values of the equal size.
%
% Let I_i be the value of an integral:
%
%     I_i := \int_{x_{i-1}}^{x_i} f(x) dx
% 
% where x_j, j = 0,1,...,n are the division points which are set up so that
%
%     I_i == I_j
%
% for all i,j = 1,...,n, x0 = p1 and x_n = p2. f(x) is either ln(x) or 1/x^S.

% ---------------------------------------------------------------------------
% Default values:
if nargin < 5
  s = [];
  if nargin < 4
    ends = [];
    if nargin < 3
      n = [];
      if nargin < 2
	p2 = [];
	if nargin < 1
	  p1 = [];
	end
      end
    end
  end
end

if length(p1) == 0
  p1 = 0;
end
if length(p2) == 0
  p2 = 1;
end
if length(n) == 0
  n = 10;
end
if length(ends) == 0
  ends = 1;
end
if length(s) == 0
  s = 1/2;
end
% ---------------------------------------------------------------------------
if ends == 3
  n1 = fix(n/2);
  nn = n/2;
else
  n1 = n;
  nn = n;
end
% ---------------------------------------------------------------------------
if isstr(s) & (strcmp(s,'ln'))
  % I := \int_0^1 f(x) dx = \int_0^1 ln(x) dx = -1
  % I_1 = \int_0^{x_1} f(x) dx = \int_0^{x_1} ln(x) dx = x1*(ln(x1)-1)
  % I_1 = 1/n * I = -1/n  =>  x1 = fzero(x*(1-ln(x))-1/n, x)
  % I_2 = \int_{x_1}^{x_2} ln(x) dx = x2*(ln(x2)-1) - x1*(ln(x1)-1)
  %     = x2*(ln(x2)-1) -   I_1 == I_1  =>  x2 = fzero(x*(1-ln(x)) - 2/n, x)
  % I_3 = \int_{x_2}^{x_3} ln(x) dx = x3*(ln(x3)-1) - x2*(ln(x2)-1)
  %     = x3*(ln(x3)-1) - 2*I_1 == I_1  =>  x3 = fzero(x*(1-ln(x)) - 3/n, x)
  % => x_i = fzero(x*(1-ln(x)) - i/n, x), i = 0,1,...,n
  ss = zeros(1,n1+1);
  ss(1) = 0;
  xrange = [0+eps 1-eps];
  xtol   = eps;
  for i = 2:n1+1
    ss(i) = fzero(sprintf('x*(1-log(x))-%24.16e',(i-1)/nn),xrange,xtol);
  end
elseif (0 <= s) & (s < 1)
  % I_1 = \int_0^{x_1} f(x) dx = \int_0^{x_1} 1/x^s dx = x_1^(1-s)/(1-s)
  % I_2 = (x_2^(1-s)-x_1^(1-s))/(1-s) == I_1  =>  x_2^(1-s) = 2*x_1^(1-s)
  % I_3 = (x_3^(1-s)-x_2^(1-s))/(1-s) == I_1  =>  x_3^(1-s) = 3*x_1^(1-s)
  %  => x_i = i^(1/(1-s)) * x_1, i = 0,1,...,n
  % and I_1 == 1/n * I = 1/n * \int_0^1 f(x) dx = 1/n * 1/(1-s)
  %  => x_1 = (1/n)^(1/(1-s))
  e = 1/(1-s);
  ss = ((0:n1)/nn).^e;
elseif s >= 1
  if (s == 1) | (nn <= 1)
    ss = (0:n1)/nn;
  else
    ss = [0 1/s 1:n1-1]/(nn-1);
  end
else
  error('S should be either 0 <= S < 1 or S >= 1');
end
% ---------------------------------------------------------------------------
% Set up division ends:

if ends == 2
  ss = 1-fliplr(ss);
elseif ends == 3
  if mod(n,2) == 0
    ss = [ss/2 1-ss(n1:-1:1)/2];
  else
    ss = [ss/2 1-fliplr(ss)/2];
  end
elseif ends ~= 1
  error('ENDS should be 1, 2 or 3!');
end
% ---------------------------------------------------------------------------
% Scale and translate points:
x = p1*(1-ss) + p2*ss;

% Check ending points:
if any(x(:,1)-p1)
  x(:,1) = p1;
end
if any(x(:,end)-p2)
  x(:,end) = p2;
end
% ---------------------------------------------------------------------------
