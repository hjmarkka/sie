function [Nb1x, Nb1y, Nb1z]=efvie_duffy2(r,pk,p1,p2,p3,P0d,w0d,k,r0,eee)

%Mapping 
ee=abs(eee);[P1,w1] = linmap3matlab(pk,r,p1,p2,P0d,w0d);
[P2,w2] = linmap3matlab(pk,r,p2,p3,P0d,w0d);
[P3,w3] = linmap3matlab(pk,r,p3,p1,P0d,w0d);
  
pp1=[0,0,0]';
pp2=[1,0,0]';
pp3=[0,1,0]';
pp4=[0,0,1]';


pp=[pp1,pp2,pp3,pp4];
rpk=pp(:,ee);

%ind=[1,2,3,4,1,2,3,4];
%rpp1=pp(:,ind(ee+1));
%rpp2=pp(:,ind(ee+2));
%rpp3=pp(:,ind(ee+3));

if(ee==1);
[rr1,ww1] = linmap2(pp2,pp3,pp4,r0,1);
rpp1=pp(:,2);
rpp2=pp(:,3);
rpp3=pp(:,4);

elseif(ee==2);
[rr1,ww1] = linmap2(pp1,pp4,pp3,r0,1);
rpp1=pp(:,1);
rpp2=pp(:,4);
rpp3=pp(:,3);


elseif(ee==3);
[rr1,ww1] = linmap2(pp1,pp2,pp4,r0,1);
rpp1=pp(:,1);
rpp2=pp(:,2);
rpp3=pp(:,4);

else
[rr1,ww1] = linmap2(pp1,pp3,pp2,r0,1);
rpp1=pp(:,1);
rpp2=pp(:,3);
rpp3=pp(:,2);

end
%keyboard

%mapping into reference subtetra
[pr1,wr1]=linmap3matlab(rpk,rr1,rpp1,rpp2,P0d,w0d);
[pr2,wr2]=linmap3matlab(rpk,rr1,rpp2,rpp3,P0d,w0d);
[pr3,wr3]=linmap3matlab(rpk,rr1,rpp3,rpp1,P0d,w0d);

%Shapefunctions into reference tetra
N1=[1-pr1(1,:)-pr1(2,:)-pr1(3,:);pr1(1,:);pr1(2,:);pr1(3,:)];
N2=[1-pr2(1,:)-pr2(2,:)-pr2(3,:);pr2(1,:);pr2(2,:);pr2(3,:)];
N3=[1-pr3(1,:)-pr3(2,:)-pr3(3,:);pr3(1,:);pr3(2,:);pr3(3,:)];



%figure(2), plot_points(P1)
%hold on
%figure(2), plot_points(P2)
%    hold on
%figure(2), plot_points(P3)

%figure(3), plot_points(pr1)
%hold on
%figure(3), plot_points(pr2)
%    hold on
%figure(3), plot_points(pr3)
%hold on
%keyboard


%hold on

    %r0=[P0(:,t);0];


 
I1=zeros(3,4);
I2=zeros(3,4);
I3=zeros(3,4);
I4=zeros(3,4);

        
for p=1:length(w0d) 
           
    rp1 = pr1(:,p);
    R1 = norm(r-rp1);
     
    I1=I1 + exp(i*k*R1)/(4*pi*R1^3)*(i*k*R1-1)*(rp1-r)*w1(p) * N1(:,p)';
    

    rp2 = pr2(:,p);
    R2 = norm(r-rp2);
            
    I2=I2 + exp(i*k*R2)/(4*pi*R2^3)*(i*k*R2-1)*(rp2-r)*w2(p)* N2(:,p)';
            
    rp3 = pr3(:,p);
    R3 = norm(r-rp3);
            
    I3= I3 + exp(i*k*R3)/(4*pi*R3^3)*(i*k*R3-1)*(rp3-r)*w3(p)* N3(:,p)';

   
	
end

I=I1+I2+I3;

Nb1x=I(1,:)';
Nb1y=I(2,:)';
Nb1z=I(3,:)';


