function [f,mode] = box_resonances(a,b,c,epar,m,n,p);
% function [f,mode] = box_resonances(a,b,c,[epar,m,n,p]);
%
% Calculate resonance frequencies of rectangular cavity. The frequencies
% will be sorted.
%
% a,b,c - Dimensions of the box.
% epar  - Electromagnetic parameters [eps, mu]. Default: [eps0, mu0].
% m,n,p - Resonance modes. Default: m,n,p = [0 1].

% Default values:
if nargin < 4
  epar = [];
end
if length(epar) < 1
  eps = 8.85418782e-012;
else
  eps = epar(1);
end
if length(epar) < 2
  mu = 4*pi*1e-7;
else
  mu = epar(2);
end
gamma = eps;

if nargin < 5
  m = [0 1];
end
if nargin < 6
  n = m;
end
if nargin < 7
  p = n;
end

% Speed of light = 2.99792458e8 m/s
d = 1/(2*sqrt(gamma*mu));

nm = length(m);
nn = length(n);
np = length(p);
f = zeros(nm*nn*np,1);
if nargout > 1
  mode = zeros(nm*nn*np,3);
end
l = 0;
for k = 1:np
  for j = 1:nn
    for i = 1:nm
      l = l + 1;
      f(l) = d*sqrt(m(i)^2/a^2 + n(j)^2/b^2 + p(k)^2/c^2);
      if nargout > 1
	mode(l,:) = [m(i) n(j) p(k)];
      end
    end
  end
end

[f,ind] = sort(f);
if nargout > 1
  mode = mode(ind,:);
end
