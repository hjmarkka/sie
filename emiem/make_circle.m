function [point,edge,triangle,basis] = make_circle(p,r,n,m,m4,half,angle)
% function [point,edge,triangle,basis] = make_circle([p,r,n,m,m4,half,angle])
%
% Create circle into point p having radius r splitted into n times m pieces.
%
%  p - point (N,1) or [p,nv] where nv is plane normal.     Default [0;0;0].   
%  r - radius, XY-plane: (1,m), general: (N,m).            Default 1.
%  n  - Number of angle subdivisions (1,1).                Default 6.
%  m  - Number of radius subdivisions.                     Default 1.
%  m4 - Number of circle rectangle subdivisions (2 ar 4).  Default 2.
%  half - If 1, creates also half basis on the boundaries. Default 0.
%  angle - Angle of the disc.                              Default 2*pi.
%  
% Usage:
%  make_circle(p,r,n,m)   - Circle having origin p and radius r in XY plane.
%  make_circle(p,[r1 r2],n,m)      - Radius from r1 to r2. Hole of radius r1.
%  make_circle([p,nv],r,n,m)       - Normal vector nv.
%  make_circle(p,[r1,...,rm],n)    - Radius from r1 ...  r2.
%  make_circle(p,r,[],[],[],[],pi) - Half disc.

% Default values:
if nargin < 7
  angle = [];
  if nargin < 6
    half = [];
    if nargin < 5
      m4 = [];
      if nargin < 4
	m = [];
	if nargin < 3
	  n = [];
	  if nargin < 2
	    r = [];
	    if nargin < 1
	      p = [];
	    end
	  end
	end
      end
    end
  end
end
  
if length(p)     == 0, p = [0;0;0] ; end
if length(r)     == 0, r     = 1   ; end
if length(n)     == 0, n     = 6   ; end
if length(m)     == 0, m     = 1   ; end
if length(m4)    == 0, m4    = 2   ; end
if length(half)  == 0, half  = 0   ; end
if length(angle) == 0, angle = 2*pi; end

% Check dimensions:
[N,M] = size(p);        % Dimension.
if (N < 2) & (N > 3)
  error('N should be either 2 or 3!');
end

% Check rectangle subdivisions:
if (m4 ~= 2) & (m4 ~= 4)
  warning('m4 should be either 2 or 4! Using 2');
  m4 = 2;
end

% Plane normal vector:
if M == 1
  nv = [0;0;1];
else
  nv = p(:,2);
  nv = nv/norm(nv);
end
% 2D -> 3D:
p0 = zeros(3,1);
p0(1:N) = p(:,1);

% Radix rs:
[nr,mr] = size(r);
if mr < 3
  if mr == 1
    if nr == 1
      r1 = 0;
    else
      r1 = [0;0;0];
    end
    r2 = r;
  else
    r1 = r(:,1);
    r2 = r(:,2);
  end
  if m == 1
    rs = [r1 r2];
  else
    rs = (r2-r1)*((0:m)/m) + r1*ones(1,m+1);
  end
else
  rs = r;
end

m = size(rs,2);
om = ones(1,m);
o3 = ones(3,1);
p0m = p0*om;
nvm = nv*om;

if nr == 1
  % Radix -> points:
  R = [rs;zeros(2,m)];
else
  % 2D -> 3D:
  R = zeros(3,m);
  R(1:nr,:) = rs;
end

ps = p0m + (o3*dot(R,nvm)).*nvm;

% Create disc:
[point,edge,triangle,basis] = calc_disc(ps,R,nvm,n,m4,half,angle);

%plot_triangles(point,edge,triangle,basis); axis equal; pause
