function h = fill_triangles(P,E,T,I,what,Tind)
% function h = fill_triangles(P,E,T,[I,what,Tind])
%
% Plot 3D triangled filled (fill3) with values I.
%
% Output:
%  h - Handle into fill3 graphics.
%
% Input:
%  P,E,T - Geometry, see "help connect".
%  I     - Colors used for triangles. See variable "what" below. Default: 1.
%  what  - 0: Constant color for all triangles.           I is (1,1) scalar.
%          1: Piecewise constant colors for triangles.    I is (1,nt) vector.
%          2: Piecewise linear colors.                    I is (1,np) vector.
%          3: Piecewise linear colors for every triangle. I is (1,3*nt) vector.
%          Default: 0 if I is (1,1) and 2 otherwise.
%  Tind  - Triangle index range. Default: 1:nt.

np = size(P,2);
nt = size(T,2);

% Default values:
if (nargin < 4) | (length(I) == 0)
  I = 1;
end
if (nargin < 5) | (length(what) == 0)
  if length(I) == 1
    what = 0;
  elseif (length(I) ==   nt) & (nt ~= np)
    what = 1;
  elseif (length(I) == 3*nt) & (3*nt ~= np)
    what = 3;
  else
    what = 2;
  end
end
if nargin < 6
  Tind = 1:nt;
end

% Get triangle end points for fill3:
l = length(Tind);
PT = T(4:6,Tind);

X = zeros(3,l);
Y = zeros(3,l);
Z = zeros(3,l);
C = zeros(3,l);
X(1,:) = P(1,PT(1,:));
X(2,:) = P(1,PT(2,:));
X(3,:) = P(1,PT(3,:));
Y(1,:) = P(2,PT(1,:));
Y(2,:) = P(2,PT(2,:));
Y(3,:) = P(2,PT(3,:));
Z(1,:) = P(3,PT(1,:));
Z(2,:) = P(3,PT(2,:));
Z(3,:) = P(3,PT(3,:));
if what == 0
  if isstr(I)
    I = I(1,1);
  else
    I = I(1,1)*ones(1,nt);
  end
end

if (what == 0) | (what == 1)
  % Piecewise constant:
  if isstr(I)
    Cc = I(1,1);
  else
    Cc(1,:) = I(Tind);
    Cc(2,:) = I(Tind);
    Cc(3,:) = I(Tind);
  end
  if what == 0
    h0 = fill3(X,Y,Z,Cc);
  else
    h0 = fill3(X,Y,Z,Cc,'EdgeColor','none');
  end
elseif what == 2
  % Interpolated values in triangle vertices:
  Cv = zeros(3,size(PT,2));
  Cv(1,:) = I(PT(1,:));
  Cv(2,:) = I(PT(2,:));
  Cv(3,:) = I(PT(3,:));
  h0 = fill3(X,Y,Z,Cv,'EdgeColor','none');
elseif what == 3
  % Exact values in triangle vertices:
  ind = 1:3:3*nt;
  Cv = [I(ind(Tind)); I(ind(Tind)+1); I(ind(Tind)+2)];
  h0 = fill3(X,Y,Z,Cv,'EdgeColor','none');
end

if nargout > 0
  h = h0;
end
