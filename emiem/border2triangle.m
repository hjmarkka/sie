function [P,E,T,B] = border2triangle(P,grid,bsize,varargin)
% function [point,edge,triangle,basis] = border2triangle(point,grid,bsize,...
%                                                 border1,border2,...)
%
% Generates triangles for given borders (BORDER1,...),
% grid points (GRID) and border subdivision size (BSIZE).
%
% Input:
%   P    - (3,np)-matrix, points.
%   grid - [d 0 0], [p1 p2 0], [p1 p2 p3], [p dx 0], [p dx dy] or [dx dy 0]:
%          scalar ([d 0 0])
%              preferred size of triangles, free triangle shape.
%              if <0, no new points will be added.
%          2-vector ([p1 p2 0], [p dx 0] or [dx dy 0]),
%              2 point indeces, starting grid for equilateral triangles
%              or a starting point index and size for an equilateral triangle
%              or 2 dimensions of rectangular grid starting from point P(1).
%          3-vector ([p1 p2 p3] or [p dx dy])
%              3 point indeces of starting grid triangle
%              or starting point index and 2 dimensions of rectangular grid.
%   bsize -  nb-vector, >0 preferred size for border subdivisions,
%                       0 automatic size, <0 no points added to the borders.
%           (max(n#)-1,nb)-matrix, same for individual border edges.
%   border# - (1,n#)-vector, point indeces of the border.
%             If all indeces are negative, the border is assumed to be
%             inner border. Otherwise outer border is assumed and
%             border#(1) should be border#(n#)!
%
% Examples:
% =========
%
% Free triangle shape:
% --------------------
%    p1 = [0;0]; p2 = [1;0]; p3 = [1;1]; p4 = [0;1];  % Unit rectangle.
%    Ps = [p1 p2 p3 p4];                              % Border points.
%    Bi = [1 2 3 4 1];                                % Border point indeces.
% 
%  No points added:
%    [P,E,T,B] = border2triangle(Ps, -1, -1, Bi);
%
%  Free triangle shape, no points added to the border:
%    [P,E,T,B] = border2triangle(Ps, 0, -1, Bi);
%
%  Free triangle shape:
%    [P,E,T,B] = border2triangle(Ps, 0, 0, Bi);
%
%  Free triangle shape of size 0.5, automatic size on the border:
%    [P,E,T,B] = border2triangle(Ps, 0.5, 0, Bi);
%
%  Free triangle shape of size 0.5, size 0.25 on the border:
%    [P,E,T,B] = border2triangle(Ps, 0.5, 0.25, Bi);
%
% Equilateral triangles:
% ----------------------
% Equilateral triangle grid starting from point 1 and of x dimension 0.1:
%    G1 = [1 0.1];
%
% Equilateral triangles of size 0.1 starting point 1, no points to the border:
%    [P,E,T,B] = border2triangle(Ps, G1, -1, Bi);
%
% Equilateral triangles of size 0.1 starting point 1, automatic border size:
%    [P,E,T,B] = border2triangle(Ps, G1, 0, Bi);
%
% Equilateral triangles of size 0.1 starting point 1, size 0.5 on the border:
%    [P,E,T,B] = border2triangle(Ps, G1, 0.5, Bi);
%
% The same examples with an additional 2nd grid point p5:
%    p5 = [0.1;0];                                 % One additional grid point.
%    Ps2 = [p1 p2 p3 p4 p5];                       % Border points.
%    Bi2 = [1 5 2 3 4 1];                          % Border point indeces.
%    G2 = [1 5];                                   % Grid point indeces.
%
% Equilateral triangles of size 0.1 (see p5), no points added to the border:
%    [P,E,T,B] = border2triangle(Ps2, G2, -1, Bi2);
%
% Equilateral triangles of size 0.1 (see p5), automatic size on the border:
%    [P,E,T,B] = border2triangle(Ps2, G2, 0, Bi2);
%
% Equilateral triangles of size 0.1 (see p5), size 0.5 on the border:
%    [P,E,T,B] = border2triangle(Ps2, G2, 0.5, Bi2);
%
% User given rectangular triangle shape:
% --------------------------------------
% Rectangular triangle grid starting from point 1 and of dimension (0.1x0.2):
%    G3 = [1 0.1 0.2];
%
% Rectangular triangles of size (0.1x0.2), no points added to the border:
%    [P,E,T,B] = border2triangle(Ps, G3, -1, Bi);
%
% Rectangular triangles of size (0.1x0.2), automatic on the border:
%    [P,E,T,B] = border2triangle(Ps, G3, 0, Bi);
%
% Rectangular triangles of size (0.1x0.2), size 0.5 on the border:
%    [P,E,T,B] = border2triangle(Ps, G3, 0.5, Bi);
%
% The same examples with an additional 3rd grid point p6: 
%    p6 = [0.1;0.2];                               % One additional grid point.
%    Ps3 = [p1 p2 p3 p4 p5 p6];                    % Border points.
%    Bi2 = [1 5 2 3 4 1];                          % Border point indeces.
%    G4 = [1 5 6];                                 % Grid point indeces.
%
% Rectangular triangles of size (0.1x0.2) (see p5-6), no points to the border:
%    [P,E,T,B] = border2triangle(Ps3, G4, -1, Bi2);
%
% Rectangular triangles of size (0.1x0.2) (see p5-6), automatic on the border:
%    [P,E,T,B] = border2triangle(Ps3, G4, 0, Bi2);
%
% Rectangular triangles of size (0.1x0.2) (see p5-6), size 0.5 on the border:
%    [P,E,T,B] = border2triangle(Ps3, G4, 0.5, Bi2);
%
% More complex examples:
% ---------------------
% A hole inside a unit rectangle with automatic border size on the outer
% border and 0.25 border size on the inner border:
%    p7 = [0.25;0.25]; p8 = [0.75;0.25]; p9 = [0.75;0.75]; p10 = [0.25;0.75];
%    Ps4 = [p1 p2 p3 p4 p5 p6 p7 p8 p9 p10];
%    B1 = [1 5 2 3 4 1];
%    B2 = [7 8 9 10 7];
%    G5 = [1 5 6];
%    [P,E,T,B] = border2triangle(Ps4, G5, [0, 0.25], B1, B2);

% Check borders:
if any(grid > size(P,2))
  error('Grid points not among P!');
end
if length(grid) < 3
  grid(length(grid)+1:3) = 0;
end

% Borders:
for i = 1:length(varargin)
  eval(sprintf('border%d = varargin{i};', i));
end

% Calculate plane normal vector: search 2 nonparaller vectors
if size(P,1) > 2         % No need to do for 2D case!
  n = 0;
  v1 = P(:,abs(border1(2)))-P(:,abs(border1(1)));
  v1 = v1/vlen(v1);
  for i = 2:length(border1)-1
    v2 = P(:,abs(border1(i+1)))-P(:,abs(border1(i)));
    v2 = v2/vlen(v2);
    n = cross(v1,v2);
    if vlen(n) > 1e-10
      break;
    end
  end
  if norm(n) == 0
    error('Cannot calculate plane normal vector!');
  end
  n = n/vlen(n);

  nz = [0;0;1];
  v = cross(n,nz);
  if vlen(v) ~= 0
    % Rotate points into XY-axis
    a = vangle(n,nz);
    [P,E,T,B] = rotate_geometry(v,[0;0;0],a,P,[],[],[]);
    t = P(:,1);
    [P,E,T,B] = translate_geometry(-t,P,[],[],[]);
  end
else
  v = 0;
  P = [P; zeros(1,size(P,2))];
end  

nb = nargin-3;
nbs = zeros(1,nb);
for i = 1:nb
  eval(sprintf('nbs(i) = length(border%d);', i));
end

% Check for empty borders:
if any(nbs == 0)
  nnbs = sum(nbs ~= 0);   % Non-empty borders.
  if sum(nbs(1:nnbs) ~= 0) == nnbs
    % The rest of the borders are empty. Skip them:
    nb = nnbs;
    nbs = nbs(1:nnbs);
  else
    error('Empty borders!');
  end
end

% Edge sizes:
m = max(nbs)-1;
[mf,nf] = size(bsize);
if mf*nf == 0
  bsize = zeros(m,nb);  % Default: allow points to be added everywhere.
elseif min(mf,nf) == 1
  if mf*nf == 1
    bsize = bsize*ones(m,nb);        % Scalar: copy value.
  elseif (nb == 1) & (mf*nf == length(border1)-1)
    bsize = bsize(:);
  elseif mf*nf ~= nb
    error(sprintf('Length of BSIZE should be %d not %d!', nb, mf*nf));
  else
    bsize = ones(m,1)*reshape(bsize,1,nb);
  end
else
  if (mf ~= m) | (nf ~= nb)
    error(sprintf('Size of BSIZE should be (%d,%d) not (%d,%d)!',m,nb,mf,nf));
  end
end

% Check if mex file generator exists:
if exist('generator') == 3
  opts = 'P';
  s = [];
  for i = 1:nb
    opts = [opts sprintf(',border%d',i)];
    eval(sprintf('s%d = bsize(1:nbs(%d)-1,i);',i,i));
    opts = [opts sprintf(',s%d',i)];
  end
  if (nargin < 2) | (length(grid) == 0)
    grid = [0 0 0];
  end
  opts = [opts ',grid'];

  % Call mex function:
  eval(sprintf('[P,E,T,B] = generator(%s);', opts));
else
  ver = version;
  if ver(1) >= '5'
    save generator -v4 P
    s = [];
    for i = 1:nb
      eval(sprintf('save generator -v4 -append border%d',i));
      eval(sprintf('s%d = bsize(1:nbs(%d)-1,i);',i,i));
      eval(sprintf('save generator -v4 -append s%d',i));
    end
    if (nargin > 1) & (length(grid) > 0)
      save generator -v4 -append grid
    end
  else
    opts = 'P';
    s = [];
    for i = 1:nb
      opts = [opts sprintf(' border%d',i)];
      eval(sprintf('s%d = bsize(1:nbs(%d)-1,i);',i,i));
      opts = [opts sprintf(' s%d',i)];
    end
    if (nargin > 1) & (length(grid) > 0)
      opts = [opts ' grid'];
    end
    eval(sprintf('save generator %s', opts));
  end

  !rm -f generator-tulos.mat
  !generator-i386-linux
  if exist('generator-tulos.mat')
    load generator-tulos
  else
    P = []; E = []; T = []; B = [];
    return
  end
end

if vlen(v) ~= 0
  % Rotate and translate back:
  [P,E,T,B] = translate_geometry(t,P,E,T,B);
  [P,E,T,B] = rotate_geometry(v,[0;0;0],-a,P,E,T,B);
end
