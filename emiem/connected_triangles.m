function [Tind,Eind,Sind] = connected_triangles(T,T0)
% function [Tind,Eind,Sind] = connected_triangles(T,T0)
%
% Returns indeces of those triangles T that are neighbours of triangle T0.
%
% Output:
%  Tind - Indeces of neighbour indeces. (1,n) vector.
%  Eind - Order number of common edges for neighbour triangles and T0.
%         (2,n) matrix.
%  Sind - Index of T0 in T.
%
% Input:
%  T  - Triangles. (3,nt) matrix of edge indeces.
%  T0 - Triangle. (3,1) vector of the 3 edge indeces.

T = T(1:3,:);

e1 = (T == T0(1));
e2 = (T == T0(2));
e3 = (T == T0(3));
e = e1 | e2 | e3;

% Dont want to return triangle T0 itself:
Sind = find(all(e));
if length(Sind) > 0
  z0 = zeros(size(T(:,1)));
  e(:,Sind) = z0;
  if nargout > 1
    e1(:,Sind) = z0;
    e2(:,Sind) = z0;
    e3(:,Sind) = z0;
  end
end

if nargout < 2
  Tind = find(any(e));
else

  [eind1,ind1] = find(e1);
  [eind2,ind2] = find(e2);
  [eind3,ind3] = find(e3);
  
%  Tind = [ind1; ind2; ind3].';
%  Eind = [eind1   ones(size(eind1)); ...
%          eind2 2*ones(size(eind2)); ...
%          eind3 3*ones(size(eind3))].';
  Tind = [ind1 ind2 ind3];
  Eind = [eind1             eind2               eind3; ...
	  ones(size(eind1)) 2*ones(size(eind2)) 3*ones(size(eind3))];
end
