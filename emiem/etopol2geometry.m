function [P,E,T,B] = etopol2geometry(coord, etopol, half)
% function [P,E,T,B] = etopol2geometry(coord, etopol, half)
%
% Converts "etopol" style system into "geometry" system.
% If HALF is non zero, creates also half bases to the borders.

% Default values:
if nargin < 3
  half = 0;
end

% Points:
P = coord.';
if size(P,1) == 2
  P(3,:) = zeros(1,size(P,2));
end

% Triangles:
nt = size(etopol,1);
T = zeros(10,nt);
T(4:6,:) = etopol.';            % Triangle points.
T = calc_triangle(T,P);         % Triangle areas and centroids.

% Edges and bases:
E = [];
ETfirst = [];
B = [];
pind = [1 2 3 1];
ind = [];
for i = 1:nt
  for j = 1:3
    e = etopol(i,pind(j:j+1));  % Edge's two points.
    if length(E) > 0
      P1 = E(1,:);
      P2 = E(2,:);
      ind = find(((P1 == e(1))&(P2 == e(2))) | ((P1 == e(2))&(P2 == e(1))));
    end
    if length(ind) == 0
      % New edge:
      E = [E [e vlen(P(:,e(1))-P(:,e(2)))].'];
      ind = size(E,2);
      ETfirst = [ETfirst i];    % Edge's first triangle.
      if half
	B = [B [ind; i; 0]];    % A half basis.
      end
    else
      if half
	% Find half basis:
	hind = find((B(1,:) == ind) & (B(3,:) == 0));
	if length(hind) > 0
	  B(3,hind(1)) = i;                % Assign the second triangle.
	else
	  B = [B [ind; ETfirst(ind); i]];  % Edge and the two triangles.
	end
      else
	B = [B [ind; ETfirst(ind); i]];    % Edge and the two triangles.
      end
    end
    T(j,i) = ind;               % Triangle's j:th edge.
  end
end
