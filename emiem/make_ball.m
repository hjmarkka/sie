function [point,edge,triangle,basis] = make_ball(p,r,n,m,m4,half,angle1,angle2)
% function [point,edge,triangle,basis] = make_ball([p,r,n,m,m4,...
%                                                          half,angle1,angle2])
%
% Create circle into point p having radius r splitted into n times m pieces.
%
%  p - point (N,1) or [p,pv1,pv2] where pvi are plane vectors. Default [0;0;0].
%                                  Default plane: pv1 = [1;0;0], pv2 = [0;1;0].
%  r - radius.                                                 Default 1.
%  n - Number of angle subdivisions.                           Default 6.
%  m - Number of angle subdivisions.                           Default 6.
%  m4 - Number of circle rectangle subdivisions (2 ar 4).      Default 2.
%  half - If 1, creates also half basis on the boundaries.     Default 0.
%  angle1 - Angle of the ball (or [start_angle end_angle]).    Default pi.
%  angle2 - Angle of the ball.                                 Default 2*pi.
%  
% Usage:
%  make_ball(p,r,n,m)            - Ball having origin p and radius r.
%  make_ball([p,pv1,pv2],r,n,m)  - Oriented using plane (pv1,pv2).
%  make_ball(p,r,[],[],[],[],pi) - Half ball.

% Default values:
if nargin < 8
  angle2 = [];
  if nargin < 7
    angle1 = [];
    if nargin < 6
      half = [];
      if nargin < 5
	m4 = [];
	if nargin < 4
	  m = [];
	  if nargin < 3
	    n = [];
	    if nargin < 2
	      r = [];
	      if nargin < 1
		p = [];
	      end
	    end
	  end
	end
      end
    end
  end
end

if length(p)      == 0, p   = [0;0;0]; end
if length(r)      == 0, r      = 1   ; end
if length(n)      == 0, n      = 6   ; end
if length(m)      == 0, m      = 6   ; end
if length(m4)     == 0, m4     = 2   ; end
if length(half)   == 0, half   = 0   ; end
if length(angle1) == 0, angle1 = pi  ; end
if length(angle2) == 0, angle2 = 2*pi; end

% Check dimensions:
[N,M] = size(p);        % Dimension.
if (N < 2) & (N > 3)
  error('N should be either 2 or 3!');
end

% Check rectangle subdivisions:
if (m4 ~= 2) & (m4 ~= 4)
  warning('m4 should be either 2 or 4! Using 2');
  m4 = 2;
end

% Plane normal vector:
if M == 1
  pv1 = [1;0;0];
  pv2 = [0;1;0];
else
  pv1 = p(:,2);
  pv2 = p(:,3);
  pv1 = pv1/norm(pv1);
  pv2 = pv2/norm(pv2);
end
P = p(:,1);

% Plane normal vector:
nv = cross(pv1,pv2);
nv = nv/norm(nv);

% Angles:
if any(abs(angle1)) > pi
  error('Maximum angle1 is pi!');
end
if length(angle1) == 1
  angle1(2) = 0;
end
angle1 = angle1-pi/2;
angle = fliplr((angle1(2)-angle1(1))*(0:n-1)/(n-1) + angle1(1));

% Radix:
X = r*cos(angle);
Z = r*sin(angle);
R = pv1*X;
PP = P*ones(1,n) - nv*Z;

% Create ball:
[point,edge,triangle,basis] = calc_disc(PP,R,nv*ones(1,n),m,m4,half,angle2);

%plot_triangles(point,edge,triangle,basis); axis equal; pause
