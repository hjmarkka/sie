function edge = calc_edge(edge,point,pa,pb)
% function edge = calc_edge(edge,point [,pa,pb])
%
% Calculates edge lenght. Also works with multiple edges.

if nargin < 3
  pa = edge(1,:);
  pb = edge(2,:);
end

edge(3,:) = vlen(point(:,pa)-point(:,pb));
