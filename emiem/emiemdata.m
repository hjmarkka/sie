% emiemdata - ElectroMagnetic Integral Equation Method data structures
%
% See also:
%    emiem               - Documentation of the utilities and routines.
%    emiemsolvers        - Documentation of the EM solvers.
% ----------------------------------------------------------------------------
% Geometry:
%
%   Point, P, (3,np) matrix of coordinates of points:
%     1: x           - X coordinate of the point.
%     2: y           - Y coordinate of the point.
%     3: z           - Z coordinate of the point.
%
%   Edge, E, (3, ne) matrix of edge vertices and lengths:
%     1: point1      - Index of the first point.
%     2: point2      - Index of the second point.
%     3: length      - Length of the edge.
%
%   Triangle, T, (10,nt) matrix of triangle edges, points, areas and centroids:
%     1: edge1       - Index of the first edge.              p3 +
%     2: edge2       - Index of the second edge.                |\
%     3: edge3       - Index of the third edge.                 | \
%     4: point1      - Index of the first point.                |  \ e2
%     5: point2      - Index of the second point.            e3 |   \
%     6: point3      - Index of the third point.                |    \
%     7: area        - Area of the triangle.                    |     \ 
%     8: centroid x  - X coordinate of the centroid.         p1 +------+ p2
%     9: centroid y  - Y coordinate of the centroid.               e1
%    10: centroid z  - Z coordinate of the centroid.
%
%   Tetrahedron, V, (18,nv) matrix of tetrahedron triangles, edges, points,
%                   volumes and centroids:
%    1-4: triangles  - 4 triangles of the tetrahedron. Triangle j is opposite
%                      to point j. Negative, if the triangle normal is not
%                      the _outer_ normal of the tetrahedron!
%    5-10: edges     - 6 edges of the tetrahedron. The three first edges are
%                      the "spanning" edges of the tetrahedron.
%    11-14: points   - 4 points of the tetrahedron (p1,p2,p3,p4).
%    15: volume      - Volume of the tetrahedron.
%    16-18: centroid - Coordinates of the centroid of the tetrahedron.
%                    + p4               Edges: (orientation not fixed here!)
%                   /|\                   e1 = between points p1 and p2
%                  / | \                  e2 =                p1 and p3
%                 /  |  \e6               e3 =                p1 and p4
%                /   |   \                e4 =                p2 and p3
%            e5 /    |e3  \               e5 =                p3 and p4
%              /     |  . / p2            e6 =                p4 and p2
%             /      |   /                   
%            /  e4.  |  /e1             Triangles: oppposite to the point
%           /  .     | /                  t1 = back  (p2,p3,p4)  (Note: _outer_
%          /.        |/                   t2 = left  (p1,p4,p3)   triangle
%      p3 +----------+ p1                 t3 = right (p1,p2,p4)   normal!)
%               e2                        t4 = below (p1,p3,p2)
% ----------------------------------------------------------------------------
% Domains and electromagnetic/material parameters:
%
%   fpar, (1,3) vector:
%     1: f           - Frequency. Default: 1 GHz
%     2: w           - Angular frequency (2*pi*f).
%     3: gtype       - Geometry type: 1 (Microstrip), 2 (Stripline) or
%                                     3 (Open). Default: 1
%
%   spar, substrate parameters, (9,ns) matrix:
%     1: h           - Substrate height.                           Default: Inf
%     2: epsr        - Relative permittivity.                      Default: 1
%     3: mur         - Relative permeability.                      Default: 1
%     4: tand        - Tangential delta (sigma/(w*epsilon)).       Default: 0
%     5: epsilon     - Permittivity (eps0*epsr).
%     6: gamma       - Complex permittivity
%                           epsilon*(1+i*tand) = epsilon+i*sigma/w.
%     7: mu          - Permeability (mu0*mur).
%     8: sigma       - Conductivity (w*epsilon*tand).
%     9: k           - Wave number (w*sqrt(mu*gamma)).
%
%   tdomain, triangle properties, (3,nt) matrix:
%     1: ismetal    - 0, if triangle is non-metallic,
%                     1, if triangle is perfectly electric conducting (PEC),
%                     2, if triangle is perfectly magnetic conducting (PMC),
%                     3, if triangle is PEC and PMC.
%     2: domain1    - The first domain of the triangle. The triangle normal
%                     is the _inner_ surface normal of this domain.
%     3: domain2    - The second domain of the triangle.
%
%   vdomain, tetrahedron properties, (2,nv) matrix: (PROPOSAL!)
%     1: vdomain    - Index of the "virtual" domain of the tetrahedron.
%     2: sdomain    - Index of the material parameters domain (spar).
%
% ----------------------------------------------------------------------------
% Basis functions:
%
%   RWG basis, B, (3,nb) matrix of RWG basis edges and the two triangles:
%     1: edge        - Index of the RWG basis edge.
%     2: triangle1   - The first triangle. Negative if the triangle normal
%                      is not the INNER normal of the domain of this basis.
%     3: triangle2   - The second triangle. Negative if the triangle normal
%                      is not the INNER normal of the domain of this basis.
%
%   Surface currents, J and M, (3,n) matrices of  [basis; domain; unknown]:
%     1: basis       - Index of RWG basis (B).
%     2: domain      - Index of the domain of the current.
%     3: unknown     - Index of unknown (J or M).
% ----------------------------------------------------------------------------
% Sources:
%
%   source_struct = {source_domain, source_fun, source_args, singular_args}
%
%     source_domain, scalar integer:
%                    - Index of the domain of the source.
%
%     source_fun, string:
%                    - Function name giving electric and/or magnetic
%                      field values at points X:
%                        [Ef,Hf] = FUN(x,[doE doH],fpar,spar,level,p1,p2,...)
%
%     source_args, cell vector:
%                    - User given parameters for SOURCE_FUN.
%
%     singular_args, cell vector:
%                    - Additional arguments for singularity handling.
% ----------------------------------------------------------------------------
