function [v,l,s,nv,A,h,m] = triangle_calculus(p1,p2,p3);
% function [v,l,s,nv,A,h,m] = triangle_calculus(p1,p2,p3);

%        p3
%         x
%        / \
%   e2  /   \ e1
%      /     \
%     /       \
%    x---------x
%   p1    e3   p2

ps = [p1 p2 p3];

% Edge vectors:
v = ps(:,[3 1 2])-ps(:,[2 3 1]);

% Edge lengths:
l = vlen(v);

% Edge direction vectors:
s = v./[l;l;l];

% Triangle normal vector:
nv = cross3(v(:,3),-v(:,2));
A2 = vlen(nv);
nv = nv/A2;

% Triangle area:
A = A2/2;

% Triangle heights:
h = [A2 A2 A2]./l;

% Edge normal vectors:
m = cross3(s,[nv nv nv]);
