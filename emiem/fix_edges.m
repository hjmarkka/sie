function basis = fix_edges(point,edge,triangle,half_basis)
% function basis = fix_edges(point,edge,triangle,half_basis)
%
% Assings triangles into edges and collects basis edges.

basis = [];
nedge = size(edge,2);

% Create n-1 bases for edges having n connected triangles:
for i = 1:nedge
  ind = find((triangle(1,:)==i) | (triangle(2,:)==i) | (triangle(3,:)==i));
  n = length(ind);
  % If half_basis wanted and only one triangle, add a half basis:
  if n > 1
    for j = 2:n
      basis = [basis, [i; ind(1); ind(j)]];
    end
  else
    if half_basis
      basis = [basis, [i; ind; 0]];     % The second triangle index zero!!!
    end
  end
end

%plot_geometry(point,edge,triangle,basis,1); pause
