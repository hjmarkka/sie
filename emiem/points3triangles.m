function [coord,etopol] = points3triangles(tp)
% function [coord,etopol] = points3triangles(tp)
%
% Converts triangles listed with point coordinates into points (coord)
% and triangles listed using point indexes of the vertices (etopol).

% Number of triangles:
n = size(tp,1);
if mod(n,3) ~= 0
  error('Number of triangle points should be 3*m!');
end
nt = n/3;

% Find same points with boundingboxes:
%same = near_boundingboxes(tp.', [], tp.', [], 0);
%same = tril(same);

% Points:
coord = zeros(n,3);
pmap = zeros(1,n);
np = 0;
%[psame,dummy] = find(same);
%prange = [1 1+cumsum(sum(same))];
X = tp(:,1);
Y = tp(:,2);
Z = tp(:,3);
for i = 1:n
  if pmap(i) == 0
    %ind = find(same(:,i));
    %ind = psame(prange(i):prange(i+1)-1);
    ind = find((X == X(i)) & (Y == Y(i)) & (Z == Z(i)));
    np = np + 1;
    pmap(ind) = np;
    coord(np,:) = tp(i,:);
  end
end

% Remove rest of the points:
coord = coord(1:np,:);

% Triangles:
etopol = reshape(pmap,3,nt).';
