function r = vlen(v)
% function r = vlen(v)
% Calculates vector length. V should contain each point in a column.

r = sqrt(sum(v.^2));



