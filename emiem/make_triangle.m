function [point,edge,triangle,basis] = make_triangle(p1,p2,p3,n1,n2,n3,...
    points1,points2,points3,half)
% function [point,edge,triangle,basis] = make_triangle(p1,p2,p3,...
%                                  [n1,n2,n3,points1,points2,points3,half])
%
% Split triangle into subtriangles dividing triangle edges into n1, n2 and
% n3 parts.
%
%                   + p3
%                  /|
%                 / |
%                /  |
%               /   |
%              /    |
%        n3   /     |
%            /      | n2
%           /       |
%          /        |
%         /         |
%        /          |
%       /           |
%   p1 +------------+ p2
%           n1
%
%  p1, p2, p3 - (N,1) point (col) vectors. N is 2 or 3.
%  n1 - Number of points between p1:p2. If 0, use points points1. Default 1.
%  n2 - Number of points between p2:p3. If 0, use points points2. Default 1.
%  n3 - Number of points between p3:p1. If 0, use points points3. Default 1.
%  points1 - (N,n1) matrix of point (col) vectors. N is 2 or 3.
%  points2 - (N,n2) matrix of point (col) vectors. N is 2 or 3.
%  points3 - (N,n3) matrix of point (col) vectors. N is 2 or 3.
%  half - If 1, creates also half basis on the boundaries.        Default 0.
%
% Usage:
%  make_triangle(p1,p2,p3,n)        - Divide each edge into n parts.
%  make_triangle(p1,p2,p3,n1,n2,n3) - Divide edges into n1, n2 and n3 parts.
%  make_triangle(p1,p2,p3,[0 0.1 0.5 1],n2,n3) - Use uneven divisions in n1.
%  make_triangle(p1,p2,p3,0,0,0,points1,points2,points3)
%                                   - Divide edges into points1, 2 and 3.
%  make_triangle(p1,p2,p3,0,n2,0,points1,[],points3)
%                                   - Divide edge1 and edge3 into points1 and
%                                     points3, edge2 into n2 parts.
%  make_triangle(points1,points2,points3)
%                                   - Use points1, 2 and 3 as division points
%                                     and the triangle is build upon the first
%                                     points of points1, 2 and 3.

% Parameters: ----------------------------------------------------------------

N = size(p1,1);        % Dimension.

% case: make_triangle(points1,points2,points3)
if (size(p1,2) > 1) & ((nargin == 3) | (nargin == 4))
  points1 = p1;
  points2 = p2;
  points3 = p3;
  p1 = points1(:,1);
  p2 = points2(:,1);
  p3 = points3(:,1);
  n1 = 0;
  n2 = 0;
  n3 = 0;
  if nargin < 4
    half = 0;
  else
    half = n1;
  end
else
  if (N < 2) & (N > 3)
    error('N should be either 2 or 3!');
  end

  if nargin == 4
    n2 = n1;
    n3 = n1;
    half = 0;
  else
    if (nargin < 7) | ((nargin > 7) & (nargin < 10))
      half = 0;
      if nargin < 6
	n3 = 1;
	if nargin < 5
	  n2 = 1;
	  if nargin < 4
	    n1 = 1;
	  end
	end
      end
    else
      if (nargin == 7)
	half = points1;
      end
    end
  end
end
% ----------------------------------------------------------------------------
if length(n1) > 1
  t1 = n1(:).';
  n1 = length(t1)-1;
else
  t1 = [];
end
if n1 ~= 0
  if length(t1) == 0
    t1 = (0:n1)/n1;
  end
  points1 = p1*(1-t1) + p2*t1;
else
  n1 = size(points1,2)-1;
  t1 = vlen(points1-p1(:,ones(1,n1+1)))/vlen(p2-p1);
end
d1 = diff(t1);

if length(n2) > 1
  t2 = n2(:).';
  n2 = length(t2)-1;
else
  t2 = [];
end
if n2 ~= 0
  if length(t2) == 0
    t2 = (0:n2)/n2;
  end
  points2 = p2*(1-t2) + p3*t2;
else
  n2 = size(points2,2)-1;
  t2 = vlen(points2-p2(:,ones(1,n2+1)))/vlen(p3-p2);
end
d2 = diff(t2);

if length(n3) > 1
  t3 = n3(:).';
  n3 = length(t3)-1;
else
  t3 = [];
end
if n3 ~= 0
  if length(t3) == 0
    t3 = (0:n3)/n3;
  end
  points3 = p3*(1-t3) + p1*t3;
else
  n3 = size(points3,2)-1;
  t3 = vlen(points3-p3(:,ones(1,n3+1)))/vlen(p1-p3);
end
d3 = diff(t3);
% ----------------------------------------------------------------------------
edge = [];
triangle = [];
basis = [];

if (n1 == n2) & (n2 == n3)
  % Splitting into equal number of parts:
  if n1 == 1
    % A single triangle:
    point = [p1 p2 p3];
    [edge,triangle] = add_triangle(1,2,3,point,edge,triangle);
  elseif all(abs(d1-d1(1))/max(d1) < 1e-12) & ...
	 all(abs(d2-d2(1))/max(d2) < 1e-12) & ...
	 all(abs(d3-d3(1))/max(d3) < 1e-12)
    % All the triangle edges are splitted into equal divisions:
    [point,edge,triangle,basis] = equal_triangles(N, points1, points3, ...
						  n1, n3, half);
  else
    % The triangle edges are splitted into unequal divisions:
    [point,edge,triangle,basis] = unequal_triangles(points1,points2,points3,...
						    t1,t2,t3);
  
    % Add half bases:
    if half
      basis = add_half_bases(points,edge,triangle);
    end
  end
else
  % Splitting into non-equal number of parts:
  ps = [points1(:,1:end-1) points2(:,1:end-1) points3(:,1:end-1)];
  grid = 1.01 * max(vlen(ps(:,[2:end 1])-ps));
  [point,edge,triangle,basis] = border2triangle(ps, grid, -1,[1:size(ps,2) 1]);
  
  % Add half bases:
  if half
    basis = add_half_bases(points,edge,triangle);
  end
end

% Check that the orientation of the triangles are the same as original:
nv = cross(p2-p1,p3-p1);
Nv = triangle_normals(point,triangle);
tind = dot(Nv,nv(:,ones(1,size(triangle,2)))) < 0;
triangle = reorder_triangle(triangle,tind);
% ===========================================================================
function [point,edge,triangle,basis] = equal_triangles(N,points1,points3,...
						       n1,n3,half)
% Example: n1 == n2 == n3 == 3
%
% p3 + 10
%    |\
%    | \
%    |  \
%  8 +---+ 9
%    |\  |\
%    | \ | \                             pind:
%    |  \|6 \                          1  2  3  4
%  5 +---+---+ 7                       5  6  7  0
%    |\  |\  |\                        8  9  0  0
%    | \ | \ | \                      10  0  0  0
%    |  \|  \|  \
% p1 +---+---+---+ p2
%    1   2   3   4

p1 = points1(:,1);

% All the points:
n31 = n3*(n1 +1/2*(1-n3))+n1+1;
point = zeros(N,n31);
pind  = zeros(n3+1,n1+1);             % Index table.
k = 0;
for j = 1:n3+1
  pj = points3(:,n3+1-(j-1))-p1;      % Vector p1->pj.
  for i = 1:n1+1-(j-1)
    pi = points1(:,i);                % Vector p1.
    k = k+1;
    point(:,k) = pj + pi;             % New point.
    pind(j,i) = k;
  end
end

% Loop over all "subrectangles":
edge = [];
triangle = [];
for j = 1:n3
  for i = 1:n1-(j-1)                %   pp4 +---+ pp3
    pp1 = pind(j,   i);             %       |\  |
    pp2 = pind(j,   i+1);           %       | \ |
    pp3 = pind(j+1, i+1);           %       |  \|
    pp4 = pind(j+1, i);             %   pp1 +---+ pp2
    [edge,triangle] = add_triangle(pp1,pp2,pp4,point,edge,triangle);
    if i ~= n1-(j-1)
      [edge,triangle] = add_triangle(pp2,pp3,pp4,point,edge,triangle);
    end
  end
end

% Create bases:
basis = fix_edges(point,edge,triangle,half);
% ============================================================================
function [point,edge,triangle,basis] = unequal_triangles(points1,points2,...
							 points3,t1,t2,t3)
n = length(t1)-2;

q1 = t1(2:end-1);  ind1 = 1+(1:n);
q2 = t2(2:end-1);  ind2 = 2+n+(1:n);
q3 = t3(2:end-1);  ind3 = 3+2*n+(1:n);
Pb = [points1(:,1:end-1) points2(:,1:end-1) points3(:,1:end-1)];

% Create middle edges:
E1 = add_edges(ind2, fliplr(ind3), Pb);
E2 = add_edges(ind3, fliplr(ind1), Pb);
E3 = add_edges(ind1, fliplr(ind2), Pb);

% Combine edges:
tol = 1e-5;
[P,E] = merge_geometries(Pb,E1,[],[],Pb,E2,[],[],tol);
[P,E] = merge_geometries(P ,E ,[],[],Pb,E3,[],[],tol);

% Create triangle from borders with fixed edges:
b1 = [1:3*(n+1) 1];   % Original triangle border.
s = '[point,edge,triangle,basis] = border2triangle(P,-1,-1, b1';
for i = 1:size(E,2)
  eval(sprintf('b%d = -E(1:2,%d);', i+1, i));
  s = [s ', b' int2str(i+1)];
end
s = [s ');'];
eval(s);
% ============================================================================
