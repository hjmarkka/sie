function [P,E,T,B,Pmap1,Pmap2,Emap1,Emap2,Tmap1,Tmap2,Bmap1,Bmap2] = ...
					remove_triangles(P,E,T,B,Tind,half)
% function [P,E,T,B,Pmap1,Pmap2,Emap1,Emap2,Tmap1,Tmap2,Bmap1,Bmap2] = ...
%                                       remove_triangles(P,E,T,B,Tind,half);
%
% Removes triangles indexed by TIND. Leaves half bases if HALF is non zero.
% Returns also mappings between new and old geometries (map1 and map2).

% ---------------------------------------------------------------------------
% Default values:
if nargin < 6
  half = 0;
end

np = size(P,2);
ne = size(E,2);
nt = size(T,2);
nb = size(B,2);
% ---------------------------------------------------------------------------
% Figure out edges with one or more removed triangles:
ebind = zeros(1,ne);
ebind(T(1:3,Tind)) = ones(3,length(Tind));

% Remove triangles:
[T,Tmap1,Tmap2] = remove_columns(Tind,T);

% Remove edges without triangles:
eind = logical(zeros(1,ne));
eind(T(1:3,:)) = ones(3,size(T,2));
eind = (eind == 0);

% Remember edges still having one of more triangles removed:
ebind(eind) = 0;
ebind = find(ebind);

% Remove points without edges:
pind = logical(zeros(1,np));
pind(E(1:2,~eind)) = 1;
pind = (pind == 0);

% Remove triangles from bases:
if nb
  Tmap3 = [-1 Tmap2];     % Note: half basis 0 -> -1!
  % Mark removed bases as zeros remap others:
  B(2:3,:) = reshape(Tmap3(B(2:3,:)+1),2,nb);

  % Remove bases without or with just one triangle:
  bind = find(any(B(2:3,:) == 0));
  [B,Bmap1,Bmap2] = remove_columns(bind,B);
  
  % Fix original half bases:
  B(B == -1) = 0;
end
% ---------------------------------------------------------------------------
% Remove edges and remap edge indeces in triangles and bases:
[E,T,B,Emap1,Emap2] = remove_edges(eind,E,T,B);

% Remove points and remap point indeces in edges and triangles:
[P,E,T,Pmap1,Pmap2] = remove_points(pind,P,E,T);
% ---------------------------------------------------------------------------
% Remap edges having removed triangles:
ebind = Emap2(ebind);

% Check that all the edges having removed triangles have proper bases:
for ie = ebind
  % Get the triangles attached into the edge:
  tind = find(any(T(1:3,:) == ie));
  
  % Get the bases over the edge:
  bind = find(B(1,:) == ie);

  % Figure out triangles of this edge without bases:
  nob = ones(1,nt);
  nob(B(2,bind)) = 0;
  half_bases = (B(3,bind) == 0);     % Half bases of this edge.
  nob(B(3,bind(~half_bases))) = 0;   % Half bases have B(3,:) = 0!
  nob = find(nob(tind));

  if length(nob) > 0
    % No support for half bases if no option half given:
    if ~half & any(half_bases)
      error('Half bases but no option HALF given!');
    end
    
    if half & any(half_bases)
      % If half bases, add those for all triangles without bases:
      n0 = length(nob);
      B = [B [ie(ones(1,n0)); tind(nob); zeros(1,n0)]];
    else
      % Check if there are any triangles having bases:
      tbind = tind;
      tbind(nob) = [];
      
      % Use one of the triangles having bases as the first triangle
      % of the bases to be added for every triangle without
      % bases. If no triangles with bases create one from the first
      % two triangles without bases:
      if (length(tbind) > 0) | (length(nob) > 1)
	if length(tbind) > 0
	  % Select the first triangle from those having bases:
	  t1 = tbind(1);
	else  % length(nob) > 1
	  % No triangles with bases. Create one basis from the first two:
	  t1 = tind(nob(1));
	  B = [B [ie; t1; tind(nob(2))]];
	  
	  % Remove the triagles from the array of triangles without bases:
	  nob = nob(3:end);
	end
	
	n0 = length(nob);
	if n0
	  % Create bases for the rest of the triangles without bases:
	  B = [B [ie(ones(1,n0)); t1(ones(1,n0)); tind(nob)]];
	end
      end
    end
  end
end

% Add new bases into Bmap1:
if nargout > 10
  Bmap1 = [Bmap1 zeros(1,size(B,2)-size(Bmap1,2))];
end
% ---------------------------------------------------------------------------
