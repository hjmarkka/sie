function plot_bases(point,edge,triangle,basis,do_numbers,color,Bind)
% function plot_bases(point,edge,triangle,basis,[do_numbers,color,Bind])
%
% point      - (N,number_of_points). N is 2 or 3.
% edge       - (3,number_of_edges).
% triangle   - (10,numbers_of_triangles). Not used.
% basis      - (3,numbers_of_bases).
% do_numbers - If 1, plot edge numbers.        Default: 0.
% color      - Color.                          Default: 'r'.
% Bind       - Only these bases.               Default: 1:size(B,2).

% Default values:
if nargin < 7
  Bind = [];
  if nargin < 6
    color = [];
    if nargin < 5
      do_numbers = [];
    end
  end
end
if length(do_numbers) == 0
  do_numbers = 0;
end
if length(color) == 0
  color = 'r-';
end
if length(Bind) == 0
  Bind = 1:size(basis,2);
end

% Plot basis edges:
Eind = zeros(size(edge,2),1);
eb = basis(1,Bind);
Eind(eb) = ones(1,length(eb));
Eind = find(Eind);
if Eind
  plot_edges(point, edge, 0, color, Eind);

  if do_numbers
    P1 = point(:,edge(1,eb));
    P2 = point(:,edge(2,eb));
    P = (P1+P2)/2;  % Center points of the basis edges
    plot_numbers(P, Bind);
  end
end
