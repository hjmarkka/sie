function R = rotmat(axis,angle)
% function R = rotmat(axis,angle)
% Returns 3D rotation matrix for rotation of angle ANGLE around vector AXIS.
%         2D rotation matrix (around Z axis) if AXIS is empty.

% Special case:
if (angle == 0) | ((length(axis) ~= 0) & (vlen(axis) == 0))
  if length(axis) == 0
    R = eye(2);
  else
    R = eye(3);
  end
  return
end

% Calculate sin(angle) and cos(angle):
[c,s] = sincos(angle);
% c = cos(angle); s = sin(angle);

if length(axis) == 0
  % 2D case, rotation matrix around Z axis:
  R = [ c -s; ...
        s  c];
else
  % Unit vector:
  a = axis/vlen(axis);

  % 3D case:
  R = c*eye(3) + a*a.'*(1-c) + s*[0    -a(3)  a(2); ...
                                  a(3)  0    -a(1); ...
                                 -a(2)  a(1)  0   ];
end
