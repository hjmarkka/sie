function [P,E,T,B] = calc_disc(ps,R,nv,n,m4,half,angle)
% function [P,E,T,B] = calc_disc(ps,R,nv,n,m4,half,angle)

% Default values:
if nargin < 7
  angle = [];
end
if length(angle) == 0
  angle = 2*pi;
end

tol = 1e-10;

mp = size(ps,2);
o3 = ones(3,1);

% Calculate local circle coordinate vectors (t,u):
t = R;
lt = vlen(t);
ind = lt < tol;
lt(ind) = zeros(size(find(ind)));
ind = lt ~= 0;
t(:,ind) = t(:,ind)./(o3*lt(ind));
u = cross(nv,t);
rt = (o3*lt).*t;
ru = (o3*lt).*u;

% Check if end points are the same:
p2 = ps+R;
if vlen(p2(:,1)-p2(:,mp)) < tol
  p0ind = [1:mp-1 1];
  P = p2(:,1:mp-1);
else
  p0ind = 1:mp;
  P = p2;
end
p2ind = p0ind;

E = []; T = []; B = [];
if angle == 2*pi
  nind = 1:n;
  nn = n;
else
  nind = 1:n-1;
  nn = n-1;
end
for i = nind
  phi = i/nn*angle;
  p3 = p2;
  p3ind = p2ind;
  p2 = ps + rt*cos(phi) + ru*sin(phi);
  if (i == n) 
    p2ind(ind) = p0ind(ind);
  else
    % Add only new points:
    p2ind(ind) = size(P,2)+(1:mp-sum(~ind));
    p2ind(~ind) = p3ind(~ind);
    ind2 = ind;
    if vlen(p2(:,1)-p2(:,mp)) < tol
      p2ind(mp) = p2ind(1);
      ind2(mp) = 0;
    end
    P = [P p2(:,ind2)];
  end
  for j = 1:mp-1
    if lt(j) == 0
      [E,T] = add_triangle(p3ind(j),p3ind(j+1),p2ind(j+1),P,E,T);
    elseif lt(j+1) == 0
      [E,T] = add_triangle(p3ind(j),p3ind(j+1),p2ind(j),P,E,T);    
    else
      pind = [p2ind(j) p3ind(j) p3ind(j+1) p2ind(j+1)];
      if m4 == 4
        pind5 = size(P,2)+1;
	P = [P sum(P(:,pind).').'/4];
	[E,T] = add_triangle(pind(1),pind(2),pind5,P,E,T);
	[E,T] = add_triangle(pind(2),pind(3),pind5,P,E,T);
	[E,T] = add_triangle(pind(3),pind(4),pind5,P,E,T);
	[E,T] = add_triangle(pind(4),pind(1),pind5,P,E,T);
      else
	[E,T] = add_triangle(pind(1),pind(2),pind(3),P,E,T);
	[E,T] = add_triangle(pind(3),pind(4),pind(1),P,E,T);
      end
    end
  end
end
B = fix_edges(P,E,T,half);
