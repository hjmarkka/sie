function [subset,map] = map_subset(n,ind)
% function [subset,map] = map_subset(n,ind)
%
% Bijective map between group of indeces IND of 1:N.
%
% INPUT:
%   N      : Size of set (max index of IND).
%   IND    : Group of indeces between 1 and N.
%
% OUTPUT:
%   SUBSET : Subset indeces of 1:N included in IND.
%   MAP    : Mapping from 1:N to SUBSET: length(map) = N.

subset = logical(zeros(1,n));
subset(ind) = 1;
subset = find(subset);

map = zeros(1,n);
map(subset) = 1:length(subset);
