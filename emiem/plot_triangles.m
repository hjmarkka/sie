function plot_triangles(point,edge,triangle,do_numbers,color,Tind)
% function plot_triangles(point,edge,triangle,[do_numbers,color,Tind])
%
% point      - (N,number_of_points). N is 2 or 3.
% edge       - (3,number_of_edges).
% triangle   - (10,numbers_of_triangles).
% do_numbers - If 1, plot triangle numbers.   Default: 0.
% color      - Color.                         Default: 'r'.
% Tind       - Only these triangles.          Default: 1:size(triangle,2).

% Default values:
if nargin < 6
  Tind = [];
  if nargin < 5
    color = [];
    if nargin < 4
      do_numbers = [];
    end
  end
end
if length(do_numbers) == 0
  do_numbers = 0;  
end
if length(color) == 0
  color = 'r-';
end
if length(Tind) == 0
  Tind = 1:size(triangle,2);
end

% Plot triangle edges:
Eind = zeros(size(edge,2),1);
Eind(triangle(1:3,Tind)) = ones(3,length(triangle(1,Tind)));
plot_edges(point, edge, 0, color, find(Eind));

if do_numbers
  P = triangle(8:10,Tind);  % Center points of the triangles
  plot_numbers(P, Tind);
end
