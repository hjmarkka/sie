function [pt,et,tt,bt] = rotate_geometry(axis,origin,angle,...
                                         point,edge,triangle,basis)
%function [pt,et,tt,bt] = rotate_geometry(axis,origin,angle,...
%                                         point,edge,triangle,basis)
% Rotates geometry:
%
% axis: (3,1) or (0,0)              x          - Rotation axis (3D) or empty
%                                   y            (2D, Z-axis [0;0;1]).
%                                   z
% origin:   (3,1)                   x          - Translation vector, rotation
%                                   y            origin.
%                                   z
% angle or rotation matrix                     - Rotation angle or matrix.
% point,edge,triangle,basis         See "help connect".

%disp('Rotating geometry...');

% Translate into origin:
[pt, et, tt, bt] = translate_geometry(-origin, point, edge, triangle, basis);

% Rotation matrix:
if size(angle,2) > 1
  R = angle;
else
  R = rotmat(axis,angle);
end

% Rotate points:
pt = R*pt;

% Rotate triangle centroids:
if size(tt,2) > 0
  tt(8:10,:) = R*tt(8:10,:);
end

% Translate origin back:
[pt, et, tt, bt] = translate_geometry(origin, pt, et, tt, bt);
