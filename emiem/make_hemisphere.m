function [P,E,T,B] = make_hemisphere(p,r,f,c,t,e,closed)
% function [P,E,T,B] = make_hemisphere(p,r,f,c,t,e,closed)
% Create a hemisphere (half sphere) into point p having radius r.
%
%  p - point (N,1).                                            Default [0;0;0].
%  r - radius.                                                 Default 1.
%  f - geodesic frequency. Should be even!                     Default 2.
%  c - class type (1 of 2).                                    Default 1.
%  t - polyhedron type 'i' for icosahedron                     Default 'i'.
%                      'o' for octahedron
%  e - ellipse and specifies eccentricity ( 0.0 < e < 2.0)     Default 1.
%  closed - 0 = the hemisphere will not be a closed surface    Default 0.
%           1 = the hemisphere will be a closed surface
%  
% Usage:
%  make_hemisphere(p,r,n)      - Half ball having origin p and radius r.
%
% See: make_ball2

% Default values:
if nargin < 7
  closed = [];
  if nargin < 6
    e = [];
    if nargin < 5
      t = [];
      if nargin < 4
	c = [];
	if nargin < 3
	  f = [];
	  if nargin < 2
	    r = [];
	    if nargin < 1
	      p = [];
	    end
	  end
	end
      end
    end
  end
end

if length(p) == 0, p        = [0;0;0]; end
if length(r) == 0, r           = 1   ; end
if length(f) == 0, f           = 2   ; end
if length(c) == 0, c           = 1   ; end
if length(t) == 0, t           = 'i' ; end
if length(e) == 0, e           = 1.0 ; end
if length(closed) == 0, closed = 0   ; end

% Check for even frequency:
if ceil(f/2) ~= f/2
  error('Even frequency!');
end

% Polyhedron type:
if (t ~= 'i') & (t ~= 'o')
  error('Polyhedron type T should be either ''i'' or ''o''!');
end

% Call make_ball2:
[P,E,T,B] = make_ball2([0;0;0],r,f,c,t,e);

% Remove triangles having points under the equator:
Ptol = max(abs(P(3,:)))*1e-12;
Punder = P(3,:) < -Ptol;
Tunder = find(any(Punder(T(4:6,:))));
[P,E,T,B] = remove_triangles(P,E,T,B,Tunder);

% Optionally close the surface:
if closed ~= 0
  % Collect all the points hear the equator:
  Peq = P(:,find(abs(P(3,:)) < Ptol));
  
  % Figure out the right order of the points:
  alpha = atan2(Peq(2,:),Peq(1,:));
  [dummy,ind] = sort(alpha);
  Peq = Peq(:,ind);
  
  % Create triangles inside the polygon of eq points:
  gsize = 1.001*max(vlen(Peq(:,2:end)-Peq(:,1:end-1)));
  [Pe,Ee,Te,Be] = border2triangle(Peq,[gsize 0 0], -1, [1:size(Peq,2) 1]);
  
  % Join the two geometries:
  [P,E,T,B] = connect(P,E,T,B,Pe,Ee,Te,Be);
%keyboard
end

% Move points:
P = P + p(:,ones(1,size(P,2)));
[P,E,T,B] = recalculate_geometry(P,E,T,B);
