function angle = vangle(v1,v2)
% function angle = vangle(v1,v2)
%
% Calculates angle between two vectors. V1 and V2 should be columns
% vectors. Works with matrices also.

angle = real(acos(dot(v1,v2)./(vlen(v1).*vlen(v2))));
