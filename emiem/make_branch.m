function [P,E,T,B] = make_branch(r1,r2,alpha,l1,s1,l2,nr,n1,m1,n2,m2,...
				 beta,gamma)
%function [P,E,T,B] = make_branch(r1,r2,alpha,l1,s1,l2,nr,n1,m1,n2,m2,...
%                                 beta,gamma)
% Create cylindrical geometry of one or more branches.
%
% Parameters:
% r1    - Radius of the main cylinder. Default: 1.
% r2    - Radius (radix) of branch(es). Default: 0.5.
% alpha - Angle between cylinder and branch(es). Default: pi/2 = 90 degrees.
% l1    - Total length of the main cylinder. Default: 5.
% s1    - Length(s) of main cylinder below branch(es). Default: 1.
% l2    - Total length(s) of branch(es). Default: 3.
% nr    - If > 0, the ends of the cylinders are covered. Default: 0.
% n1    - Number of angle subdivisions for main cylinde. Default: 8.
% m1    - Number of length subdivisions for main cylinder. Default: 5.
% n2    - Number of angle subdivisions for branch(es). Default: 6.
% m2    - Number of length subdivisions for branch(es). Default: 3.
% beta  - Planar angle for the main cylinder. Default: 2*pi.
% gamma - Angle between main cylinder and branch(s) (plane). Default: 0.
%
%        r1                                   r2
%     +------+------+    / r2                +--+--+
%     |             |   /    /               |     |
%     |             |  /    /                |     |
%     |             |alpha /                 |     |
%     |             |/    /              __--+     +--__
%     |                  / l2          +'               `+
%     |                 /             /                   \
% l1  |                /             +                     +
%     |               /               \                   /
%     |              /                  \               /
%     |             /                     \    beta   /
%     |             |                  r1   \       /
%     |             |                         \   /
%     |             | s1                        +
%     |             |
%     +-------------+

% Default values:
if nargin < 13
  gamma = [];
  if nargin < 12
    beta = [];
    if nargin < 11
      m2 = [];
      if nargin < 10
	n2 = [];
	if nargin < 9
	  m1 = [];
	  if nargin < 8
	    n1 = [];
	    if nargin < 7
	      nr = [];
	      if nargin < 6
		l2 = [];
		if nargin < 5
		  s1 = [];
		  if nargin < 4
		    l1 = [];
		    if nargin < 3
		      alpha = [];
		      if nargin < 2
			r2 = [];
			if nargin < 1
			  r1 = [];
			end
		      end
		    end
		  end
		end
	      end
	    end
	  end
	end
      end
    end
  end
end
if length(r1)    == 0, r1    = 1; end
if length(r2)    == 0, r2    = 0.5; end
if length(alpha) == 0, alpha = pi/2; end
if length(l1)    == 0, l1    = 5; end
if length(s1)    == 0, s1    = 1; end
if length(l2)    == 0, l2    = 3; end
if length(nr)    == 0, nr    = 0; end
if length(n1)    == 0, n1    = 8; end
if length(m1)    == 0, m1    = 5; end
if length(n2)    == 0, n2    = 6; end
if length(m2)    == 0, m2    = 3; end
if length(beta)  == 0, beta  = 2*pi; end
if length(gamma) == 0, gamma = 0; end

% Check angles:
if any(alpha <= 0) | any(alpha >= pi)
  error('Angle alpha should be > 0 and < pi');
end
if (beta <= 0) | (beta > 2*pi)
  error('Angle beta should be > 0 and <= 2*pi');
end
if any(gamma < 0) | any(gamma > 2*pi)
  error('Angle gamma should be >= 0 and <= 2*pi');
end

if any(s1 < 0)
  error('s1 should be > 0!');
end
if (l1 < 0)
  error('l1 should be > 0!');
end

nb = length(r2);

% nr = [bottom top branch1 branch2 ...]:
if length(nr) == 1
  nr = nr*ones(1,2+nb);                  % Copy nr for top and all branches.
elseif length(nr) == 2
  nr = [nr(1) nr(2) nr(2)*ones(1,nb)];   % Copy nr(2) for all branches.
elseif length(nr) == 3
  nr = [nr(1) nr(2) nr(3)*ones(1,nb)];   % Copy nr(3) foo all branches.
elseif length(nr) ~= 2+nb
  error('Length of nr should be either 1, 2, 3 or 2+length(r2)!');
end

do_debug = 0;
% ----------------------------------------------------------------------------
% Loop over branches:

P2 = [];  E2 = [];  T2 = [];  B2 = [];

if do_debug
  clf;
  plot_points([[0;0;-max(s1)],[0;0;l1]]);
  hold on
end

cb = cos(beta);
sb = sin(beta);
if nb > 9
  error('Only 9 branches supported! Limitation in border2triangle.');
end

% -----------------------------------------------
% The program is under construction:
if nb > 1
  warning('Currently only one branch supported!');
end
if beta ~= 2*pi
  warning('Settings of beta might not work!');
end
if gamma
  warning('Settings of gamma might not work!');
end
% -----------------------------------------------

h = 2*r2./sin(alpha);

for j = 1:nb
  ca = cos(alpha(j));
  sa = sin(alpha(j));
  cg = cos(gamma(j));
  sg = sin(gamma(j));

  if h(j)+s1(j) > l1
    warning(sprintf(['Too short main cylinder for branch %d, ' ...
		     'increasing l1!'],j));
    l1 = 1.1*(h(j)+s1(j));
  end

  l = 2*r2(j)/tan(alpha(j));
  if (l2(j) < l)
    warning(sprintf('Too short branch %d, increasing l2(%d)!',j,j));
    l2(j) = 1.1*l;
  end

  v  = [cg*sa, sg*sa, ca].';
  t1 = [cg*(r1-r2(j)*ca), sg*(r1-r2(j)*ca), r2(j)*sa+s1(j)].';

  %nt = 15;
  %t = (0:nt-1)/(nt-1)*l;
  %ot = ones(1,nt);

  % Find cross section points pc on branch lines:
  pc = zeros(3,n2(j));
  tc = zeros(1,n2(j));
  thetas = zeros(1,n2(j));
  for i = 1:n2(j)
    theta = (i-1)/n2(j)*2*pi - pi/2;
    thetas(i) = theta;
    ct = cos(theta);
    st = sin(theta);
    u  = [-cg*st*ca-sg*ct, -sg*st*ca+cg*ct, st*sa].';  
    if i == 1
      u0 = u;
    end
%    p = v*t + t1(:,ot) + r2(j) *u(:,ot);
%    plot_points(p,0,'k-'); hold on
    tc(i) = r2(j)*cot(alpha(j))*(1 + st) + (sqrt(r1^2-(r2(j)*ct).^2) - r1)/sa;
    pc(:,i) = v*tc(i) + t1 + r2(j)*u;
  end

  if do_debug
    hold on; plot_points(pc,0,'k-',[1:n2(j) 1]); hold off; axis equal
  end

  % Convert cartesian coordinates into branch cylinder coordinates:
  Pc2 = [r2(j)*(thetas-pi/2); tc];
  Pc2 = [Pc2 [r2(j)*pi; tc(1)]];  % Add first point to the end.
  
  % Create boundary for the branch:
  xtheta1 = -r2(j)*pi;
  xtheta2 = -xtheta1;
  pt1 = [xtheta1; l2(j)];
  pt2 = [xtheta2; l2(j)];
  Pb2 = [pt1 pt2 Pc2];
  bt = [1 2+(1:n2(j)+1) 2 1];

  % Create triangles for the branch surface:
  grid2 = [1 r2(j)*2*pi/n2(j) l2(j)/m2(j)];
  bsize2 = [0 -1*ones(1,n2(j)) 0 0].';
  [Pp2,Ep2,Tp2,Bp2] = border2triangle(Pb2,grid2,bsize2,bt);

  % Roll plane back to the branch surface:
  nv = cross(v,cross(v,u0));
  [Pt2,Et2,Tt2,Bt2] = plane2cylinder(Pp2,Ep2,Tp2,Bp2,r2(j)*nv,v,t1);

  % Add branch cover:
  if nr(2+j)
    [Pc,Ec,Tc,Bc] = make_circle([t1+l2(j)*v v],r2(j)*nv,n2(j),nr(2+j));
    [Pt2,Et2,Tt2,Bt2] = connect(Pt2,Et2,Tt2,Bt2,Pc,Ec,Tc,Bc);
  end

  if do_debug
    hold on; plot_triangles(Pt2,Et2,Tt2,0,'b-'); axis equal; hold off
  end

  % Connect branches:
  [P2,E2,T2,B2] = connect(P2,E2,T2,B2,Pt2,Et2,Tt2,Bt2);
  
  % Convert cartesian coordinates into main cylinder coordinates:
  phic = atan2(pc(2,:),pc(1,:));
  zc   = pc(3,:);

  % Create border points:
  Pc = [phic;zc];
  eval(sprintf('Pc%d = Pc;',j));
end

% ----------------------------------------------------------------------------
% Try to figure out vertical cutting line for opening up the cylinder plane:

% Min and max angles of branches:
phi1 = gamma - asin(r2/r1);
phi2 = gamma + asin(r2/r1);

% Create main cylinder bottom and top points:
if beta == 2*pi
  phib = ((0:n1-1)/n1 - 1/2)*beta;

  % Check if any straight line is free of branches:
  sep = phib(ones(1,nb),:)-gamma(ones(1,n1),:).';
  phisep = abs(atan2(sin(sep),cos(sep)));
  ind  = phisep >  asin(r2(ones(1,n1),:).'/r1);
  ind0 = phisep == asin(r2(ones(1,n1),:).'/r1);
  
  if any(all(ind | ind0, 1))
    indb  = find(all(ind, 1));
    indb0 = find(all(ind | ind0, 1));
    if length(indb)
      phicut = phib(indb(1));
    else
      phicut = phib(indb0(1));
    end
  else
    if do_debug
      x = r1*cos(phib);
      y = r1*sin(phib);
      z = l1*ones(size(phib));
      hold on; plot3([x;x],[y;y],[0*z;z],'r-'); hold off; axis equal
      hold on; plot_points([x;y;z],1,'k-'); hold off; axis equal    
    end
    keyboard
    error('Could not find straight line to cut cylinder open into a plane!');
  end
else
  % This case (beta < 2*pi) is easier. We know where to cut cylinder open!
  phib = ((0:n1-1)/(n1-1) - 1/2)*beta;
  phicut = -beta/2;
end
% ----------------------------------------------------------------------------
% Move phicut -> -beta/2:
phimove = phicut+beta/2;
phib = atan2(sin(phib-phimove),cos(phib-phimove));
phi1 = atan2(sin(phi1-phimove),cos(phi1-phimove));
phi2 = atan2(sin(phi2-phimove),cos(phi2-phimove));
phi0 = atan2(sin(gamma-phimove),cos(gamma-phimove));

% Check branches:
if any(phi1 > phi2)
  error('Branch not inside cylinder angle!');
end

% ----------------------------------------------------------------------------
PC = []; nn = 0;
b1 = []; b2 = []; b3 = []; b4 = []; b5 = [];
b6 = []; b7 = []; b8 = []; b9 = []; b10 = []; 

indl = []; indr = []; indd = []; indu = [];
for j = 1:nb
  eval(sprintf('phic = Pc%d(1,:);',j));
  eval(sprintf('zc   = Pc%d(2,:);',j));

  % Move angle:
  phic = atan2(sin(phic-phimove),cos(phic-phimove));

  % Check for border points:
  if (abs(phi1(j)-(-beta/2)) < 1e-10) & (mod(n2(j),4) == 0)
    pind = 3*n2(j)/4+1;
    phic(pind) = -beta/2;
    indl = [indl nn+pind];
  end
  if (abs(phi2(j)-(+beta/2)) < 1e-10) & (mod(n2(j),4) == 0)
    pind = 1*n2(j)/4+1;
    phic(pind) = +beta/2;    % Move point into the border.
    indr = [indr nn+pind];       % Save the right side border point index.
  end
  if (s1(j) < 1e-10)
    pind = 0*n2(j)/4+1;
    zc(pind) = 0;
    indd = [indd nn+pind];
  end
  if (abs(h(j)+s1(j)-1) < 1e-10)
    pind = 2*n2(j)/4+1;
    zc(pind) = l1;
    indu = [indu nn+pind];
  end
  
  % Check for duplicate points:
  bind = 1:n2(j);     % Point indeces for border.
  if j > 1
    oC = ones(1,size(PC,2));
    near = sqrt((phic(oC,:)-PC(  ones(1,length(phic)),:).').^2 + ...
		(zc(oC,:)  -PC(2*ones(1,length(phic)),:).').^2) < 1e-10;
    indsep = find(any(near, 1));
    if length(indsep)
      % Remove duplicate points:
      for jj = 1:length(indsep)
	same = find(near(:,indsep(jj)));
	bind(indsep(jj)) = same-nn;   % Map border point into duplicate point.
	largeind = find(bind > indsep(jj));
	bind(largeind) = bind(largeind)-1;
      end
      phic(:,indsep) = [];
      zc(:,indsep) = [];
      n2(j) = n2(j)-length(indsep);
    end
  else
    indsep = [];
  end
  
  % Create border points:
  Pc = [phic;zc];
  PC = [PC Pc];
  eval(sprintf('b%d = nn + [bind 1];',j));  
  nn = nn + n2(j);
end
% ----------------------------------------------------------------------------
% Create boundaries for the main cylinder:

xphi1 = -beta/2;
xphi2 = -xphi1;
yz1   = 0;
yz2   = l1;
pb1 = [xphi1; yz1];
pb2 = [xphi2; yz1];
pb3 = [xphi2; yz2];
pb4 = [xphi1; yz2];

% Sort additional border points:
[dummy,sortd] = sort(PC(1,indd));
[dummy,sortr] = sort(PC(2,indr));
[dummy,sortu] = sort(-PC(1,indu));
[dummy,sortl] = sort(-PC(2,indl));

% Add branch points in to the list of outer border points:
Pb = [pb1 pb2 pb3 pb4];
bn = [nn+1 indd(sortd) nn+2 indr(sortr) nn+3 indu(sortu) nn+4 indl(sortl)];
eval(sprintf('b%d = [bn nn+1];',nb+1));;
PC = [PC Pb];

% Multiply angles with radius:
PC(1,:) = r1*PC(1,:);

% Create triangles for the main cylinder surface:
grid1 = [nn+1 r1*beta/n1 l1/m1];
bsize1 = [-1*ones(1,nb) 0];
%keyboard
[Pp1,Ep1,Tp1,Bp1] = border2triangle(PC,grid1,bsize1,...
				    b1,b2,b3,b4,b5,b6,b7,b8,b9,b10);

% Roll plane back to the main cylinder surface:
[P1,E1,T1,B1] = plane2cylinder (Pp1,Ep1,Tp1,Bp1,r1,[],[],beta);

% Move angle back to the original:
[P1,E1,T1,B1] = rotate_geometry([0;0;1],[0;0;0],phimove,P1,E1,T1,B1);

% Add bottom and top covers:
if nr(1)
  [Pb,Eb,Tb,Bb] = make_circle([0;0;0],[r1;0;0],n1,nr(1));
  [P1,E1,T1,B1] = connect(P1,E1,T1,B1,Pb,Eb,Tb,Bb);
end
if nr(2)
  [Pt,Et,Tt,Bt] = make_circle([0;0;l1],[r1;0;0],n1,nr(2));
  [P1,E1,T1,B1] = connect(P1,E1,T1,B1,Pt,Et,Tt,Bt);
end

if do_debug
  hold on; plot_triangles(P1,E1,T1); axis equal; hold off
  pause
end
% ----------------------------------------------------------------------------
% Connect the main cylinder and branches:
[P,E,T,B] = connect(P1,E1,T1,B1,P2,E2,T2,B2,1);
%figure(2); hold on; plot_triangles(P,E,T); axis equal; hold off
% ----------------------------------------------------------------------------
