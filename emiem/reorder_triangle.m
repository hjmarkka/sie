function T = reorder_triangle(T,Tind)
%function T = reorder_triangle(T,Tind)
% Reorder triangle edges (and points) so that the triangle normal
% vector points to the other direction.

if nargin < 2
  Tind = 1:size(T,2);
end

T(1:3,Tind) = T([1 3 2],Tind);
T(4:6,Tind) = T(3+[2 1 3],Tind);
