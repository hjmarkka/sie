function [Pf,JKf,dJKf,Nf] = surface_currents(P,E,T,B,JK,where,what,Tind)
% function [Pf,JKf,dJKf,Nf] = surface_currents(P,E,T,B,JK,where,what,Tind)
%
% Calculates electric and/or magnetic surface currents (and
% divergencies of currents) for given geometry (P,E,T,B) and current
% solution (J/K).
%
% Output:
%  Pf   - Field points. (3,np) matrix. See help on input parameter where.
%  JKf  - Electric and/or magnetic current densities in points Pf.
%         (3,np) or (6,np) matrix [Jf; Kf].
%  dJKf - Divergencies of electric and/or magnetic currents in points Pf.
%         (1,np) or (2,np) vector [dJf; dKf].
%  Nf  - Normal vectors of the surface in points Pf. (3,np) matrix.
%
% Input:
%  P,E,T,B - geometry, see "help connect"  (n = number of bases below)
%  JK      - solution vector of electric (J) and/or magnetic (K) current
%            densities for given geometry. Either (n,1) vector (J or K)
%            or (n,2) matrix (both) [J K].
%  where   - scalar (0 or 1) or (3,np) matrix. Where the fields are calculated.
%            0      = Centroids of the triangles.  Default.
%            1      = Vertices of the triangles.
%            2      = Vertices of the triangles. No interpolation.
%                     Returns Pf as (3,3*np) matrix: [Pf(p1) Pf(p2) Pf(p3)].
%            (3,np) = given 3D points.
%  what    - Part of a logical (1,2) vector specifing whether to calculate
%            currents densities and/or divergencies. Default: [1 1].
%  Tind    - Triangle indeces to be used in case where = 0/1/2. Default: all.

npoint    = size(P,2);
ntriangle = size(T,2);
[nj,mj] = size(JK);

% Default values:
if nargin < 6
  where = 0;
end

What = zeros(1,2);
if (nargin < 7)
  what = [];
end
if (length(what) == 0)
  What = ones(1,2);
else
  What(1:length(what)) = what;
end

if (nargin < 8)
  Tind = [];
end
if (length(Tind) == 0)
  Tind = 1:ntriangle;
end
nTind = length(Tind);

% Field points:  -------------------------------------------------------------
if size(where,1) > 1
  Pf = where;                    % Given points.
elseif where == 0
  Pf = T(8:10,Tind);             % Triangle centroids.
elseif where == 1
  Pfind = zeros(1,npoint);       % Triangle vertices.
  Pfind(T(4:6,Tind)) = ones(3,nTind);  % Mark used points.
  Pf = P;                        % All points now. Remove unused later.
else % where == 2
  Pf = P(:,T(4:6,Tind));         % Triangle vertices for every triangle.
end

% Assing currents: -----------------------------------------------------------
doJ  = (nargout > 1) & What(1);
doDJ = (nargout > 2) & What(2);
doK  = doJ & (mj > 1);
doDK = doK & (mj > 1);
doN  = (nargout > 3);

if doJ | doDJ
  J = JK(:,1);
  if doK | doDK
    K = JK(:,2);
  end
elseif doK | doDK
  K = JK(:,1);
elseif ~doN
  % No currents and no normal vectors wanted. Return field points (Pf) only.
  if where == 1
    Pf = Pf(:,logical(Pfind));      % Return only used points.
  end
  return
end

% Calculate currents and normal vectors: -------------------------------------

np = size(Pf,2);
if doN
  Nf = zeros(3,np);   % Normal vectors at field points.
end
cP = zeros(1,np);     % Count of field point values used.
if doJ,   Jf = zeros(3,np); else Jf  = []; end
if doK,   Kf = zeros(3,np); else Kf  = []; end
if doDJ, dJf = zeros(1,np); else dJf = []; end
if doDK, dKf = zeros(1,np); else dKf = []; end
docur = doJ | doK | doDJ | doDK;
pind = [3 1 2];

for i = 1:nTind
  it = Tind(i);
  t = T(:,it);
  A = t(7);
  p = t(4:6);
  pp = P(:,p);

  % Find fields points inside this triangle:
  if where == 0
    Pind = i;
  elseif where == 1
    Pind = p;
  elseif where == 2
    Pind = (1:3)+(i-1)*3;
  else
    Pind = find(p_in_tri(Pf,pp)).';
  end

  lp = length(Pind);
  if lp > 0
    op = ones(1,lp);
    cP(Pind) = cP(Pind) + op;  % Add one for these points.
    if doN
      nn = cross(pp(:,2)-pp(:,1),pp(:,3)-pp(:,1));          % Normal vector.
      nn = nn/norm(nn);
      Nf(:,Pind) = Nf(:,Pind) + nn(:,op);
    end

    if docur
      btri = find(any(B(2:3,:) == it));
      for j = 1:3
	edge_index = t(j);
	bind = find(B(1,btri) == edge_index);
	if length(bind) > 0
	  p1 = pp(:,pind(j));  % Reorder points: edge -> opposite point.
	  e = E(:,edge_index);
	  l = e(3);
	  r1 = l/(2*A)*(Pf(:,Pind) - p1(:,op));  % RWG basis function.
	  d = l/A;                               % Surface divergence of RWG.
	  for b = btri(bind)
	    if B(2,b) == it
	      R1 = r1;
	      D = d;
	    elseif B(3,b) == it
	      R1 = -r1;    % Negative for the second triangle of a basis.
	      D = -d;
	    else
	      R1 = 0;      % This triangle does not belong to this basis!
	      D = 0;       % Matlab does not have "continue"!
	    end
	    if doJ,   Jf(:,Pind) = Jf(:,Pind) + J(b)*R1; end
	    if doK,   Kf(:,Pind) = Kf(:,Pind) + K(b)*R1; end
	    if doDJ, dJf(Pind)   = dJf(Pind)  + J(b)*D; end
	    if doDK, dKf(Pind)   = dKf(Pind)  + K(b)*D; end
	  end
	end
      end
    end
  end
end

if where == 1
  Pf = Pf(:,logical(Pfind));       % Return only used points.
end

% Do not divite by zero:
ind0 = (cP == 0);
ind = find(ind0);
cP(ind) = ones(size(ind));

% Divide by point counts = calculate possible mean values:
cP3 = cP([1 1 1],:);
if doJ,  Jf  = Jf./cP3; end
if doK,  Kf  = Kf./cP3; end
if doDJ, dJf = dJf./cP; end
if doDK, dKf = dKf./cP; end

if doK
  JKf = [Jf; Kf];
else
  JKf = Jf;
end

if doDK
  dJKf = [dJf; dKf];
else
  dJKf = dJf;
end

if doN
  % Divide by point counts = calculate possible mean values:
  ind = find(~ind0);
  Nf(:,ind) = Nf(:,ind)./cP3(:,ind);
  v = vlen(Nf(:,ind));
  Nf(:,ind) = Nf(:,ind)./v([1 1 1],:);
end
% ----------------------------------------------------------------------------

