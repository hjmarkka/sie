function [P,E,T,Pmap1,Pmap2] = remove_points(pind,P,E,T)
% function [P,E,T,Pmap1,Pmap2] = remove_points(pind,P,E,T)
%
% Removes points P(:,pind) and remaps point indeces in E and T.

% Remove edges and get mappings between new and old edges:
[P,Pmap1,Pmap2] = remove_columns(pind,P);

% Fix the point indeces in edges:
if size(E,2)
  E(1:2,:) = reshape(Pmap2(E(1:2,:)),2,size(E,2));
  if any(any(E(1:2,:) == 0))
    warning('Removed points still in the edges!');
  end
end

% Fix the point indeces in triangles:
if size(T,2)
  T(4:6,:) = reshape(Pmap2(T(4:6,:)),3,size(T,2));
  if any(any(T(4:6,:) == 0))
    warning('Removed points still in the triangles!');
  end
end
