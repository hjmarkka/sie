function [coord,etopol] = points2triangles(tp)
% function [coord,etopol] = points2triangles(tp)
%
% Converts triangles listed with point coordinates into points (coord)
% and triangles listed using point indexes of the vertices (etopol).

% Number of triangles:
n = size(tp,1);
if mod(n,3) ~= 0
  error('Number of triangle points should be 3*m!');
end
nt = n/3;

% Points and triangles:
coord = NaN*ones(n,3);
etopol = zeros(nt,3);

% Loop over the triangles:
np = 0;
k = 0;
for i = 1:nt
  % Check if the points are already found:
  for j = 1:3
    k = k + 1;
    ind = find(all(coord(1:np,:) == tp(k*ones(1,np),:),2));
    if length(ind) == 0
      % Add new point:
      np = np + 1;
      coord(np,:) = tp(k,:);
      ind = np;
    end
    % Assign point index of the vertex:
    etopol(i,j) = ind;
  end
end

% Remove rest of the points:
coord = coord(1:np,:);
