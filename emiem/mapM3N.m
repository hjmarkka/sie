function [map,ind] = mapM2N(l,obj1,obj2)
%function [map,ind] = mapM2N(l,obj1,[obj2])
%
% Create M <-> N mapping from obj1 to obj2. Zeros in obj1 are not mapped.
%
% Input:
%   l    : Maximum index of objects 1.
%   obj1 : (1,n) row vector or (m,n) matrix of indeces of objects 1.
%   obj2 : (1,n) or same size as obj1, indeces of objects 2. Default: 1:n.
%
% Output:
%  map : sparse (n,l) matrix of mapping from obj1 to obj2.
%
% Optional non-sparse output:
%  map : (1,*) mapping between obj1 and obj2: The i'th object 1 maps to
%        map(ind(i):ind(i+1)-1).
%  ind : (1,n+1) starting indeces of map: The i'th object 1 has indeces
%         ind(i):ind(i+1)-1.
%
% Example: Mapping from points to triangles:
%   np = size(P,2);
%   TP = mapM2N(np,T(4:6,:));      % Triangles of point i: find(TP(:,i))
% or
%   [TP,tp] = mapM2N(np,T(4:6,:)); % Triangles of point i: TP(tp(i):tp(i+1)-1)

% Sizes:
[m,n] = size(obj1);
% ----------------------------------------------------------------------------
% Default values:
if nargin < 3
  obj2 = [];
end
if length(obj2) == 0
  obj2 = 1:n;
end
% ----------------------------------------------------------------------------
% Check sizes and optionally replicate obj2:
[m2,n2] = size(obj2);
if       (m2 == 1) & (n2 == n)
  obj2 = obj2(ones(1,m),:);
elseif ~((m2 == m) & (n2 == n))
  error('Obj2 should be (1,n) or (m,n) where [m,n] = size(obj1)!');
end
% ----------------------------------------------------------------------------
% Zeros are mapped to l+1 and later removed:
obj1(obj1 == 0) = l+1;

if nargout == 1
  % Create sparse matrix:
  map = sparse(obj2, obj1, 1, n, l+1);
  
  % Remove extra column where zeros are mapped:
  map(:,l+1) = [];
else
  % --------------------------------------------------------------------------
  % Optional output:
  uniq = unique([obj1(:) obj2(:)], 'rows'); 
  ind = [1 1+cumsum(sparse(ones(1,size(uniq,1)), uniq(:,1).', 1, 1, l+1))];
  
  % Remove l+1 values where zeros are mapped:
  map = uniq(:,2).';
  map(ind(l+1):ind(l+2)-1) = [];
  ind(l+2) = [];
end
% ----------------------------------------------------------------------------
