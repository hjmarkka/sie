function I = duffy3d2(r,pk, p1,p2, p3,P0d,w0d,k)

[P1,w1] = linmap3matlab(pk,r,p1,p2,P0d,w0d);
[P2,w2] = linmap3matlab(pk,r,p2,p3,P0d,w0d);
[P3,w3] = linmap3matlab(pk,r,p3,p1,P0d,w0d);


%plot_points(P1)
%hold on
%plot_points(P2)
%plot_points(P3)

I1 = 0;
I2 = 0;
I3 = 0;

%keyboard

for j = 1:length(w1)
    
    rp1 = P1(:,j);
    R1 = norm(r - rp1);
    I1 = I1 + exp(i*k*R1)./(4*pi*R1)*w1(j);

    rp2 = P2(:,j);
    R2 = norm(r - rp2);
    I2 = I2 + exp(i*k*R2)./(4*pi*R2)*w2(j);

    rp3 = P3(:,j);
    R3 = norm(r - rp3);
    I3 = I3 + exp(i*k*R3)./(4*pi*R3)*w3(j);
  

end

I=I1+I2+I3;


