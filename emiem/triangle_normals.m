function [N,p1,p2,p3,v1,v2,v3,A] = triangle_normals(P,T,tind)
% function [N,p1,p2,p3,v1,v2,v3,A] = triangle_normals(P,T,tind)
%
% Calculate triangle unit normal vectors.

if nargin < 3
  tind = 1:size(T,2);
end

% Triangle edge vectors:
p1 = P(:,T(4,tind));
p2 = P(:,T(5,tind));
p3 = P(:,T(6,tind));
v3 = p2-p1;
v1 = p3-p2;
v2 = p1-p3;
A = T(7,tind);  % Areas.
  
% Triangle plane normal vectors:
N = cross(v3,-v2);
N = N./[A;A;A]/2;
