function [common,common2] = common_indeces(I1,I2)
% function [common,common2] = common_indeces(I1,I2)
%
% Check for common index groups I2 agaist I1.
%
% Returns true, if all the integer indeces in columns of I2 are same as
% indeces in some of the column of I1 in any of the possible order.
%
% common - sparse logical (m,n) matrix, m = size(I1,2), n = size(I2,2)
%          or optionally I1 and I2 indeces of non-zeros entries of the
%          sparse matrix in two output variables common1 and common2.
%
%              common = sparse(common1,common2,ones(length(common1)))
%              [common1,common2] = find(common);

% Get sizes:
[m,M] = size(I1);
[n,N] = size(I2);
l = min(M,N);

% Fast exit if either edges missing:
if (m == 0) | (n == 0) | (l == 0)
  if nargout > 1
    common = zeros(0,1);
    common2 = common;
  else
    common = sparse(M,N);
  end
  return
end

% m and n should be the same:
if m ~= n
  error('size(I1,1) should be size(I2,1)!');
end
l = m;

% Check if mex file check_indeces exists:
if exist('check_indeces') == 3
  if m == 1
    ind = check_indeces(I1,I2);
  else
    % Sort indeces:
    ind = check_indeces(sort(I1),sort(I2));
  end
  if nargout > 1
    common  = ind(:,1);
    common2 = ind(:,2);
  else
    common = sparse(ind(:,1), ind(:,2), ones(size(ind,1),1), M, N);
  end
else
  
  o1 = ones(1,M);
  o2 = ones(1,N);

  if     l == 1
    i1 = I1(1*o2,:).';
    i2 = I2(1*o1,:);
    common = (i1 == i2);
  elseif l == 2
    % Edges are common if both point indeces are the same:
    p11 = I1(1*o2,:).';
    p12 = I1(2*o2,:).';
    p21 = I2(1*o1,:);
    p22 = I2(2*o1,:);
    common = ((p11 == p21) & (p12 == p22)) | ((p11 == p22) & (p12 == p21));
  elseif l == 3
    % Triangles are common if all the three edge indeces are the same:
    e11 = I1(1*o2,:).';
    e12 = I1(2*o2,:).';
    e13 = I1(3*o2,:).';
    e21 = I2(1*o1,:);
    e22 = I2(2*o1,:);
    e23 = I2(3*o1,:);
    common = ((e11 == e21) & (e12 == e22) & (e13 == e23)) | ...
	     ((e11 == e21) & (e12 == e23) & (e13 == e22)) | ...
	     ((e11 == e22) & (e12 == e21) & (e13 == e23)) | ...
	     ((e11 == e22) & (e12 == e23) & (e13 == e21)) | ...
	     ((e11 == e23) & (e12 == e21) & (e13 == e22)) | ...
	     ((e11 == e23) & (e12 == e22) & (e13 == e21));
  elseif l == 4
    % Tetrahedra are common if all the four triangle indeces are the same:
    t11 = I1(1*o2,:).';
    t12 = I1(2*o2,:).';
    t13 = I1(3*o2,:).';
    t14 = I1(4*o2,:).';
    t21 = I2(1*o1,:);
    t22 = I2(2*o1,:);
    t23 = I2(3*o1,:);
    t24 = I2(4*o1,:);
    common = ((t11 == t21) & (t12 == t22) & (t13 == t23) & (t14 == t24)) | ...
	     ((t11 == t21) & (t12 == t22) & (t13 == t24) & (t14 == t23)) | ...
	     ((t11 == t21) & (t12 == t23) & (t13 == t22) & (t14 == t24)) | ...
	     ((t11 == t21) & (t12 == t23) & (t13 == t24) & (t14 == t22)) | ...
	     ((t11 == t21) & (t12 == t24) & (t13 == t22) & (t14 == t23)) | ...
	     ((t11 == t21) & (t12 == t24) & (t13 == t23) & (t14 == t22)) | ...
	     ((t11 == t22) & (t12 == t21) & (t13 == t23) & (t14 == t24)) | ...
	     ((t11 == t22) & (t12 == t21) & (t13 == t24) & (t14 == t23)) | ...
	     ((t11 == t22) & (t12 == t23) & (t13 == t21) & (t14 == t24)) | ...
	     ((t11 == t22) & (t12 == t23) & (t13 == t24) & (t14 == t21)) | ...
	     ((t11 == t22) & (t12 == t24) & (t13 == t21) & (t14 == t23)) | ...
	     ((t11 == t22) & (t12 == t24) & (t13 == t23) & (t14 == t21)) | ...
	     ((t11 == t23) & (t12 == t21) & (t13 == t22) & (t14 == t24)) | ...
	     ((t11 == t23) & (t12 == t21) & (t13 == t24) & (t14 == t22)) | ...
	     ((t11 == t23) & (t12 == t22) & (t13 == t21) & (t14 == t24)) | ...
	     ((t11 == t23) & (t12 == t22) & (t13 == t24) & (t14 == t21)) | ...
	     ((t11 == t23) & (t12 == t24) & (t13 == t21) & (t14 == t22)) | ...
	     ((t11 == t23) & (t12 == t24) & (t13 == t22) & (t14 == t21)) | ...
	     ((t11 == t24) & (t12 == t21) & (t13 == t22) & (t14 == t23)) | ...
	     ((t11 == t24) & (t12 == t21) & (t13 == t23) & (t14 == t22)) | ...
	     ((t11 == t24) & (t12 == t22) & (t13 == t21) & (t14 == t23)) | ...
	     ((t11 == t24) & (t12 == t22) & (t13 == t23) & (t14 == t21)) | ...
	     ((t11 == t24) & (t12 == t23) & (t13 == t21) & (t14 == t22)) | ...
	     ((t11 == t24) & (t12 == t23) & (t13 == t22) & (t14 == t21));
  else
    error(sprintf('Common_indeces of length %d not implented!',l));
  end
  
  if nargout > 1
    [common,common2] = find(common);
  else
    common = sparse(common);
  end
end
