function c = dot3(u,v)
%
% Dot product of 3-vectors without complex conjugate.

c = sum(u.*v);