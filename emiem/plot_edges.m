function plot_edges(point,edge,do_numbers,color,Eind)
% function plot_edges(point,edge,[do_numbers,color,Eind])
%
% point      - (N,number_of_points). N is 2 or 3.
% edge       - (3,number_of_edges).
% do_numbers - If 1, plot edge numbers.     Default: 0.
% color      - Color.                       Default: 'r'.
% Eind       - Only these edges.            Default: 1:size(edge,2).

% Default values:
if nargin < 5
  Eind = [];
  if nargin < 4
    color = [];
    if nargin < 3
      do_numbers = [];
    end
  end
end
if length(do_numbers) == 0
  do_numbers = 0;
end
if length(color) == 0
  color = 'r-';
end
if length(Eind) == 0
  Eind = 1:size(edge,2);
end

P1 = point(:,edge(1,Eind));
P2 = point(:,edge(2,Eind));

X = [P1(1,:); P2(1,:)];
Y = [P1(2,:); P2(2,:)];

N = size(point,1);
if N == 3
  Z = [P1(3,:); P2(3,:)];
  plot3(X, Y, Z, color);
else
  plot(X, Y, color);
end

if do_numbers
  P = (P1+P2)/2;  % Center points of the basis edges
  plot_numbers(P, Eind);
end

