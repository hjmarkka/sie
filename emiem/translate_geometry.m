function [pt,et,tt,bt] = translate_geometry(translation,...
                                            point,edge,triangle,basis)
% function [pt,et,tt,bt] = translate_geometry(translation,...
%                                             point,edge,triangle,basis)
% Translates geometry:
%
% translation: (3,1)                x          - Translation vector.
%                                   y
%                                   z
% point,edge,triangle,basis         See "help connect".

%disp('Translating geometry...');

% Translate points:
pt = point+translation(:,ones(1,size(point,2)));

% Translate triangle centroids:
tt = triangle;
if size(tt,2) > 0
  tt(8:10,:) = tt(8:10,:) + translation(:,ones(1,size(tt,2)));
end

% No translation needed for edge and basis:
et = edge;
bt = basis;
