clear
path(path,'~/GITrepos/sie/emiem');
path(path,'~/mysimlabs');
path(path,'~/iso2mesh');

N=32;

N_th = 90;
N_phi = 32;

EcH = zeros(3,N_th*N_phi);
EcV = zeros(3,N_th*N_phi);


for i1 = 1:N
  filename = ['out_',int2str(i1),'.h5']

EH = h5read(filename,'/EH_r') + i*h5read(filename,'/EH_i');
EV = h5read(filename,'/EV_r') + i*h5read(filename,'/EV_i');
r= h5read(filename,'/angles');

EcH = EcH + EH/N;
EcV = EcV + EV/N;

end


Em = zeros(1,N_th);
Ep = zeros(1,N_th);

las = 1;
for i1 = 1:N_phi
     for i2 = 1:N_th

	 %  Z(i1,i2) = dot(EcH(:,las), EcH(:,las)) + dot(EcV(:,las), EcV(:,las));


per = dot(EcH(1,las), EcH(1,las)) + dot(EcV(1,las), EcV(1,las));
par = dot(EcH(2,las), EcH(2,las)) + dot(EcV(2,las), EcV(2,las));

Z(i1,i2) = par+per;
Z2(i1,i2) = (per-par);


           r2(i2) = r(2,i2);
           phi2(i1) = r(1,las);

	   Em(i2) = Em(i2) + Z(i1,i2)/N_phi;
           Ep(i2) = Ep(i2) + Z2(i1,i2)/N_phi;
           las = las + 1;
     end
end 

phi2 = [phi2,2*pi];
zz = ([Z;Z(1,:)]);
zz2 = ([Z2;Z2(1,:)]);

[R PHI] = meshgrid(r2,phi2);

%figure(1), surf(R.*cos(PHI)*180/pi, R.*sin(PHI)*180/pi, zz,  'EdgeColor','none','facecolor','interp');
%xlabel('x')
%ylabel('y')
%zlabel('z')


%figure(2), surf(R.*cos(PHI)*180/pi, R.*sin(PHI)*180/pi, zz2,  'EdgeColor','none','facecolor','interp');
%xlabel('x')
%ylabel('y')
%zlabel('z')

figure(2), subplot(1,2,1), semilogy(1:90,Em)
  figure(2), subplot(1,2,2), plot(1:90,Ep./Em)


  
Em = zeros(1,N_th);
Ep = zeros(1,N_th);
Z=zeros(N_phi,N_th);
Z2=zeros(N_phi,N_th);

for i0 = 1:N

 filename = ['out_',int2str(i0),'.h5']

EH = h5read(filename,'/EH_r') + i*h5read(filename,'/EH_i');
EV = h5read(filename,'/EV_r') + i*h5read(filename,'/EV_i');

EicH = EH - EcH;
EicV = EV - EcV;

%EicH = EH;
%EicV = EV;


las = 1;
for i1 = 1:N_phi
     for i2 = 1:N_th
     
	      per = dot(EicH(1,las), EicH(1,las)) + dot(EicV(1,las), EicV(1,las));
              par = dot(EicH(2,las), EicH(2,las)) + dot(EicV(2,las), EicV(2,las));

  

       Z(i1,i2) = Z(i1,i2) + (per+par)/N;
       Z2(i1,i2) = Z2(i1,i2) + (per-par)/N;


           r22(i2) = r(2,i2);
           phi22(i1) = r(1,las);

	   Em(i2) = Em(i2) + Z(i1,i2)/N_phi;
           Ep(i2) = Ep(i2) + Z2(i1,i2)/N_phi;
           las = las + 1;
     end
end 

end
     
phi2 = [phi22,2*pi];
zz = ([Z;Z(1,:)]);
zz2 = ([Z2;Z2(1,:)]);

[R PHI] = meshgrid(r22,phi2);

figure(8), surf(R.*cos(PHI)*180/pi, R.*sin(PHI)*180/pi, zz,  'EdgeColor','none','facecolor','interp');
xlabel('x')
ylabel('y')
zlabel('z')
view(2)

figure(9), surf(R.*cos(PHI)*180/pi, R.*sin(PHI)*180/pi, zz2./zz,  'EdgeColor','none','facecolor','interp');
xlabel('x')
ylabel('y')
zlabel('z')
view(2)

figure(10), subplot(1,2,1), semilogy(1:90,Em)
  figure(10), subplot(1,2,2), plot(1:90,Ep./Em)
