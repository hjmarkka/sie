clear
pkg load hdf5oct

path(path,'~/iso2mesh')
load geomball7

coord = P;
etopol = T(4:6,:);



 filename = 'mesh.h5'

    %h5create(filename,'/coord',[size(coord')]);
    %h5write(filename,'/coord',coord');

    %h5create(filename,'/etopol',[size(etopol')]);
    %h5write(filename,'/etopol',int32(etopol'));

%hdf5write('mesh.h5','/coord',coord);
%hdf5write('mesh.h5','/etopol',int32(etopol),'WriteMode','append');

figure(1), plotmesh(coord',etopol')


load x.h5

x= J_r + i*J_i;	     
	     
xE = x(1:length(x)/2);
xdE = x(length(x)/2+1:end);

Ex = xE(1:3:end);
Ey = xE(2:3:end);
Ez = xE(3:3:end);

Ess = [Ex,Ey,Ez].';

dEx = xdE(1:3:end);
dEy = xdE(2:3:end);
dEz = xdE(3:3:end);


for i1 = 1:size(etopol,2)

Tc = coord(:,etopol(:,i1));


cp(:,i1) = (Tc(:,1) + Tc(:,2) + Tc(:,3))/3;

nv = cross(Tc(:,3)-Tc(:,1),Tc(:,2)-Tc(:,1));
 nvec(:,i1) = nv/sqrt(sum(nv.*nv));



   nE(i1) = dot(xE(3*(i1-1)+1:3*i1), nvec(:,i1)); 
ndE(i1) = dot(xdE(3*(i1-1)+1:3*i1), nvec(:,i1)); 

end

	     
figure(1), plot(1:980,real(ndE), 1:980,-real(nE)/5)
figure(2), plot(1:980,imag(ndE), 1:980,-imag(nE)/5)
