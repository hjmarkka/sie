clear
path(path,'~/GITrepos/sie/emiem');
path(path,'~/mysimlabs');
path(path,'~/iso2mesh');
l=10;
N=20;
h=0.1;
clx=0.5;

p1 = [-l/2, -l/2, 0]';
p2 = [l/2, -l/2, 0]';
p3 = [l/2, l/2, 0]';
p4 = [-l/2, l/2, 0]';

[P,E,T,B] = make_rectangle(p1,p2,p3,p4,N,N);

for i0 = 1:32
[f,x,y] = rsgene2D(N+1,l,h,clx,clx);

for i1 = 1:N+1
for i2 =1:N+1
	  ind=(i1-1)*(N+1) + i2;
P(3,ind)=f(i1,i2);	  
end
end

%figure(1), imagesc(f)
coord=P;
etopol=T(4:6,:);

%[cnew,enew] = meshresample(coord',etopol',5);

  figure(2), plotmesh(coord',etopol');

filename = ['mesh',int2str(i0),'.h5']
hdf5write(filename,'/coord',coord);
hdf5write(filename,'/etopol',int32(etopol),'WriteMode','append');

end
