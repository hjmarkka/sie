clear

path(path,'~/iso2mesh')
path(path,'~/Koodit/emiem')
path(path,'~/Koodit/mlfma/mlfma_oma')
path(path,'~/Koodit/MIE')

c0=[0;0;0];
r=1;
tsize = 0.3
maxvol = 10*tsize^3;

%[node,face,elem]=meshabox([-1,-1,-1],[1,1,1],1.2);
%[node,face,elem] = meshasphere(c0,r,tsize,maxvol);


cp=[0;0;0];
%[P,E,T,B] = make_ball(cp,1,10,10,4);
%[P,E,T,B] = make_box(cp,1,1,1,6,6,6,2,'y');
load geomball7

coord = P;
etopol = T(4:6,:);

%s_par1
%load grs10k
%coord=p';
%etopol=t';

hdf5write('mesh.h5','/coord',coord);
hdf5write('mesh.h5','/etopol',int32(etopol),'WriteMode','append');

%coord = hdf5read('s_par1.h5','coord');
%etopol = hdf5read('s_par1.h5','etopol');

figure(1), plotmesh(coord',etopol')

[P2,w] = sample_points(90,2);
P2=1e6*P2;
hdf5write('field_points.h5','/coord',P2);


E=hdf5read('A.h5','/A_r') + i*hdf5read('A.h5','/A_i');
%xx=hdf5read('x.h5','/J_r') + i*hdf5read('x.h5','/J_i');

%-----------MIE--------------
lambda = 0.5*pi
R = 1;
k=2*pi/lambda;
f=299792458/(lambda);
omega = 2*pi*f;
mu0 = 4*pi*1e-7;

n=40;
[cp,cm]=vecwacoplaw(1,f,n);
%cr = vecwacopcs(R,f,n);
epsr=[1,4+i*1e-16];
myyr=[1,1];
sig=[0,0];
[COin,cr] = vecwacohs(R,f,n,epsr,myyr,sig,'in');

cs=cr.*cp;
[Es_mie,Escurl]=vecwafd(P2,cs,f,1,1,0,'out');

figure(2),plot(1:size(Es_mie,2),real(Es_mie),1:size(Es_mie,2),real(E),'--' )
figure(3),plot(1:size(Es_mie,2),imag(Es_mie),1:size(Es_mie,2),imag(E),'--' )

  HH = Escurl./(i*omega*mu0);

%figure(4),plot(1:size(Escurl,2),real(HH),1:size(Es_mie,2),real(E) )
%figure(5),plot(1:size(Escurl,2),imag(HH),1:size(Es_mie,2),imag(E) )


S=hdf5read('mueller.h5','/mueller');



rcs = hdf5read('rcs.h5','/mueller');

lambda = (2*pi)/k;
RR = 1000*lambda;
phi = 0.0;
phi2 = pi/2;
N_theta = 180;

for i2 = 0: N_theta

   theta = pi * (i2) / (N_theta);
  
   r(1) = RR*sin(theta)*cos(phi);
   r(2) = RR*sin(theta)*sin(phi);
   r(3) = RR*cos(theta);
   PP(:,i2+1) = r;
   
   r2(1) = RR*sin(theta)*cos(phi2);
   r2(2) = RR*sin(theta)*sin(phi2);
   r2(3) = RR*cos(theta);
   PP2(:,i2+1) = r2;
   
   th(i2+1) = theta;
end

[Es_mie2,Escurl2]=vecwafd(PP,cs,f,1,1,0,'out');
rcs_mieE = 4*pi* RR^2 / lambda^2 * dot(Es_mie2,Es_mie2);

[Es_mie3,Escurl3]=vecwafd(PP2,cs,f,1,1,0,'out');
rcs_mieH = 4*pi* RR^2 / lambda^2 * dot(Es_mie3,Es_mie3);

th = 180*th/pi;

figure(22), plot(th(1:3:end), 10*log10(rcs(1:3:end,1)),'*', ...
                 th(1:3:end), 10*log10(rcs(1:3:end,2)),'o', ...
                 th(2:end-1), 10*log10(rcs_mieE(2:end-1)),...
                 th(2:end-1), 10*log10(rcs_mieH(2:end-1)),'linewidth',1.5)
grid on
set(gca,'fontsize',12)
xlabel('\theta [deg]')
ylabel('RCS [dB]')
legend('SIE (E-plane)','SIE (H-plane)','Mie (E-plane)',['Mie (H-' ...
                    'plane)'])
%axis([0 180 1-15 15])
set(gca,'xtick', [0 30 60 90 120 150 180])

x = hdf5read('x.h5','/J_r') + i*hdf5read('x.h5','/J_i');
%x = hdf5read('rhs.h5','/J_r') + i*hdf5read('rhs.h5','/J_i');

xE = x(1:length(x)/2);
xdE = x(length(x)/2+1:end);

Ex = xE(1:3:end);
Ey = xE(2:3:end);
Ez = xE(3:3:end);

Ess = [Ex,Ey,Ez].';

dEx = xdE(1:3:end);
dEy = xdE(2:3:end);
dEz = xdE(3:3:end);





for i1 = 1:length(T)

    coor = P(:,T(4:6,i1));

    dp1(:,i1) = (coor(:,1) +coor(:,2) + coor(:,3))/3;  
end 

[cp,cm]=vecwacoplaw(1,f,n);
[COin,cr] = vecwacohs(R,f,n,epsr,myyr,sig,'in');
cs=cr.*cp;
[E_mie_out,Escurl]=vecwafd(dp1,cs,f,1,1,0,'out');
[E_mie_in,Escurl]=vecwafd(dp1,cp,f,1,1,0,'in');
E_mie = E_mie_in+E_mie_out;

figure(100), fill_triangles(P,E,T,real(Ez))
axis equal
figure(101), fill_triangles(P,E,T,real(E_mie(3,:)))
axis equal


