module field
use common
use integration_points
implicit none
 

contains


function fields(matrices, mesh, r) result(EH)
type (mesh_struct) :: mesh
type (data) :: matrices

integer :: test_T, i1, te, test, ti, tj
double precision :: T_coord(3,3), T_dN(3,3), T_nxdN(3,3), T_nvec(3), r(3), rp(3), rhat(3)
double precision, allocatable :: P0_tri(:,:), Pt(:,:), shape_tri(:,:)
double precision, allocatable :: w0_tri(:),wt(:)
double complex :: alok(3), E(3), a(3), b(3), c(3), a2(3), b2(3), c2(3), H(3), alok2(3,3), EH(3,2)
double precision :: signt, el, At

call inttri(P0_tri,w0_tri,5)

allocate(shape_tri(3,size(w0_tri)))
shape_tri(1,:) = 1.0 - P0_tri(1,:) - P0_tri(2,:)
shape_tri(2,:) = P0_tri(1,:)
shape_tri(3,:) = P0_tri(2,:)

allocate(Pt(3,size(w0_tri)))
allocate(wt(size(w0_tri)))

E(:) = dcmplx(0.0,0.0)
H(:) =dcmplx(0.0,0.0)

do test_T = 1, mesh%N_tri

   T_coord = mesh%coord(:,mesh%etopol(:,test_T))
   call gradshape_tri(T_dN,T_nxdN, T_nvec, T_coord)
   At = tri_area(T_coord)

   call linmap_tri(Pt, wt, T_coord, P0_tri, W0_tri)

   alok(:) = dcmplx(0.0,0.0)
   alok2(:,:) = dcmplx(0.0,0.0)

   do i1 = 1,size(w0_tri)
      rp=Pt(:,i1)
      rhat = (r - rp)/norm(r,rp)

      alok = alok + Gr(r,rp,dcmplx(mesh%k,0.0)) * shape_tri(:,i1) * wt(i1)
      
      alok2(:,1) = alok2(:,1) + grad_G(r,rp,dcmplx(mesh%k,0.0)) * shape_tri(1,i1)* wt(i1)
      alok2(:,2) = alok2(:,2) + grad_G(r,rp,dcmplx(mesh%k,0.0)) * shape_tri(2,i1)* wt(i1)
      alok2(:,3) = alok2(:,3) + grad_G(r,rp,dcmplx(mesh%k,0.0)) * shape_tri(3,i1)* wt(i1)

   end do

   do te = 1, 3
      test = mesh%etopol_edges(te,test_T)
      signt = RWG_sign(mesh, test, test_T,te)
      
      el = norm(mesh%coord(:,mesh%edges(1,test)), mesh%coord(:,mesh%edges(2,test)))

      ti = mesh%Eori(1,te)
      tj = mesh%Eori(2,te)

      a =  signt * el * (T_nxdN(:,tj)*alok(ti) - T_nxdN(:,ti)*alok(tj)) * matrices%x(test)
      
      b = signt * el * (alok2(:,1) + alok2(:,2) + alok2(:,3)) * matrices%x(test)

      c = signt * el * (crossRC(T_nxdN(:,tj),alok2(:,ti)) - crossRC(T_nxdN(:,ti),alok2(:,tj))) * matrices%x(mesh%N_edge+test)

      a2 =  signt * el * (T_nxdN(:,tj)*alok(ti) - T_nxdN(:,ti)*alok(tj)) * matrices%x(mesh%N_edge+test)
      
      b2 = signt * el * (alok2(:,1) + alok2(:,2) + alok2(:,3)) * matrices%x(mesh%N_edge+test)

      c2 = signt * el * (crossRC(T_nxdN(:,tj),alok2(:,ti)) - crossRC(T_nxdN(:,ti),alok2(:,tj))) * matrices%x(test)
     
       E = E - sqrt(mu/epsilon) * dcmplx(0.0,mesh%k)*a + &
            1.0/At * sqrt(mu/epsilon)/dcmplx(0.0,mesh%k)*b - c
          
       H = H - 1.0/sqrt(mu/epsilon) * dcmplx(0.0,mesh%k)*a2 + &
            1.0/At * 1.0/sqrt(mu/epsilon)/dcmplx(0.0,mesh%k)*b2 + c2

       
   end do

end do

EH(:,1) = E
EH(:,2) = H

end function fields


function farfield_efie(matrices, mesh, r) result(E)


type (mesh_struct) :: mesh
type (data) :: matrices

integer :: test_T, i1, te, test, ti, tj
double precision :: T_coord(3,3), T_dN(3,3), T_nxdN(3,3), T_nvec(3), r(3), rp(3), rhat(3)
double precision, allocatable :: P0_tri(:,:), Pt(:,:)
double precision, allocatable :: w0_tri(:),wt(:)
double complex :: alok,alok2, E(3), b(3), b2(3), E2(3), codE(3), coE(3)
double precision :: signt, el, T_area

call inttri(P0_tri,w0_tri,5)

allocate(Pt(3,size(w0_tri)))
allocate(wt(size(w0_tri)))

E(:) = dcmplx(0.0,0.0)
E2(:) =dcmplx(0.0,0.0)


do test_T = 1, mesh%N_tri

   T_coord = mesh%coord(:,mesh%etopol(:,test_T))
   T_area = tri_area(T_coord)
   call gradshape_tri(T_dN,T_nxdN, T_nvec, T_coord)

   call linmap_tri(Pt, wt, T_coord, P0_tri, W0_tri)

   alok = dcmplx(0.0,0.0)
   alok2 = dcmplx(0.0,0.0)
   do i1 = 1,size(w0_tri)
      rp=Pt(:,i1)
      rhat = (r - rp)/norm(r,rp)

      alok = alok + Gr(r,rp,dcmplx(mesh%k,0.0)) * wt(i1)
      alok2 = alok2 + n_gradGr(r,rp,dcmplx(mesh%k,0.0),T_nvec)*wt(i1)
   end do

   coE = matrices%x(3*(test_T-1)+1:3*(test_T-1)+3)/sqrt(T_area)
   codE = matrices%x(3*mesh%N_tri + 3*(test_T-1)+1: 3*mesh%N_tri+ 3*(test_T-1)+3)/(T_area)
  
   E = E - alok2*coE - alok*codE 


   !E = E - alok*(codE)! - dcmplx(0.0,mesh%k)*coE*dot_product(T_nvec,rhat))

   !E = E - alok2*matrices%x(3*(test_T-1)+1:3*(test_T-1)+3)/sqrt(T_area)
end do

end function farfield_efie

subroutine calc_fields(matrices,mesh) 
type (mesh_struct) :: mesh
type (data) :: matrices

integer :: i1
double precision :: r(3)

allocate(matrices%E_field(3,size(matrices%field_points,2)))

do i1 = 1, size(matrices%field_points,2)
   r = matrices%field_points(:,i1)

   matrices%E_field(:,i1) = farfield_efie(matrices, mesh, r)

end do

end subroutine

!_______________________________________________________________
subroutine compute_rcs(matrices,mesh)
type (data) :: matrices
type (mesh_struct), intent(in) :: mesh
integer :: N_theta, i2
double precision :: r(3), r2(3), RR, lambda, theta, phi, phi2
double complex :: E(3), E2(3)


N_theta = 180

allocate(matrices%rcs(N_theta+1,2))

lambda = (2*pi)/mesh%k
RR = 100*lambda

phi = 0.0!pi/2.0
phi2 = pi/2.0

do i2 = 0, N_theta

   theta = pi * (i2) / (N_theta) 
  
   r(1) = RR*sin(theta)*cos(phi)
   r(2) = RR*sin(theta)*sin(phi)
   r(3) = RR*cos(theta)

   r2(1) = RR*sin(theta)*cos(phi2)
   r2(2) = RR*sin(theta)*sin(phi2)
   r2(3) = RR*cos(theta)

   E = farfield_efie(matrices, mesh, r)
   E2 = farfield_efie(matrices, mesh, r2)

   matrices%rcs(i2+1,1) = 4*pi*RR**2/(lambda**2) * dble(dot_product(E,E))
    matrices%rcs(i2+1,2) = 4*pi*RR**2/(lambda**2) * dble(dot_product(E2,E2))  

end do


end subroutine compute_rcs



end module field
