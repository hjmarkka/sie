module matvec
use common
use mpi
implicit none 

contains

subroutine matmul_pmchwt_mpi(mesh, matrices)
type (mesh_struct) :: mesh
type (data) :: matrices

integer :: ierr

matrices%Ax(:) = dcmplx(0.0,0.0)

matrices%Ax(mesh%N1:mesh%N2) = matmul(matrices%AJ_loc, matrices%x)  
matrices%Ax(mesh%N_edge+mesh%N1:mesh%N_edge+mesh%N2) = matmul(matrices%AM_loc, matrices%x)



call MPI_ALLREDUCE(MPI_IN_PLACE,matrices%Ax,size(matrices%Ax),MPI_DOUBLE_COMPLEX,MPI_SUM,MPI_COMM_WORLD,ierr)

end subroutine matmul_pmchwt_mpi

end module matvec
