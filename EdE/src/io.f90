module io
use hdf5

implicit none

contains 

subroutine write2file(A,fname)
double complex, intent(in) :: A(:,:)
CHARACTER(LEN=28), intent(in) :: fname
CHARACTER(LEN=28) :: filename
!CHARACTER(LEN=7), PARAMETER :: filename = "A.h5" ! File name
CHARACTER(LEN=3), PARAMETER :: dsetname1 = "A_r" ! Dataset name
CHARACTER(LEN=3), PARAMETER :: dsetname2 = "A_i" ! Dataset name

INTEGER(HID_T) :: file_id       ! File identifier
INTEGER(HID_T) :: dset_id1       ! Dataset identifier
INTEGER(HID_T) :: dset_id2       ! Dataset identifier
INTEGER(HID_T) :: dspace_id     ! Dataspace identifier


INTEGER(HSIZE_T), DIMENSION(2) :: dims  ! Dataset dimensions
INTEGER     ::    rank = 2                       ! Dataset rank

INTEGER     ::   error ! Error flag
!INTEGER     :: i, j

filename = fname
dims = (/size(A,1),size(A,2)/)
     
CALL h5open_f(error)

!
! Create a new file using default properties.
!
CALL h5fcreate_f(filename, H5F_ACC_TRUNC_F, file_id, error)

!
! Create the dataspace.
!
CALL h5screate_simple_f(rank, dims, dspace_id, error)

!
! Create and write dataset using default properties.
!
CALL h5dcreate_f(file_id, dsetname1, H5T_NATIVE_DOUBLE, dspace_id, &
                       dset_id1, error, H5P_DEFAULT_F, H5P_DEFAULT_F, &
                       H5P_DEFAULT_F)

CALL h5dwrite_f(dset_id1, H5T_NATIVE_DOUBLE, real(A), dims, error)

CALL h5dcreate_f(file_id, dsetname2, H5T_NATIVE_DOUBLE, dspace_id, &
                       dset_id2, error, H5P_DEFAULT_F, H5P_DEFAULT_F, &
                       H5P_DEFAULT_F)

CALL h5dwrite_f(dset_id2, H5T_NATIVE_DOUBLE, imag(A), dims, error)

!
! End access to the dataset and release resources used by it.
!
CALL h5dclose_f(dset_id1, error)
CALL h5dclose_f(dset_id2, error)
!
! Terminate access to the data space.
!
CALL h5sclose_f(dspace_id, error)

!
! Close the file.
!
CALL h5fclose_f(file_id, error)

!
! Close FORTRAN interface.
!
CALL h5close_f(error)

end subroutine write2file

!_______________________________________________________________________

subroutine real_write2file(A,fname)
double precision, intent(in) :: A(:,:)
CHARACTER(LEN=28), intent(in) :: fname
CHARACTER(LEN=28) :: filename 

CHARACTER(LEN=7), PARAMETER :: dsetname1 = "mueller" ! Dataset name


INTEGER(HID_T) :: file_id       ! File identifier
INTEGER(HID_T) :: dset_id1       ! Dataset identifier
INTEGER(HID_T) :: dspace_id     ! Dataspace identifier


INTEGER(HSIZE_T), DIMENSION(2) :: dims  ! Dataset dimensions
INTEGER     ::    rank = 2                       ! Dataset rank

INTEGER     ::   error ! Error flag
!INTEGER     :: i, j

dims = (/size(A,1),size(A,2)/)
filename = fname  
 
CALL h5open_f(error)

!
! Create a new file using default properties.
!
CALL h5fcreate_f(filename, H5F_ACC_TRUNC_F, file_id, error)

!
! Create the dataspace.
!
CALL h5screate_simple_f(rank, dims, dspace_id, error)

!
! Create and write dataset using default properties.
!
CALL h5dcreate_f(file_id, dsetname1, H5T_NATIVE_DOUBLE, dspace_id, &
                       dset_id1, error, H5P_DEFAULT_F, H5P_DEFAULT_F, &
                       H5P_DEFAULT_F)

CALL h5dwrite_f(dset_id1, H5T_NATIVE_DOUBLE, A, dims, error)


!
! End access to the dataset and release resources used by it.
!
CALL h5dclose_f(dset_id1, error)

!
! Terminate access to the data space.
!
CALL h5sclose_f(dspace_id, error)

!
! Close the file.
!
CALL h5fclose_f(file_id, error)

!
! Close FORTRAN interface.
!
CALL h5close_f(error)

end subroutine real_write2file
!_---______________________________________________________________

subroutine cmplx_vec_write2file(vec,fname)
double complex, intent(in) :: vec(:)
CHARACTER(LEN=28), intent(in) :: fname
CHARACTER(LEN=28) :: filename

!CHARACTER(LEN=7), PARAMETER :: filename = "J.h5" ! File name
CHARACTER(LEN=3), PARAMETER :: dsetname1 = "J_r" ! Dataset name
CHARACTER(LEN=3), PARAMETER :: dsetname2 = "J_i" ! Dataset name
!INTEGER, PARAMETER :: NX = 1128

INTEGER(HID_T) :: file_id       ! File identifier
INTEGER(HID_T) :: dset_id1       ! Dataset identifier
INTEGER(HID_T) :: dset_id2       ! Dataset identifier
INTEGER(HID_T) :: dspace_id     ! Dataspace identifier


INTEGER(HSIZE_T), DIMENSION(1) :: dims           ! Dataset dimensions
INTEGER     ::    rank = 1                       ! Dataset rank

INTEGER     ::   error ! Error flag
!INTEGER     :: i, j

filename = fname
dims = (/size(vec)/)
     
CALL h5open_f(error)

! Create a new file using default properties.
CALL h5fcreate_f(filename, H5F_ACC_TRUNC_F, file_id, error)

! Create the dataspace.
CALL h5screate_simple_f(rank, dims, dspace_id, error)

! Create and write dataset using default properties.

CALL h5dcreate_f(file_id, dsetname1, H5T_NATIVE_DOUBLE, dspace_id, &
                       dset_id1, error, H5P_DEFAULT_F, H5P_DEFAULT_F, &
                       H5P_DEFAULT_F)

CALL h5dwrite_f(dset_id1, H5T_NATIVE_DOUBLE, real(vec), dims, error)

CALL h5dcreate_f(file_id, dsetname2, H5T_NATIVE_DOUBLE, dspace_id, &
                       dset_id2, error, H5P_DEFAULT_F, H5P_DEFAULT_F, &
                       H5P_DEFAULT_F)

CALL h5dwrite_f(dset_id2, H5T_NATIVE_DOUBLE, imag(vec), dims, error)


! End access to the dataset and release resources used by it.
CALL h5dclose_f(dset_id1, error)
CALL h5dclose_f(dset_id2, error)

! Terminate access to the data space.
CALL h5sclose_f(dspace_id, error)


! Close the file.
CALL h5fclose_f(file_id, error)

! Close FORTRAN interface.
CALL h5close_f(error)

end subroutine cmplx_vec_write2file

!________________________________________________________________________

subroutine real_vec_write2file(vec)
double precision, intent(in) :: vec(:)

CHARACTER(LEN=7), PARAMETER :: filename = "rcs.h5" ! File name
CHARACTER(LEN=3), PARAMETER :: dsetname1 = "rcs" ! Dataset name

INTEGER(HID_T) :: file_id       ! File identifier
INTEGER(HID_T) :: dset_id1       ! Dataset identifier
INTEGER(HID_T) :: dspace_id     ! Dataspace identifier


INTEGER(HSIZE_T), DIMENSION(1) :: dims     ! Dataset dimensions
INTEGER     ::    rank = 1                       ! Dataset rank

INTEGER     ::   error ! Error flag

dims  =  (/size(vec)/)

CALL h5open_f(error)

!
! Create a new file using default properties.
!
CALL h5fcreate_f(filename, H5F_ACC_TRUNC_F, file_id, error)

!
! Create the dataspace.
!
CALL h5screate_simple_f(rank, dims, dspace_id, error)

!
! Create and write dataset using default properties.
!
CALL h5dcreate_f(file_id, dsetname1, H5T_NATIVE_DOUBLE, dspace_id, &
                       dset_id1, error, H5P_DEFAULT_F, H5P_DEFAULT_F, &
                       H5P_DEFAULT_F)

CALL h5dwrite_f(dset_id1, H5T_NATIVE_DOUBLE, vec, dims, error)

!
! End access to the dataset and release resources used by it.
!
CALL h5dclose_f(dset_id1, error)

!
! Terminate access to the data space.
!
CALL h5sclose_f(dspace_id, error)

!
! Close the file.
!
CALL h5fclose_f(file_id, error)

!
! Close FORTRAN interface.
!
CALL h5close_f(error)

end subroutine real_vec_write2file








end module 

