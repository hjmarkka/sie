subroutine gmres_full_mpi(matrices,mesh)
use possu
use mpi
use matvec

use, intrinsic :: iso_c_binding
implicit none

!include 'fftw3-mpi.f03'

type (data) :: matrices
type (mesh_struct) :: mesh

double complex, dimension(:), allocatable :: r, x, w, cs, sn, g, y 
double complex, dimension(:,:), allocatable :: v, h

integer :: N, max_iter, k, j, i, iter, m, ite, T1, T2, rate, dest, ierr, N_procs, my_id
double precision :: err_tol, b_norm, error, nu, normav, normav2, res_norm
double complex :: temp(2), tmp, hr, w_dot_w, v_dot_w, h_loc
integer :: cont, M1, M2, M1v, M2v, N1, N2, block_size, N_loc 

integer :: status(MPI_STATUS_SIZE)
type(C_PTR) :: plan_in, plan_out, cdata1, cdata2,cdata3,cdata4,cdata5,cdata6
integer(C_INTPTR_T) :: Nx2, Ny2, Nz2, loc_Nz, loc_Nz_offset, loc_Mz, loc_Mz_offset
integer(C_INTPTR_T) :: alloc_local, howmany, NNN(3)
complex(C_DOUBLE_COMPLEX), pointer :: Ax(:,:,:), Ay(:,:,:), Az(:,:,:)
complex(C_DOUBLE_COMPLEX), pointer :: Bx(:,:,:), By(:,:,:), Bz(:,:,:)

call MPI_COMM_SIZE(MPI_COMM_WORLD,N_procs,ierr)
call MPI_COMM_RANK (MPI_COMM_WORLD, my_id, ierr)


N = size(matrices%rhs)

block_size = int(ceiling(dble(N)/dble(N_procs)))
N1 = 1 + my_id * block_size
N2 =  (my_id + 1) * block_size

if((my_id)*block_size > N) then
  N1 = N
end if

if((my_id+1)*block_size > N) then 
  N2 = N
end if


N_loc = N2-N1+1

!print*, N1, N2, N_loc, N

!_____________________________________________________

err_tol = mesh%tol
max_iter = mesh%restart ! restart number
m = mesh%maxit ! number of iterations / restarts


allocate(x(N))

matrices%x = matrices%rhs ! Initial guess

x=matrices%x
b_norm = dble(sqrt(dot_product(matrices%rhs,matrices%rhs)))

allocate(y(m))
   
allocate(r(N), w(N))
allocate(v(N,m+1))
allocate(h(m+1,m))
allocate(cs(m+1), sn(m+1), g(m+1))



v(:,:) = dcmplx(0.0, 0.0)
h(:,:) = dcmplx(0.0, 0.0)

cs(:) = dcmplx(0.0, 0.0)
sn(:) = dcmplx(0.0, 0.0)

w(:) = dcmplx(0.0, 0.0)
! GMRES ITERATIONS
ite = 0

if(my_id == 0) then
   print*, 'Start iterating'
end if

do iter = 1,max_iter
  
   matrices%x(:) = dcmplx(0.0,0.0)
   matrices%x = x
   
!print*,'tt'        
   !call compute_Ax_mpi(matrices, mesh, plan_in, plan_out, &
!cdata1,cdata2,cdata3)

   !matrices%Ax = matmul(matrices%A,matrices%x)
   call matmul_pmchwt_mpi(mesh, matrices)


  
   r = matrices%rhs-matrices%Ax
   !call MPI_BARRIER
   res_norm = sqrt(dot_product(r,r))
   v(:,1) = r / res_norm
   g(:) = dcmplx(0.0,0.0)
   g(1) = res_norm
 
   do i = 1, m
      call system_clock(T1,rate)
      matrices%x(:) = dcmplx(0.0,0.0)
      matrices%x = v(:,i)

      !call compute_Ax_mpi(matrices, mesh, plan_in, plan_out, &
!cdata1,cdata2,cdata3)
      !matrices%Ax = matmul(matrices%A,matrices%x)
      call matmul_pmchwt_mpi(mesh, matrices)

      w = matrices%Ax
      	
      normav = dble(sqrt(dot_product(matrices%Ax, matrices%Ax)))

     
         !_______Modified Gram-Schmidt____________________________
         do k = 1,i
            
            !v_dot_w = dot_product(v(:,k),w)
           
            !CALL MPI_ALLREDUCE(v_dot_w, h_loc, 1, MPI_DOUBLE_COMPLEX, MPI_SUM, MPI_COMM_WORLD, ierr)
            !print*,h_loc

            !h(k,i) = h_loc
            
            
            h(k,i) = dot_product(v(:,k),w)
            w = w - h(k,i)*v(:,k)
         end do

         !print*, size(w)

         !call MPI_ALLREDUCE(dot_product(w,w),w_dot_w,1,MPI_DOUBLE_COMPLEX,MPI_SUM, MPI_COMM_WORLD,ierr)

         h(i+1,i) = sqrt(dot_product(w, w))
         !h(i+1,i) = sqrt(w_dot_w)
         normav2 = dble(h(i+1,i))
         v(:,i+1) = w
     
        !print*, normav2, 'normav2'
         !stop

         !_____________Reorthogonalize?________________________________
         if(normav + 0.001*normav2 == normav) then
            do j = 1,i
               hr = dot_product(v(:,j), v(:,i+1))

               !CALL MPI_ALLREDUCE(dot_product(v(:,j),v(:,i+1)), hr, 1, MPI_DOUBLE_COMPLEX, MPI_SUM, MPI_COMM_WORLD, ierr)

               h(j,i) = h(j,i) + hr
               v(:,i+1) = v(:,i+1) - hr*v(:,j)
            end do

            h(i+1,i) = sqrt(dot_product(v(:,i+1), v(:,i+1)))

            !CALL MPI_ALLREDUCE(dot_product(v(:,i+1),v(:,i+1)), hr, 1, MPI_DOUBLE_COMPLEX, MPI_SUM, MPI_COMM_WORLD, ierr)
            !h(i+1,i) = sqrt(hr)
            !print*, 're'
         end if
      !______________________________________________________

         if(h(i+1,i) .ne. 0.0) then
            v(:,i+1) = v(:,i+1) / h(i+1,i)
         end if
      

         !_____ apply Givens rotations_________________________________
         if(i>1) then        
            do k = 1,i-1                
               tmp = cs(k)*h(k,i) - sn(k)*h(k+1,i)
               h(k+1,i) = sn(k)*h(k,i) + conjg(cs(k))*h(k+1,i)  
               h(k,i) = tmp
            end do
         end if
         !________________________________________________
         nu = dble(sqrt(dot_product(H(i:i+1,i), H(i:i+1,i))))

         if(nu .ne. 0.0) then 
     
            cs(i) = conjg(h(i,i)/nu) 
            sn(i) = -h(i+1,i)/nu  
            H(i,i) = cs(i)*H(i,i) - sn(i)*H(i+1,i);
            H(i+1,i) = 0.0;
            temp(1:2) = g(i:i+1)
            g(i) = cs(i)*temp(1) - sn(i)*temp(2)
            g(i+1) = sn(i)*temp(1) + conjg(cs(i))*temp(2)
         end if

         error  = abs(g(i+1)) / b_norm;
       
     
         if(error < err_tol) then
            y = matmul(Cinv(H(1:i,1:i)) , g(1:i));
            x = x + matmul(V(:,1:i),y) 
            exit       
         end if
  
         call system_clock(T2)

         if(my_id == 0) then
            print *,'RE (',ite+1,')','=', real(error),  'time/iter =',real(T2-T1) / real(rate)
         end if
         ite = ite + 1

         error  = abs(g(i+1)) / b_norm;

      end do

      if (error < err_tol) then
         exit
      end if

      y = matmul(Cinv(H(1:m,1:m)) , g(1:m));
      x = x + matmul(V(:,1:m),y)

      matrices%x(:) = dcmplx(0.0,0.0)
      matrices%x = x
   
      
     ! call compute_Ax_mpi(matrices, mesh, plan_in, plan_out, &
!cdata1,cdata2,cdata3)
      !matrices%Ax = matmul(matrices%A,matrices%x)
     call matmul_pmchwt_mpi(mesh, matrices)

      r = matrices%rhs - matrices%Ax
    
      if (error < err_tol) then
         exit
      end if

   end do

    matrices%x(:) = dcmplx(0.0,0.0)
   matrices%x = x

   !call MPI_ALLREDUCE(MPI_IN_PLACE,matrices%x,size(matrices%x),MPI_DOUBLE_COMPLEX,MPI_SUM,MPI_COMM_WORLD,ierr)

  print*, 'GMRES converged in', ite, 'iterations'


end subroutine gmres_full_mpi


