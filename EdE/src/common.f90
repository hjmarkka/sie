module common

implicit none

double precision, parameter :: pi = 3.141592653589793
double precision, parameter :: epsilon = 8.854187817*(10**(-12.0))
double precision, parameter :: mu = 4.0*pi*10.0**(-7.0)
integer, parameter :: nthreads = 8
integer, parameter :: dp = selected_real_kind(15, 307)

type mesh_struct
   integer, dimension(:,:), allocatable :: etopol, etopol_edges, edges, edgesT
   double precision, dimension(:,:), allocatable :: coord
   integer :: N_node, N_tri, N_edge, Eori(2,3), restart, maxit, N1, N2, N_loc
   double precision :: k, tol
   double complex :: eps_r
end type mesh_struct

type data
   double complex, dimension(:,:), allocatable :: A, E_field, AJ_loc, AM_loc
   double complex, dimension(:), allocatable :: x, rhs, Ax
   double precision, dimension(:,:), allocatable :: field_points, rcs
   double precision :: khat(3)
   double complex :: E0(3), force(3), torque(3)
end type data

contains

!**************************************************************

function crossRR(a,b) result(c)
double precision, intent(in) :: a(3), b(3)
double precision :: c(3)
c(1) = a(2)*b(3) - a(3)*b(2)
c(2) = -a(1)*b(3) + a(3)*b(1)
c(3) = a(1)*b(2) - a(2)*b(1)

end function crossRR

!***********************************************************

function crossRC(a,b) result(c)
double complex, intent(in) :: b(3)
double precision, intent(in) :: a(3)
double complex :: c(3)
c(1) = a(2)*b(3) - a(3)*b(2)
c(2) = -a(1)*b(3) + a(3)*b(1)
c(3) = a(1)*b(2) - a(2)*b(1)

end function crossRC

!*************************************************************************
function crossCC(a,b) result(c)
double complex, intent(in) :: a(3), b(3)
double complex :: c(3)
c(1) = a(2)*b(3) - a(3)*b(2)
c(2) = -a(1)*b(3) + a(3)*b(1)
c(3) = a(1)*b(2) - a(2)*b(1)

end function crossCC

!*************************************************************************

function norm(r, rp) result(c)
implicit none
double precision, dimension(3), intent(in) :: r, rp
double precision :: c
c = sqrt((r(1)-rp(1))**2 + (r(2)-rp(2))**2 + (r(3)-rp(3))**2)
!norm=sqrt(dot_product(r-rp,r-rp))
end function norm

!*************************************************************************
function vlen(r) result(c)
implicit none
double precision, dimension(3), intent(in) :: r
double precision :: c
c = sqrt((r(1))**2 + (r(2))**2 + (r(3))**2)
!norm=sqrt(dot_product(r-rp,r-rp))
end function vlen

!**********************************************************************

subroutine linmap_line(P,w,p1,p2,P0,w0)
double precision, intent(in) :: P0(:), w0(:)
double precision, intent(in) :: p1(3), p2(3)
double precision :: P(3,size(w0)), w(size(w0))
integer :: i1

do i1 = 1,size(w0)
   P(:,i1) = p2*P0(i1) + p1 *(1.0-P0(i1))

   w(i1) = norm(p2,p1)*w0(i1)
end do

end subroutine linmap_line

subroutine linmap_tri(P, W, tri_coord, P0, W0)
implicit none
double precision, intent(in) :: tri_coord(3,3)
double precision, intent(in) :: P0(:,:)
double precision, intent(in) :: W0(:)

double precision :: P(3,size(W0)), W(size(W0))

double precision, dimension(3) :: a, b, ww
double precision :: ww1
integer i1, n

n=size(W0)

a=tri_coord(:,2)-tri_coord(:,1);
b=tri_coord(:,3)-tri_coord(:,1);

ww(1) = (a(1)*a(1) +  a(2)*a(2) + a(3)*a(3)) * b(1) 
ww(2) = (a(1)*a(1) +  a(2)*a(2) + a(3)*a(3)) * b(2) 
ww(3) = (a(1)*a(1) +  a(2)*a(2) + a(3)*a(3)) * b(3) 

ww1=sqrt((ww(1)*b(1)+ww(2)*b(2)+ww(3)*b(3))-((a(1)*b(1)+a(2)*b(2)+a(3)*b(3))*(a(1)*b(1)+a(2)*b(2)+a(3)*b(3))));

do i1 = 1,n 
P(:,i1) = tri_coord(:,1) + a * P0(1,i1) + b * P0(2,i1)
end do

W = W0 * ww1 

end subroutine linmap_tri

!*****************************************************************************


function Gr(rf,rp,k) result(Green)
double precision, intent(in) :: rf(3), rp(3)
double complex, intent(in) :: k
double precision :: R
double complex :: Green
R=norm(rf,rp)
Green = cdexp(dcmplx(0.0,1.0) * k*R) / (4*pi*R)
end function Gr

!************************************************************************

function grad_G(rf,rp,k) result(gradG) 
double precision, intent(in) :: rf(3), rp(3)
double complex, intent(in) :: k
double complex :: gradG(3)  

double precision :: R
R=norm(rf,rp)
gradG = cdexp(dcmplx(0.0,1.0)*k*R) / (4*pi*R**3) * (-1.0 + dcmplx(0.0,1.0)*k*R) * (rf-rp)

end function grad_G

!*****************************************************************************


function n_gradGr(rf,rp,k,nvec) result(Green)
double precision, intent(in) :: rf(3), rp(3), nvec(3)
double complex, intent(in) :: k
double precision :: R, r_rp(3)

double complex :: Green, G

r_rp = rf-rp
R=norm(rf,rp)
G = cdexp(dcmplx(0.0,1.0)*k*R) / (4*pi*R*R*R) * (-1.0 + dcmplx(0.0,1.0)*k*R)

Green = G*sum(nvec*r_rp)

end function n_gradGr

!************************************************************************


function tri_n_vectors(tri_coord) result(nvec)
double precision, intent(in) :: tri_coord(3,3)
double precision :: nvec(3)

double precision :: a1(3), a2(3), n(3), len_n

a1 = tri_coord(:,2) - tri_coord(:,1)
a2 = tri_coord(:,3) - tri_coord(:,1)

n=crossRR(a1,a2)
len_n = sqrt(n(1)*n(1) + n(2)*n(2) + n(3)*n(3)) 

nvec = n/len_n

end function tri_n_vectors


!*********************************************************************************


function same_edge(fnodes,fnodes2) result(a)
integer :: fnodes(2), fnodes2(2)
integer :: a, i1, i2

a=0
do i1 = 1,2
   do i2 = 1,2
     if(fnodes(i1) == fnodes2(i2)) then
        a = a+1
     end if
   end do
end do

end function same_edge

!***********************************************************************

subroutine gradshape_tri(gradS, ngradS, nhat, tri_coord)

double precision, intent(in) :: tri_coord(3,3)
double precision :: a(3), b(3), c(3), n(3), nL, nhat(3)
double precision :: s1(3), s2(3), s3(3), gradS(3,3), ngradS(3,3)

a = tri_coord(:,2) - tri_coord(:,1)
b = tri_coord(:,3) - tri_coord(:,1)
c = tri_coord(:,3) - tri_coord(:,2)

n = crossRR(a,b)
nL = vlen(n)
!A = nL/2.0

nhat = n/nL

s1 = crossRR(nhat,c)
s2 = crossRR(nhat,b)
s3 = crossRR(nhat,a)

gradS(:,1) = s1/nL
gradS(:,2) = -s2/nL
gradS(:,3) = s3/nL

ngradS(:,1) = crossRR(nhat,s1)/nL
ngradS(:,2) = -crossRR(nhat,s2)/nL
ngradS(:,3) = crossRR(nhat,s3)/nL

end subroutine gradshape_tri

function RWG_sign(mesh, edge, tri, tk) result(sign)
type (mesh_struct) :: mesh
integer, intent(in) :: edge, tri, tk
double precision :: sign
integer :: enodes(2), enodes2(2)

enodes = mesh%edges(:,edge)
enodes2 = mesh%etopol(mesh%Eori(:,tk),tri)

if(enodes(1)  == enodes2(1)) then
   sign = 1.0
else
   sign = -1.0
end if

end function RWG_sign

function tri_area(coord) result(c)
implicit none
double precision, dimension(3,3), intent(in) :: coord
double precision :: c, tri1, tri2, tri3, tri4


tri1 = vlen(crossRR(coord(:,2)-coord(:,1),coord(:,3)-coord(:,1))) / 2

c = tri1
!norm=sqrt(dot_product(r-rp,r-rp))
end function tri_area

function near_zone(T_node, B_node) result(near)
integer, intent(in) :: T_node(3), B_node(3)
integer :: near, i1,i2

near = 0
do i1 = 1,3
   do i2 = 1,3
      if(T_node(i1) == B_node(i2)) then
         near = 1
      end if
   end do
end do

end function near_zone



function unique(invec) result(outvec)
integer :: invec(:)
integer, allocatable :: outvec(:)

integer :: i1, i2, las

las = 1
do i1 = 1,size(invec)
   do i2 = i1+1, size(invec)
       if(invec(i1) == invec(i2)) then 
          invec(i2) = 0
       end if
   end do  
end do

las = 0
do i1 = 1,size(invec)
   if(invec(i1) .ne. 0) then
      las = las + 1
   end if
end do

allocate(outvec(las))



las = 1
do i1 = 1,size(invec)
   if(invec(i1) .ne. 0) then
      outvec(las) = invec(i1)
      las = las + 1
   end if
end do




end function unique
!____________________________


function sph_unit_vectors(theta,phi) result(vec)
double precision, intent(in) :: theta, phi
double precision :: vec(3,3)

vec(:,1) = [sin(theta)*cos(phi), sin(theta)*sin(phi), cos(theta)] ! r_hat
vec(:,2) = [cos(theta)*cos(phi), cos(theta)*sin(phi), -sin(theta)] ! theta_hat
vec(:,3) = [-sin(phi), cos(phi), dble(0.0)] ! phi_hat

end function sph_unit_vectors

function sph2cart(r,theta,phi) result(x)
double precision, intent(in) :: r, theta, phi
double precision :: x(3)

x(1) = r*sin(theta)*cos(phi)
x(2) = r*sin(theta)*sin(phi)
x(3) = r*cos(theta)

end function sph2cart



function cart2sph(x) result(vec)
double precision, intent(in) :: x(3)
double precision :: vec(3)

vec(1) = sqrt(x(1)**2 + x(2)**2 + x(3)**2) ! r
vec(2) = acos(x(3)/vec(1)) ! theta
vec(3) = atan2(x(2),x(1))   

end function cart2sph





function rotation(x, axis, angle) result(xp)
double precision :: x(3), angle, xp(3)
integer :: axis

if(axis == 1) then
   xp(1) = x(1)
   xp(2) = cos(angle)*x(2) - sin(angle)*x(3)
   xp(3) = sin(angle)*x(2) + cos(angle)*x(3)
end if

if(axis == 2) then
   xp(1) = cos(angle)*x(1) + sin(angle)*x(3)
   xp(2) = x(2)
   xp(3) = -sin(angle)*x(1) + cos(angle)*x(3)
end if

if(axis == 3) then
   xp(1) = cos(angle)*x(1) - sin(angle)*x(2)
   xp(2) = sin(angle)*x(1) + cos(angle)*x(2)
   xp(3) = x(3)
end if

end function rotation

function halton_seq(index, base) result(num)
integer :: base, index, i
double precision :: num, f
num = 0.0

f = 1.0/dble(base)

i = index

do while(i>0) 
   num = num + f * mod(i,base)
   i = floor(dble(i)/dble(base))
   f = f / dble(base)
end do

end function halton_seq








end module common
