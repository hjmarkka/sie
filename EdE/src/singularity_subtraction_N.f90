module singularity_subtraction_N
use common
implicit none

! Number of terms to be extracted
!integer, parameter :: Np = 1


contains

!***********************************************************************
!
! int_S N 1/R + R + R³ +...   dS,  S is a triangle, N is a shape function
!
!************************************************************************

function analytical_integration_N(tri_coord, n_vec, r,Np) result(intN) 
double precision, intent(in) :: tri_coord(3,3), n_vec(3), r(3)
integer :: Np
double precision :: intN(3,Np)
double precision :: Kl(Np+1), rmp1(3), slv(3), sl(3), ml(3), pl1mr(3), pl2mpl1(3), rmplm1(3), pl1mpl0(3), tl(3), Il(3), mvek(3,3)
double precision :: w0, normslv, tl0, ll, sml, spl, Rpl, Rml, R0l, Bl, Km3,hl, Inl(3,Np+1), v1(3,Np+1), d1,d2,d3, hhl(3)
integer :: ind(7), nn, m, n, l2, l1, l0, lm1, l, i1, i2, nm
ind = [3,1,2,3,1,2,3]

Kl(:) = 0.0d0
nn=1

rmp1 = r - tri_coord(:,1)
w0 = dot_product(rmp1,n_vec)

do m = 0,Np
   n = 2*m-1

   do l = 1,3
      l2=ind(l+3)
      l1=ind(l+2)
      l0=ind(l+1)
      lm1=ind(l)
      
      slv = tri_coord(:,l2) - tri_coord(:,l1)

      normslv = sqrt(dot_product(slv,slv))
      sl=slv/normslv
      ml = crossRR(sl,n_vec)

      pl1mr = tri_coord(:,l1) - r
      tl0 = dot_product(pl1mr,ml)

      pl2mpl1 = tri_coord(:,l2) - tri_coord(:,l1)

      ll=sqrt(dot_product(pl2mpl1,pl2mpl1))

      sml = dot_product(pl1mr,sl)
      spl = sml + ll

      rmplm1 = r - tri_coord(:,lm1)

      Rpl = sqrt(dot_product(rmplm1,rmplm1))
      Rml = sqrt(dot_product(pl1mr,pl1mr))

      R0l = sqrt(tl0*tl0 + w0*w0)

      pl1mpl0 = tri_coord(:,l1) - tri_coord(:,l0)

      hl = dot_product(pl1mpl0,ml)

      !---------------------------------------------------------------!

      if(n == -1) then
         mvek(:,l) = ml
         tl(l) = tl0
         hhl(l) = hl

         if(abs(w0) > 1.0d-10) then ! Check this

            Bl = atan((tl0*spl)/(R0l*R0l + abs(w0)*Rpl)) - atan((tl0*sml)/(R0l*R0l + abs(w0)*Rml));

            Km3 = Bl/abs(w0)! 1.0d0/abs(w0)*Bl

         else 
            
            Km3=0.0d0
            Il(l) = 0.0d0   
         end if

         !if(abs(Rml + sml) > abs(Rpl-spl)) then          
         !   Il(l) = log(((Rpl+spl)/(Rml+sml)))           
         !else
         !   Il(l) = log(((Rml-sml)/(Rpl-spl)))            
         !end if

         if(abs(Rml + sml) > 1.0d-10) then          
            Il(l) = log(abs((Rpl+spl)/(Rml+sml)))           
         else
            Il(l) = 0.0            
         end if


         !if(Rml + sml == 0) then
         !   Il(l) = 0.0
         !else
         !   Il(l) = log(abs((Rpl+spl)/(Rml+sml)))
         !end if

         Kl(nn) = Kl(nn) + 1/(dble(n)+2.0) * (dble(n)*w0*w0 * Km3 + tl0*Il(l))
         

      else !----------------------------------------------------!
            
         Il(l) = 1.0/(dble(n)+1.0) * (spl * Rpl**dble(n) - sml * Rml**dble(n) + dble(n) * R0l * R0l * Il(l))
        
      end if
         
   end do

      if(n .ne. -1) then
         Kl(nn) = 1.0/(dble(n)+2.0) * (dble(n)*w0*w0 * Kl(nn-1) + (tl(1)*Il(1) +tl(2)*Il(2) + tl(3)*Il(3)))
      end if

      Inl(:,nn) = Il
      nn = nn+1    
end do

v1(:,:) = 0.0
do i1 = 1,Np+1
   do i2 = 1,3
      v1(:,i1) = v1(:,i1) + Inl(i2,i1)*mvek(:,i2)
   end do
end do 
!print*,v1
do nm=1,Np

   d1=dot_product(mvek(:,1),v1(:,nm+1))
   d2=dot_product(mvek(:,2),v1(:,nm+1))
   d3=dot_product(mvek(:,3),v1(:,nm+1))
   
   intN(1,nm) = 1.0/hhl(1) * (tl(1) * Kl(nm) - 1.0/(2.0*dble(nm)-1.0) * d1)
   intN(2,nm) = 1.0/hhl(2) * (tl(2) * Kl(nm) - 1.0/(2.0*dble(nm)-1.0) * d2)
   intN(3,nm) = 1.0/hhl(3) * (tl(3) * Kl(nm) - 1.0/(2.0*dble(nm)-1.0) * d3)
end do

!print*,intN

end function analytical_integration_N



!***********************************************************************
!
! int_S N n dot 1/grad R + R + R³ +...   dS,  S is a triangle, N is a shape function
!
!************************************************************************

function analytical_integration_N_ngradG(tri_coord, n_vec, r,Np) result(intN) 
double precision, intent(in) :: tri_coord(3,3), n_vec(3), r(3)
integer :: Np
double precision :: intN(3,Np+1)
double precision :: Kl(Np+2), rmp1(3), slv(3), sl(3), ml(3), pl1mr(3), pl2mpl1(3), rmplm1(3), pl1mpl0(3), tl(3), Il(3), mvek(3,3)
double precision :: w0, normslv, tl0, ll, sml, spl, Rpl, Rml, R0l, Bl, Km3,hl, Inl(3,Np+1), & 
     v1(3,Np+1), d1,d2,d3, hhl(3), vvek(3), IIN(3,Np+1)
integer :: ind(7), nn, m, n, l2, l1, l0, lm1, l, i1, i2, nm, j1
ind = [3,1,2,3,1,2,3]

Kl(:) = 0.0d0
nn=1

rmp1 = r - tri_coord(:,1)
w0 = dot_product(rmp1,n_vec)

do m = 0,Np
   n = 2*m-1

   do l = 1,3
      l2=ind(l+3)
      l1=ind(l+2)
      l0=ind(l+1)
      lm1=ind(l)
      
      slv = tri_coord(:,l2) - tri_coord(:,l1)

      normslv = sqrt(dot_product(slv,slv))
      sl=slv/normslv
      ml = crossRR(sl,n_vec)

      pl1mr = tri_coord(:,l1) - r
      tl0 = dot_product(pl1mr,ml)

      pl2mpl1 = tri_coord(:,l2) - tri_coord(:,l1)

      ll=sqrt(dot_product(pl2mpl1,pl2mpl1))

      sml = dot_product(pl1mr,sl)
      spl = sml + ll

      rmplm1 = r - tri_coord(:,lm1)

      Rpl = sqrt(dot_product(rmplm1,rmplm1))
      Rml = sqrt(dot_product(pl1mr,pl1mr))

      R0l = sqrt(tl0*tl0 + w0*w0)

      pl1mpl0 = tri_coord(:,l1) - tri_coord(:,l0)

      hl = dot_product(pl1mpl0,ml)

      !---------------------------------------------------------------!

      if(n == -1) then
         mvek(:,l) = ml
         tl(l) = tl0
         hhl(l) = hl

         if(abs(w0) > 1.0d-10) then ! Check this

            Bl = atan((tl0*spl)/(R0l*R0l + abs(w0)*Rpl)) - atan((tl0*sml)/(R0l*R0l + abs(w0)*Rml));

            Km3 = 1.0d0/abs(w0)*Bl

         else 
            
            Km3=0.0d0
            Il(l) = 0.0d0   
         end if

         !if(abs(Rml + sml) > abs(Rpl-spl)) then          
         !   Il(l) = log(((Rpl+spl)/(Rml+sml)))           
         !else
         !   Il(l) = log(((Rml-sml)/(Rpl-spl)))            
         !end if

         if(abs(Rml + sml) > 1.0d-10) then          
            Il(l) = log(abs((Rpl+spl)/(Rml+sml)))           
         else
            Il(l) = 0.0d0           
         end if


         Kl(nn+1) = Kl(nn+1) + 1.0d0/(dble(n)+2.0d0) * (dble(n)*w0*w0 * Km3 + tl0*Il(l))
         Kl(1) = Kl(1) + Km3

      else !----------------------------------------------------!
            
         Il(l) = 1.0d0/(dble(n)+1.0) * (spl * Rpl**dble(n) - sml * Rml**dble(n) + dble(n) * R0l * R0l * Il(l))
        
      end if
         
   end do

      if(n .ne. -1) then
         !print*, nn+1, size(Kl)
         Kl(nn+1) = 1.0d0/(dble(n)+2.0d0) * (dble(n)*w0*w0 * Kl(nn) + (tl(1)*Il(1) +tl(2)*Il(2) + tl(3)*Il(3)))
      end if

      Inl(:,nn) = Il
      nn = nn+1    
end do

v1(:,:) = 0.0
do i1 = 1,Np+1
   do i2 = 1,3
      v1(:,i1) = v1(:,i1) + Inl(i2,i1)*mvek(:,i2)
   end do
end do 
!print*,v1

do nm=1,Np+1

   vvek = v1(:,nm)

   d1=dot_product(mvek(:,1),vvek)
   d2=dot_product(mvek(:,2),vvek)
   d3=dot_product(mvek(:,3),vvek)
   
   IIN(1,nm) = 1.0/hhl(1) * (tl(1) * Kl(nm) - 1.0/(2.0*dble(nm-1)-1.0) * d1)
   IIN(2,nm) = 1.0/hhl(2) * (tl(2) * Kl(nm) - 1.0/(2.0*dble(nm-1)-1.0) * d2)
   IIN(3,nm) = 1.0/hhl(3) * (tl(3) * Kl(nm) - 1.0/(2.0*dble(nm-1)-1.0) * d3)
end do

do j1 = 1, Np+1
   intN(:,j1) = w0*(2.0*(j1-1)-1.0)*IIN(:,j1)
end do

!print*,intN

end function analytical_integration_N_ngradG



!********************************************************************************
!
! Computes int(rf) = int_S GN dS anatytically
!
!*****************************************************************************

function singularity_subtraction_int_S_GN(rf, tri_coord, tri_n, k, Pb_tri, wb_tri,tri_shape) result(intN)
!type (element_data), intent(in) :: elem

double precision, intent(in) :: rf(3), tri_n(3)
double precision, intent(in) :: tri_coord(3,3)
double precision, dimension(:,:), intent(in) :: Pb_tri, tri_shape
double precision, dimension(:), intent(in) :: wb_tri
double complex, intent(in) :: k

integer, parameter :: Np=2

double precision, parameter :: fact(6)=[1.0, -0.5, 1.0/24, -1.0/720, 1.0/40320, -1.0/3628800]  

double precision :: rp(3), R, intR2(3,Np), cut
double complex :: intN(3), intR(3), II, G0, f, f2(3)
integer :: i1, l, l2



!_____________________Numerical part____________________________________!

intR = 0
cut = 1.0d-10 * sum(wb_tri)**(1.0/2.0) 
do i1 = 1, size(wb_tri)

   rp = Pb_tri(:,i1)
   R = norm(rp,rf)
   !if(R< 0.0000000001) then ! Check this
   if(R < cut) then
      II = k/(4*pi)   
   else
      f = dcmplx(0.0, 0.0)
      do l = 1,Np
         f = f + 1/(4*pi) * k**(2.0*(dble(l)-1.0)) * fact(l) * R**(2.0*(dble(l)-1)-1)
      end do
      G0=Gr(rf, rp, k)
      II = G0 - f
   end if

  intR = intR + II*wb_tri(i1)*tri_shape(:,i1)

end do

!________________Analytical part_______________________________!

intR2 = analytical_integration_N(tri_coord, tri_n, rf,Np)

f2(:) = dcmplx(0.0, 0.0)

do l2 = 1,Np
   f2 = f2 + 1/(4*pi) * k**(2.0*(dble(l2)-1.0)) * fact(l2) * (intR2(:,l2))
end do

intN=intR + f2

end function singularity_subtraction_int_S_GN



!********************************************************************************
!
! Computes int(rf) = int_S n grad GN dS anatytically
!
!*****************************************************************************

function singularity_subtraction_int_S_ngradGN(rf, tri_coord, tri_n, k, Pb_tri, wb_tri,tri_shape) result(intN)
!type (element_data), intent(in) :: elem

double precision, intent(in) :: rf(3), tri_n(3)
double precision, intent(in) :: tri_coord(3,3)
double precision, dimension(:,:), intent(in) :: Pb_tri, tri_shape
double precision, dimension(:), intent(in) :: wb_tri
double complex, intent(in) :: k

integer, parameter :: Np=3

double precision, parameter :: fact(6)=[1.0, -0.5, 1.0/24, -1.0/720, 1.0/40320, -1.0/3628800]  

double precision :: rp(3), R, intR2(3,Np+1), cut, nR, r_rp(3), intR22(3,Np)
double complex :: intN(3), intR(3), II, G0, f, f2(3)
integer :: i1, l, l2



!_____________________Numerical part____________________________________!

intR = 0
cut = 1.0d-10 * sum(wb_tri)**(1.0/2.0) 
do i1 = 1, size(wb_tri)

   rp = Pb_tri(:,i1)
   r_rp = rf-rp

   R = norm(rp,rf)
   nR = dot_product(r_rp,tri_n)

   !if(R< 0.0000000001) then ! Check this
   if(R < cut) then
      II = k/(4*pi)   
   else
      f = dcmplx(0.0,0.0)
      do l = 1,Np
         f = f + 1/(4*pi) * k**(2.0*(dble(l)-1.0)) * fact(l) * (2.0*(l-1)-1.0) * R**(2.0*(dble(l)-1)-2) * nR/R
      end do
      G0 = n_gradGr(rf, rp, k, tri_n)
      II = G0 - f
   end if

  intR = intR + II*wb_tri(i1)*tri_shape(:,i1)

end do

!________________Analytical part_______________________________!

intR2 = analytical_integration_N_ngradG(tri_coord, tri_n, rf,Np)
!intR22 = analytical_integration_N(tri_coord, tri_n, rf,Np)


f2(:) = dcmplx(0.0, 0.0)

do l2 = 1,Np
   f2 = f2 + 1/(4*pi) * k**(2.0*(dble(l2)-1.0)) * fact(l2) * (intR2(:,l2))
end do

intN=intR + f2

end function singularity_subtraction_int_S_ngradGN



end module singularity_subtraction_N
