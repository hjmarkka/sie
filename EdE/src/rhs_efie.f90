subroutine rhs_efie(matrices, mesh)
use common
use integration_points
implicit none

type (mesh_struct) :: mesh
type (data) :: matrices

integer :: test_T, i1, te, test, ti, tj
double precision :: T_coord(3,3), T_dN(3,3), T_nxdN(3,3), T_nvec(3), r(3)
double precision, allocatable :: P0_tri(:,:), Pt(:,:), shape_tri(:,:)
double precision, allocatable :: w0_tri(:),wt(:)
double complex :: alok(3,3), alokH(3,3), H0(3)
double precision :: signt, el

call inttri(P0_tri,w0_tri,5)

allocate(shape_tri(3,size(w0_tri)))
shape_tri(1,:) = 1.0 - P0_tri(1,:) - P0_tri(2,:)
shape_tri(2,:) = P0_tri(1,:)
shape_tri(3,:) = P0_tri(2,:)

allocate(Pt(3,size(w0_tri)))
allocate(wt(size(w0_tri)))

H0 = crossCC(dcmplx(matrices%khat, 0.0), matrices%E0) * sqrt(epsilon/mu)

do test_T = 1, mesh%N_tri

   T_coord = mesh%coord(:,mesh%etopol(:,test_T))
   call gradshape_tri(T_dN,T_nxdN, T_nvec, T_coord)

   call linmap_tri(Pt, wt, T_coord, P0_tri, W0_tri)
   
   alok(:,:) = dcmplx(0.0,0.0)
   alokH(:,:) = dcmplx(0.0,0.0)

   do i1 = 1,size(w0_tri)
      r=Pt(:,i1)
      
      alok(1,:) = alok(1,:) + matrices%E0(1) * & 
           exp(dcmplx(0.0,mesh%k*dot_product(matrices%khat,r))) &
           * shape_tri(:,i1) * wt(i1)

      alok(2,:) = alok(2,:) + matrices%E0(2) * & 
           exp(dcmplx(0.0,mesh%k*dot_product(matrices%khat,r))) &
           * shape_tri(:,i1) * wt(i1)

      alok(3,:) = alok(3,:) + matrices%E0(3) * & 
           exp(dcmplx(0.0,mesh%k*dot_product(matrices%khat,r))) &
           * shape_tri(:,i1) * wt(i1)

      !alokH(1,:) = alokH(1,:) + H0(1) * & 
      !     exp(dcmplx(0.0,mesh%k*dot_product(matrices%khat,r))) &
      !     * shape_tri(:,i1) * wt(i1)

      !alokH(2,:) = alokH(2,:) + H0(2) * & 
      !     exp(dcmplx(0.0,mesh%k*dot_product(matrices%khat,r))) &
      !     * shape_tri(:,i1) * wt(i1)

      !alokH(3,:) = alokH(3,:) + H0(3) * & 
      !     exp(dcmplx(0.0,mesh%k*dot_product(matrices%khat,r))) &
      !     * shape_tri(:,i1) * wt(i1)



   end do
  

   do te = 1, 3
      test = mesh%etopol_edges(te,test_T)
      signt = RWG_sign(mesh, test, test_T,te)
      
      el = norm(mesh%coord(:,mesh%edges(1,test)), mesh%coord(:,mesh%edges(2,test)))

      ti = mesh%Eori(1,te)
      tj = mesh%Eori(2,te)

      matrices%rhs(test) = matrices%rhs(test) - signt * el * & 
           (sum(T_nxdN(:,tj)*alok(:,ti)) - sum(T_nxdN(:,ti)*alok(:,tj)))

      !matrices%rhs(mesh%N_edge+test) = matrices%rhs(mesh%N_edge+test) - signt * el * & 
       !    (sum(T_nxdN(:,tj)*alokH(:,ti)) - sum(T_nxdN(:,ti)*alokH(:,tj)))

      
      end do

end do
!print*,matrices%rhs
end subroutine rhs_efie
