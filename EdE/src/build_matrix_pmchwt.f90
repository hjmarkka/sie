subroutine build_matrix_pmchwt(matrices, mesh)
use common
use integrals
use integration_points

implicit none

type (mesh_struct) :: mesh
type (data) :: matrices

integer :: test_T, basis_T, T_nodes(3), B_nodes(3), my_id, N_procs, N1, N2, N_loc
double precision :: T_coord(3,3), B_coord(3,3)
double precision, allocatable :: P0_tri(:,:), P02_tri(:,:)
double precision, allocatable :: w0_tri(:), P0_line(:), w0_line(:)
double precision, allocatable :: w02_tri(:), P02_line(:), w02_line(:)
double precision :: T_dN(3,3), T_nxdN(3,3), T_nvec(3)
double precision :: B_dN(3,3), B_nxdN(3,3), B_nvec(3)
integer :: te, be, test, basis, near, block_size, ierr
double precision :: signt, signb, sign, eta0
double complex :: k1, k2, eta1, eta2
double complex :: alok(3,3), alok2(3,3), alok3(3,3), alok4(3,3,3), elem(5)
double complex :: alok_2(3,3), alok2_2(3,3), alok3_2(3,3), alok4_2(3,3,3), elem_2(5)

eta0 = sqrt(mu/epsilon)

allocate(matrices%A(2*mesh%N_edge,2*mesh%N_edge))
matrices%A(:,:) = dcmplx(0.0,0.0)


alok3(:,:) = 0.0
alok4(:,:,:) = 0.0

k1 = dcmplx(mesh%k, 0.0)
k2 = mesh%k * sqrt(mesh%eps_r)

eta1 = sqrt(mu/epsilon)
eta2 = sqrt(mu/(epsilon*mesh%eps_r))

call inttri(P0_tri,w0_tri,5)
call gaussint(P0_line,w0_line,5)

call inttri(P02_tri,w02_tri,12)
call gaussint(P02_line,w02_line,10)


do test_T = 1,mesh%N_tri
   
   !T_nodes = mesh%etopol_edges(:,test_T)
   T_coord = mesh%coord(:,mesh%etopol(:,test_T))

   call gradshape_tri(T_dN,T_nxdN, T_nvec, T_coord)

   

   do basis_T = 1,mesh%N_tri

      !B_nodes = mesh%etopol_edges(:,basis_T)
      B_coord = mesh%coord(:,mesh%etopol(:,basis_T))     

      call gradshape_tri(B_dN,B_nxdN, B_nvec, B_coord)

      near = near_zone(mesh%etopol(:,test_T), mesh%etopol(:,basis_T))
      
      if(near == 1) then
          
         call integrate_S_S_GNN(alok2, P02_tri, w02_tri, T_coord, B_coord, k1)
         call integrate_S_S_ngradGNN(alok3, P02_tri, w02_tri, T_coord, B_coord, k1)
         call integrate_dS_S_GNN(alok4, P02_tri, w02_tri, P02_line, w02_line, T_coord, B_coord, k1, mesh%Eori)
       
         call integrate_S_S_GNN(alok2_2, P02_tri, w02_tri, T_coord, B_coord, k2)
         call integrate_S_S_ngradGNN(alok3_2, P02_tri, w02_tri, T_coord, B_coord, k2)
         call integrate_dS_S_GNN(alok4_2, P02_tri, w02_tri, P02_line, w02_line, T_coord, B_coord, k2, mesh%Eori)
       
      else

         call integrate_S_S_GNN(alok2, P0_tri, w0_tri, T_coord, B_coord, k1)
         call integrate_S_S_ngradGNN(alok3, P0_tri, w0_tri, T_coord, B_coord, k1)
         call integrate_dS_S_GNN(alok4, P0_tri, w0_tri, P0_line, w0_line, T_coord, B_coord, k1, mesh%Eori)
         
         call integrate_S_S_GNN(alok2_2, P0_tri, w0_tri, T_coord, B_coord, k2)
         call integrate_S_S_ngradGNN(alok3_2, P0_tri, w0_tri, T_coord, B_coord, k2)
         call integrate_dS_S_GNN(alok4_2, P0_tri, w0_tri, P0_line, w0_line, T_coord, B_coord, k2, mesh%Eori)

      end if


      alok(:,:) = dcmplx(0.0,0.0)
       
       if(test_T == basis_T) then
           call integrate_S_NN(alok, P0_tri, w0_tri, T_coord)
       end if


       !___________________________________________________!
      

       do te = 1,3
          test = mesh%etopol_edges(te,test_T)
          signt = RWG_sign(mesh, test, test_T,te)

          do be = 1,3
             basis = mesh%etopol_edges(be,basis_T)
             signb = RWG_sign(mesh, basis, basis_T,be)

             !print*, test, basis

             sign = signt*signb
           
             elem = EFIE_elements(alok, alok2, alok3, alok4,T_dN, B_dN, T_nxdN, B_nxdN, te, be, mesh%Eori, T_coord, B_coord, B_nvec)

             elem_2 = EFIE_elements(alok, alok2_2, alok3_2, alok4_2,T_dN, B_dN, &
                  T_nxdN, B_nxdN, te, be, mesh%Eori, T_coord, B_coord, B_nvec)


             !print*, elem
             
             matrices%A(test,basis) = matrices%A(test,basis) + &
                  1/eta0*eta1/(dcmplx(0.0,1.0)*k1) * sign*(k1**2*elem(1)-elem(2)) + &
                  1/eta0*eta2/(dcmplx(0.0,1.0)*k2) * sign*(k2**2*elem_2(1)-elem_2(2)) 
             
             matrices%A(mesh%N_edge+test, mesh%N_edge+basis) = &
                  matrices%A(mesh%N_edge+test, mesh%N_edge+basis) + &
                  eta0*1.0/(eta1*dcmplx(0.0,1.0)*k1) * sign*(k1**2*elem(1)-elem(2)) + &
                  eta0*1.0/(eta2*dcmplx(0.0,1.0)*k2) * sign*(k2**2*elem_2(1)-elem_2(2)) 
             
             matrices%A(test, mesh%N_edge+basis) = &
                  matrices%A(test, mesh%N_edge+basis) + &
                  1/eta0*sign*(-elem(3)+elem(4)) + 1/eta0*sign*(-elem_2(3)+elem_2(4))
            
             matrices%A(mesh%N_edge+test, basis) = &
                  matrices%A(mesh%N_edge+test, basis) - &
                 eta0*sign*(-elem(3)+elem(4)) - eta0*sign*(-elem_2(3)+elem_2(4))
            

          end do
       end do

   end do

end do


end subroutine build_matrix_pmchwt
