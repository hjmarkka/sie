subroutine read_mesh2(mesh,file)
use common
use hdf5
implicit none

type (mesh_struct) :: mesh 
CHARACTER(LEN=28) :: file

CHARACTER(LEN=5), PARAMETER :: coord_dataset = "coord"     ! Dataset name
CHARACTER(LEN=6), PARAMETER :: etopol_dataset = "etopol"     ! Dataset2 name
CHARACTER(LEN=5), PARAMETER :: edges_dataset = "edges"     ! Dataset name
CHARACTER(LEN=12), PARAMETER :: etopol_edges_dataset = "etopol_edges"     ! Dataset2 nam

integer(HID_T) :: file_id 
integer(HID_T) :: coord_dataset_id, etopol_dataset_id
integer(HID_T) :: edges_dataset_id, etopol_edges_dataset_id

integer(HID_T) :: coord_dataspace_id, etopol_dataspace_id
integer(HID_T) :: edges_dataspace_id, etopol_edges_dataspace_id

integer(HSIZE_T), dimension(2) :: dims_out, coord_dims, etopol_dims
integer(HSIZE_T), dimension(2) :: edges_dims, etopol_edges_dims

integer :: error

double precision, dimension(:,:), allocatable :: coord
integer, dimension(:,:), allocatable :: etopol, etopol_edges, edges

call h5open_f(error) ! initialize interface
call h5fopen_f(file, H5F_ACC_RDWR_F, file_id, error) ! open file

if(error==-1) then
   print*, 'Cannot find mesh file: ', file
   stop
end if
!_________________ Read coord__________________________________________________!

call h5dopen_f(file_id, coord_dataset, coord_dataset_id, error) ! open dataset

call h5dget_space_f(coord_dataset_id, coord_dataspace_id, error) 
call H5sget_simple_extent_dims_f(coord_dataspace_id, dims_out, coord_dims, error)

allocate(coord(coord_dims(1), coord_dims(2)))
call h5dread_f(coord_dataset_id, H5T_NATIVE_DOUBLE, coord, coord_dims, error)

call h5dclose_f(coord_dataset_id, error) ! close dataset

!___________________ Read etopol_______________________________________________!

call h5dopen_f(file_id, etopol_dataset, etopol_dataset_id, error) ! open dataset

call h5dget_space_f(etopol_dataset_id, etopol_dataspace_id, error) 
call H5sget_simple_extent_dims_f(etopol_dataspace_id, dims_out, etopol_dims, error)

allocate(etopol(etopol_dims(1), etopol_dims(2)))
call h5dread_f(etopol_dataset_id, H5T_NATIVE_INTEGER, etopol, etopol_dims, error)

call h5dclose_f(etopol_dataset_id, error)

!___________________ Read etopol_edges_______________________________________________!

call h5dopen_f(file_id, etopol_edges_dataset, etopol_edges_dataset_id, error) ! open dataset

call h5dget_space_f(etopol_edges_dataset_id, etopol_edges_dataspace_id, error) 
call H5sget_simple_extent_dims_f(etopol_edges_dataspace_id, dims_out, etopol_edges_dims, error)

allocate(etopol_edges(etopol_edges_dims(1), etopol_edges_dims(2)))
call h5dread_f(etopol_edges_dataset_id, H5T_NATIVE_INTEGER, etopol_edges, etopol_edges_dims, error)

call h5dclose_f(etopol_edges_dataset_id, error)

!___________________ Read etopol_edges_______________________________________________!

call h5dopen_f(file_id, edges_dataset, edges_dataset_id, error) ! open dataset

call h5dget_space_f(edges_dataset_id, edges_dataspace_id, error) 
call H5sget_simple_extent_dims_f(edges_dataspace_id, dims_out, edges_dims, error)

allocate(edges(edges_dims(1), edges_dims(2)))
call h5dread_f(edges_dataset_id, H5T_NATIVE_INTEGER, edges, edges_dims, error)

call h5dclose_f(edges_dataset_id, error)


!______________________________________________________________________________!


call h5fclose_f(file_id, error) ! close file
call h5close_f(error) ! close inteface

allocate(mesh%coord(3,size(coord,2)))
mesh%coord = coord
print *,'   Number of nodes      =',size(coord,2)
mesh%N_node = size(coord,2)

allocate(mesh%etopol(3,size(etopol,2)))
mesh%etopol = etopol
print *,'   Number of elements   =',size(etopol,2)
mesh%N_tri = size(etopol,2)

allocate(mesh%etopol_edges(3,size(etopol_edges,2)))
mesh%etopol_edges = etopol_edges

allocate(mesh%edges(2,size(edges,2)))
mesh%edges = edges
print *,'   Number of edges   =',size(edges,2)
mesh%N_edge = size(edges,2)



end subroutine read_mesh2
