module integrals
use singularity_subtraction_N

implicit none

contains


!*************************************************************************
subroutine integrate_S_S_GNN(alok, P0_tri, w0_tri, T_coord, B_coord, k) 
double complex :: alok(3,3)
double precision, dimension(:,:), intent(in) :: P0_tri
double precision, dimension(:), intent(in) :: w0_tri
double precision, intent(in) :: T_coord(3,3), B_coord(3,3)
double complex, intent(in) :: k

double precision :: Pt(3,size(w0_tri)), wt(size(w0_tri))
double precision :: Pb(3,size(w0_tri)), wb(size(w0_tri)), shape_tri(3,size(w0_tri))

double precision :: rf(3), B_nvec(3)
double complex :: intN(3)
integer :: i1

shape_tri(1,:) = 1.0 - P0_tri(1,:) - P0_tri(2,:)
shape_tri(2,:) = P0_tri(1,:)
shape_tri(3,:) = P0_tri(2,:)

alok(:,:) = dcmplx(0.0,0.0)

call linmap_tri(Pt, wt, T_coord, P0_tri, W0_tri)
call linmap_tri(Pb, wb, B_coord, P0_tri, W0_tri)
B_nvec = tri_n_vectors(B_coord)


do i1 = 1, size(w0_tri)

   rf = Pt(:,i1)
   intN = singularity_subtraction_int_S_GN(rf, B_coord, B_nvec, k, Pb, wb,shape_tri)
   
   alok(1,:) = alok(1,:) + intN * shape_tri(1,i1) * wt(i1)
   alok(2,:) = alok(2,:) + intN * shape_tri(2,i1) * wt(i1)
   alok(3,:) = alok(3,:) + intN * shape_tri(3,i1) * wt(i1)

end do

end subroutine integrate_S_S_GNN

!*************************************************************************


!*************************************************************************
subroutine integrate_S_S_ngradGNN(alok, P0_tri, w0_tri, T_coord, B_coord, k) 
double complex :: alok(3,3)
double precision, dimension(:,:), intent(in) :: P0_tri
double precision, dimension(:), intent(in) :: w0_tri
double precision, intent(in) :: T_coord(3,3), B_coord(3,3)
double complex, intent(in) :: k

double precision :: Pt(3,size(w0_tri)), wt(size(w0_tri))
double precision :: Pb(3,size(w0_tri)), wb(size(w0_tri)), shape_tri(3,size(w0_tri))

double precision :: rf(3), B_nvec(3)
double complex :: intN(3)
integer :: i1

shape_tri(1,:) = 1.0 - P0_tri(1,:) - P0_tri(2,:)
shape_tri(2,:) = P0_tri(1,:)
shape_tri(3,:) = P0_tri(2,:)

alok(:,:) = dcmplx(0.0,0.0)

call linmap_tri(Pt, wt, T_coord, P0_tri, W0_tri)
call linmap_tri(Pb, wb, B_coord, P0_tri, W0_tri)
B_nvec = tri_n_vectors(B_coord)


do i1 = 1, size(w0_tri)

   rf = Pt(:,i1)
   intN = singularity_subtraction_int_S_ngradGN(rf, B_coord, B_nvec, k, Pb, wb,shape_tri)
   
   alok(1,:) = alok(1,:) + intN * shape_tri(1,i1) * wt(i1)
   alok(2,:) = alok(2,:) + intN * shape_tri(2,i1) * wt(i1)
   alok(3,:) = alok(3,:) + intN * shape_tri(3,i1) * wt(i1)

end do

end subroutine integrate_S_S_ngradGNN

!*************************************************************************

!*************************************************************************
subroutine integrate_dS_S_GNN(alok, P0_tri, w0_tri, P0_line, w0_line, T_coord, B_coord, k, Eori) 
double complex :: alok(3,3,3)
double precision, dimension(:,:), intent(in) :: P0_tri
double precision, dimension(:), intent(in) :: w0_tri, P0_line, w0_line
double precision, intent(in) :: T_coord(3,3), B_coord(3,3)
integer, intent(in) :: Eori(2,3)
double complex, intent(in) :: k

double precision :: Pt(3,size(w0_tri)), wt(size(w0_tri)), shape_tri(3,size(w0_tri))
double precision :: Pb(3,size(w0_line)), wb(size(w0_line)), shape_line(3,size(w0_line))

double precision :: rf(3), B_nvec(3), T_nvec(3), p1(3), p2(3)
double complex :: intN(3)
integer :: i1,i3, edge

shape_tri(1,:) = 1.0 - P0_tri(1,:) - P0_tri(2,:)
shape_tri(2,:) = P0_tri(1,:)
shape_tri(3,:) = P0_tri(2,:)

shape_line(1,:) = 1.0 - P0_line
shape_line(2,:) = P0_line
shape_line(3,:) = 0.0

alok(:,:,:) = dcmplx(0.0,0.0)

call linmap_tri(Pt, wt, T_coord, P0_tri, W0_tri)

B_nvec = tri_n_vectors(B_coord)
T_nvec = tri_n_vectors(T_coord)

do edge = 1,3

   p1 = B_coord(:,Eori(1,edge))
   p2 = B_coord(:,Eori(2,edge))

   call linmap_line(Pb, wb, p1, p2, P0_line, w0_line)

   do i1 = 1, size(wb)

      rf = Pb(:,i1)
      intN = singularity_subtraction_int_S_GN(rf, T_coord, T_nvec, k, Pt, wt,shape_tri)
      !print*, intN
      alok(:,Eori(1,edge),edge) = alok(:,Eori(1,edge),edge) + intN * shape_line(1,i1) * wb(i1)
      alok(:,Eori(2,edge),edge) = alok(:,Eori(2,edge),edge) + intN * shape_line(2,i1) * wb(i1)
      alok(:,edge,edge) = alok(:,edge,edge) + intN * shape_line(3,i1) * wb(i1)
    
      
   end do

end do

end subroutine integrate_dS_S_GNN


!*************************************************************************
subroutine integrate_dS_S_G(alok, P0_tri, w0_tri, P0_line, w0_line, T_coord, B_coord, k, Eori) 
double complex :: alok(3)
double precision, dimension(:,:), intent(in) :: P0_tri
double precision, dimension(:), intent(in) :: w0_tri, P0_line, w0_line
double precision, intent(in) :: T_coord(3,3), B_coord(3,3)
integer, intent(in) :: Eori(2,3)
double complex, intent(in) :: k

double precision :: Pt(3,size(w0_tri)), wt(size(w0_tri)), shape_tri(3,size(w0_tri))
double precision :: Pb(3,size(w0_line)), wb(size(w0_line))

double precision :: rf(3), B_nvec(3), T_nvec(3), p1(3), p2(3)
double complex :: intN(3)
integer :: i1,i3, edge



shape_tri(1,:) = 1.0 - P0_tri(1,:) - P0_tri(2,:)
shape_tri(2,:) = P0_tri(1,:)
shape_tri(3,:) = P0_tri(2,:)


alok(:) = dcmplx(0.0,0.0)

call linmap_tri(Pt, wt, T_coord, P0_tri, W0_tri)

B_nvec = tri_n_vectors(B_coord)
T_nvec = tri_n_vectors(T_coord)



do edge = 1,3

   p1 = B_coord(:,Eori(1,edge))
   p2 = B_coord(:,Eori(2,edge))

   call linmap_line(Pb, wb, p1, p2, P0_line, w0_line)

   do i1 = 1, size(wb)

      rf = Pb(:,i1)
      intN = singularity_subtraction_int_S_GN(rf, T_coord, T_nvec, k, Pt, wt,shape_tri)
      !print*, intN

      alok(edge) = alok(edge) + sum(intN) * wb(i1)
    
  
   end do

end do

end subroutine integrate_dS_S_G



!*************************************************************************


subroutine integrate_S_NN(alok, P0_tri, w0_tri, T_coord)
double precision, dimension(:,:), intent(in) :: P0_tri
double precision, dimension(:), intent(in) :: w0_tri
double precision, intent(in) ::  T_coord(3,3)
double complex, dimension(3,3) :: alok

double complex, dimension(3,3) :: intN
double precision :: Pt(3,size(w0_tri)), wt(size(w0_tri))
double precision :: shape_tri(3,size(w0_tri))
integer :: i1

shape_tri(1,:) = 1.0 - P0_tri(1,:) - P0_tri(2,:)
shape_tri(2,:) = P0_tri(1,:)
shape_tri(3,:) = P0_tri(2,:)


call linmap_tri(Pt, wt, T_coord, P0_tri, w0_tri)

intN(:,:) = dcmplx(0.0,0.0)
do i1 = 1,size(wt)

   intN(:,1) = intN(:,1) + shape_tri(1,i1) * shape_tri(:,i1) * wt(i1)
   intN(:,2) = intN(:,2) + shape_tri(2,i1) * shape_tri(:,i1) * wt(i1)
   intN(:,3) = intN(:,3) + shape_tri(3,i1) * shape_tri(:,i1) * wt(i1)
  
end do

alok = intN


end subroutine integrate_S_NN

function EFIE_elements(alok1, alok2, alok3,alok4, T_dN, B_dN, T_nxdN, B_nxdN, te, be, Eori, T_coord, B_coord, B_nvec) result(elem)
double complex, intent(in) :: alok1(3,3), alok2(3,3), alok3(3,3), alok4(3,3,3)
double precision, intent(in) :: T_nxdN(3,3), B_nxdN(3,3), T_coord(3,3), B_coord(3,3), T_dN(3,3), B_dN(3,3), B_nvec(3)
integer, intent(in) :: te, be, Eori(2,3)
double complex :: elem(5)

double precision :: djj, dji, dij, dii, At, Ab, elt, elb, ajj, aji, aij, aii, bii, bij, bji, bjj, cii, cji,cij,cjj
double precision :: pe(3), pen(3), pn(3,3), pni(3), pnj(3)
double complex :: elem1, elem2, elem3, elem4, elem5
integer :: ti, tj, bi, bj, edge

elem = 0.0

ti = Eori(1,te)
tj = Eori(2,te)

bi = Eori(1,be)
bj = Eori(2,be)

djj = dot_product(T_nxdN(:,tj), B_nxdN(:,bj))
dji = dot_product(T_nxdN(:,tj), B_nxdN(:,bi))
dij = dot_product(T_nxdN(:,ti), B_nxdN(:,bj))
dii = dot_product(T_nxdN(:,ti), B_nxdN(:,bi))

cjj = dot_product(T_dN(:,tj), B_nxdN(:,bj))
cji = dot_product(T_dN(:,tj), B_nxdN(:,bi))
cij = dot_product(T_dN(:,ti), B_nxdN(:,bj))
cii = dot_product(T_dN(:,ti), B_nxdN(:,bi))


ajj = dot_product(T_nxdN(:,tj), B_dN(:,bj))
aji = dot_product(T_nxdN(:,tj), B_dN(:,bi))
aij = dot_product(T_nxdN(:,ti), B_dN(:,bj))
aii = dot_product(T_nxdN(:,ti), B_dN(:,bi))



elem1 = cjj * alok1(ti,bi) - &
 cji * alok1(ti,bj) - &
 cij * alok1(tj,bi) + &
 cii * alok1(tj,bj) 

elem2 = djj * alok2(ti,bi) - &
 dji * alok2(ti,bj) - &
 dij * alok2(tj,bi) + &
 dii * alok2(tj,bj) 

elem4 = ajj * alok3(ti,bi) - &
 aji * alok3(ti,bj) - &
 aij * alok3(tj,bi) + &
 aii * alok3(tj,bj) 

elem5 = dcmplx(0.0,0.0)
do edge = 1,3

   pe = B_coord(:,Eori(2,edge)) - B_coord(:,Eori(1,edge))
   pen = pe/vlen(pe)

   pn(1,:) = B_nvec(1) * pen
   pn(2,:) = B_nvec(2) * pen
   pn(3,:) = B_nvec(3) * pen

   pni = matmul(pn,B_nxdN(:,bi))
   pnj = matmul(pn,B_nxdN(:,bj))

   bii = dot_product(T_nxdN(:,ti), pni)
   bij = dot_product(T_nxdN(:,ti), pnj)
   bji = dot_product(T_nxdN(:,tj), pni)
   bjj = dot_product(T_nxdN(:,tj), pnj)

   elem5 = elem5 + bjj * alok4(ti,bi,edge) - &
        bji * alok4(ti,bj,edge) - &
        bij * alok4(tj,bi,edge) + &
        bii * alok4(tj,bj,edge) 
end do




At = tri_area(T_coord)
Ab = tri_area(B_coord)

elt = norm(T_coord(:,ti),T_coord(:,tj))
elb = norm(B_coord(:,bi),B_coord(:,bj))


elem3 = 1/At * 1/Ab * (sum(alok2))

elem(1) = elt * elb * elem2
elem(2) = elt * elb * (elem3)
elem(3) = elt * elb * elem4
elem(4) = elt * elb * elem5
elem(5) = elt * elb * elem1

end function EFIE_elements

!*************************************************


function MFIE_elements(alok1, alok2, alok3, T_dN, B_dN, T_nxdN, B_nxdN, te, be, Eori, T_coord, B_coord, B_nvec) result(elem)
double complex, intent(in) :: alok1(3,3), alok2(3,3), alok3(3,3,3)
double precision, intent(in) :: T_nxdN(3,3), B_nxdN(3,3), T_coord(3,3), B_coord(3,3), T_dN(3,3), B_dN(3,3), B_nvec(3)
integer, intent(in) :: te, be, Eori(2,3)
double complex :: elem(4)

double precision :: djj, dji, dij, dii, At, Ab, elt, elb, ajj, aji, aij, aii, bii, bij, bji, bjj
double precision :: pe(3), pen(3), pn(3,3), pni(3), pnj(3)
double complex :: elem1, elem2, elem3, elem4, elem5
integer :: ti, tj, bi, bj, edge

elem(:) = 0.0

ti = Eori(1,te)
tj = Eori(2,te)

bi = Eori(1,be)
bj = Eori(2,be)

djj = dot_product(T_nxdN(:,tj), B_nxdN(:,bj))
dji = dot_product(T_nxdN(:,tj), B_nxdN(:,bi))
dij = dot_product(T_nxdN(:,ti), B_nxdN(:,bj))
dii = dot_product(T_nxdN(:,ti), B_nxdN(:,bi))

ajj = dot_product(T_dN(:,tj), B_dN(:,bj))
aji = dot_product(T_dN(:,tj), B_dN(:,bi))
aij = dot_product(T_dN(:,ti), B_dN(:,bj))
aii = dot_product(T_dN(:,ti), B_dN(:,bi))

elem1 = djj * alok1(ti,bi) - &
 dji * alok1(ti,bj) - &
 dij * alok1(tj,bi) + &
 dii * alok1(tj,bj) 

elem2 = ajj * alok2(ti,bi) - &
 aji * alok2(ti,bj) - &
 aij * alok2(tj,bi) + &
 aii * alok2(tj,bj) 


elem3 = dcmplx(0.0,0.0)
do edge = 1,3

   pe = B_coord(:,Eori(2,edge)) - B_coord(:,Eori(1,edge))
   pen = pe/vlen(pe)

   pn(1,:) = B_nvec(1) * pen
   pn(2,:) = B_nvec(2) * pen
   pn(3,:) = B_nvec(3) * pen

   pni = matmul(pn,B_nxdN(:,bi))
   pnj = matmul(pn,B_nxdN(:,bj))

   bii = dot_product(T_dN(:,ti), pni)
   bij = dot_product(T_dN(:,ti), pnj)
   bji = dot_product(T_dN(:,tj), pni)
   bjj = dot_product(T_dN(:,tj), pnj)

   elem3 = elem3 + bjj * alok3(ti,bi,edge) - &
        bji * alok3(ti,bj,edge) - &
        bij * alok3(tj,bi,edge) + &
        bii * alok3(tj,bj,edge) 
end do



elt = norm(T_coord(:,ti),T_coord(:,tj))
elb = norm(B_coord(:,bi),B_coord(:,bj))


elem(1) = -elt * elb * elem1
elem(2) = elt * elb * elem2
elem(3) = -elt * elb * elem3


end function MFIE_elements


function elements(alok4, Eori, T_coord, B_coord, B_nvec) result(elem)
double complex, intent(in) :: alok4(3)
double precision, intent(in) :: T_coord(3,3), B_coord(3,3), B_nvec(3)
integer, intent(in) :: Eori(2,3)
double complex :: elem(3)

double precision :: djj, dji, dij, dii, At, Ab, elt, elb, ajj, aji, aij, aii, bii, bij, bji, bjj, cii, cji,cij,cjj
double precision :: pe(3), pen(3), pn(3,3), pni(3), pnj(3), m(3)
double complex :: elem1, elem2, elem3, elem4, elem5
integer :: ti, tj, bi, bj, edge

elem(:) = dcmplx(0.0,0.0)

do edge = 1,3

   pe = B_coord(:,Eori(2,edge)) - B_coord(:,Eori(1,edge))
   pen = pe/vlen(pe)

   m = crossRR(pen, B_nvec)
   
   elem = elem + m*alok4(edge)

end do


end function elements


!*************************************************************************
subroutine int_gradG(alok, P0_tri, w0_tri, T_coord, B_coord, k) 
double complex :: alok(3)
double precision, dimension(:,:), intent(in) :: P0_tri
double precision, dimension(:), intent(in) :: w0_tri
double precision, intent(in) :: T_coord(3,3), B_coord(3,3)
double complex, intent(in) :: k

double precision :: Pt(3,size(w0_tri)), wt(size(w0_tri))
double precision :: Pb(3,size(w0_tri)), wb(size(w0_tri)), shape_tri(3,size(w0_tri))

double precision :: rf(3), B_nvec(3), rp(3)
double complex :: intN(3)
integer :: i1, i2

alok(:) = dcmplx(0.0,0.0)

call linmap_tri(Pt, wt, T_coord, P0_tri, W0_tri)
call linmap_tri(Pb, wb, B_coord, P0_tri, W0_tri)
B_nvec = tri_n_vectors(B_coord)


do i1 = 1, size(w0_tri)

   rf = Pt(:,i1)

   do i2 = 1, size(w0_tri)
      rp = Pb(:,i2)

      alok = alok + grad_G(rf,rp,k)*wt(i1)*wb(i2)
   end do
   
end do

end subroutine int_gradG
!*************************************************************************
subroutine int_ngradG(alok, P0_tri, w0_tri, T_coord, B_coord, k) 
double complex :: alok
double precision, dimension(:,:), intent(in) :: P0_tri
double precision, dimension(:), intent(in) :: w0_tri
double precision, intent(in) :: T_coord(3,3), B_coord(3,3)
double complex, intent(in) :: k

double precision :: Pt(3,size(w0_tri)), wt(size(w0_tri))
double precision :: Pb(3,size(w0_tri)), wb(size(w0_tri)), shape_tri(3,size(w0_tri))

double precision :: rf(3), B_nvec(3), rp(3)
double complex :: intN
integer :: i1, i2

alok = dcmplx(0.0,0.0)

call linmap_tri(Pt, wt, T_coord, P0_tri, W0_tri)
call linmap_tri(Pb, wb, B_coord, P0_tri, W0_tri)
B_nvec = tri_n_vectors(B_coord)


do i1 = 1, size(w0_tri)

   rf = Pt(:,i1)

   do i2 = 1, size(w0_tri)
      rp = Pb(:,i2)

      alok = alok + n_gradGr(rf,rp,k,B_nvec) * wt(i1)*wb(i2)
   end do
   
end do

end subroutine int_ngradG
end module integrals
