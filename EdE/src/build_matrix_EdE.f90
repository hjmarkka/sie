subroutine build_matrix_EdE(matrices, mesh)
use common
use integrals
use integration_points

implicit none

type (mesh_struct) :: mesh
type (data) :: matrices

integer :: test_T, basis_T, T_nodes(3), B_nodes(3), my_id, N_procs, N1, N2, N_loc
double precision :: T_coord(3,3), B_coord(3,3)
double precision, allocatable :: P0_tri(:,:), P02_tri(:,:)
double precision, allocatable :: w0_tri(:), P0_line(:), w0_line(:)
double precision, allocatable :: w02_tri(:), P02_line(:), w02_line(:)
double precision :: T_dN(3,3), T_nxdN(3,3), T_nvec(3)
double precision :: B_dN(3,3), B_nxdN(3,3), B_nvec(3)
integer :: te, be, test, basis, near, block_size, ierr
double precision :: signt, signb, sign, eta0, T_area, B_area
double complex :: k1, k2, eta1, eta2, uu, uu2, alokg(3), alokngradg
double complex :: alok(3,3), alok2(3,3), alok3(3,3), alok4(3), elem(5), elems(3)
double complex :: alok_2(3,3), alok2_2(3,3), alok3_2(3,3), alok4_2(3), elem_2(5)
integer :: Tind_Ex, Tind_Ey, Tind_Ez, Bind_Ex, Bind_Ey, Bind_Ez
double complex :: scale, scale2
eta0 = sqrt(mu/epsilon)

allocate(matrices%A(6*mesh%N_tri,6*mesh%N_tri))
matrices%A(:,:) = dcmplx(0.0,0.0)


alok3(:,:) = 0.0
alok4(:) = 0.0

k1 = dcmplx(mesh%k, 0.0)
k2 = mesh%k * sqrt(mesh%eps_r)

eta1 = sqrt(mu/epsilon)
eta2 = sqrt(mu/(epsilon*mesh%eps_r))

call inttri(P0_tri,w0_tri,5)
call gaussint(P0_line,w0_line,5)

call inttri(P02_tri,w02_tri,12)
call gaussint(P02_line,w02_line,10)


do test_T = 1,mesh%N_tri
   
   T_coord = mesh%coord(:,mesh%etopol(:,test_T))
   T_area = tri_area(T_coord)

   call gradshape_tri(T_dN,T_nxdN, T_nvec, T_coord)

   do basis_T = 1,mesh%N_tri

      B_coord = mesh%coord(:,mesh%etopol(:,basis_T))     
      B_area = tri_area(B_coord)
      call gradshape_tri(B_dN,B_nxdN, B_nvec, B_coord)

      near = near_zone(mesh%etopol(:,test_T), mesh%etopol(:,basis_T))
      
      if(near == 1) then
          
         call integrate_S_S_GNN(alok2, P02_tri, w02_tri, T_coord, B_coord, k1)
         call integrate_S_S_ngradGNN(alok3, P02_tri, w02_tri, T_coord, B_coord, k1)
         call integrate_dS_S_G(alok4, P02_tri, w02_tri, P02_line, w02_line, T_coord, B_coord, k1, mesh%Eori)
       
         call integrate_S_S_GNN(alok2_2, P02_tri, w02_tri, T_coord, B_coord, k2)
         call integrate_S_S_ngradGNN(alok3_2, P02_tri, w02_tri, T_coord, B_coord, k2)
         call integrate_dS_S_G(alok4_2, P02_tri, w02_tri, P02_line, w02_line, T_coord, B_coord, k2, mesh%Eori)
         alokg(:)=dcmplx(0.0,0.0)

         !call int_gradG(alokg, P0_tri, w0_tri, T_coord, B_coord, k1)
         !call int_ngradG(alokngradg, P0_tri, w0_tri, T_coord, B_coord, k1)

         !print*, alokngradg, sum(alok3)

      else

         call integrate_S_S_GNN(alok2, P0_tri, w0_tri, T_coord, B_coord, k1)
         call integrate_S_S_ngradGNN(alok3, P0_tri, w0_tri, T_coord, B_coord, k1)
         !call integrate_dS_S_G(alok4, P0_tri, w0_tri, P0_line, w0_line, T_coord, B_coord, k1, mesh%Eori)
         
         call integrate_S_S_GNN(alok2_2, P0_tri, w0_tri, T_coord, B_coord, k2)
         call integrate_S_S_ngradGNN(alok3_2, P0_tri, w0_tri, T_coord, B_coord, k2)
         call integrate_dS_S_G(alok4_2, P0_tri, w0_tri, P0_line, w0_line, T_coord, B_coord, k2, mesh%Eori)

         !call int_gradG(alokg, P0_tri, w0_tri, T_coord, B_coord, k2)
         !call int_ngradG(alokngradg, P0_tri, w0_tri, T_coord, B_coord, k2)

         !print*, alokngradg, sum(alok3_2)
      end if

      

      alok(:,:) = dcmplx(0.0,0.0)
       
       if(test_T == basis_T) then
           call integrate_S_NN(alok, P0_tri, w0_tri, T_coord)
           !print*, sum(alok)/ sqrt(tri_area(B_coord)) /sqrt(tri_area(T_coord))
       end if

       scale = 1.0d0/sqrt(T_area*B_area)
       scale2 =  1.0d0/(sqrt(T_area)*(B_area))

       signt = -1.0 !normal derivative
       signb = -1.0 !grad G
       !___________________________________________________!
      

       Tind_Ex = 3*(test_T-1) + 1
       Bind_Ex = 3*(basis_T-1) + 1

       Tind_Ey = 3*(test_T-1) + 2
       Bind_Ey = 3*(basis_T-1) + 2

       Tind_Ez = 3*(test_T-1) + 3
       Bind_Ez = 3*(basis_T-1) + 3


       ! ************** Block 11 ****************************
       ! Identity operator
       matrices%A(Tind_Ex,Bind_Ex) = matrices%A(Tind_Ex,Bind_Ex) + 0.5d0 * scale*sum(alok)
       matrices%A(Tind_Ey,Bind_Ey) = matrices%A(Tind_Ey,Bind_Ey) + 0.5d0 * scale*sum(alok)
       matrices%A(Tind_Ez,Bind_Ez) = matrices%A(Tind_Ez,Bind_Ez) + 0.5d0 * scale*sum(alok)


       ! - n dot grad G_1 E 
       matrices%A(Tind_Ex,Bind_Ex) = matrices%A(Tind_Ex,Bind_Ex) - signt*scale*sum(alok3)
       matrices%A(Tind_Ey,Bind_Ey) = matrices%A(Tind_Ey,Bind_Ey) - signt*scale*sum(alok3)
       matrices%A(Tind_Ez,Bind_Ez) = matrices%A(Tind_Ez,Bind_Ez) - signt*scale*sum(alok3)

       !**************** Block 22 *****************************

       ! -G_2

       matrices%A(3*mesh%N_tri + Tind_Ex,3*mesh%N_tri + Bind_Ex) = &
       matrices%A(3*mesh%N_tri + Tind_Ex,3*mesh%N_tri + Bind_Ex) - scale2*sum(alok2_2)
       matrices%A(3*mesh%N_tri + Tind_Ey,3*mesh%N_tri + Bind_Ey) = &
       matrices%A(3*mesh%N_tri + Tind_Ey,3*mesh%N_tri + Bind_Ey) - scale2*sum(alok2_2)
       matrices%A(3*mesh%N_tri + Tind_Ez,3*mesh%N_tri + Bind_Ez) = &
       matrices%A(3*mesh%N_tri + Tind_Ez,3*mesh%N_tri + Bind_Ez) - scale2*sum(alok2_2)

      
       !**************** Block 12 *****************************

       ! G_1

       matrices%A(Tind_Ex,3*mesh%N_tri + Bind_Ex) = &
       matrices%A(Tind_Ex,3*mesh%N_tri + Bind_Ex) + scale2*sum(alok2)

       matrices%A(Tind_Ey,3*mesh%N_tri + Bind_Ey) = &
       matrices%A(Tind_Ey,3*mesh%N_tri + Bind_Ey) + scale2*sum(alok2)

       matrices%A(Tind_Ez,3*mesh%N_tri + Bind_Ez) = &
       matrices%A(Tind_Ez,3*mesh%N_tri + Bind_Ez) + scale2*sum(alok2)

       !****************Block 21 *******************************

       
       ! I 
       matrices%A(3*mesh%N_tri + Tind_Ex, Bind_Ex) = &
       matrices%A(3*mesh%N_tri + Tind_Ex, Bind_Ex) + 0.5d0*scale*sum(alok)

       matrices%A(3*mesh%N_tri + Tind_Ey, Bind_Ey) = &
       matrices%A(3*mesh%N_tri + Tind_Ey, Bind_Ey) + 0.5d0*scale*sum(alok)

       matrices%A(3*mesh%N_tri + Tind_Ez,Bind_Ez) = &
       matrices%A(3*mesh%N_tri + Tind_Ez, Bind_Ez) + 0.5d0*scale*sum(alok)
       

       ! (eps_r^-1 - 1 ) nn dot

       !xx       
       matrices%A(3*mesh%N_tri + Tind_Ex,Bind_Ex) = &
       matrices%A(3*mesh%N_tri + Tind_Ex,Bind_Ex) + &
       0.5d0*(1.0d0/mesh%eps_r - 1.0d0) * T_nvec(1)*T_nvec(1)* scale*sum(alok)

       !yx
       matrices%A(3*mesh%N_tri + Tind_Ey, Bind_Ex) = &
       matrices%A(3*mesh%N_tri + Tind_Ey, Bind_Ex) + &
       0.5d0*(1.0d0/mesh%eps_r - 1.0d0) * T_nvec(2)*T_nvec(1)* scale*sum(alok)

       !zx
       matrices%A(3*mesh%N_tri + Tind_Ez, Bind_Ex) = &
       matrices%A(3*mesh%N_tri + Tind_Ez, Bind_Ex) + &
       0.5d0*(1.0d0/mesh%eps_r - 1.0d0) * T_nvec(3)*T_nvec(1)* scale*sum(alok)

       !xy       
       matrices%A(3*mesh%N_tri + Tind_Ex, Bind_Ey) = &
       matrices%A(3*mesh%N_tri + Tind_Ex, Bind_Ey) + &
       0.5d0*(1.0d0/mesh%eps_r - 1.0d0) * T_nvec(1)*T_nvec(2)* scale*sum(alok)
       
       !yy
       matrices%A(3*mesh%N_tri + Tind_Ey, Bind_Ey) = &
       matrices%A(3*mesh%N_tri + Tind_Ey, Bind_Ey) + &
       0.5d0*(1.0d0/mesh%eps_r - 1.0d0) * T_nvec(2)*T_nvec(2)* scale*sum(alok)

       !zy
       matrices%A(3*mesh%N_tri + Tind_Ez, Bind_Ey) = &
       matrices%A(3*mesh%N_tri + Tind_Ez, Bind_Ey) + &
       0.5d0*(1.0d0/mesh%eps_r - 1.0d0) * T_nvec(3)*T_nvec(2)* scale*sum(alok)

       !xz       
       matrices%A(3*mesh%N_tri + Tind_Ex, Bind_Ez) = &
       matrices%A(3*mesh%N_tri + Tind_Ex, Bind_Ez) + &
       0.5d0*(1.0d0/mesh%eps_r - 1.0d0) * T_nvec(1)*T_nvec(3)* scale*sum(alok)

       !yz
       matrices%A(3*mesh%N_tri + Tind_Ey, Bind_Ez) = &
       matrices%A(3*mesh%N_tri + Tind_Ey, Bind_Ez) + &
       0.5d0*(1.0d0/mesh%eps_r - 1.0d0) * T_nvec(2)*T_nvec(3)* scale*sum(alok)

       !zz
       matrices%A(3*mesh%N_tri + Tind_Ez, Bind_Ez) = &
       matrices%A(3*mesh%N_tri + Tind_Ez, Bind_Ez) + &
       0.5d0*(1.0d0/mesh%eps_r - 1.0d0) * T_nvec(3)*T_nvec(3)* scale*sum(alok)
       

    
       ! n grad G_2

     
       matrices%A(3*mesh%N_tri + Tind_Ex, Bind_Ex) = &
       matrices%A(3*mesh%N_tri + Tind_Ex, Bind_Ex) + signt*scale*sum(alok3_2)

       matrices%A(3*mesh%N_tri + Tind_Ey, Bind_Ey) = &
       matrices%A(3*mesh%N_tri + Tind_Ey, Bind_Ey) + signt*scale*sum(alok3_2)

       matrices%A(3*mesh%N_tri + Tind_Ez, Bind_Ez) = &
       matrices%A(3*mesh%N_tri + Tind_Ez, Bind_Ez) + signt*scale*sum(alok3_2)


      
       !************  grad G n dot*********

       ! -(1-eps_r^-1 ) n grad G_2 n (n dot)
      
       
       !xx       
       matrices%A(3*mesh%N_tri + Tind_Ex, Bind_Ex) = &
       matrices%A(3*mesh%N_tri + Tind_Ex, Bind_Ex) + &
       signb*(1.0d0/mesh%eps_r - 1.0d0) * B_nvec(1)*B_nvec(1)* scale*sum(alok3_2)

       !yx
       matrices%A(3*mesh%N_tri + Tind_Ey, Bind_Ex) = &
       matrices%A(3*mesh%N_tri + Tind_Ey, Bind_Ex) + &
       signb*(1.0d0/mesh%eps_r - 1.0d0) * B_nvec(2)*B_nvec(1)* scale*sum(alok3_2)

       !zx
       matrices%A(3*mesh%N_tri + Tind_Ez, Bind_Ex) = &
       matrices%A(3*mesh%N_tri + Tind_Ez, Bind_Ex) + &
       signb*(1.0d0/mesh%eps_r - 1.0d0) * B_nvec(3)*B_nvec(1)* scale*sum(alok3_2)

       !xy       
       matrices%A(3*mesh%N_tri + Tind_Ex, Bind_Ey) = &
       matrices%A(3*mesh%N_tri + Tind_Ex, Bind_Ey) + &
       signb*(1.0d0/mesh%eps_r - 1.0d0) * B_nvec(1)*B_nvec(2)* scale*sum(alok3_2)

       !yy
       matrices%A(3*mesh%N_tri + Tind_Ey, Bind_Ey) = &
       matrices%A(3*mesh%N_tri + Tind_Ey, Bind_Ey) + &
       signb*(1.0d0/mesh%eps_r - 1.0d0) * B_nvec(2)*B_nvec(2)* scale*sum(alok3_2)

       !zy
       matrices%A(3*mesh%N_tri + Tind_Ez, Bind_Ey) = &
       matrices%A(3*mesh%N_tri + Tind_Ez, Bind_Ey) + &
       signb*(1.0d0/mesh%eps_r - 1.0d0) * B_nvec(3)*B_nvec(2)* scale*sum(alok3_2)

       !xz       
       matrices%A(3*mesh%N_tri + Tind_Ex, Bind_Ez) = &
       matrices%A(3*mesh%N_tri + Tind_Ex, Bind_Ez) + &
       signb*(1.0d0/mesh%eps_r - 1.0d0) * B_nvec(1)*B_nvec(3)* scale*sum(alok3_2)

       !yz
       matrices%A(3*mesh%N_tri + Tind_Ey, Bind_Ez) = &
       matrices%A(3*mesh%N_tri + Tind_Ey, Bind_Ez) + &
       signb*(1.0d0/mesh%eps_r - 1.0d0) * B_nvec(2)*B_nvec(3)* scale*sum(alok3_2)

       !zz
       matrices%A(3*mesh%N_tri + Tind_Ez, Bind_Ez) = &
       matrices%A(3*mesh%N_tri + Tind_Ez, Bind_Ez) + &
       signb*(1.0d0/mesh%eps_r - 1.0d0) * B_nvec(3)*B_nvec(3)* scale*sum(alok3_2)
       

        ! -(1-eps_r^-1) int_c m G_2 n dot
       elems = signb*elements(alok4_2, mesh%Eori, T_coord, B_coord, B_nvec)

       !xx
       matrices%A(3*mesh%N_tri + Tind_Ex, Bind_Ex) = &
       matrices%A(3*mesh%N_tri + Tind_Ex, Bind_Ex) + &
       (1.0d0/mesh%eps_r - 1.0d0) * B_nvec(1)* scale*elems(1)

       !yx
       matrices%A(3*mesh%N_tri + Tind_Ey, Bind_Ex) = &
       matrices%A(3*mesh%N_tri + Tind_Ey, Bind_Ex) + &
       (1.0d0/mesh%eps_r - 1.0d0) * B_nvec(1)* scale*elems(2)

       !zx
       matrices%A(3*mesh%N_tri + Tind_Ez, Bind_Ex) = &
       matrices%A(3*mesh%N_tri + Tind_Ez, Bind_Ex) + &
       (1.0d0/mesh%eps_r - 1.0d0) * B_nvec(1)* scale*elems(3)

       !xy
       matrices%A(3*mesh%N_tri + Tind_Ex, Bind_Ey) = &
       matrices%A(3*mesh%N_tri + Tind_Ex, Bind_Ey) + &
       (1.0d0/mesh%eps_r - 1.0d0) * B_nvec(2)* scale*elems(1)

       !yy
       matrices%A(3*mesh%N_tri + Tind_Ey, Bind_Ey) = &
       matrices%A(3*mesh%N_tri + Tind_Ey, Bind_Ey) + &
       (1.0d0/mesh%eps_r - 1.0d0) * B_nvec(2)* scale*elems(2)

       !zy
       matrices%A(3*mesh%N_tri + Tind_Ez, Bind_Ey) = &
       matrices%A(3*mesh%N_tri + Tind_Ez, Bind_Ey) + &
       (1.0d0/mesh%eps_r - 1.0d0) * B_nvec(2)* scale*elems(3)

       !xz
       matrices%A(3*mesh%N_tri + Tind_Ex, Bind_Ez) = &
       matrices%A(3*mesh%N_tri + Tind_Ex, Bind_Ez) + &
       (1.0d0/mesh%eps_r - 1.0d0) * B_nvec(3)* scale*elems(1)

       !yz
       matrices%A(3*mesh%N_tri + Tind_Ey, Bind_Ez) = &
       matrices%A(3*mesh%N_tri + Tind_Ey, Bind_Ez) + &
       (1.0d0/mesh%eps_r - 1.0d0) * B_nvec(3)* scale*elems(2)

       !zz
       matrices%A(3*mesh%N_tri + Tind_Ez, Bind_Ez) = &
       matrices%A(3*mesh%N_tri + Tind_Ez, Bind_Ez) + &
       (1.0d0/mesh%eps_r - 1.0d0) * B_nvec(3)* scale*elems(3)



       
       !uu=(1.0d0/mesh%eps_r - 1.0d0) * (B_nvec(2)* scale*elems(3) +  B_nvec(2)*B_nvec(3)* scale*sum(alok3_2))
       !uu2 = (1.0d0/mesh%eps_r - 1.0d0) * alokg(3)*B_nvec(2)*scale

       !print*, uu, uu2

       
   end do

end do


end subroutine build_matrix_EdE
