module geometry
use common 

implicit none

contains

subroutine find_edges(mesh)
type(mesh_struct) :: mesh

integer :: Eori(2,3), las, tri, nodes(3), i1, edge, flag, e, edge2

integer :: edges(2,3*mesh%N_tri), enodes(2), enodes2(2)
integer :: etopol_edges(3,mesh%N_tri)

Eori(:,1) = [3,2]
Eori(:,2) = [1,3]
Eori(:,3) = [2,1]

mesh%Eori = Eori

las = 1 
do tri = 1, mesh%N_tri
   nodes = mesh%etopol(:,tri)
   do i1 = 1,3
      edges(:,las) = nodes(Eori(:,i1))
      etopol_edges(i1,tri) = las 
      las = las + 1
   end do
end do

las = 1

do edge = 2, size(edges,2)
   enodes = edges(:,edge)
   
   flag = 0
   do i1 = 1, edge-1
      enodes2 = edges(:,i1)
      !flag = 0
      if(same_edge(enodes,enodes2) == 2) then
         flag = 1
         exit
      end if
   end do

   if (flag == 0) then
      las = las+1
   end if
end do

mesh%N_edge = las

allocate(mesh%edges(2,mesh%N_edge))
allocate(mesh%etopol_edges(3,mesh%N_tri))



mesh%etopol_edges(1,1) = 0
!mesh%edges(:,1) = etopol_edges(:,1)
mesh%edges(:,1) = edges(:,1)

las = 1

do tri = 1,mesh%N_tri

   do e=1,3

      edge = 3*(tri-1) + e
      
      !if(edge .ne. 1) then
         enodes = edges(:,edge)

         flag = 0
         do i1 = 1, las
            enodes2 = mesh%edges(:,i1)
            flag = 0
            !print*, enodes, enodes2
            if(same_edge(enodes,enodes2) == 2) then 
               flag = 1
               edge2 = i1
               
               exit
            end if
         end do

         if(flag == 0) then
            !edge2 = las
            las = las + 1
            edge2 = las
            mesh%edges(:,las) = enodes
            
         end if
         mesh%etopol_edges(e,tri) = edge2
      !end if

   end do
end do
!print*, int(mesh%etopol_edges)

allocate(mesh%edgesT(2,mesh%N_edge))

mesh%edgesT(:,:) = 0

do tri = 1,mesh%N_tri

   do i1 = 1, 3

      edge = mesh%etopol_edges(i1,tri)

      if(mesh%edgesT(1,edge) == 0) then
         mesh%edgesT(1,edge) = tri
      else 
         mesh%edgesT(2,edge) = tri
      end if

   end do
end do
end subroutine find_edges


subroutine tri_in_edges(mesh, e1, e2, tri)
type(mesh_struct) :: mesh
integer, dimension(:), allocatable :: tri, tri2
integer :: i2, e1, e2, i1, edge, las, t(2), t1, t2

!print*, e1, e2

las = 0
do edge = 1,mesh%N_edge
  
   do i2 = 1,2
      t1 = mesh%edgesT(i2,edge)

      if(edge >= e1 .and. edge <= e2) then
         las = las + 1
      end if
end do
end do

allocate(tri2(las))



las = 1
do edge = 1,mesh%N_edge
  
   do i2 = 1,2
      t1 = mesh%edgesT(i2,edge)

      if(edge >= e1 .and. edge <= e2) then
         tri2(las) = t1
         las = las + 1
      end if
end do
end do
tri = unique(tri2)

!print*, tri

end subroutine tri_in_edges

subroutine unscale(matrices, mesh)

implicit none

type (mesh_struct) :: mesh
type (data) :: matrices

integer :: test_T
double precision :: T_coord(3,3)


double precision :: T_area


do test_T = 1, mesh%N_tri

   T_coord = mesh%coord(:,mesh%etopol(:,test_T))
   T_area = tri_area(T_coord)

   matrices%x(3*(test_T-1)+1) = matrices%x(3*(test_T-1)+1)/sqrt(T_area)
   matrices%x(3*(test_T-1)+2) = matrices%x(3*(test_T-1)+2)/sqrt(T_area)
   matrices%x(3*(test_T-1)+3) = matrices%x(3*(test_T-1)+3)/sqrt(T_area)

   matrices%x(3*mesh%N_tri + 3*(test_T-1)+1) = matrices%x(3*mesh%N_tri +3*(test_T-1)+1)/(T_area)
   matrices%x(3*mesh%N_tri +3*(test_T-1)+2) = matrices%x(3*mesh%N_tri +3*(test_T-1)+2)/(T_area)
   matrices%x(3*mesh%N_tri +3*(test_T-1)+3) = matrices%x(3*mesh%N_tri +3*(test_T-1)+3)/(T_area)


   matrices%rhs(3*(test_T-1)+1) = matrices%rhs(3*(test_T-1)+1)/sqrt(T_area)
   matrices%rhs(3*(test_T-1)+2) = matrices%rhs(3*(test_T-1)+2)/sqrt(T_area)
   matrices%rhs(3*(test_T-1)+3) = matrices%rhs(3*(test_T-1)+3)/sqrt(T_area)
end do

end subroutine unscale



end module geometry
